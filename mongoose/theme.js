'use strict';

var mongoose = require('mongoose');

var themeSchema = mongoose.Schema({
  _id: Number,
  name: String,
  created_at: {
    type: Date,
    default: Date.now
  },
  description: String,
  templates: {
    //this is dynamic but each page wil have
    // name: String,
    // img: String,
    // id: Number,
    // path: String,
    // css: String
  },
  sort: Number,
  default_pages: [{
    slug: String,
    nav_title: String,
    nav_order: Number,
    meta_description: String,
    meta_title: String,
    layout_category: String,
    layout: Number
  }]
},{
  timestamps: true
});

var Theme = mongoose.model('Theme', themeSchema);
module.exports = Theme;
