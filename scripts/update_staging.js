db.site.find({active: true }).forEach(function(site) {  
  print('found site: ' + site.uid  + '(' + site.hostname + ')');

  var siteData = db.site_data_live.findOne({_id: site.uid});
  db.site.update({ _id: site._id }, {
    $set: {
      migrated0131: true
    }
  });  
  if (!siteData) return;
  db.pages_live.remove({site: site.uid});
  siteData.navOrder.forEach(function(navItem, index) {
    if (navItem.title == 'home') {
      return;
    }
    var customPage;
    var layout_category = navItem.role;
    var layout = siteData.layouts[navItem.role];
    if (navItem.role == 'custom') {
      //print('found custom page: ' + navItem.title);
      var customPage = db.components_live.findOne({component_type: 'page', tag: navItem.tag});
      layout = customPage.component_specific_data.layout + 1;
    }  else if (navItem.role == 'role-1') {
      layout_category = 'resume';
      layout = siteData.layouts['resume'];
    } else if (navItem.role == 'role-2') {
      layout_category = 'resume';
      layout = siteData.layouts['resume2'];
    }
    var pageId = new ObjectId();
    var page = {
      _id: pageId,
      site: site.uid,
      slug: navItem.slug || navItem.title,
      nav_title: navItem.title,
      active: true,
      on_navbar: true,
      nav_order: index + 1,
      hidden: false,
      deleted_at: null,
      meta_description: navItem.title,
      meta_title: navItem.title,
      layout_category: layout_category,
      layout: layout,
      createdAt: new Date()
    }

    //print('navItem' + navItem.title);
    
    db.pages_live.insert(page);
    if (navItem.role == 'custom') {
      //print('about to update custom components/images with site ' + site.uid + ' and page ' + navItem.tag);
      db.components_live.update({
        site: site.uid,
        page: navItem.tag
      }, { $set: {
        page: pageId
      }}, {multi: true});
      //print('updated components_live on custom page');
      db.images_live.update({
        site: site.uid,
        page: navItem.tag
      }, { $set: {
        page: pageId
      }}, {multi: true});       
      //print('updated images_live on custom page');
    }
    else if (navItem.title == 'about') {
      //print('updating about page with site:' + site.uid + ' and page ' + pageId );

      db.components_live.update({
        site: site.uid,
        tag: { $in: ['aboutHtml']}
      }, { $set: {
        page: pageId
      }}, {multi: true});
      db.images_live.update({
        site: site.uid,
        tag: { $in: ['aboutMainImage', 'about_layout_3_1','about_layout_3_2','about_layout_3_3','about_layout_3_4']}
      }, { $set: {
        page: pageId
      }}, {multi: true});              
    }
    else if (navItem.title == 'resume' || layout_category == 'resume') {
      db.components_live.update({
        site: site.uid,
        tag: { $in: ['dualResume1File', 'dualResume2File', 'resumeFile', 'Resume','headshotFile']}
      }, { $set: {
        page: pageId
      }}, {multi: true});  
      db.images_live.update({
        site: site.uid,
        tag: { $in: ['resumeImage1', 'resumeImage2', 'resumeHeadShot1', 'resumeHeadShot2', 'resumeHeadShot3']}
      }, { $set: {
        page: pageId
      }}, {multi: true});               
    }
    else if (navItem.title == 'news') {
      db.components_live.update({
        site: site.uid,
        tag: { $in: ['newsHtml']}
      }, { $set: {
        page: pageId
      }}, {multi: true});    
      db.images_live.update({
        site: site.uid,
        tag: { $in: ['mainNews', 'mainNews1', 'mainNews2', 'mainNews3', 'mainNews4', 'mainNews3', 'news1', 'news2', 'news3',' news4']}
      }, { $set: {
        page: pageId
      }}, {multi: true});                


      db.images_live.update({
        site: site.uid,
        tag: 'mainNews'
      }, { $set: {
        tag: 'mainNews1'
      }}, {multi: true});    

      db.images_live.update({
        site: site.uid,
        tag: 'news1'
      }, { $set: {
        tag: 'mainNews1'
      }}, {multi: true});                

      db.images_live.update({
        site: site.uid,
        tag: 'news2'
      }, { $set: {
        tag: 'mainNews2'
      }}, {multi: true});                

      db.images_live.update({
        site: site.uid,
        tag: 'news3'
      }, { $set: {
        tag: 'mainNews3'
      }}, {multi: true});                

      db.images_live.update({
        site: site.uid,
        tag: 'news4'
      }, { $set: {
        tag: 'mainNews4'
      }}, {multi: true});                
      
    }
    else if (navItem.title == 'contact') {
      db.components_live.update({
        site: site.uid,
        tag: { $in: ['contactHtml']}
      }, { $set: {
        page: pageId
      }}, {multi: true});         
      db.images_live.update({
        site: site.uid,
        tag: { $in: ['contactImage']}
      }, { $set: {
        page: pageId
      }}, {multi: true});                   
    }          
  });  
 db.components.remove({site: site.uid});
 db.images.remove({site: site.uid});
 db.pages.remove({site: site.uid});
 var imagesLive = db.images_live.find({site: site.uid}).toArray();
 var pagesLive = db.pages_live.find({site: site.uid}).toArray();
 var componentsLive = db.components_live.find({site: site.uid}).toArray();
 
 
 db.images.insert(imagesLive);
 db.pages.insert(pagesLive);
 db.components.insert(componentsLive);
});

print('exiting');
