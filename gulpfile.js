var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var expect = require('gulp-expect-file');


//these are all the scripts for the NON-edit mode
gulp.task('noneditscripts', function() {
  var files = [
    'src/js/app.js',
    'src/js/constants.js',
    'src/js/components/pages/gallery.js',
    'src/js/components/pages/page.js',
    'src/js/components/pages/home.js',
    'src/js/components/pages/media.js',
    'src/js/components/pages/custom.js',
    'src/js/components/navbar/mainnav.js',
    'src/js/components/gallery/galleryblock.js',
    'src/js/components/gallery/mediablock.js',
    'src/js/components/resume/resumeblock.js',
    'src/js/components/social/social.js',
    'src/js/components/linky/linky_nonedit.js',
    'src/js/components/carousel.js',
    'src/js/directives/gallery-without-section.js',
    'src/js/directives/dynamic-styles.js',
    'src/js/services/getSite.js',
    'src/js/services/navStyling.js',
    'src/js/components/image-upload/imgcropModal.js',
    'src/js/directives/show-hide.js', //IMPORTANT THIS IS DEF ONLY FOR NON EDIT PAGE
    'src/js/directives/navItem.js',
    'src/js/directives/footerText.js',
    'src/js/components/goto/goto.js',
    'src/js/components/videos/video.js',
    'src/js/components/audio/soundCloud.js',
    'src/js/components/gallery/expandingGallery.js',
    'src/js/constants/colorPalette.js',
    'src/js/directives/gRecaptcha.js',
    'src/js/components/contact-form/contactForm.js',
    'src/js/components/html-editable/html-editable.js',
  ];
  return gulp.src(files)
    .pipe(expect(files))
    .pipe(sourcemaps.init())
    .pipe(concat('js/index_non_edit.js'))
    .pipe(sourcemaps.write('./js/sourcemaps'))
    .pipe(gulp.dest('./public/'));
});

//these are all the scripts for the edit mode
gulp.task('editscripts', function() {
  var files = [
    'src/js/app.js',
    'src/js/constants.js',
    'src/js/components/pages/gallery.js',
    'src/js/components/pages/page.js',
    'src/js/components/pages/home.js',
    'src/js/components/pages/media.js',
    'src/js/components/pages/custom.js',
    'src/js/components/gallery/galleryblock.js',
    'src/js/components/gallery/mediablock.js',
    'src/js/components/resume/resumeblock.js',
    'src/js/components/navbar/mainnav.js',
    'src/js/components/social/social.js',
    'src/js/components/linky/linky.js',
    'src/js/components/carousel.js',
    'src/js/directives/moveToNextInput.js',
    'src/js/directives/no-enter.js',
    'src/js/directives/gallery-without-section.js',
    'src/js/directives/resumeTemplateBuilder.js',
    'src/js/directives/dynamic-styles.js',
    'src/js/directives/edit_gallery_media.js',
    'src/js/directives/navItem.js',
    'src/js/directives/footerText.js',
    'src/js/services/getSite.js',
    'src/js/services/navStyling.js',
    'src/js/controllers/edit-controller.js',
    'src/js/components/image-upload/imgcrop.js',
    'src/js/components/image-upload/imgcropModal.js',
    'src/js/components/contenteditable/contenteditable.js',
    'src/js/components/html-editable/html-editable.js',
    'src/js/components/app-loading/app-loading.js',
    'src/js/components/goto/goto.js',
    'src/js/components/videos/video.js',
    'src/js/components/gallery/expandingGallery.js',
    'src/js/constants/colorPalette.js',
    'src/js/components/audio/soundCloud.js',
    'src/js/components/contact-form/contactForm.js'
  ];
  return gulp.src(files)
    .pipe(expect(files))
    .pipe(sourcemaps.init())
    .pipe(concat('js/index_edit.js'))
    .pipe(sourcemaps.write('./js/sourcemaps'))
    .pipe(gulp.dest('./public/'));
});

// scripts between front end and admin
gulp.task('shared_scripts', function() {
  var files = [
    'src/js/services/getSite.js',
    'src/js/directives/edit_gallery_media.js'
  ];
  return gulp.src(files)
    .pipe(expect(files))
    .pipe(sourcemaps.init())
    .pipe(concat('js/shared.js'))
    .pipe(sourcemaps.write('./js/sourcemaps'))
    .pipe(gulp.dest('./public/'));
});

// scripts between front end and admin
gulp.task('oldbrowser', function() {
  var files = [
    'src/js/oldbrowser.js'
  ];
  return gulp.src(files)
    .pipe(expect(files))
    .pipe(sourcemaps.init())
    .pipe(concat('js/oldbrowser.js'))
    .pipe(sourcemaps.write('./js/sourcemaps'))
    .pipe(gulp.dest('./public/'));
});


//these are all the third party scripts for the edit mode
gulp.task('editscripts_thirdparty', function() {
  var files = [
    'src/js/thirdparty/sweetalert/dist/sweetalert.min.js',
    'src/js/thirdparty/angular-ui-router/1.0.15/angular-ui-router.min.js',
    'src/js/thirdparty/angular-xeditable-0.1.11/js/xeditable.js',
    'src/js/thirdparty/angular-bootstrap-colorpicker/js/bootstrap-colorpicker-module.js',
    'src/js/thirdparty/lodash/dist/lodash.min.js',
    'src/js/thirdparty/ng-dialog/js/ngDialog.js',
    'src/js/thirdparty/croppie/croppie.js',
    'src/js/thirdparty/lightslider/lightslider.min.js',
    'src/js/thirdparty/jquery-throttle-debounce/1.1/jquery.ba-throttle-debounce.min.js',
    'src/js/thirdparty/ui-bootstrap-custom-build/ui-bootstrap-custom-2.1.3.min.js',
    'src/js/thirdparty/ui-bootstrap-custom-build/ui-bootstrap-custom-tpls-2.1.3.min.js',
    'src/js/thirdparty/textangular/dist/textAngular-rangy.min.js',
    'src/js/thirdparty/textangular/dist/textAngular-sanitize.min.js',
    'src/js/thirdparty/textangular/dist/textAngular.min.js',
    'src/js/thirdparty/ng-file-upload/ng-file-upload.min.js',
    'src/js/thirdparty/angular-css-injector-1.4.0/angular-css-injector.min.js',
    'src/js/thirdparty/jquery-ui/jquery-ui.min.js', //for sortable
    'src/js/thirdparty/angular-ui-sortable/sortable.min.js',
    'src/js/thirdparty/object-fit-polyfill/ofi.min.js',
    'src/js/thirdparty/ng-fi-text/ng-fi-text.js'
  ];
  return gulp.src(files)
    .pipe(expect(files))
    .pipe(sourcemaps.init())
    .pipe(concat('js/index_edit_thirdparty.js'))
    .pipe(sourcemaps.write('./js/sourcemaps'))
    .pipe(gulp.dest('./public/'));
});

//these are all the third party scripts for the NONedit mode
gulp.task('noneditscripts_thirdparty', function() {
  var files = [
    'src/js/thirdparty/angular-ui-router/1.0.15/angular-ui-router.min.js',
    'src/js/thirdparty/lodash/dist/lodash.min.js',
    'src/js/thirdparty/ng-dialog/js/ngDialog.js',
    'src/js/thirdparty/lightslider/lightslider.min.js',
    'src/js/thirdparty/jquery-throttle-debounce/1.1/jquery.ba-throttle-debounce.min.js',
    'src/js/thirdparty/ui-bootstrap-custom-build/ui-bootstrap-custom-2.1.3.min.js',
    'src/js/thirdparty/ui-bootstrap-custom-build/ui-bootstrap-custom-tpls-2.1.3.min.js',
    'src/js/thirdparty/ng-file-upload/ng-file-upload.min.js',
    'src/js/thirdparty/angular-css-injector-1.4.0/angular-css-injector.min.js',
    'src/js/thirdparty/object-fit-polyfill/ofi.min.js',
    'src/js/thirdparty/ng-fi-text/ng-fi-text.js'
  ];
  return gulp.src(files)
    .pipe(expect(files))
    .pipe(sourcemaps.init())
    .pipe(concat('js/index_non_edit_thirdparty.js'))
    .pipe(sourcemaps.write('./js/sourcemaps'))
    .pipe(gulp.dest('./public/'));
});

gulp.task('admin_scripts', function() {
  var files = [
    'src/js/thirdparty/sweetalert/dist/sweetalert.min.js',
    'src/js/thirdparty/ng-file-upload/ng-file-upload.min.js',
    'src/js/thirdparty/jquery-ui/jquery-ui.min.js',
    'src/js/thirdparty/angular-ui-sortable/sortable.min.js',
    'src/js/thirdparty/ui-bootstrap-custom-build/ui-bootstrap-custom-2.1.3.min.js',
    'src/js/thirdparty/ui-bootstrap-custom-build/ui-bootstrap-custom-tpls-2.1.3.min.js',
    'src/js/thirdparty/croppie/croppie.js',
    'src/js/thirdparty/ng-dialog/js/ngDialog.js',
    'src/js/components/image-upload/imgcrop.js',
    'src/js/components/image-upload/imgcropModal.js',
    'src/js/directives/howto.js'
  ];

  return gulp.src(files)
    .pipe(expect(files))
    .pipe(sourcemaps.init())
    .pipe(concat('js/admin.js'))
    .pipe(sourcemaps.write('./js/sourcemaps'))
    .pipe(gulp.dest('./public/'));
});


gulp.task('sass', function() {
  return gulp.src('./src/scss/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./public/css'));
});

gulp.task('serve', ['sass', 'noneditscripts', 'editscripts', 'editscripts_thirdparty', 'noneditscripts_thirdparty', 'admin_scripts', 'shared_scripts', 'oldbrowser'], function() {
  gulp.watch('./src/scss/*.scss', ['sass']);
  gulp.watch('./src/js/**/*.js', ['noneditscripts', 'editscripts', 'editscripts_thirdparty', 'noneditscripts_thirdparty', 'admin_scripts', 'shared_scripts', 'oldbrowser']);
});

gulp.task('default', ['serve']);
gulp.task('build', ['sass', 'noneditscripts', 'editscripts', 'editscripts_thirdparty', 'noneditscripts_thirdparty', 'admin_scripts', 'shared_scripts', 'oldbrowser']);
