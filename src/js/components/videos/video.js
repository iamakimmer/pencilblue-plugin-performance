angular.module('pencilblueApp').component('videoElement', {
  bindings: {
    videoType: '<',
    video: '<'
  },
  templateUrl: '/public/performance/partials/video.html',
  controllerAs: '$video',
  controller: function($sce, $http) {
    var self = this;
    var youtubeThumbUrl = 'https://img.youtube.com/vi/{{videoId}}/hqdefault.jpg';
    var vimeoThumbUrl = 'https://developer.vimeo.com/api/v2/video/{{videoId}}.json';

    self.idRegs = {
      'youtube': /(embed\/)([a-zA-Z0-9_-]+)/i,
      'vimeo': /vimeo\.com\/video\/([a-zA-Z0-9_-]+)/i
    };

    if (self.video.url.match(self.idRegs.youtube)) {
      self.thumbnailUrl = youtubeThumbUrl.replace('{{videoId}}', self.video.url.match(self.idRegs.youtube)[2]);
    } else if (self.video.url.match(self.idRegs.vimeo)) {
      $http.post('/vimeoThumbnail', {videoId: self.video.url.match(self.idRegs.vimeo)[1]}).then(function(response) {
        self.thumbnailUrl = response.data[0].thumbnail_large;
      }).catch(function(err) {
        console.error(err);
      });
      // self.thumbnailUrl = youtubeThumbUrl.replace('{{videoId}}', self.video.url.match(self.idRegs.youtube)[2]);
    } else {
      getIncludeUrl.call(self);
    }

    this.getIncludeUrl = getIncludeUrl.bind(self);

    function getIncludeUrl() {
      console.log('videoType', self.videoType);
      if (self.videoType === 'iframe') {
        console.log('urlurlurl', self.video.url);
        self.videoFile = $sce.trustAsResourceUrl(self.video.url + '?autoplay=true');
        self.includeUrl = '/public/performance/partials/video-iframe.html';
      } else if (this.videoType === 'javascript') {
        self.includeUrl = '/public/performance/partials/video-js.html';
      }
    }
  }
});
