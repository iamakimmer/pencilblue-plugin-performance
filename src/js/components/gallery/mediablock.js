
angular.module('pencilblueApp').component('mediablock', {
  bindings: {
    pageId: '=',
    editing: '='
  },
  templateUrl: ['$element', '$attrs', function($element, $attrs) {
    if (!$attrs.partial) {
      console.error('partial url required.');
      return false;
    }
    var url = '/public/performance/pages/media/' + $attrs.partial;
    return url;
  }],
  controllerAs: '$ctrl',
  controller: function($http, $sce, GetMedia, ngDialog, videoRegexes) {
    var self = this;
    GetMedia.get({
      pageId: self.pageId
    }).then(function(resp) {
      self.media = resp.data;
    });

    this.regexes = videoRegexes;

		if (! document.getElementById('wistiaScript')) {

			var wistiaTag = document.createElement("script");
			wistiaTag.type = "text/javascript";
			wistiaTag.async;
			wistiaTag.id = "wistiaScript";
			wistiaTag.keepScript = "true";
			wistiaTag.src = "//fast.wistia.net/assets/external/E-v1.js";
			$("head").append(wistiaTag);
		}

		if (! document.getElementById('scScript')) {

			var soundcloudTag = document.createElement("script");
			soundcloudTag.type = "text/javascript";
			soundcloudTag.async;
			soundcloudTag.id = "scScript";
			soundcloudTag.keepScript = "true";
			soundcloudTag.src = "//connect.soundcloud.com/sdk/sdk-3.1.2.js";
			$("head").append(soundcloudTag);
		}


    
    this.testUrl = function(url) {
      if (self.regexes.iframe.test(url)) {
        return 'iframe';
      } else if (self.regexes.soundcloud.test(url)) {
        return 'soundcloud';
      } else {
        return 'javascript';
      }
    };

    this.trustAsUrl = function(url) {
      return $sce.trustAsResourceUrl(url);
    };    
    

  }
});
