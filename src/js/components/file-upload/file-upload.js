//the modal directive to upload and image and crop
//the main navigation
angular.module('pencilblueApp')
  .directive('fileUpload', ['SiteData', function(SiteData) {
    return {
      scope: {
        ngModel: '=',        
        tag: '@'
      },
      restrict: 'A',
      replace: true,
      link: function(scope, elem, attrs) {
        var siteData = SiteData.getSiteData();
        if (!scope.ngModel) {        
          if (siteData.editing) {
          } else {
            elem.remove();
          }
        } else if (scope.ngModel) {
          attrs.$set('href', scope.ngModel.url);
          elem.text(scope.ngModel.text);
        }
        if (!siteData.editing) {
          return;
        }

        elem.on('click', function(e) {
          e.preventDefault();
        });


      }
    };
  }]);
