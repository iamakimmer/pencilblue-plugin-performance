"use strict";

var mongoose = require('mongoose');
var schema = {
  _id: String, //the public id
  site: String, //website
  metadata: {},
  url: String,
	href: String,
	href_target: {
		type: String,
		enum: ['_self', '_blank'],
		default: "_self"
	},
  thumbnail: String, //public url,
  thumbnail_id: String, //if there's a custom thumbnail
  title: String,
  credit: String,
  zoom: Number,
  points: String,
  original: {
    public_id: String,
    url: String
  },
  tag: {
    type: String,
    deafult: 'default'
  },
  page: {
		type: mongoose.Schema.Types.ObjectId //you cant ref Page explicitly here because you have Page and PageLive			
	},
  active: {
    type: Boolean,
    default: true
  }
};

var imageSchema = mongoose.Schema(schema, {
  timestamps: true,
  collection: 'images'
});

var imageSchemaLive = mongoose.Schema(schema, {
  timestamps: true,
  collection: 'images_live'
});

var Image = mongoose.model('Image', imageSchema);
var ImageLive = mongoose.model('ImageLive', imageSchemaLive);

module.exports = {
  Image: Image, //edit mode image
  ImageLive: ImageLive //live mode image
};
