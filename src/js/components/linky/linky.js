//the modal directive to upload and image and crop
//the main navigation
angular.module('pencilblueApp')
  .directive('linky', ['$window', 'Upload', 'ComponentFactory', 'SiteData', function($window, Upload, ComponentFactory, SiteData) {
    return {
      scope: {
        ngModel: '=',
        tag: '@',
        type: '@',
        accept: '@',
        text: '@',
        editText: '@',
        pageId: '@'
      },
      restrict: 'E',
      replace: true,
      template: function(tElem, tAttrs) {
        var accept = tAttrs.accept ? ' accept="' + tAttrs.accept + '" ngf-pattern="\'' + tAttrs.accept + '\'"' : '';
        console.log('accept', accept);

        return '<' + tAttrs.type +  (window.editing ? ' ngf-select="uploadFile($files[0])" ' + accept : ' ') + '></' + tAttrs.type + '>';
      },
      link: function(scope, elem, attrs) {
        var siteData = SiteData.getSiteData();
        elem.addClass(scope.type);

        if (!scope.pageId) {
          console.error('No Page Id Specified on ImageCrop' + scope.tag);
        }

        if (attrs.text) {
          elem.text(attrs.text);
        }

        if (siteData.editing) {
          console.log('THE TEXT: ', scope.ngModel);
          if (!scope.ngModel) {
            elem.text(scope.editText || 'Click here to upload résumé in .pdf format.');
          } else if (scope.ngModel.metadata) {
            console.log('ngModel', scope.ngModel);
            elem.text(scope.ngModel.metadata.original_filename + '.' + scope.ngModel.metadata.format);
          }
        } else if (!scope.ngModel) {
          elem.remove();
        } else if (scope.ngModel.text) {
          elem.text(scope.ngModel.text);
        }


        elem.on('click', function(e) {
          console.log('clicked!');
          if (scope.ngModel && !siteData.editing) {
            $window.open(scope.ngModel.url);
            if (scope.ngModel.text) {
              elem.text(scope.ngModel.text);
            }
          }
        });

        if (siteData.editing) {
          scope.uploadFile = function(file) {
            Upload.upload({
              url: window.CLOUDINARY_UPLOAD_API_URL,
              data: {
                folder: window.location.host,
                upload_preset: 'zymnlox3',
                file: file
              }
            }).then(function(resp) {
              console.log('update component');
              ComponentFactory.update({
                url: resp.data.secure_url,
                tag: scope.tag,
                page: scope.pageId,
                metadata: resp.data
              }).then(function(data) {
                scope.ngModel = data.data;
                alert('Uploaded!');
                $window.open(scope.ngModel.url);
              });
            }, function(resp) {
              //todo error handling on pending image
              console.log('resp err', resp);
              console.log('Error status: ' + resp.status);
            }, function(evt) {
              //todo loading info
              console.log('resp progressPercentage', evt);
              var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
              console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
          };
        }
      }
    };
  }]);
