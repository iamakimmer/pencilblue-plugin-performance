/*
Copyright (C) 2016  PencilBlue, LLC

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module.exports = function ApiTokenLoginControllerModule(pb) {
  
    //dependencies
    var util = pb.util;
  
    /**
    * Creates authentication token for administrators
    * and managing editors for cross site access
    *
    * @class ApiTokenLoginController
    * @constructor
    * @extends BaseController
    */
    function ApiTokenLoginController() {}
    util.inherits(ApiTokenLoginController, pb.BaseController);
  
    ApiTokenLoginController.prototype.render = function(cb) {
      var self = this;
      var options = {
        site: this.site, // Site uid
        onlyThisSite: false
      };
      var callback = this.query.callback;
      pb.security.authenticateSession(this.session, this.query.token, new pb.ApiTokenAuthentication(options), function(err, user) {
  
        if(util.isError(err)) {
          return cb({
            code: 500,
            content: jsonpResponse(callback, pb.BaseController.apiResponse(pb.BaseController.API_FAILURE, self.ls.g('generic.ERROR_SAVING')))
          });
        }
  
        if(!user) {
          return cb({
            code: 400,
            content: jsonpResponse(callback, pb.BaseController.apiResponse(pb.BaseController.API_FAILURE, self.ls.g('sites.INVALID_TOKEN')))
          });
        }
  
        // redirect to edit
        return self.redirect('/edit', cb);        
      });
    };
  
    function jsonpResponse(callback, data) {
      return  callback + '(' + data + ')';
    }
  
    ApiTokenLoginController.getRoutes = function(cb) {
      var routes = [{
        method: 'get',
        path: '/actions/admin/sites/api_token_login',
        auth_required: false,
        content_type: 'application/json'
      }];
  
      return cb(null, routes);
    };
  
    //exports
    return ApiTokenLoginController;
  };
  