#!/usr/local/node

const _ = require('lodash');
const argv = require('yargs').argv;
const {name, description, sort, dburl} = argv;

const mongoose = require('mongoose');
const mongodb_uri = dburl || 'mongodb://localhost:27017/performance_nyu';

const Theme = require('../mongoose/theme');

mongoose.Promise = global.Promise;

if (argv.help || !argv.name || !argv.description) {
  console.log(`
    Creates a theme in db from given parameters:
    --name="string" (required),
    --description="string" (required),
    --sort="number" (optional, will use theme.length + 1 if not provided),
    --dburl="string(mongodb uri)" (optional, will use localhost uri if not provided),
  `);

  return process.exit(0);
}

mongoose.connect(mongodb_uri, function(err) {
  if (err) {
    console.log(`Error connecting to db: ${JSON.stringify(err, null, 2)}`);
    process.exit(1);
  } else {
    console.log('Mongoose Connected to ' + mongodb_uri);
  }
});

Theme.find({}, (err, themes) => {
  if (err) console.log(`Error getting current themes: ${JSON.stringify(err, null, 2)}`);

  let themeIsUsed = false;

  if (themes.length) {
    let matchedThemes = _.filter(themes, (t) => { return t.name.toLowerCase() === name.toLowerCase(); });

    if (matchedThemes.length) {
      themeIsUsed = true;
    }
  }

  if (!themeIsUsed) {
    let newTheme =  new Theme({
      name,
      description,
      sort: sort || themes.length + 1,
      _id: themes.length + 1
    });

    newTheme.save((err) => {
      if (err) {
        console.error(err);
        console.error(`Error saving new theme: ${JSON.stringify(err, null, 2)}`);
        process.exit(1);
      }

      console.log(`Theme ${name} saved.`);
      process.exit(0);
    });
  } else {
    console.log(`Theme name, ${name}, is already used`);
    process.exit(1);
  }
});