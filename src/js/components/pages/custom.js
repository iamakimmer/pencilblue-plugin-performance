angular.module('pencilblueApp').component('customPage', {
  templateUrl: function(SiteData, $state) {
    var siteData = SiteData.getSiteData();
    var title = $state.params.title;
    var tag = $state.params.tag;

    console.log('siteData.components[tag]', siteData.components[tag]);
    if (!siteData.components[tag]) return $state.go('home', {}, {reload: true});

    var pageLayout = siteData.components[tag][0].component_specific_data.layout;
    console.log('pageLayout', pageLayout);
    if (pageLayout > -1) {
      return '/public/performance/pages/custom/' + (pageLayout + 1) + '.html';
    } else {
      $state.go('home', {}, {reload: true});
    }
  },
  controller: function($scope, $rootScope, $state, SiteData, $sce) {

    $scope.editing = $rootScope.editing;

    $scope.title = $state.params.title;
    var tag = $state.params.tag;
    $scope.siteData = SiteData.getSiteData();
    $scope.components = $scope.siteData.components;
    $scope.componentData = $scope.siteData.components[tag][0];

    $scope.images = {};
    $scope.texts = {};

    //todo this is super inefficient, it's gonan go through gallery and all images, and all components
    console.log('IMAGES? ', $scope.siteData.images);
    for (var image in $scope.siteData.images) {
      var imageData = $scope.siteData.images[image];
      imageData.forEach(function(p) {
        if (p && p.page === $scope.componentData._id) {
          if (!$scope.images[image]) $scope.images[image] = [];
          $scope.images[image][0] = p;
        }
      });
    }

    for (var text in $scope.components) {
      var component = $scope.components[text];
      component.forEach(function(c) {
        if (c && c.page === $scope.componentData._id) {
          if (!$scope.texts[text]) $scope.texts[text] = [];
          $scope.texts[text][0] = c;
        }
      });
    }

    $scope.$on('text-updated', function() {
      location.reload();
    });

    $scope.$on('image-updated', function() {
      location.reload();
    });

    $scope.trustText = function(text) {
      return $sce.trustAsHtml(text);
    };
  }
});
