'use strict';

var mongoose = require('mongoose');

var schema = {
  site: {type: String, required: true}, //website
  name: String,
  urls: {},
  videos: String,
  comments: String,
  created_at: {
    type: Date,
    default: Date.now
  }
};

var customBuildSchema = mongoose.Schema(schema, {
  timestamps: true,
  collection: 'custombuild'
});

var CustomBuild = mongoose.model('CustomBuild', schema);

module.exports = {
  CustomBuild: CustomBuild
};
