'use strict';

pencilblueApp.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', 'cssInjectorProvider', '$httpProvider', '$provide', function($stateProvider, $urlRouterProvider, $locationProvider, cssInjectorProvider, $httpProvider, $provide) {

  $provide.decorator('$exceptionHandler', ['$delegate', function($delegate) {
    return function(exception, cause) {
      Raygun.send(exception, {
        cause: cause
      });
      $delegate(exception, cause);
    };
  }]);

  // $httpProvider.interceptors.push(['$q', function($q) {
  //   return {
  //     'requestError': function(rejection) {
  //       Raygun.send(new Error('Failed $http request'), rejection);
  //       return $q.reject(rejection);
  //     },
  //     'responseError': function(rejection) {
  //       Raygun.send(new Error('Failed $http response'), rejection);
  //       return $q.reject(rejection);
  //     }
  //   };
  // }]);


  //cssInjectorProvider.setSinglePageMode(true);

  $urlRouterProvider.otherwise('/');

  var editPanelQuery = {
    pages: null,
    theme: null,
    layout: null,
    colors: null,
    fonts: null
  };

  var params = '?pages&theme&layout&colors&fonts&showCustomColors';

/*
  var newsState = {
    name: 'news',
    url: '/news' + params,
    component: 'page',
    params: editPanelQuery,
    reloadOnSearch: false
  };

  var mediaState = {
    name: 'media',
    url: '/media' + params,
    component: 'media',
    params: editPanelQuery,
    reloadOnSearch: false
  };

  var resumeState = {
    name: 'resume',
    url: '/resume',
    component: 'page',
    params: editPanelQuery,
    reloadOnSearch: true
  };

  var contactState = {
    name: 'contact',
    url: '/contact' + params,
    component: 'page',
    params: editPanelQuery,
    reloadOnSearch: false
  };

  var aboutState = {
    name: 'about',
    url: '/about' + params,
    component: 'page',
    params: editPanelQuery,
  };

  var galleryState = {
    name: 'gallery',
    url: '/gallery' + params + '&section',
    component: 'gallery',
    params: editPanelQuery,
    resolve: {
      gallery: function(GetGallery) {
        return GetGallery.get();
      }
    },
    reloadOnSearch: false
  };
  */

  var pageState = {
    name: 'page',
    url: '/:slug',
    component: 'page',
    resolve: {
      pagedata: function($transition$, SiteData, $http) {
        
        var slug = $transition$.params().slug;
        var url = '/api/components/page/' + slug;
        if (!SiteData.getSiteData().editing && window.isLive) {
          url += '/live'
        }

        return $http.get(url).then(function(resp) {
          var data = resp.data;
          return {
            page: data.page,
            images: data.images,
            components: data.components
          };
        });
      },
      editing: function(SiteData) {
        return SiteData.getSiteData().editing;
      }
    },
    reloadOnSearch: false
  };

  var homeState = {
    name: 'home',
    url: '/' + params + '&modal',
    component: 'home',
    params: editPanelQuery,
    reloadOnSearch: false
  };


  var indexState = {
    name: 'index',
    url: '/index',
    component: 'home',
    params: editPanelQuery,
    reloadOnSearch: false
  };

  $stateProvider.state(homeState);
  $stateProvider.state(pageState);
  $stateProvider.state(indexState);

}]).run(['$rootScope', '$state', '$stateParams', '$transitions', function($rootScope, $state, $stateParams, $transitions) {
  $rootScope.$state = $state;
  $rootScope.$stateParams = $stateParams;

  $transitions.onSuccess({}, function(transition) {
    try {
      if (window.analytics) {
        var slug = transition.params().slug;      
        window.analytics.sendPageView(slug);  
      }
    } catch (e) {
      console.error(e);
    }    
  });
}]);


pencilblueApp.controller('SiteController', ['$scope', '$rootScope', '$http', '$location', '$state', '$interval', '$sce', 'cssInjector', 'SiteData', 'colorPalettes', 'NavStyling',  function($scope, $rootScope, $http, $location, $state, $interval, $sce, cssInjector, SiteData, colorPalettes, NavStyling) {
  cssInjector.removeAll();
  
  $scope.sitecontroller = true;
  $scope.state = $state;
  $scope.colorPalettes = colorPalettes;

  $scope.stateName = '';
	$scope.$watch('state.current.name', function(newVal) {
		if (newVal.length) $scope.stateName = newVal;
	});

  $scope.editing = false;
  $rootScope._site = $scope.siteData;
  $rootScope._site.components = _.groupBy($scope.components, 'tag');
  $scope._site.images = _.groupBy($scope.siteImages, 'tag');
  var cssPath;

  $scope._site.theme = $scope._site.theme || 4;
  cssPath = '/public/performance/themes/' + $scope._site.theme + '/theme.css';
  cssInjector.add(cssPath);

  $scope.setSiteStyle = function() {
    
    var length = $scope.pages.length;
    NavStyling.setNavStyle(length);
    $scope.navStyle = NavStyling.getNavStyle();
  };
  

  SiteData.setSiteData({
    images: _.groupBy($scope.siteImages, 'tag'),
    components: _.groupBy($scope.components, 'tag'),
    editing: false,
    pages: $scope.pages,
    _site: $scope._site
  });

  
  $scope.pages.forEach(function(page) {
  
    var css = '/public/performance/pages/' + page.layout_category + '/' + page.layout + '.css';
  
    cssInjector.add(css);
  });


  $scope.publishSite = function() {
    swal({
      title: 'Are You Sure?',
      text: 'Are you sure you want to publish your changes?',
      type: 'warning',
      closeOnCofirm: true,
      showCancelButton: true,
      closeOnCancel: true
    }, function(isConfirm) {
      if (isConfirm) {
        $http.post('/api/publish/site').then(function(response) {
          if (response.data.message === 'ok') {
            location.reload();
          }
        }).catch(function(err) {
          console.error(err);
        });
      }
    });
  };

  $scope.getTrialHoursRemaining = function() {
    var date = new Date();
    var expireDate = new Date($rootScope._site.expiration_date);
    var html;
    if (expireDate < date) {
      html = '<span>Your site is currently expired. Please signup for a subscription.</span>';
    } else {
      html = '<span>Your site is currently in trial mode. Your trial will expire on ' + moment($rootScope._site.expiration_date).format('dddd, MMMM Do YYYY h:mm a') + '.</span>';
    }

    $scope.trialText = $sce.trustAsHtml(html);
  };
}]);

pencilblueApp.filter('externalurl', function() {
  return function(input) {
    var cleanURL = input.replace(/http(\:|s\:)\/\//, '');
    return 'http://' + cleanURL;
  };
});

pencilblueApp.filter('pagestyle', function() {
  var pageStyles = {
    background_color: 'Background',
    header_background_color: 'Header Background',
    color: 'Text',
    nav_text_color: 'Navbar Text',
    nav_active_color: 'Navbar Active Page Text',
    navbar_background: 'Navbar Background',
    link_color: 'Link',
    link_hover_color: 'Link Hover',
    button_text_color: 'Button Text',
    button_bg_color: 'Button Background',
    social_background_color: 'Social Icon Background',
    social_font_color: 'Social Icon Text',
    title_color: 'Header Name'
  };

  return function(input) {

    if (pageStyles[input]) {
      return pageStyles[input];
    }
    if (input === 'color') input = 'Font ' + input;
    if (input === 'color_footer') {
      input = input.replace('color_', '');
      input += ' Color';
    }
    var cleanURL = input.replace(/_|-/gi, ' ');
    return cleanURL;
  };
});

pencilblueApp.filter('externalurl', function() {
  return function(input) {
    var cleanURL = input.replace(/http(\:|s\:)\/\//, '');
    return 'http://' + cleanURL;
  };
});

pencilblueApp.filter('telephone', function() {
  return function(input) {
    if (input && input.length) {
      var arr = input.split('');
      arr.splice(0, 0, '(');
      arr.splice(4, 0, ')');
      arr.splice(5, 0, ' ');
      arr.splice(9, 0, '-');
      return arr.join('');
    } else {
      return input;
    }
  };
});
