pencilblueApp.directive('navItem', function($rootScope, $state, $http) {
  return {
    restrict: 'A',
    scope: {
      item: '=',
      link: '@',
      page: '=',
      editPage: '&',
      removePage: '&',
    },
    template:
              '<i ng-show="!link" class="nav-handle fa fa-bars"></i>' +
              '<a ui-sref-active="active" ui-sref="page({ slug: item.slug })" ng-show="link">{{item.nav_title}}</a>' +
              '<span ng-hide="link" style="{{item.role === \'home\' ? \'padding-left: 30px;\' : \'\'}}">{{item.nav_title}}</span>' +
              '<i style="margin-left: 10px;" class="fa fa-pencil" ng-click="editPageName1(item, $index)" title="Edit Page Name" ng-show="!link"></i>',
    link: function(scope, element, attrs) {      
      scope.editPageName1 = function(item) {
        swal({
          title: 'Change Page Name',
          type: 'input',
          inputPlaceholder: item.nav_title,
          showCancelButton: true
        }, function(newPageName) {
          if (newPageName) {
            scope.editPage()(scope.item._id, newPageName);
          }
        });
      };

      scope.removePage1 = function(item) {
        scope.removePage()(item);
      };
    }
  };
});

function capitalize(string) {
  var output = string.split(' ');
  output.forEach(function(i, idx) {
    output[idx] = (i.charAt(0).toUpperCase() + i.substring(1));
  });

  return output.join(' ');
}
