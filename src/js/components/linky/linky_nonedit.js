//the modal directive to upload and image and crop
//the main navigation
angular.module('pencilblueApp')
.directive('linky', ['$window', 'SiteData', function($window, SiteData) {
	return {
		scope: {
			ngModel: '=',
			tag: '@',
			type: '@',
			accept: '@',
			text: '@',
			editText: '@'
		},
		restrict: 'E',
		replace: true,
		template: function(tElem, tAttrs) {
			return '<' + tAttrs.type + '></' + tAttrs.type + '>';
		},
		link: function(scope, elem, attrs) {
			var siteData = SiteData.getSiteData();
			elem.addClass(scope.type);

			if (attrs.text) {
				elem.text(attrs.text);
			}

			if (!scope.ngModel) {
				elem.remove();
			} else if (scope.ngModel.text) {
				elem.text(scope.ngModel.text);
			}

			elem.on('click', function(e) {
				if (scope.ngModel) {
					$window.open(scope.ngModel.url);
					if (scope.ngModel.text) {
						elem.text(scope.ngModel.text);
					}
				}
			});
		}
	};
}]);
