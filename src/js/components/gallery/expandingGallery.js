pencilblueApp.component('expandingGallery', {
  bindings: {
    gallery: '=',
    nocrop: '='
  },
  template: '<div id="expanding-gallery">'+
                
								'<h1 class="text-center gallery-title">{{$exp.detailSection.name || "GALLERY"}}</h1>' +
								'<h2 ng-show="$exp.detailSection.subtitle" class="gallery-subtitle text-center">{{$exp.detailSection.subtitle || ""}}</h2>' +
                '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>'+
                '<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.css"/>'+
                '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick-theme.min.css" />'+
                '<div class="gallery-overview" ng-if="!$exp.detailSection">'+
                  '<ul>'+
										'<li ng-repeat="section in $exp.gallery" class="gallery-section" ng-click="$exp.expandSection(section)">'+
                      '<div class="main-img-holder">' +
												'<img ng-src="{{$exp.nocrop ? section.images[0].url : (section.images[0].url | expGalleryImage)}}" alt="" />'+
											'</div>' +
                      '<div class="details">'+
                        '<div class="section-name">{{section.name}}</div>'+
                        '<div class="section-subtitle">{{section.subtitle}}</div>'+
                        '<div class="section-view">view photos&nbsp;<i class="fa fa-caret-right"></i></div>'+
                      '</div>'+
                    '</li>'+
                  '</ul>'+
                '</div>'+
                '<div class="section-detail" ng-if="$exp.detailSection">'+
                  '<p class="text-center" ng-click="$exp.goBack()"><i class="fa fa-chevron-left"></i>&nbsp;Return to Gallery</p>'+
                  '<ul>'+
                    '<li ng-repeat="image in $exp.detailSection.images track by image._id" ng-click="$exp.openLightBox($index)">'+
                      '<div class="flex-column-start">'+
                        '<img ng-src="{{image.thumbnail}}" alt="" />'+
                        '<p ng-if="image.title" ng-cloak>{{image.title}}</p>'+
                        '<p ng-if="image.credit" ng-cloak>{{image.credit}}</p>'+
                      '</div>'+
                    '</li>'+
                  '</ul>'+
                '</div>'+
                '<div class="overlay" ng-if="$exp.lightBoxImages" ng-click="$exp.dismiss()">'+
                  '<div class="fa fa-close" ng-click="$exp.dismiss()"></div>'+
                  '<div class="wrapper" ng-click="$event.stopPropagation()">'+
                    '<ul id="light-box-images">'+
                      '<li ng-repeat="image in $exp.lightBoxImages" class="light-box-image" ng-cloak>'+
                        '<img ng-src="{{image.url}}" alt="" />'+
                      '</li>'+
                    '</ul>'+
                  '</div>'+
                  '<script>'+
                    'if ($().slick) {'+
                      'imagesLoaded($("#light-box-images"), function() {'+
                        '$("#light-box-images").slick({'+
                            'infinite: true,'+
                            'accessibility: true'+
                        '});'+
                      '});'+
                    '}'+
                  '</script>'+
                '</div>'+
              '</div>',
  controllerAs: '$exp',
  controller: function($location) {
    var self = this;
    console.log('self expandingGallery', self);

    self.expandSection = function(section) {
        self.detailSection = section;
        $location.search({section: section._id});
    };

    if ($location.search().section) {
      var section = _.find(self.gallery, function(g) { return g._id === $location.search().section; });
      if (section) self.expandSection(section);
    }

    self.goBack = function() {
      self.detailSection = null;
      $location.search({section: null});
    };

    self.openLightBox = function(index) {
      self.lightBoxImages = self.detailSection.images.slice(index).concat(self.detailSection.images.slice(0,index));
    };

    self.dismiss = function() {
      self.lightBoxImages = null;
    };
  }
}).filter('expGalleryImage', function() {
  return function(input, uppercase) {
    if (!input || !input.length) return input;
    var split = input.split(/\/(?!\/)/);
    var sizing = 'c_crop,g_faces,w_650,h_390';
    var idx = split.indexOf('upload');

    if (idx > -1) {
      split.splice(idx + 1, 0, sizing);
      console.log(split.join('/'));
      return split.join('/');
    } else {
      return input;
    }
  };
});
