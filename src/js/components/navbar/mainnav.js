  //the main navigation
  angular.module('pencilblueApp')
  .directive('mainnav', function() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: function(elem, attr) {
        return attr.override || '/public/performance/partials/mainnav.html';
      },
      controller: function($scope, $rootScope) {
        $scope.editing = $rootScope.editing;
      }
    };
  });
