pencilblueApp.directive('resumeBuilder', ['ngDialog', function(ngDialog) {
  return {
    restrict: 'A',
    scope: {
      ngModel: '=',
      pageId: '@'
    },
    link: function(scope, el) {
      el.bind('click', function() {
        ngDialog.open({
          template: '/public/performance/themes/partials/resumeBuilder.html',
          appendClassName: 'resume-modal',
          resolve: {
            ngModel: function() {
              return scope.ngModel;
            },
            updateTable: function() {
              return scope.updateTable;
            }
          },
          controller: function($scope, ngModel, updateTable, GetSite) {  
            console.log('ngModel', ngModel);          
            $scope.table = ngModel;
            $scope.onlyNumbers = /^\d+$/;

            if (ngModel && ngModel.length) {
              $scope.tableInitialized = true;
            } else {
              $scope.tableInitialized = false;
            }

            // $scope.phoneReg = function(infoKey) {
            //   return /^\d+$/;
            //   if (key === 'phone') {
            //     return /^[0-9]*$/;
            //   } else {
            //     return null;
            //   }
            // };

            $scope.addData = function(row) {
              row.data.push({});
              updateTable($scope.table);
            };

            $scope.addRow = function(key) {
              var newData;
              switch (key) {
              case 'experience':
                newData = {header: '', data: [{title: '', role: '', credit: ''}]};
                break;
              case 'education':
                newData = {school: '', degree: '', data: [{title: '', professors: ''}]};
                break;
              }

              $scope.table[key].push(newData);
              updateTable($scope.table);
            };

            $scope.updateTable = function() {
              updateTable($scope.table);
            };

            $scope.saveTable = function() {
              ngDialog.closeAll();
              
              GetSite.set({
                resume: {
                  table: ngModel
                }
              }, true);
            };

            $scope.removeRow = function(section, rowIdx, idx) {
              $scope.table[section][rowIdx].data.splice(idx, 1);
            };

            $scope.deleteSection = function(rowIdx) {
              $scope.table.experience.splice(rowIdx, 1);
            };
          }
        });
      });

      scope.updateTable = function(table) {
        scope.ngModel = table;
      };
    }
  };
}]);
