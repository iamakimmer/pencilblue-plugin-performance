pencilblueApp.controller('ContactController', ['$scope', '$http', function($scope, $http) {
  $scope.formData = {};
  $scope.info = {};

  $scope.submitForm = function() {
    console.log('Submitting contact form');
    if ($scope.page.contactInfo.email) {
      $scope.formData.toEmail = $scope.page.contactInfo.email;
      $http.post('/api/contact', $scope.formData).then(function(res) {
        console.log(res);
        if (res.data.statusCode === 200 || res.data.statusCode === 202) {
          if ($scope.info.error) $scope.info.error = null;
          $scope.info.success = 'Thanks! Your email was sent and I will get back to you as soon as possible!';
          $scope.formData = {};
        }
      }).catch(function(err) {
        console.error(err);
        if ($scope.info.success) $scope.info.success = null;
        $scope.info.error = err.data;
      });
    }
  };
}]);
