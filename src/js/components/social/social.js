angular.module('pencilblueApp').component('social', {
  templateUrl: function() {
    return '/public/performance/partials/social.html';
  },
  bindings: {
    social: '=',
    _site: '=site',
    editing: '=',
    showHandle: '@'
  },
  controller: function(ngDialog, $filter) {

    var scope = this;
    
    
    this.available_social = [{
      name: 'facebook',
      icon: 'facebook',
      url: 'https://www.facebook.com/'
    },{
      name: 'twitter',
      icon: 'twitter',
      url: 'https://www.twitter.com/'
    },{
      name: 'instagram',
      icon: 'instagram',
      url: 'https://www.instagram.com/'
    },{
      name: 'youtube',
      icon: 'youtube',
      url: 'https://www.youtube.com/',
      title: 'Full Youtube url'
    },{
      name: 'email',
      icon: 'envelope-o',
      url: 'mailto:'
    },{
      name: 'imdb',
      icon: 'imdb',
      url: 'http://www.imdb.com/'
    }, {
      name: "backstage",
      icon: 'backstage',
      url: 'https://www.backstage.com/u/'
    }, {
      name: 'actors_access',
      icon: 'actors_access',
      url: 'http://resumes.actorsaccess.com/'
    }];


    this.edit = function() {
      ngDialog.open({
        template: '/public/performance/partials/social-edit-modal.html',
        controllerAs: '$ctrl',
        appendClassName: 'social-edit-modal',
        resolve: {
          social: function() {
            return scope.social;
          },
          available_social: function() {
            return scope.available_social;
          }
        },
        controller: ['social', 'available_social', 'GetSite', function(social, available_social, GetSite) {
          var self = this;
          this.social = social;
          console.log('social', social);
          this.available_social = available_social;
          this.saveSocial = function() {            
            GetSite.set({
              social: self.social
            }).then(function(updatedPage) {
              console.log('upadted');
              console.log('self.social', self.social);
              console.log('social', social);
              ngDialog.close();
            }, function(err) {
              console.error(err);
              ngDialog.close();
            });                        
          };
          this.handleChanged = function(val) {
            console.log('handle changed', self.social[val].handle);
            var clean = self.social[val].handle.replace(/(http(s|)\:\/\/(www\.|)(youtube\.com|youtu\.be)(\/|))/gi, '');
            var noSpaces = clean.replace(/\s| /g, '');
            self.social[val].handle = noSpaces;
          };
        }]
      });
    };
  }
}).filter('socialFormat', function() {
  return function(input) {
    if (input) {
      var mainSplit = input.split('_');
      if (mainSplit.length > 1) {
        mainSplit.forEach(function(i) {
          var split = i.split('');
          var firstLet = split[0];
          i = firstLet.toUpperCase() + split.slice(1);
        });
        return mainSplit.join(' ');
      } else {
        return input;
      }
    }

    return input;
  };
});
