'use strict';

pencilblueApp
  .constant('videoRegexes', {
    iframe: /(youtube|youtu\.be|vimeo)/i,
    notIframe: /(wistia|[a-z0-9]{5,15})/i,
    soundcloud: /soundcloud\.com/i
  })
  .constant('placeholdertext', {
    role1: 'Click to Edit Role 1',
    role2: 'Click to Edit Role 2',
    role1img: 'Upload a photo or headshot that best represents Role 1 (i.e. Actor, Singer, Choreographer).',
    role2img: 'Upload a photo or headshot that best represents Role 2 (i.e. Actor, Singer, Choreographer).',
    subheadAboutText: 'About',
    upcoming1: 'Click to edit Project 1 description',
    upcoming2: 'Click to edit Project 2 description',
    upcoming3: 'Click to edit Project 3 description',
    aboutme: 'Click to enter a brief bio about your multiple disciplines. ',
    projectdescription: {
      header: 'Edit Project Description',
      placeholder: 'Enter a brief description for your recent or upcoming project. Include your role, dates, and location. Boxes left blank in this section will not appear on your website.'
    },
    role1htmleditor: {
      header: 'Edit Role 1',
      description: 'Enter the title of your first discipline (i.e. Actor, Singer, Choreographer) in the text box, below. This title will appear in your navigation bar, as the title of the corresponding Role 1 page of your website, and above the corresponding image on your homepage when a user hovers over the photo.',
      placeholder: 'Example: Actor, Singer, Choreographer'
    },
    role2htmleditor: {
      header: 'Edit Role 2',
      description: 'Enter the title of your second discipline (i.e. Actor, Singer, Choreographer) in the text box, below. This title will appear in your navigation bar, as the title of the corresponding Role 2 page of your website, and above the corresponding image on your homepage when a user hovers over the photo.',
      placeholder: 'Example: Actor, Singer, Choreographer'
    },
    social: 'Click the blue button below to add your social media accounts.',
    classic: 'Want to use a portrait-oriented headshot for your homepage? The Classic template is designed specifically for landscape-oriented headshots, so we recommend choosing a different template.',
    spotlight: 'The homepage of the Spotlight template works best with a landscape-oriented headshot that has a visible background at the top. The navigation bar should not cover your face, so close-up images may not work with this template.',
    abouteditor: {
      header: 'Edit About',
      description: '',
      placeholder: 'This is where you’ll include your bio, which should provide a narrative snapshot of who you are as an artist. <br/><br/> Some actors go traditional, presenting their professional background more or less they way it would appear in a playbill.  Others choose to get a bit more personal and expand upon the usual credits to include information about other skills, interests, or projects related to their work as an actor. <br/><br/>  Either approach is fine, as long as your bio is well written and relevant. Always write in the third person (she/he/they pronouns), use good grammar, and remember to proofread! '
    },
    resumeHeader: 'Click to Upload Photo or Résumé in .jpg, .png, or .pdf format',
    resumeInstruction: 'Your résumé should be clear, legible, and well organized. Present your experience professionally and honestly, and be sure to update whenever you have a new credit to add or to emphasize the experience that best aligns with the roles you’re going out for at any given time.  <br/><br/> Don’t have a résumé? Don’t worry! Our Résumé Builder page layout will guide you through the process of creating one. <br/><br/>  <strong>For safety’s sake, never include your physical address on your résumé.</strong>',
    resumeInstruction2: 'To include a downloadable résumé that visitors can keep or print, click the link below.',
    newseditor: {
      header: 'Edit News',
      placeholder: 'Create and update your news items here. Feature current or upcoming projects, including casting announcements and show dates. Be sure to include venue names and locations, as well as links to relevant production or ticketing websites, news articles, and reviews. (Links can be created in the text editor using the chain link icon.'
    },
    newsImageInstruction: 'Click to upload portrait-oriented photo, graphic, or press clip in .jpg, .png, or .pdf format.',
    newsImageLandscapeInstruction: 'Click to upload landscape-oriented photo, graphic, or press clip in .jpg, .png, or .pdf format.',
    galleryInstructions: 'Click below to upload headshots, production stills, modeling photos, and any images that represent you as a performer. Choose from thumbnail galleries, image carousels, and expanding photo albums in the Page Layouts feature. Group your photos into albums, by projects, headshot session, or modeling campaigns.',
    mediaInstructions: 'Click below to upload audio and video files to your Media page. Group your media clips into albums by project type (TV appearances, commercials, theatre) or, for projects with multiple clips, create albums for each project. You can upload files from YouTube, Vimeo, Wistia, or Soundcloud.',
    contacteditor: {
      description: 'Click to upload portrait-oriented photo in .jpg, .png, or .pdf format.',
      placeholder: 'Use this optional text box to greet your visitors personally, or to include your contact information. <br/><br/>If you don’t want to share your personal information, the optional contact form allows site visitors to send a message directly to your linked email address. <br/><br/>Or, remove this text box or the contact form altogether! To do this, use the page layout tab in the left-hand sidebar area.',
      header: 'Edit Contact'
    },
    landscape: 'This page layout is best for landscape-oriented photos.',
    portrait: 'This page layout is best for portrait-oriented photos.'
  });
