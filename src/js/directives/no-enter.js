pencilblueApp.directive('noEnter', function() {
  return {
    link: function(scope, elem) {
      elem.bind('keydown', function(e) {
        var code = e.keyCode || e.which;

        if (code === 13) {
          e.preventDefault();
        }
      });
    }
  };
});
