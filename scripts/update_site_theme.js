//part of revamp scripts
db.themes.update({_id: 6}, { $set: { name: 'Focus', description: 'Ideal for interdisciplinary artists. Showcase multiple talents on your homepage.'}}); 
db.themes.update({_id: 3}, { $set: { name: 'Marquee', description: 'A picture’s worth a thousand words—so why not include four? Multiple photos provide quick links to your website’s pages.'}});
db.themes.update({_id: 1}, { $set: { description: 'A traditional look, perfect for the no-frills actor.'}});
db.themes.update({_id: 2}, { $set: { name: 'Encore', description: 'For a more dramatic statement, choose a background that really makes your headshot pop.'}});
db.themes.update({_id: 5}, { $set: { name: 'Dynamic', description: 'A clean, streamlined homepage—the perfect option for the actor with a headshot that says it all.'}});
db.themes.update({_id: 7}, { $set: { name: 'Spotlight', description: 'A sleek, borderless homepage that highlights a headshot with stunning composition and a unique background.'}});
db.themes.update({_id: 8}, { $set: { name: 'Runway', description: 'For actors with a lot to share, a carousel homepage offers space for headshots, upcoming performances, and more.'}});