'use strict';

var mongoose = require('mongoose');

var profileSchema = mongoose.Schema({
  wordpress_user_id: Number,
  first_name: String,
  image: String,
  last_name: String,
  subtitle: String,
  about: String,
  news_header: {type: String, default: 'What I\'m working on'},
  news: [{type: String}],  
  representation_header : { type: String, default: "Representation"},
  representation_name : String,
  representation_phone : String,
  representation_email : String,
  facebook : String,
  instagram : String,
  twitter : String,
  active : { type: Boolean, default: false},
  created_at: {
    type: Date,
    default: Date.no
  },
  
},{
  timestamps: true
});

var Profile = mongoose.model('Profile', profileSchema);
module.exports = Profile;
