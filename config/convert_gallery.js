#!/usr/node/bin

var db_url = process.env.MONGODB_URI;

var async = require('async');

var MongoClient = require('mongodb').MongoClient;
// var db = mongoose.connect(db_url);

MongoClient.connect(db_url, function(err, db) {
  var site = db.collection('performance');
  site.find({}).toArray(function(err, sites) {
    if (err) {
      console.log('Error: ', err);
      return process.exit(1);
    } else if (sites) {
      var count = 0;
      async.each(sites, function(s, done) {
        if (s.pictures && s.pictures.length) {
          var section = {name: '', images: []};
          s.pictures.forEach(function(pic) {
            var start = pic.url.lastIndexOf('/') + 1;
            var newId = s._id + '/' + pic.url.substring(start).replace(/(\.jpg|\.jpeg|\.png|\.gif)/gi, '');
            section.images.push({public_id: newId, name: pic.title || ''});
          });

          site.update({_id: s._id}, {$addToSet: {gallery: section}}, function(err) {
            if (!err) count++;
            done(err);
          });
        } else {
          done(null);
        }
      }, function(err) {
        if (err) {
          console.error('An error occured updating a site: ', err);
        } else {
          console.log(count + ' Collections Updated');
          return process.exit(0);
        }
      });
    } else {
      console.log('No sites found!');
      return process.exit(0);
    }
  });
});