angular.module('pencilblueApp').component('home', {
  templateUrl: function ($stateParams, $state, $location, SiteData) {
    var siteData = SiteData.getSiteData();
    var themeId = siteData._site.theme;
		var pageName = $state.current.name;
		if (pageName == 'index') { //needed for prerender
			pageName = 'home';
		}
		
    var templatePath = '/public/performance/themes/' + siteData._site.theme + '/home' + (siteData._site.layouts[pageName]) + '.html';
    return templatePath;
  },
  controller: function($sce, SiteData, placeholdertext) {
    var siteData = SiteData.getSiteData();
    this._site = siteData._site;
    this.images = siteData.images;
    this.placeholdertext = placeholdertext;    
    this.components = siteData.components;
    this.editing = siteData.editing;
    this.trustText = function(text) {
      return $sce.trustAsHtml(text);
    };
  }
});
