angular.module('pencilblueApp')
  .directive('appLoading', function() {
    return {
      restrict: 'E',
      templateUrl: '/public/performance/partials/app-loading.html', // or template: 'template html code inline' Display none to the code is important so is not visible if youre not caling the methods
      replace: true,
      link: function(scope, elem) {
        scope.$on('app-start-loading', function() {
          elem.addClass('active');
        });
        scope.$on('app-finish-loading', function() {
          elem.removeClass('active');
        });
      }
    }
  })
