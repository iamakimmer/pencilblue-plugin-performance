angular.module('pencilblueApp').directive('howto', ['ngDialog', '$location', function(ngDialog, $location) {
  return {
    restrict: 'A',
    link: function(scope, el) {

      // if (window.location.search.indexOf('howto') >= 0) {
      //   openWindow();
      // }
      el.bind('click', openWindow);

      function openWindow() {
        ngDialog.open({
          template: '/public/performance/themes/partials/howto.html',
          //className: 'ngdialog-theme-default',
          width: '75%',
          appendClassName: 'howto-modal',
          controller: function() {
          }
        });
      }
    }
  };
}]);
