angular.module('pencilblueApp').component('contactForm', {
  templateUrl: function(SiteData, $state) {
    return '/public/performance/themes/partials/contact-form.html';
  },
  controllerAs: '$ctrl',
  controller: function($http) {

    var self = this;
    self.info = {};
    self.registerToken = function(token) {
      console.log('registering token', token);
      self.token = token;
    }

    self.submitForm = function() {
      console.log('SUBMIT FORM');
      $http.post('/api/contact', {
        name: self.formData.name,
        message: self.formData.message,
        fromEmail: self.formData.fromEmail,
        gRecaptchaResponse: self.token
      }).then(function(res) {
        if (res.status >= 200 && res.status < 300) {
          if (self.info.error) self.info.error = null;
          self.alertClass = 'alert-success';
          self.info.success = 'Thanks! Your email was sent and I will get back to you as soon as possible!'; 
          self.showAlert = true;
          console.log('SHOULD MAKE VALID AGAIN');
 
          self.formData = {};
          self.contactForm.$setPristine();
          self.contactForm.$setUntouched();
        } else {
          var error = JSON.parse(res.data.body);
          console.error(error);
          var text = 'It seems there was a problem sending an email: <pre>STATUS CODE: ' + res.data.statusCode + '\n' + error.errors[0].message + '</pre> You may try to contact me at: <a href="mailto:' + self.page.social.email.handle + '">' + self.page.social.email.handle + '</a>, or you can contact <a href="mailto:' + window.HELP_EMAIL + '">' + window.HELP_EMAIL + '</a> and let them know!';
          if (self.info.success) self.info.success = null;
          self.alertClass = 'alert-danger';
          self.info.error = text;
          self.showAlert = true;
        }
      }).catch(function(err) {
        console.error(err);
        if (self.info.success) self.info.success = null;
        self.info.error = err.data;
        self.showAlert = true;
      });      
    }
  }
});
