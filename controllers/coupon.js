'use strict';

var coupon = require('../mongoose/coupon');

module.exports = function CouponController(pb) {
  var util = pb.util;

  var Coupon = function() {};

  util.inherits(Coupon, pb.BaseAdminController);

  Coupon.prototype.validate = function(cb) {
    var self = this;

    this.getJSONPostParams(function(err, post) {
      if (err) return cb({error: new Error(err)});

      if (post && post.coupon && typeof post.coupon === 'string') {
        coupon.findOne({_id: post.coupon.toUpperCase() }, function(err, coupon) {
          if (err) return cb({error: new Error(err)});

          if (!coupon) return cb({content: {coupon: null}});

          return cb({content: {coupon: coupon}});
        });
      } else {
        return cb({error: 'Bad Request', code: 400});
      }
    });
  };

  Coupon.getRoutes = function(cb) {
    var routes = [{
      path: '/api/validate-coupon',
      method: 'post',
      handler: 'validate',
      auth_required: true,
      access_level: pb.SecurityService.ACCESS_EDITOR,
      content_type: 'application/json'
    }];

    return cb(null, routes);
  };

  return Coupon;
};
