'use strict';

module.exports = function InactiveSiteController(pb) {
  var InactiveSite = function() {};
  var util = pb.util;

  //inheritance
  util.inherits(InactiveSite, pb.ErrorViewController);

  InactiveSite.prototype.render = function(cb) {
    var self = this;


    this.gatherData(function(err, data) {
      if (util.isError(err)) {

        //to prevent loops we just bury the error
        pb.log.error('ErrorController: %s', err.stack);
        data = {
          navItems: {}
        };
      }

      //build angular controller
      var angularController = pb.ClientJs.getAngularController(
        {
          navigation: data.navItems.navigation,
          contentSettings: data.contentSettings,
          loggedIn: pb.security.isAuthenticated(self.session),
          accountButtons: data.navItems.accountButtons
        }
      );

      //register the model with the template service
      var errMsg = self.getErrorMessage();
      var errStack = self.error && pb.config.logging.showErrors ? self.error.stack : '';
      var model = {
        navigation: new pb.TemplateValue(data.navItems.navigation, false),
        account_buttons: new pb.TemplateValue(data.navItems.accountButtons, false),
        angular_objects: new pb.TemplateValue(angularController, false),
        status: self.status,
        error_message: errMsg,
        error_stack: errStack
      };
      self.ts.registerModel(model);

      //load template
      self.ts.load(self.getTemplatePath(), function(err, content) {
        if (util.isError(err)) {

          //to prevent loops we just bury the error
          pb.log.error('ErrorController: %s', err.stack);
        }

        cb({
          content: content,
          code: self.status,
          content_type: 'text/html'
        });
      });
    });
  };

  InactiveSite.prototype.getTemplatePath = function() {
    return 'inactive-site';
  };

  InactiveSite.getRoutes = function(cb) { cb(null, []); };

  return InactiveSite;
};
