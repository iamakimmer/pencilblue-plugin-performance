'use strict';

var mongoose = require('mongoose');

var schema = {
  _id: String, //the site uid
  title: String,
  subtitle: String,
  created_at: {
    type: Date,
    default: Date.now
  },
  search_title: String,
  description: String,
  trial: {
    type: Boolean,
    default: false
  },
  favicon: String,
  footerTextHomeOnly: {
    type: Boolean,
    default: false
  },
  expiration_date: Date,
  banner_top: {
    type: String,
    default: '/public/performance/partials/banner_top.html'
  },
  banner_bottom: {
    type: String,
    default: '/public/performance/partials/banner_bottom.html'
  },
  theme: {
    type: Number,
    ref: 'Theme',
    default: 2
  },
  layouts: {
    about: {type: Number, default: 1 },
    resume: {type: Number, default: 5},
    resume2: {type: Number, default: 5},
    news: {type: Number, default: 1},
    gallery: {type: Number, default: 1},
    media: {type: Number, default: 1},
    contact: {type: Number, default: 1},
    home: {type: Number, default: 1},
    customPage: {}
  },
  inactivePages: [{
    type: String //todo this is unused in revamped version
  }],
  gallery_thumbnail_nocrop: Boolean,
  gallery: [{
    name: String,
    subtitle: String,
    created_at: Date,
    images: [{
      type: String,
      ref: 'Image',
      _id: false
    }]
  }],
  homeCarouselImages: [
    {url: String}
  ],
  social: {
    facebook: {
      handle: String,
      url: String
    },
    youtube: {
      handle: String,
      url: String
    },
    instagram: {
      handle: String,
      url: String
    },
    email: {
      handle: String,
      url: String
    },
    twitter: {
      handle: String,
      url: String
    },
    imdb: {
      handle: String,
      url: String
    },
    phone: {
      handle: String
    },
    backstage: {
      handle: String,
      url: String
    },
    actors_access: {
      handle: String,
      url: String
    }
  },
  resume: {
    table: {
      personal_info: {
        name: {type: String, default: ''},
        header: {type: String, default: ''},
        union: { type: String, default: '' },
        email: {type: String, default: ''},
        phone: {type: String, default: ''},
        height: {type: String, default: ''},
        weight: {type: String, default: ''},
        hair_color: {type: String, default: ''},
        eye_color: {type: String, default: ''},
        range: {type: String, default: ''}
      },
      experience: [{
        header: { type: String, default: '' },
        data: [{ title: String, role: String, credit: String }]
      }],
      education: [
        {school: String, degree: String, data: [{title: String, professors: String, /*years: String*/}]}
      ],
      special_skills: String
    }
  },
  videos: [{
    title: String,
    url: String,
    provider: String
  }],
  media: [{
    name: String,
    subtitle: String,
    files: [{
      title: String,
      url: String,
      provider: {
        type: String,
        enum: ['youtube', 'vimeo', 'wistia', 'soundcloud']
      }
    }]
  }],
  colorPalette: Number,
  pageStyles: {
    background_color: String,
    header_background_color: String,
    color: String,
    nav_text_color: String,
    nav_active_color: String,
    navbar_background: String,
    link_color: String,
    link_hover_color: String,
    button_text_color: String,
    button_bg_color: String,
    font: [],
    social_background_color: String,
    social_font_color: String,
    title_color: String,
    color_footer: String,
    about_background: String
  },
  active: {
    type: Boolean,
    default: true
  },
  //this is going away in revamp
  // navOrder: {
  //   type: Array,
  //   default: [
  //     {
  //       role: 'home',
  //       title: 'home'
  //     },
  //     {
  //       role: 'about',
  //       title: 'about',
  //     },
  //     {
  //       role: 'resume',
  //       title: 'resume',
  //     },
  //     {
  //       role: 'news',
  //       title: 'news',
  //     },
  //     {
  //       role: 'gallery',
  //       title: 'gallery',

  //     },
  //     {
  //       role: 'media',
  //       title: 'media',
  //     },
  //     {
  //       role: 'contact',
  //       title: 'contact',
  //     }
  //   ]
  // }
};

var siteDataSchema = mongoose.Schema(schema, {
  timestamps: true,
  collection: 'site_data'
});

var siteDataSchemaLive = mongoose.Schema(schema, {
  timestamps: true,
  collection: 'site_data_live'
});

var SiteData = mongoose.model('SiteData', siteDataSchema);
var SiteDataLive = mongoose.model('SiteDataLive', siteDataSchemaLive);

module.exports = {
  SiteData: SiteData,
  SiteDataLive: SiteDataLive
};
