
angular.module('pencilblueApp').component('resumeblock', {
  bindings: {
    pageId: '@',
    editing: '='
  },
  templateUrl: '/public/performance/partials/resumeblock.html',
  controllerAs: '$ctrl',
  controller: function($http, $sce, GetResumeBuilder, ngDialog) {
    var self = this;
    GetResumeBuilder.get({
      pageId: self.pageId
    }).then(function(resp) {

      self.resume = resp.data;
    });    
    
  }
});
