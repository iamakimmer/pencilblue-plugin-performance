'use strict';

var mongoose = require('mongoose');

var schema = {
  site: {
		type: String,
		required: true
	}, //website
	slug: {
		type: String,
		required: true		
	},
	nav_title: {
    type:String,  
    required: true
  },
	on_navbar: {
		type: Boolean,
		default: true
	},	
	nav_order: {
		type: Number,
		default: 1
	},
	deleted_at: {
		type: Date		
	},
  hidden: {
    type: Boolean,
    default: false
  },
	meta_description: String,
	meta_title: String,
	layout_category: {
    type: String,
    default: 'custom',
		required: true
  },
	layout: {
		type: Number,
		default: 1,
		required: true
  },
  last_modified: {
    type: Date
  },
  metadata: {} //for gallery nocrop
};

var pageSchema = mongoose.Schema(schema, {
  timestamps: true,
  collection: 'pages'
});

var pageSchemaLive = mongoose.Schema(schema, {
  timestamps: true,
  collection: 'pages_live'
});

var Page = mongoose.model('Page', pageSchema);
var PageLive = mongoose.model('PageLive', pageSchemaLive);

module.exports = {
  Page: Page, //edit mode component
  PageLive: PageLive //live mode component
};
