pencilblueApp.directive('gallery1', [function () {
  return {
    restrict: 'A',
    scope: {
      gallery: '='
    },
    link: function(scope, element, attrs) {
      console.log('in gallery1');
      if (scope.gallery) {

        var el = angular.element('<div class="gallery"/>');
        scope.gallery.forEach(function(section) {
          var ul = angular.element('<ul id="image-list"/>');
          var row = angular.element('<div class="row"/>');
          var col = angular.element('<div class="col-sm-12"/>');
          var p = angular.element('<p class="gallery-title">' + section.name + '</p>');

          if (section.images.length) {


            section.images.forEach(function(image) {
              ul.append(
                '<li>' +
                    '<a href="' + image.url + '" class="fluid-box">' +
                      '<img src="' + image.thumbnail + '" alt="' + image.title + '" style="display: block;margin: 0 auto;" />' +
                    '</a>' +
                '</li>'
                );
            });
            col.append(p);
            col.append(ul);
            row.append(col);
            el.append(row);
          }
        });

        element.append(el);
        $(document).ready(function() {
          //i took out fluid box library so this wont work
          $('.fluid-box').fluidbox();
        });
      }
    }
  };
}]);
