db.themes.remove({});
db.themes.insertMany([
{
    "_id" : 1.0,
    "updatedAt" : ISODate("2016-09-28T14:05:41.259Z"),
    "createdAt" : ISODate("2016-09-28T14:05:41.259Z"),
    "name" : "Classic",
    "description" : "A traditional look, perfect for the no-frills actor.",
    "created_at" : ISODate("2016-09-28T14:05:41.253Z"),
    "sort" : 6,
    "__v" : 0
},
{
    "_id" : 2.0,
    "updatedAt" : ISODate("2016-09-28T14:05:41.266Z"),
    "createdAt" : ISODate("2016-09-28T14:05:41.266Z"),
    "name" : "Encore",
    "description" : "For a more dramatic statement, choose a background that really makes your headshot pop.",
    "created_at" : ISODate("2016-09-28T14:05:41.256Z"),
    "sort" : 3,
    "__v" : 0
},
{
    "_id" : 4.0,
    "updatedAt" : ISODate("2016-09-28T14:05:41.269Z"),
    "createdAt" : ISODate("2016-09-28T14:05:41.269Z"),
    "name" : "Storyboard",
    "description" : "Got multiple looks? Show off all your best angles with a homepage featuring side-by-side headshots.",
    "created_at" : ISODate("2016-09-28T14:05:41.256Z"),
    "sort" : 4,
    "__v" : 0
},
{
    "_id" : 5.0,
    "updatedAt" : ISODate("2016-09-28T14:05:41.269Z"),
    "createdAt" : ISODate("2016-09-28T14:05:41.269Z"),
    "name" : "Dynamic",
    "description" : "A clean, streamlined homepage—the perfect option for the actor with a headshot that says it all.",
    "created_at" : ISODate("2016-09-28T14:05:41.256Z"),
    "sort" : 5,
    "__v" : 0,
    "templates" : {
        "home" : [ 
            {
                "name" : "Portrait",
                "img" : "/public/performance/themes/5/home1.jpg",
                "id" : 1,
                "path" : "/public/performance/themes/5/home1.html",
                "css" : "/public/performance/themes/5/home1.css"
            }, 
            {
                "name" : "Landscape",
                "img" : "/public/performance/themes/5/home2.jpg",
                "id" : 2,
                "path" : "/public/performance/themes/5/home2.html",
                "css" : null
            }
        ]
    }
},
{
    "_id" : 7,
    "updatedAt" : ISODate("2016-09-28T14:05:41.269Z"),
    "createdAt" : ISODate("2016-09-28T14:05:41.269Z"),
    "name" : "Spotlight",
    "description" : "A sleek, borderless homepage that highlights a headshot with stunning composition and a unique background.",
    "created_at" : ISODate("2016-09-28T14:05:41.256Z"),
    "sort" : 1,
    "__v" : 0
},
{
    "_id" : 8,
    "updatedAt" : ISODate("2016-09-28T14:05:41.269Z"),
    "createdAt" : ISODate("2016-09-28T14:05:41.269Z"),
    "name" : "Runway",
    "description" : "For actors with a lot to share, a carousel homepage offers space for headshots, upcoming performances, and more.",
    "created_at" : ISODate("2016-09-28T14:05:41.256Z"),
    "sort" : 1,
    "__v" : 0
},
{
    "_id" : 3.0,
    "updatedAt" : ISODate("2016-09-28T14:05:41.268Z"),
    "createdAt" : ISODate("2016-09-28T14:05:41.268Z"),
    "name" : "Marquee",
    "description" : "A picture’s worth a thousand words—so why not include four? Multiple photos provide quick links to your website’s pages.",
    "created_at" : ISODate("2016-09-28T14:05:41.256Z"),
    "sort" : 2,
    "__v" : 0,
    "templates" : {
        "home" : [ 
            {
                "name" : "Portrait",
                "img" : "/public/performance/themes/3/home1.jpg",
                "id" : 1,
                "path" : "/public/performance/themes/3/home1.html",
                "css" : null
            }, 
            {
                "name" : "Landscape",
                "img" : "/public/performance/themes/3/home2.jpg",
                "id" : 2,
                "path" : "/public/performance/themes/3/home2.html",
                "css" : null
            }
        ]
    }
},
{
    "_id" : 6,
    "updatedAt" : ISODate("2016-09-28T14:05:41.269Z"),
    "createdAt" : ISODate("2016-09-28T14:05:41.269Z"),
    "name" : "Focus",
    "description" : "Ideal for interdisciplinary artists. Showcase multiple talents on your homepage.",
    "created_at" : ISODate("2016-09-28T14:05:41.256Z"),
    "sort" : 1,
    "__v" : 0,
    "default_pages" : [ 
        {
            "slug" : "about",
            "nav_title" : "About",
            "nav_order" : 2,
            "meta_description" : "About",
            "meta_title" : "About",
            "layout_category" : "about",
            "layout" : 1
        }, 
        {
            "slug" : "role1",
            "nav_title" : "Role 1",
            "nav_order" : 3,
            "meta_description" : "Resume",
            "meta_title" : "Resume",
            "layout_category" : "resume",
            "layout" : 5
        }, 
        {
            "slug" : "role2",
            "nav_title" : "Role 2",
            "nav_order" : 4,
            "meta_description" : "Resume",
            "meta_title" : "Resume",
            "layout_category" : "resume",
            "layout" : 5
        }, 
        {
            "slug" : "news",
            "nav_title" : "News",
            "nav_order" : 5,
            "meta_description" : "News",
            "meta_title" : "News",
            "layout_category" : "news",
            "layout" : 1
        }, 
        {
            "slug" : "gallery",
            "nav_title" : "Gallery",
            "nav_order" : 6,
            "meta_description" : "Gallery",
            "meta_title" : "gallery",
            "layout_category" : "gallery",
            "layout" : 1
        }, 
        {
            "slug" : "media",
            "nav_title" : "Media",
            "nav_order" : 7,
            "meta_description" : "Media",
            "meta_title" : "media",
            "layout_category" : "media",
            "layout" : 1
        }, 
        {
            "slug" : "contact",
            "nav_title" : "Contact",
            "nav_order" : 8,
            "meta_description" : "Contact",
            "meta_title" : "contact",
            "layout_category" : "contact",
            "layout" : 1
        }
    ]
}])