/*
    Copyright (C) 2016  PencilBlue, LLC

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
'use strict';

module.exports = function(pb) {

    //pb dependencies
    var util = pb.util;
    var SiteMapService = pb.SiteMapService;
    var ArticleServiceV2 = pb.ArticleServiceV2;
    var PageService = pb.PageService;
    const PageLive = require('../mongoose/page').PageLive;
    var SiteDataLive      = require('../mongoose/site_data').SiteDataLive,
      ImageLive           = require('../mongoose/image').ImageLive,
      ComponentLive       = require('../mongoose/component').ComponentLive,
      SiteCache       		= require('../mongoose/site_cache').SiteCache,
      Page 								= require('../mongoose/page').Page;

    /**
     * Google sitemap
     * @class SiteMap
     * @extends BaseController
     * @constructor
     */
    function SiteMap(){}
    util.inherits(SiteMap, pb.BaseController);

    //constants
    var PARALLEL_LIMIT = 2;

    /**
     * Initializes the controller
     * @method init
     * @param {Object} context
     * @param {Function} cb
     */
    SiteMap.prototype.init = function(context, cb) {
        var self = this;
        var init = function(err) {

            //build dependencies for site map service
            var articleSerivce = new ArticleServiceV2(self.getServiceContext());
            var pageService = new PageService(self.getServiceContext());
            var context = self.getServiceContext();
            context.articleService = articleSerivce;
            context.pageService = pageService;
            context.supportedLocales = Object.keys(context.siteObj.supportedLocales);

            /**
             *
             * @property service
             * @type {SiteMapService}
             */
            self.service = new SiteMapService(context);

            cb(err, true);
        };
        SiteMap.super_.prototype.init.apply(this, [context, init]);
    };

    SiteMap.prototype.render = function(cb) {
      const self = this;
      const isPreview = self.req.url == '/preview';
      const mode = isPreview ? 'edit':'live';
      const siteId = this.siteObj.uid;

      SiteDataLive.findById(siteId).exec(function(err, site) {
        if (err) return cb(err);

        PageLive.find({site: siteId, hidden: false}).sort({nav_order: 1}).exec(function(error, pages) {
          if (error) return cb({error, code: 500});

          const items = [];
          console.log('siteObj', self.siteObj);
          const hostname = self.siteObj.custom_domain || self.siteObj.hostname;
          items.push({
            url: `http://${hostname}/`,
            last_modified: pages[0].last_modified || new Date()
          });

          console.log('pages: ', pages);
          if (pages.length) {
            pages.forEach((page) => {
              items.push({
                url: `http://${hostname}/${page.slug}`,
                last_modified: page.last_modified || new Date()
              });
            });
          }

          self.service.toXml(items, {}, (error, content) => {
            if (error) return cb({error, code: 500});

            cb({
              content,
              code: self.status,
              content_type: 'text/xml'
            });
          });
        });
      });
    };

    SiteMap.getRoutes = function(cb) {
      var routes = [{
        method: 'get',
        path: '/sitemap',
        auth_required: false,
        handler: 'render',
        content_type: 'text/xml'
      }];

      cb(null, routes);
    };

    //exports
    return SiteMap;
};
