pencilblueApp.factory('GetSite', ['$http', function($http) {
  var saveUrl = '/api/savesite';
  return {
    get: function() {
      return $http.get('/api/site');
    },
    set: function(data) {
      return $http.post(saveUrl, data);
    }
  };
}]).factory('GetGallery', ['$http', function($http) {
  var galleryUrl = '/api/gallery';
  return {
    get: function(data) {
      
      return $http.get(galleryUrl, {
        params : data
      });
    },
    set: function(data) {
      return $http.post(galleryUrl, data);
    }
  };
}]).factory('GetResumeBuilder', ['$http', function($http) {
  var resumeUrl = '/api/resume-builder';
  return {
    get: function(data) {
      
      return $http.get(resumeUrl, {
        params : data
      });
    },
    set: function(data) {
      return $http.post(resumeUrl, data);
    }
  };
}]).factory('GetMedia', ['$http', function($http) {
  var mediaUrl = '/api/media';
  return {
    get: function(data) {
      
      return $http.get(mediaUrl, {
        params : data
      });
    },
    set: function(data) {
      return $http.post(mediaUrl, data);
    }
  };
}]).factory('ImgFactory', ['$http', function($http) {
  return {
    get: function() {
      return $http.get('/api/image');
    },
    update: function(img) {
      return $http.post('/api/image/update', img);
    },
    delete: function(img) {
      return $http.post('/api/image/delete', img);
    }
  };
}]).factory('ComponentFactory', ['$http', function($http) {
  return {
    get: function() {
      return $http.get('/api/components');
    },
    update: function(component) {
      return $http.post('/api/components/update', component);
    },
    updatePage: function(component) {
      return $http.post('/api/components/update-page', component);
    }
  };
}]).factory('SiteData', function() {
  var siteData = {};
  return {
    setSiteData: function(data) {
      siteData = data;
    },
    updateSiteData: function(key, value) {
      siteData[key] = value;
    },
    getSiteData: function() {
      return siteData;
    }
  };
}).factory('Component', ['$http', function($http) {
  return {
    query: function() {
      return $http.get('/api/components');
    }
  };
}]);
