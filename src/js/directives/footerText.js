pencilblueApp.directive('footertext', ['GetSite', '$location', '$transitions', '$state', function(GetSite, $location, $transitions, $state) {
  return {
    restrict: 'E',
    scope: {
      _site: '=site',
      editing: '='
    },
    template: '<div class="footer-text-wrap" ng-if="show()" ng-cloak>'+
                '<p html-editable global="true" ng-model="_site.components.footerHtml[0]" tag="footerHtml" empty="Footer" ng-bind-html="_site.components.footerHtml[0].html || (editing ? placeholder : null)"></p>'+
                '<div id="footer-text" class="form-group" ng-if="editing && _site.theme !== 7" ng-cloak>'+
                  '<input type="checkbox" ng-model="_site.footerTextHomeOnly" ng-change="toggleFooterText()" />'+
                  '<span class="sitewide">&nbsp;<small>Show footer text only in home page</small></span>'+
                '</div>'+
              '</div>',
    link: function($scope) {
      $scope.onStateChange = function() {
        //var navOrder = $scope._site.navOrder.map(function(i) { return i.slug ? '/page/' + i.slug : '/' + i.title; }).filter(function(i) { return i !== 'home'; });
        
        $scope.show = function() {
          //todo never do stuff like this, unmaintainable          
          // if ($scope._site.theme === 7 || $scope.editing) {
          //   return true;
          // } else {
          //   return navOrder.indexOf($location.path()) > -1 ? !$scope._site.footerTextHomeOnly : true;
          // }
          return $scope._site.footerTextHomeOnly;
        };
      };

      $scope.onStateChange();
      $transitions.onSuccess( { to: '*', from: '*' }, $scope.onStateChange);

      
      
      $scope.placeholder = 'You can put anything you want in this space here -- a quote, a mantra, nothing, anything!<br><br>Click the blue button below to add your social media accounts and email!';
      $scope.toggleFooterText = function() {              
        GetSite.set({
          footerTextHomeOnly: $scope._site.footerTextHomeOnly
        }).then(function() {
          //$state.go($state.current, $state.params, {reload: true});
        });        
        
      };
    }
  };
}]);
