pencilblueApp.directive('gRecaptcha', function() {
  return {
    restrict: 'E',
    template: '<div id="grecaptcha-element"></div>',
    controllerAs: '$gRecap',
    scope: {
      callback: '&'
    },
    link: function(scope, el) {
      var elem = el.find('#contact-submit-btn');
      console.log('the elem: ', elem);
      var self = this;
      var id;

      self.checkTheCaptcha = function(token) {
        console.log('checkTheCaptcha: ', token);
        scope.callback({token: token});
      };

      var isCaptchaReady = function() {
        if (typeof grecaptcha !== 'object') {
          console.log('grecaptcha no good try again');
          return setTimeout(isCaptchaReady, 300);
        }

        if (!id) {
          id = grecaptcha.render('grecaptcha-element', {
            sitekey: '6LcGUTEUAAAAAD3r9K0NARXgGHQ46qOmU-1R0Voz',
            callback: self.checkTheCaptcha,
            size: 'normal'
          });
        }
      };

      isCaptchaReady();
    }
  };
});
