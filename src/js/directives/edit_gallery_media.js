pencilblueApp.value('ui.config', {
  sortable: {
    connectWith: '.draggable'
  }
}).directive('editGalleryMedia', function(ngDialog) {
  return {
    restrict: 'E',
    scope: {
      modal: '=',
      media: '=',
      images: '=',
      layout: '<',
      title: '@',
      context: '@',//gallery or media
      pageId: '@'
    },
    templateUrl: '/public/performance/partials/edit-gallery-media.html',
    controller: function($scope, $rootScope, $state, $http, $window, Upload, videoRegexes) {
      $scope.regexes = videoRegexes;
      $scope.contextKeys = {
        gallery: 'images',
        media: 'files'
      };
      if ($scope.images) {
        $scope.files = $scope.images;
      } else if ($scope.media) {
        $scope.files = $scope.media;
        console.log('setting filse to ', $scope.files);
      }

      $scope.setShowModal = function(bool) {
        $scope.saveMedia(false, function() {
          $scope.showModal = bool;
          if (bool == true) {
            angular.element('body').css({
              overflow: 'hidden'
            });
          } else {
            angular.element('body').css({
              overflow: 'auto'
            });
            $state.go($state.current, {}, {reload: true});
          }
        });
      };

      var saveUrl = '/api/' + $scope.context + '/update';
      var updateImage = '/api/image/update';
      var uploadImage = '/api/image/upload';

      $scope.sortableOptions = {
        tolerance: 'pointer',
        connectWith: '.draggable',
        start: function(e, ui) {
          $scope.dragEl = ui.helper;
          $(ui.helper).css('boxShadow', '2px 4px 6px rgba(59,59,59,0.4)');
          $('.draggable').css('background-color', '#ccc');
        },
        stop: function() {
          console.log('stop dragging');
          $($scope.dragEl).css('boxShadow', 'none');
          $('.draggable').css('background-color', 'transparent');
          $scope.dragEl = null;

          var newGallery = angular.copy($rootScope._site[$scope.context]);
          for (var i in $scope.files) {
            newGallery[i] = {};
            var key = $scope.contextKeys[$scope.context];
            newGallery[i][key] = $scope.files[i][key].map(function(obj) {
              if (key === 'images') {
                return obj._id;
              } else {
                return obj;
              }
            });
          }
          console.log('The New Gallery: ', newGallery);
          $rootScope._site[$scope.context] = newGallery;
          $scope.saveMedia(false);
        }
      };

      $http.get('/api/' + $scope.context).then(function(res) {
        $scope.files = res.data;
      }, function(res) {
        console.log('error: ', res);
      });


      $scope.getSectionFiles = function(section) {
        if (!section) return;
        if ($scope.context === 'gallery' && section.images) {
          return {files: section.images};
        } else if (section.files) {
          return {files: section.files};
        }
      };

      $scope.testUrl = function(url) {
        if ($scope.regexes.iframe.test(url)) {
          return 'iframe';
        } else if ($scope.regexes.soundcloud.test(url)) {
          return 'soundcloud';
        } else {
          return 'javascript';
        }
      };

      $scope.addVideoModal = function(sectionIndex) {
        ngDialog.openConfirm({
          className: 'add-video-dialog',
          appendClassName: 'video-modal',
          template: '/public/performance/themes/upload.html',
          scope: $scope,
          controller: ['$scope', '$rootScope', function($scope, $rootScope) {
            $scope.selection = 'youtube';
            $scope.newVideo = {};
            $scope.placeholders = {
              youtube: 'https://youtube.com/watch?v=<your_video_id> or youtu.be/<your_video_id>',
              vimeo: 'Enter the vimeo id',
              wistia: 'Enter the id of your Wistia video here',
              soundcloud: 'https://soundcloud.com/<username>/<track-name>'
            };
            $scope.changeSelection = function(val) {
              $scope.selection = val;
            };


            $scope.regexes = videoRegexes;

            $scope.testUrl = function(url) {
              return $scope.regexes.iframe.test(url);
            };

            var vimeoErrorSwal = swal.bind(swal, {
              title: 'Oops!',
              text: 'You\'ve entered an invalid vimeo id, the Vimeo id must formatted like this: <pre>294208496</pre>',
              html: true,
              type: 'error'
            });

            $scope.addVideo = function(isValid) {
              if (isValid) {
                $scope.newVideo.provider = $scope.selection;

                if ($scope.newVideo.provider === 'youtube' && $scope.newVideo.url.indexOf('embed') === -1) {
                  // youtube
                  var url = $scope.newVideo.url.replace('watch?v=', 'embed/').replace(/&.*$/, '');
                  if (!/(youtu\.be)/gi.test(url)) {
                    $scope.newVideo.url = url;
                  } else {
                    return swal('Oops!', 'We currently do not support shortened youtube links: "https://youtu.be/videoid", please enter the full video link', 'error');
                  }
                } else if ($scope.newVideo.provider === 'vimeo') {
                  // vimeo
                  var id = $scope.newVideo.url;
                  if (id.indexOf('vimeo.com') >= 0) {
                    var vimeoReg = /(http(s|):\/\/|)vimeo\.com(\/\w*)*\/(\d{1,15}$)/i;
                    var vimeoIdMatch = id.match(vimeoReg);
                    console.log('REG MATCH: ', vimeoIdMatch);
                    try {
                      var vimeoId = vimeoIdMatch[vimeoIdMatch.length - 1];
                      console.log('vimeoid: ', vimeoId);
                      $scope.newVideo.url = 'https://player.vimeo.com/video/' + vimeoId;
                    } catch (error) {
                      vimeoErrorSwal();
                      return;
                    }
                  } else if (/^\d{1,}$/.test(id)) {
                    $scope.newVideo.url = 'https://player.vimeo.com/video/' + id;
                  } else {
                    vimeoErrorSwal();
                    return;
                  }

                } else if ($scope.newVideo.provider === 'wistia') {
                  if ($scope.newVideo.url.length > 15) {
                    return swal('Oops!', 'It looks like the wistia video url you entered is invalid, please make sure you enter only the 10-digit video id', 'error');
                  }
                }

                console.log('files', $scope.files);
                console.log('sectionIndex', sectionIndex);
                console.log('$scope.newVideo', $scope.newVideo);
                if (!$scope.files[sectionIndex]) $scope.files[sectionIndex] = { files: [] };
                $scope.files[sectionIndex].files.push($scope.newVideo);

                $scope.saveMedia(false, function() {
                  $scope.newVideo = {};
                  $scope.addVideoForm.$setPristine(true);
                  $scope.addVideoForm.$setUntouched(true);
                  angular.element('.ng-invalid').removeClass('ng-invalid');
                });
              } else {
                return swal('Oops!', 'You must fill out all fields and the url must match the video type', 'error');
              }
            };
          }]
        }).then(function(data) {
          console.log('closed with data', data);

        });
      };
      //
      $scope.addSection = function() {
        if (!$scope.files) $scope.files = [];

        var obj = {
          name: $scope.newSection,
          created_at: new Date()
        };
        console.log('context', $scope.context);

        console.log('NEW SECTION: ', obj);

        obj[$scope.contextKeys[$scope.context]] = [];

        $scope.files.unshift(obj);

        var url = '/api/' + $scope.context + '/addGallerySection';
        console.log('url', url);
        $('#addSection').modal('hide');
        $scope.newSection = '';
        $http.post(url, {gallery: obj}).then(function() {
          // all good
        }).catch(function(err) {
          swal('Oops!', 'There was an error creating the ' + $scope.context + ': ' + JSON.stringify(err.data, null, 2), 'error');
        });
      };

      $scope.saveMedia = function(reload, cb) {
        console.log('saveMedia $scope.files', $scope.files);
        var newData = $scope.files.map(function(section) {
          console.log('in saveMedia', section);
          if (!section) {
            return;
          }
          var obj = {
            name: section.name,
            subtitle: section.subtitle,
            created_at: section.created_at
          };

          if ($scope.context === 'gallery') {
            if (section.images) obj.images = section.images.filter(function (image) { return image !== null; }).map(function(image) { return image._id; });
          } else {
            if (section.files) obj.files = section.files.filter(function (file) { return file !== null; }).map(function(file) { return {
              title: file.title,
              url: file.url,
              provider: file.provider
            }; });
          }

          return obj;
        });

        var body = {};
        body[$scope.context] = newData;

        $http.post(saveUrl, body).then(function(response) {
          if (reload) {
            swal('Success!', 'Changes saved.', 'success');
            angular.element('body').css({
              overflow: 'auto'
            });
            $window.location.reload();
          }

          console.log('back from saving the stuff');
          console.log(response);
          $rootScope._site = response.data;
          if (typeof cb === 'function') cb();
        }, function(err) {
          console.error(err);
          swal('Error!', 'Chages not saved.', 'error');
        });
      };


      $scope.updateImage = function(imageId, field, data) {
        if ($scope.images) {
          var opts = {
            _id: imageId
          };
          opts[field] = data;
          return $http.post(updateImage, opts).then(function() {
            $scope.saveMedia(false);
          });
        }
      };

      $scope.cropImage = function($sectionIdx, $index, img) {
        console.log('sectionIdx', $sectionIdx);
        console.log('index', $index);
        $scope.gallery[$sectionIdx].images.splice($index, 1);
        $scope.saveMedia(false);
      };


      $scope.deleteImage = function($sectionIdx, $index) {
        console.log('sectionIdx', $sectionIdx);
        console.log('index', $index);
        console.log($scope[$scope.context], $scope.context);
        $scope.files[$sectionIdx][$scope.contextKeys[$scope.context]].splice($index, 1);
        $scope.saveMedia(false);
      };

      $scope.deleteSection = function(idx) {
        swal({
          title: 'Are You Sure?',
          text: 'Do you want to delete this album?',
          type: 'warning',
          showCancelButton: true,
          closeOnConfirm: false,
          confirmButtonText: 'Yes, delete it!'
        }, function(isConfirm) {
          if (isConfirm) {
            removeSection(idx, function() {
              swal({
                title: 'Success',
                text: 'Section Deleted!',
                type: 'success',
                timer: 5000
              });
            });
          }
        });
      };

      $scope.moveSection = function(index, direction) {
        var newIndex = index + direction;
        if (newIndex > $scope.files.length - 1) {
          newIndex = $scope.files.length - 1;
        } else if (newIndex < 0) {
          newIndex = 0;
        }

        var item = $scope.files.splice(index, 1);
        if (item) {
          $scope.files.splice(newIndex, 0, item[0]);
          $scope.saveMedia(false);
        }
      };

      function removeSection(idx, callback) {
        $scope.$apply(function() {
          $scope.files.splice(idx, 1);

          $scope.saveMedia(false);
          callback();
        });
      }

      $scope.upload = function(files, section) {
        $scope.fileUploadError = null;

        var validFiles = files.filter(function (f) { return f.size <= 4000000; });
        if (validFiles.length !== files.length) $scope.fileUploadError = 'One or more files uploaded are too large!';

        if (!validFiles.length) return;

        var fileCount = validFiles.length;

        var fileCompleted = 0;
        validFiles.forEach(function(i) {
          var pendingImage = {};
          pendingImage.file = i;
          pendingImage.name = i.name;
          section.images.push(pendingImage);
          Upload.upload({
            url: uploadImage,
            data: {
              file: i,
              tag: 'gallery',
              height: 800
            }
          }).then(function(img) {
            fileCompleted++;
            var imgIndex = _.findIndex(section.images, { name: pendingImage.name});
            section.images[imgIndex] = img.data;
            if (fileCompleted === fileCount) {
              $scope.saveMedia(false);
            }
          }, function(resp) {
            //todo error handling on pending image
            console.log('resp err', resp);
            console.log('Error status: ' + resp.status);
          }, function(evt) {
            //todo loading info
            console.log('resp progressPercentage', evt);
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            pendingImage.progressPercentage = progressPercentage;
            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
          });
        });
      };

      $scope.updateSectionName = function() {
        $scope.saveMedia(false);
      };
    }
  };
});
