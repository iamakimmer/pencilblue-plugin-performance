angular.module('pencilblueApp').component('page', {
  bindings: {
    pagedata: '<', 
    editing: '<'
  },  
  templateUrl: function($state, SiteData, $rootScope) {    
		var page = _.find(SiteData.getSiteData().pages, {slug: $state.params.slug});
		if (page) {		
      
      $rootScope.currentPage = page;			
      var url = '/public/performance/pages/' + page.layout_category + '/' + page.layout + '.html';
      
			return url;
		} else {
			$rootScope.currentPage = null;
		}
  },
  controller: function($scope, $http, $state, $stateParams, $sce, placeholdertext) {
    var self = this;
    this.placeholdertext = placeholdertext;
    this.controllerName = 'Page Component';
    
    this.page = this.pagedata.page;
    this.images = this.pagedata.images;
    this.components = this.pagedata.components;

    this.pageId = this.page._id;    
    this.stateParams = $stateParams;          
    this.info = {success: null, error: null};
    this.alertClass = null;
    this.showAlert = false;

    var pageName = $state.current.name;
    
    this.trustText = function(text) {
      return $sce.trustAsHtml(text);
    };
    
  }
});
