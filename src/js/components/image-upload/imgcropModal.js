//the modal directive to upload and image and crop
//the main navigation
angular.module('pencilblueApp')
  .directive('imgcropModal', ['$rootScope', '$uibModal', 'SiteData', 'ImgFactory', function($rootScope, $uibModal, SiteData, ImgFactory) {
    return {
      scope: {
        ngModel: '=',
        mobileImage: '=',
        outputwidth: '@',
        thumbnailmode: '@',
        idealSize:'@',
        mobileIdealSize: '@',
        tag: '@',
        pageId: '@',
        instruction: '@',
        instructionHeader: '@',
        ignoremessage: '=',
        mobileUpload: '='
      },
      restrict: 'A',
      replace: true,
      link: function(scope, elem, attrs) {
        if (!scope.pageId) {
          console.warn('no page id specified.');
        }
        var siteData = SiteData.getSiteData();

        var uploadMessage = '<div class="upload-message"><div class="message" style="padding: 1em;text-align: center;color:white;">' + (scope.instructionHeader || 'Click to Upload Image')+ '</div></div>';
        var u = angular.element(uploadMessage);

        if (siteData.editing && !scope.ignoremessage) {
          elem.wrap('<div class="imgcropwrap" style="position:relative;height: 100%;width: 100%;"></div>');
          //u.css('display', 'none');
          u.on('click', function() {
            openImageCropModal();
          });
          elem.after(u);
        }



        if (!scope.ngModel || (scope.ngModel && !scope.ngModel.url)) {
          if (siteData.editing) {
            var url = '/public/performance/img/placeholders/' + (scope.idealSize || '200x200') + '.png';
            if (elem[0].tagName === 'IMG') {
              attrs.$set('src', url);
              //u.css('display', 'flex');
              //var uploadIcon = angular.element('<div style="position:absolute;top:20px;left:20px;color:white;"><i class="fa fa-upload fa-2x"></i></div>');
              //uploadIcon.on('click', openImageCropModal);
              //elem.after(uploadIcon);
            } else if (!elem[0].hasAttribute('noshowbg')) {
              console.log('in imgcropmodal');
              elem.css({
                backgroundImage: 'url(/public/performance/img/placeholders/' + (scope.idealSize || '200x200') + '.png)'
              });
              elem.addClass('coverbackground');
            }
          } else {
            elem.remove();
            return;
          }
        } else if ((scope.ngModel && scope.ngModel.url) || (scope.mobileImage && scope.mobileImage.url)) {
          if (elem[0].tagName === 'IMG') {
            attrs.$set('src', scope.ngModel.url);
          } else if (!elem[0].hasAttribute('noshowbg')) { //don't do it for the crop button
            updateBackgroundImage(scope.ngModel, scope.mobileImage);
            // elem.css({
            //     backgroundImage: 'url(' + scope.ngModel.url + ')'
            // });
            elem.addClass('coverbackground');
          }
        }
        if (scope.ngModel && scope.ngModel.title) {
          if (elem[0].tagName === 'IMG') {
            attrs.$set('alt', scope.ngModel.title);
          } else {
            attrs.$set('title', scope.ngModel.title);
          }
        }

				function nonEditOpenImage() {
          if (scope.ngModel.href) {
            location.assign(scope.ngModel.href);
            return;
          }
          var $uibModalInstance = $uibModal.open({
            animation: true,
            size: 'lg',
            template: '<div class="imgcropmodal-modal" style="position:absolute;"><i class="fa fa-times pull-right" ng-click="$ctrl.cancel()"></i></div><img src="' + scope.ngModel.url + '" />',
            windowTopClass: 'fullscreen-lb',
            controllerAs: '$ctrl',
            controller: function() {
              this.cancel = function() {
                $uibModalInstance.dismiss('cancel');
              };
            }
          });
        }

        function openImageCropModal() {

          var modalInstance = $uibModal.open({
            animation: false,
            size: 'md',
            ariaLabelledBy: 'modal-title',
						ariaDescribedBy: 'modal-body',
						windowClass: 'imgCropModal',
            templateUrl: '/public/performance/partials/imgcrop-modal.html',
            controllerAs: '$ctrl',

            controller: function($uibModalInstance, ngModel, viewport, mobileViewport, $scope, outputwidth, thumbnailmode, tag, pageId, idealSize, title, instruction, instructionHeader) {
              var $ctrl = this;
							$ctrl.ngModel = ngModel;
              $ctrl.viewport = viewport;
              $ctrl.mobileViewport = mobileViewport;
              $ctrl.outputwidth = outputwidth;
              $ctrl.thumbnailmode = thumbnailmode;
              $ctrl.tag = tag;
              $ctrl.title = title;
              $ctrl.idealSize = idealSize;
              $ctrl.instruction = instruction;
              $ctrl.instructionHeader = instructionHeader;
              $ctrl.pageId = pageId;
              $ctrl.mobileUpload = scope.mobileUpload;
              $ctrl.mobileImage = scope.mobileImage;
              $ctrl.mobileIdealSize = scope.mobileIdealSize;


              $ctrl.updateLink = function(model) {

								if ($ctrl.ngModel.href && !$ctrl.ngModel.href.startsWith('http')) {
									$ctrl.ngModel.href = 'http://' + $ctrl.ngModel.href;
								}
								ImgFactory.update(model).then(function(resp) {
									$uibModalInstance.dismiss('cancel');
								});
              };

              $ctrl.done = function(img) {
                console.log('INFO: ', scope.mobileUpload, $ctrl.mobileUpload);
                $uibModalInstance.close({img: img, isMobile: scope.mobileUpload});
              };

              $ctrl.cancel = function() {

                $uibModalInstance.dismiss('cancel');
              };
            },
            resolve: {
              idealSize: function() {
                return scope.idealSize;
              },
              tag :function() {
                return scope.tag;
              },
              pageId: function() {
                return scope.pageId;
              },
              thumbnailmode: function() {

                return scope.thumbnailmode;
              },
              ngModel: function() {

                return scope.ngModel;
              },
              title: function() {
                return scope.title;
              },
              instruction: function() {

                return scope.instruction;
              },
              instructionHeader: function() {

                return scope.instructionHeader;
              },
              viewport: calculateViewport(scope.thumbnailmode, scope.idealSize),
              mobileViewport: calculateViewport(scope.thumbnailmode, scope.mobileIdealSize),
              outputwidth: function() {
                var tempWidth = scope.outputwidth ? parseInt(scope.outputwidth, 10) : 600;
                var width = tempWidth || 600;
                if (scope.idealSize) {
                  var idealSizeArr = scope.idealSize.split('x');
                  if (idealSizeArr[0] > width) {
                    width = idealSizeArr[0];
                  }
                }
                return width;
              },
              library: function() {
                return scope.library;
              }
            }
          });

          modalInstance.result.then(function(data) {
            var img = data.img;
            var isMobile = data.isMobile;
            console.log('MORE INFO: ', data);
            if (img && img.img) {
              //u.css('display', 'none');
              if (isMobile) {
                scope.mobileImage = img.img;
              } else {
                scope.ngModel = img.img;
              }
							if (elem.attr('background-image') == 'true') {
								// elem.css({
								// 		backgroundImage: 'url(' + scope.ngModel.url + ')'
                // });
                updateBackgroundImage(scope.ngModel, scope.mobileImage);
							} else {
								attrs.$set('src', img.img.url);
								attrs.$set('alt', img.img.title);
								siteData.images[img.img.tag] = [img.img];
							}
            } else {
              //u.css('display', 'flex');

              attrs.$set('src', '/public/performance/img/placeholders/' + (scope.idealSize || '200x200') + '.png');
              scope.ngModel = null;
              if (siteData.images[scope.tag]) {
                siteData.images[scope.tag] = [];
              }
            }
          }, function() {

          });

        }

        if (!siteData.editing) {
          if (elem.parent()[0].tagName == 'A' || elem[0].hasAttribute('hideshadowbox')) {
            return;
          }
          elem.on('click', nonEditOpenImage);
          return;
        }
        elem.addClass('imgcrop');
        elem.on('click', function() {
          openImageCropModal();
        });


      }
    };
  }]);

  function updateBackgroundImage(desktop, mobile) {
    var splashEl = '#hero-image';
    angular.element('#full-page').before('<style>'+
                        splashEl + '{'+
                          'background-image: url(' + desktop.url + ');'+
                        '}'+
                        '@media (max-width: 767px) {'+
                          splashEl + '{'+
                            'background-image: url(' + (mobile ? mobile.url : desktop.url) + ')'+
                          '}'+
                        '}'+
                      '</style>');
  }

  function calculateViewport(thumbnailmode, idealSize) {
    if (thumbnailmode) {
      return {
        w: 200,
        h: 200
      };
    }

    var w,h;


    if (idealSize) {
      var idealSizeArr = idealSize.split('x');
      var idealSizeObj = {
        w: parseInt(idealSizeArr[0], 10),
        h: parseInt(idealSizeArr[1], 10)
      };

      if (idealSizeObj.w >= idealSizeObj.h) {
        w = 300;
        h = w * idealSizeObj.h / idealSizeObj.w;
      } else {
        h = 300;
        w = h * idealSizeObj.w / idealSizeObj.h;
      }

      return {
        w: w,
        h: h
      };
    } else {
      console.warn('WARNING NO IDEAL SIZE INDICATED, PLEASE FIX');
      return {
        w: 300,
        h: 300
      };
    }
  }
