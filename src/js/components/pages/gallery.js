angular.module('pencilblueApp').component('gallery', {
  bindings: {
    gallery: '<'
  },
  templateUrl: function($stateParams, $state, SiteData) {
    var siteData = SiteData.getSiteData();
    var themeId = siteData._site.theme;
    var pageName = $state.current.name;
    console.log('/public/performance/pages/' + pageName + '/' + (siteData._site.layouts[pageName] || 1) + '.html');
    return '/public/performance/pages/' + pageName + '/' + (siteData._site.layouts[pageName] || 1) + '.html';
  },  controller: function(GetGallery, ngDialog, SiteData) {
    var self = this;

    var siteData = SiteData.getSiteData();

    this._site = siteData._site;
    this.images = siteData.images;
    this.components = siteData.components;
    this.editing = siteData.editing;


    this.gallery = this.gallery.data;

    this.carouselIdx = Array(this.gallery.length).fill(0);

    this.slide = function(idx, dir, sectionIdx) {
      console.log('slide dir', dir);
      if (idx) {
        if (idx > -1) {
          this.carouselIdx[sectionIdx] = idx;
        } else {
          this.carouselIdx[sectionIdx]++;
        }
      } else if (!idx && dir) {
        if (sectionIdx) {
          this.carouselIdx[sectionIdx] = dir === 'next' ? this.carouselIdx[sectionIdx] + 1 : this.carouselIdx[sectionIdx] - 1;
          $('#carousel-element-' + sectionIdx).carousel(dir);
        } else {
          $('#carousel-element').carousel(dir);
        }
      }
    };

    
  }
});
