pencilblueApp.directive('galleryWithoutSection', [function () {
  return {
    restrict: 'A',
    scope: {
      gallery: '='
    },
    link: function(scope, element, attrs) {
      console.log('scope', scope);
      if (scope.gallery) {
        console.log('in gallery');
        var caption = angular.element('<div id="caption">&nbsp;</div>');

        var ul = angular.element('<ul id="gallery-list"/>');
        scope.gallery.forEach(function(section) {
          if (section.images.length) {
            section.images.forEach(function(image) {
              var caption = image.credit ? '<p style="text-align: center;padding: 1em 0;">Photo By: ' + image.credit + '</p>' : '&nbsp;';
              var title = image.title ? '<h3 style="text-align: center;">' + image.title + '</h3>' : '';
              ul.append(
                '<li data-credit="' + (image.credit || '') + '"' + ' data-thumb="' + image.thumbnail + '" class="carousel-item">' +
                      '<img src="' + image.url + '" alt="' + image.title + '" />' +
                       title +
                       caption +
                '</li>'
                );
            });
          }
        });
        element.append(ul);
        //element.append(caption);

        var updateCaption = function(li) {
          var title = li.getAttribute('data-title') || '';
          var credit = li.getAttribute('data-credit') || '';
          if (credit) {
            caption.html('Photo by ' + credit);
          } else {
            caption.html('&nbsp;');
          }

        };

        $(document).ready(function() {
            $('#gallery-list').lightSlider({
              gallery: true,
              item: 1,
              keyPress: true,
              // slideMargin: 20,
              thumbMargin: 10,
              loop: true,
              slideMargin: 0,
              thumbItem: 7,
              onSliderLoad: function(el) {
                var li = el.children()[0];
                //updateCaption(li);
              },
              onAfterSlide: function(el, index) {
                var li = el.children()[index];
                //updateCaption(li);
              }
            });
        });
      } else {
        console.log('NOT IN GALLERY');
      }
    }
  };
}]);
