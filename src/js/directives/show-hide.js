pencilblueApp.directive('showHide', [function () {
  return {
    restrict: 'A',
    scope: {
      ngModel: '=',
      multiple: '=',
      identifier: '='
    },
    link: function(scope, element) {
      console.log(scope.ngModel);
      if (scope.ngModel) {
        if (scope.multiple) {
          var hide = 0;
          if (Array.isArray(scope.ngModel)) {
            scope.ngModel.forEach(function(i) {
              if (scope.identifier) {
                if (!i[scope.identifier]) hide++;
              } else {
                if (!i) hide++;
              }
            });
          } else {
            for (var i in scope.ngModel) {
              if (!scope.ngModel[i].length) hide++;
            }
          }

          if (hide === scope.ngModel.length || hide === Object.keys(scope.ngModel).length) element.remove();
        } else {
          if (!scope.ngModel || scope.ngModel === 'undefined') {
            element.remove();
          }
        }
      } else {
        element.remove();
      }
    }
  };
}]);
