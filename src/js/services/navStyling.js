pencilblueApp.factory('NavStyling', function() {
    var navStyle = {};

    return {
        setNavStyle: function(count) {
            navStyle['font-size'] = 'calc(10vw / ' + count + ')';
            navStyle.width = 'calc(100% / ' + count + ')';
        },
        getNavStyle: function() {
            return navStyle;
        }
    }
});