angular.module('pencilblueApp').component('carousel', {
  bindings: {
    ngModel: '=',
    idealSize: '<'
  },
  template: '<div class="carousel">' +
              '<button style="margin-top: 10px;" class="btn btn-primary sitewide" ng-show="$carousel.editing" ng-click="$carousel.editImages()"><i class="fa fa-plus"></i> Add/Edit Images</button>' +
              '<div id="main-carousel" class="slick-carousel" ng-style="$carousel.loadedStyle">' +
                '<div class="slick-img-wrap" ng-repeat="img in $carousel.ngModel.images track by $index">' +
									'<a ng-href="{{img.href || \'#\'}}" target="{{img.href_target}}"><img ng-src="{{img.url}}" alt="" /></a>' +
                '</div>' +
              '</div>' +
            '</div>',
  controllerAs: '$carousel',
  controller: function($scope, $location, $state, $stateParams, SiteData, Upload, $uibModal, $timeout) {
    var self = this;
    self.loadedStyle = {'opacity': 0};
    var openModal = $stateParams.modal === 'true';
    console.log('ngModel: ', self.ngModel);

    var siteData = SiteData.getSiteData();
    self.editing = siteData.editing;

    if (!self.ngModel) {
      self.ngModel = {images: []};
    }

    // if (!self.ngModel || (self.ngModel && (!self.ngModel.images || self.ngModel.images.length < 1))) {
    //   if (self.editing) {
    //     //var image = {url: '/public/performance/img/placeholders/' + (self.idealSize || '200x200') + '.png'};
    //     //self.ngModel.images = [image, image, image];
    //     //self.ngModel.placeholders = true;
    //   }
    // }

    imagesLoaded($('.slick-carousel img'), function() {
      self.loadedStyle = {'opacity': 1};
      $scope.$apply();
      objectFitImages();
    });

    self.editImages = function(hasQueryStringInURL) {
      $state.go($state.current, {modal: true}, {reload: false});
      var modalInstance = $uibModal.open({
        animation: false,
        size: 'lg',
        windowClass: 'home-page-image-edit',
        templateUrl: '/public/performance/partials/carouselUpload.html',
        resolve: {
          ngModel: function() {
            return self.ngModel;
          },
          thumbnailmode: function() {
            return false;
          },
          idealSize: function() {
            return '1200x700';
          }
        },
        controllerAs: 'carouselUploader',
        controller: function($http, ngModel, thumbnailmode, idealSize) {
					var carouselUploader = this;

					carouselUploader.thumbnailmode = thumbnailmode;
					carouselUploader.idealSize = idealSize;

          carouselUploader.ngModel = ngModel;
          carouselUploader.context = 'gallery';

          var firstSort = true;

          var uploadImage = '/api/image/upload';

          carouselUploader.sortableOptions = {
            tolerance: 'pointer',
            handle: '.drag-tab',
            helper: 'clone',
            stop: function() {
              $http.post('/api/carousel/update', carouselUploader.ngModel).then(function(res) {
                carouselUploader.ngModel = res.data;
                $state.go($state.current, {modal: false}, {reload: true});
              });
            }
          };

          carouselUploader.upload = function(files) {
            console.log('carousel files: ', files);
            carouselUploader.fileUploadError = null;

            var validFiles = files.filter(function (f) { return f.size <= 10000000; });
            if (validFiles.length !== files.length) carouselUploader.fileUploadError = 'One or more files uploaded are too large!';

            if (!validFiles.length) return;

            var fileCount = validFiles.length;

            var fileCompleted = 0;
            carouselUploader.updatePercentage();
            validFiles.forEach(function(i) {
              var pendingImage = {};
              pendingImage.file = i;
              pendingImage.name = i.name;
							console.log('siteData', siteData);

							var uploadData = {
                  file: i,
                  tag: 'homeCarouselImage',
									height: 700,
									width: 1200,
									crop: 'pad'
							};
							if (siteData._site.pageStyles && siteData._site.pageStyles.backgroundColor) {
								uploadData.background = siteData._site.pageStyles.backgroundColor;
							}

              Upload.upload({
                url: uploadImage,
                data: uploadData
              }).then(function(img) {
                fileCompleted++;

                if (!carouselUploader.ngModel.images.length || carouselUploader.ngModel.placeholders) carouselUploader.ngModel.images = [];
                carouselUploader.ngModel.images.push(img.data);

								if (fileCompleted === fileCount) {
									$http.post('/api/carousel/update', carouselUploader.ngModel).then(function(res) {
											carouselUploader.ngModel = res.data;
									}).catch(function(err) {
										console.error(err);
									});
								}
              }, function(resp) {
                console.log('resp err', resp);
                console.log('Error status: ' + resp.status);
              }, function(evt) {
                console.log('resp progressPercentage', evt);
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                pendingImage.progressPercentage = progressPercentage;
                // carouselUploader.progressPercentage = progressPercentage;
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
              });
            });
          };

          carouselUploader.deleteImage = function(index) {
            swal({
              title: 'Are you sure?',
              text: 'Delete this image?',
              closeOnConfirm: false,
              showCancelButton: true,
              type: 'warning'
            }, function(confirmed) {
              if (confirmed) {
								carouselUploader.ngModel.images.splice(index, 1);

                $http.post('/api/carousel/update', carouselUploader.ngModel).then(function(res) {
                  carouselUploader.ngModel = res.data;
                  swal('Success', '', 'success');
                });
              }
            });
          };

          carouselUploader.percentageCount = 0;
          carouselUploader.progressPercentage = 0;
          carouselUploader.updatePercentage = function() {
            console.log('progress: ', carouselUploader.percentageCount, carouselUploader.progressPercentage);
            if (carouselUploader.percentageCount <= 5 && carouselUploader.progressPercentage < 100) {
              carouselUploader.percentageCount++;
              carouselUploader.progressPercentage += 25;
              $timeout(function() {
                carouselUploader.updatePercentage();
              }, 200);
            } else {
              carouselUploader.progressPercentage = 100;
              $timeout(function() {
                carouselUploader.progressPercentage = 0;
              }, 3000);
            }
          };
        }
			});

			modalInstance.result.then(function(component) {
				location.replace("/edit");
			}, function() {
				console.log('Modal dismissed at: ' + new Date());
			});
    };

    if (openModal && self.editing) {
      self.editImages(true);
    }
  }
});
