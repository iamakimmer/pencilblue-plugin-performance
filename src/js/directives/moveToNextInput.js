pencilblueApp.directive('moveToNextInput', function() {
  return {
    restrict: 'A',
    scope: {
      closest: '@'
    },
    link: function($scope, elem) {
      elem.bind('keydown', function(e) {
        var code = e.keyCode || e.which;
        var closest = $scope.closest.replace('class_', '.').replace('id_', '#');

        if (code === 13) {
          e.preventDefault();
          if ($scope.closest === 'input') {
            elem.next('input').focus();
          } else if (elem.parent(closest).next()[0]) {
            var length = elem.parent('.form-group').next().find('input').length;

            if (length > 1) {
              elem.parent(closest || 'div').next().find('input')[0].focus();
            } else {
              elem.parent(closest || 'div').next().find('input').focus();
            }
          } else {
            e.preventDefault();
          }
        }
      });
    }
  };
});
