/*
Copyright (C) 2015  PencilBlue, LLC

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
var path = require('path');

module.exports = function HomePageSettingsModule(pb) {

  //pb dependencies
  var util          = pb.util;
  var SUB_NAV_KEY = 'performance_home_page_settings';

  /**
  * Settings for the display of home page content in the Performance theme
  * @class HomePageSettings
  * @author Blake Callens <blake@pencilblue.org>
  */
  function HomePageSettings() {}
  util.inherits(HomePageSettings, pb.BaseAdminController);

  HomePageSettings.prototype.render = function(cb) {

    console.log('in performance home page settings');
    var self = this;
    var tabs = [
      {
        active: 'active',
        href: '#home_layout',
        icon: 'home',
        title: self.ls.get('HOME_LAYOUT')
      },
      {
        href: '#media',
        icon: 'picture-o',
        title: self.ls.get('HOME_MEDIA')
      },
      {
        href: '#callouts',
        icon: 'th',
        title: self.ls.get('CALLOUTS')
      }
    ];

    var opts = {
      where: {settings_type: 'home_page'}
    };
    self.siteQueryService.q('performance_theme_settings', opts, function(err, settings) {
      console.log('settings', settings);
      if(settings.length > 0) {
        settings = settings[0];
      }
      else {
        settings = {callouts: [{}, {}, {}], site:self.site};
      }

      var mservice = new pb.MediaService(self.site, true);
      mservice.get(function(err, media) {
        if(settings.page_media) {
          var pageMedia = [];
          for(var i = 0; i < settings.page_media.length; i++) {
            for(var j = 0; j < media.length; j++) {
              if(pb.DAO.areIdsEqual(media[j][pb.DAO.getIdField()], settings.page_media[i])) {
                pageMedia.push(media[j]);
                media.splice(j, 1);
                break;
              }
            }
          }
          settings.page_media = pageMedia;
        }

        console.log('settings', settings);

        var objects = {
          navigation: pb.AdminNavigation.get(self.session, ['plugins', 'manage'], self.ls, self.site),
          pills: self.getAdminPills(SUB_NAV_KEY, self.ls, null),
          tabs: tabs,
          media: media,
          homePageSettings: settings
        };
        console.log('rendering');
        self.ts.registerLocal('angular_script', '');
        self.ts.registerLocal('angular_objects', new pb.TemplateValue(pb.ClientJs.getAngularObjects(objects), false));
        self.ts.load(path.join('admin', 'settings', 'home_page_settings'), function(err, result) {
          console.log('err', err);
          console.log('result', result);
          cb({content: result});
        });

      });
    });
  };

  HomePageSettings.getSubNavItems = function() {
    return [
      {
        name: 'content_settings',
        title: 'Home Page Settings',
        icon: 'chevron-left',
        href: '/admin/plugins/performance/settings'
      }
    ];
  };

  HomePageSettings.getRoutes = function(cb) {
    var routes = [
      {
        method: 'get',
        path: '/admin/plugins/performance/settings/home_page',
        auth_required: true,
        inactive_site_access: true,
        access_level: pb.SecurityService.ACCESS_EDITOR,
        content_type: 'text/html'
      }
    ];
    cb(null, routes);
  };
  //register admin sub-nav
  pb.AdminSubnavService.registerFor(SUB_NAV_KEY, HomePageSettings.getSubNavItems);

  //exports
  return HomePageSettings;
};
