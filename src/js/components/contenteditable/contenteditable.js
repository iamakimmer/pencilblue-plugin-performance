angular.module('pencilblueApp')
  .directive('contenteditable', [function() {
  return {
    restrict: "A",
    require: "ngModel",
    link: function(scope, element, attrs, ngModel) {

      function read() {
        ngModel.$setViewValue(element.html());
      }

      // element.bind("keydown keypress", function (event) {
      //     if(event.which === 13) {
      //       console.log('enter key');
      //       event.preventDefault();
      //       scope.$apply(read);
      //     }
      // });

      ngModel.$render = function() {
        element.html(ngModel.$viewValue);
      };

      element.bind("keyup change", function() {
        scope.$apply(read);
        console.log('onblue keyup change');
      });
      element.bind("blur", function() {
        scope.$apply(read);
        console.log('onblur');
      });

    }
  };
}]);
