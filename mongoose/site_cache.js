'use strict';

var mongoose = require('mongoose');

var schema = {
  _id: String, //the site uid
	keys: [{type: String}]
};

var siteCacheSchema = mongoose.Schema(schema, {
  timestamps: true,
  collection: 'site_cache'
});


var SiteCache = mongoose.model('SiteCache', siteCacheSchema);

module.exports = {
  SiteCache: SiteCache
};
