pencilblueApp.config(['$provide', function($provide) {
  $provide.decorator('taOptions', ['$delegate', 'taRegisterTool', function(taOptions, taRegisterTool) {
    taOptions.toolbar = [
      [/*'h1', 'h2', 'h3', 'h4', 'h5', 'h6',*/ 'p', 'quote'],
      ['bold', 'italics', 'underline', 'strikeThrough', 'ul', 'ol', 'redo', 'undo', 'clear'],
      ['justifyLeft', 'justifyCenter', 'justifyRight'],
      ['html','insertLink', 'wordcount', 'charcount']
    ];
    taRegisterTool('fontSize', {
      display:  "<span class='bar-btn-dropdown dropdown'>" +
                  "<button title='text-size' class='btn btn-blue btn-xs dropdown-toggle' type='button' ng-disabled='showHtml()' style='padding-top: 4px'>"+
                    "<span>Text Size</span><i class='fa fa-caret-down'></i>"+
                  "</button>" +
                  "<ul id='font-size-dropdown' class='dropdown-menu' style='color: white;'>"+
                    "<p style='margin-bottom: 0;padding: 0 5px;color: black;'>Text Size:</p>" +
                    "<li ng-repeat='o in options'>"+
                      "<button class='btn btn-blue checked-dropdown' style='font-size: {{o.css}}; width: 100%' type='button' ng-click='action($event, o.value)'>"+
                        "<i ng-if='o.active' class='fa fa-check'></i> {{o.name}}"+
                        "</button>"+
                      "</li>"+
                    "</ul>" +
                "</span>",
      action: function (event, size) {
        //Ask if event is really an event.
        if (event.stopPropagation) {
          //With this, you stop the event of textAngular.
          event.stopPropagation();
          //Then click in the body to close the dropdown.
          $("body").trigger("click");
        }
        return this.$editor().wrapSelection('fontSize', parseInt(size));
      },
      options: [
        { name: 'xx-small', css: 'xx-small', value: 1 },
        { name: 'x-small', css: 'x-small', value: 2 },
        { name: 'small', css: 'small', value: 3 },
        { name: 'medium', css: 'medium', value: 4 },
        { name: 'large', css: 'large', value: 5 },
        { name: 'x-large', css: 'x-large', value: 6 },
        { name: 'xx-large', css: 'xx-large', value: 7 }

      ]
    });
    taOptions.toolbar[3].push('fontSize');
    return taOptions;
  }]);
}]).run(['editableOptions', function(editableOptions) {
  editableOptions.buttons = 'no';
  editableOptions.blurElem = 'submit';
}]).directive('dropdownToggle', ['$document', '$location', function ($document, $location) {
  var openElement = null,
    closeMenu = angular.noop;
  return {
    restrict: 'CA',
    link: function (scope, element, attrs) {
      scope.$watch('$location.path', function () { closeMenu(); });
      element.parent().bind('click', function () { closeMenu(); });
      element.bind('click', function (event) {
        var elementWasOpen = (element === openElement);

        event.preventDefault();
        event.stopPropagation();

        if (openElement) {
          closeMenu();
        }

        if (!elementWasOpen && !element.hasClass('disabled') && !element.prop('disabled')) {
          element.parent().addClass('open');
          openElement = element;
          closeMenu = function (event) {
            if (event) {
              event.preventDefault();
              event.stopPropagation();
            }
            $document.unbind('click', closeMenu);
            element.parent().removeClass('open');
            closeMenu = angular.noop;
            openElement = null;
          };
          $document.bind('click', closeMenu);
        }
      });
    }
  };
}]);

pencilblueApp.controller('EditController', ['$scope', '$rootScope', '$http', '$location', '$window', '$state', '$stateParams', '$interval', '$sce', 'cssInjector', '$transitions', 'SiteData', 'colorPalettes', 'NavStyling', 'ngDialog', '$timeout', 'placeholdertext', 'GetSite', function($scope, $rootScope, $http, $location, $window, $state, $stateParams, $interval, $sce, cssInjector, $transitions, SiteData, colorPalettes, NavStyling, ngDialog, $timeout, placeholdertext, GetSite) {
  $scope.locationQuery = $location.search();
  $scope.editMenu = null;
  $scope.state = $state;
  $scope.stateName = '';

	$scope.$watch('state.current.name', function(newVal) {
		if (newVal.length) $scope.stateName = newVal;
	});
  $scope.stateParams = $stateParams;

  $scope.editPanelNavItems = [
    'theme',
    'pages',
    'page layout',
    'colors',
    'fonts'
  ];


  for (var i in $scope.locationQuery) {
    if ($scope.editPanelNavItems.indexOf(i) > -1) {
      $scope.editMenu = i;
      break;
    }
  }

  $scope.$state = $state;
  $scope.navSortableOptions = {
    tolerance: 'pointer',
    handle: '.nav-handle',
    update: function(e, ui) {
      if (ui.item.sortable.model.role === 'home') {
        ui.item.sortable.cancel();
      }
    },
    stop: function(e, ui) {
      $scope.pages.forEach(function(page, index) {
        page.nav_order = index + 1;
      });
      $http.post('/api/updateNavOrder', {pages: $scope.pages}).then(function(response) {

      }).catch(function(err) {
        console.error(err);
        swal({
          title: 'Error',
          text: 'There was an error saving the nav order: ' + JSON.stringify(err.message, null, 2),
          type: 'error'
        });
      });
    }
  };

  $scope.shouldShowNavItem = function(item) {
    // todo why hardcode this never do this
    return (($scope._site.theme === 6 && item.role === 'resume') || ($scope._site.theme !== 6 && (item.role === 'role-1' || item.role === 'role-2')) || item.role === 'home');
  };

  $scope.setSiteStyle = function() {
    var length = $scope.pages.length;
    NavStyling.setNavStyle(length);
    $scope.navStyle = NavStyling.getNavStyle();
  };

  $scope.colorType = 'palette';

  // $scope.$on('colorpicker-selected', function(event, colorObject) {
  //   console.log('colorpicker event', event);
  //   console.log('colorpicker colorObject', colorObject);
  //   GetSite.set({
  //     pageStyles: $rootScope._site.pageStyles
  //   }).then(function() {
  //   });
  // });

  $scope.$on('colorpicker-closed', function(event, colorObject) {
    GetSite.set({
      pageStyles: $rootScope._site.pageStyles
    }).then(function() {
    });
  });

  $scope.isHomeState = function() {
    return $state.current.name === 'home';
  };

  $scope.orderPageStyles = function() {
    var pageStyles = $rootScope._site.pageStyles;
    $scope._site.pageStyles = {
      background_color: pageStyles.background_color,
      header_background_color: pageStyles.header_background_color,
      navbar_background: pageStyles.navbar_background,
      nav_text_color: pageStyles.nav_text_color,
      nav_active_color: pageStyles.nav_active_color,
      title_color: pageStyles.title_color,
      color: pageStyles.color,
      link_color: pageStyles.link_color,
      link_hover_color: pageStyles.link_hover_color,
      button_bg_color: pageStyles.button_bg_color,
      button_text_color: pageStyles.button_text_color,
      social_background_color: pageStyles.social_background_color,
      social_font_color: pageStyles.social_font_color,
      font: pageStyles.font,
      color_footer: pageStyles.color_footer
    };
  };

  $scope.getTrialHoursRemaining = function() {
    var date = new Date();
    var expireDate = new Date($rootScope._site.expiration_date);
    var html;
    if (expireDate < date) {
      html = '<span>Your site is currently expired. Please signup for a subscription.</span>';
    } else {
      html = '<span>Your site is currently in trial mode. Your trial will expire on ' + moment($rootScope._site.expiration_date).format('dddd, MMMM Do YYYY h:mm a') + '.</span>';
    }

    $scope.trialText = $sce.trustAsHtml(html);
  };

  $(document).ready(function() {
    var $myGroup = $('#bottom-menu');
    $myGroup.on('show.bs.collapse', '.collapse', function() {
      $myGroup.find('.collapse.in').collapse('hide');
    });
  });


  $scope.state = $state;

  cssInjector.removeAll();
  var saveUrl = '/api/savesite';
  var setThemeUrl = '/api/settheme';


  $rootScope.editing = window.editing = true;

  $rootScope._site = $scope.siteData;
  $scope.palettes = colorPalettes;
  //todo what is this?

  $scope.choosePalette = function(idx) {
    var pageStyles = $scope.palettes[$rootScope._site.theme][idx];
    pageStyles.font = $rootScope._site.pageStyles.font || [];
    console.log('choose palette');
    GetSite.set({
      pageStyles: pageStyles,
      colorPalette: idx
    }).then(function() {
      $rootScope._site.pageStyles = pageStyles;
    });
  };

   //BAN BAN BAN THIS FUNCTION IT"S CRAZY TO SAVE ENTIRE OBJECT.
  $rootScope.save = function(showAlert, cb) {
    alert('Could not save. Please contact support');
    // console.log('savesite');
    // if (typeof showAlert === 'undefined') showAlert = true;
    // $scope.$broadcast('app-start-loading');
    // $http.post(saveUrl, $scope._site).then(function(updatedPage) {
    //   $scope._site.__v = updatedPage.data.__v;
    //   $scope.$broadcast('app-finish-loading');
    //   if (showAlert) {
    //     swal('Success!', 'Changes saved.', 'success');
    //   }
    //   if (cb) {
    //     if (typeof cb === 'function') cb(null);
    //   } else {
    //     // $state.reload();
    //     location.reload();
    //   }

    // }, function(err) {
    //   $scope.$broadcast('app-finish-loading');
    //   if (showAlert) {
    //     swal('Error!', 'Changes not saved. Error: ' + err.message, 'error');
    //   }
    //   if (cb && typeof cb === 'function') {
    //     cb('Changes not saved');
    //   }
    // });
  };

  $scope.greenRoom = function() {
    location.href = window.GREEN_ROOM;
  }
  $scope.publishSite = function() {
    swal({
      title: 'Are You Sure?',
      text: 'Are you sure you want to publish your changes?',
      type: 'warning',
      closeOnCofirm: true,
      showCancelButton: true,
      closeOnCancel: true
    }, function(isConfirm) {
      if (isConfirm) {
        $http.post('/api/publish/site').then(function(response) {
          if (response.data.message === 'ok') {
            location.reload();
          }
        }).catch(function(err) {
          console.error(err);
        });
      }
    });
  };

  $scope.unpublishSite = function() {
    swal({
      title: 'Are You Sure?',
      text: 'Are you sure you want to unpublish your site? It won\'t be accessible to anyone but you.',
      type: 'warning',
      showCancelButton: true,
      closeOnCancel: true,
      closeOnConfirm: false
    }, function(isConfirm) {
      if (isConfirm) {
        $http.post('/api/unpublish/site').then(function(response) {
          if (response.data.message === 'ok') {
            console.log('good response');
            swal({
              title: 'Success',
              text: 'Your site is now only visible to you. If you want to make it live again, click "Publish"',
              type: 'success'
            }, function() {
              location.reload();
            });
          }
        }).catch(function(err) {
          console.error(err);
        });
      }
    });
  };

  var cssPath;

  $scope._site.theme = $scope._site.theme || 1;
  $scope._site.components = _.groupBy($scope.components, 'tag');
  console.log('components', $scope._site.components);
  $scope._site.images = _.groupBy($scope.siteImages, 'tag');
  cssPath = '/public/performance/themes/' + $scope._site.theme + '/theme.css';
  cssInjector.add(cssPath);
  //TODO This needs to be dynamic
  var theme = _.find($scope.themes, {_id: $scope._site.theme});

  SiteData.setSiteData({
    images: $scope._site.images,
    components: $scope._site.components,
		editing: true,
		pages: $scope.pages,
    _site: $scope.siteData
  });

  $scope.pages.forEach(function(page) {
    var css =       '/public/performance/pages/' + page.layout_category + '/' + page.layout + '.css';
    cssInjector.add(css);
  });


  $scope.currentStep = localStorage.getItem('editTourComplete') ? -1 : 0;

  $scope.endTour = function() {
    localStorage.setItem('editTourComplete', true);
  };

  $scope.pageStyleUrl = '/public/performance/themes/' + $rootScope._site.theme + '/pagestyle.json';
  console.log('pageStyleUrl: ', $scope.pageStyleUrl);
  $http.get($scope.pageStyleUrl).then(function(response) {
    console.log('pageStyle: ', response.data);
    $scope.defaultStyles = response.data;
  }).catch(function(err) {
    console.error(err);
  });

  $scope.defaultColor = function(key) {
    $scope.userColors = $scope._site.pageStyles;
    console.log('pageStyle: ', $scope.defaultStyles);
    $scope._site.pageStyles = $scope.defaultStyles;

    $scope.revertColorsPreview = true;
  };

  $scope.revertColors = function() {
    $scope.revertColorsPreview = false;
  };

  $scope.keepColors = function() {
    $scope._site.pageStyles = $scope.userColors;
    $scope.revertColorsPreview = false;
  };


  $scope.fonts = [
    'Playfair Display/Open Sans',
    'Oswald/Lato',
    'Abril Fatface/Lato',
    'Fjalla One/Didact Gothic',
    'Bitter/Raleway',
    'Montserrat Bold/Montserrat Regular',
    'Josefin Sans Bold/Josefin Sans Regular',
    'Alegreya/Open Sans',
    'Oswald/Droid Serif',
    'Raleway/Libre Baskerville',
    'Montserrat/Hind',
    'Dosis/Open Sans',
    'Raleway/Cabin',
    'PT Sans/Didact Gothic',
    'Oxygen/Source Sans Pro',
    'Josefin Sans Bold/Open Sans',
    'Josefin Sans Regular/Playfair Display',
    'Open Sans/Lora',
    'Cardo/Josefin Sans Regular',
    'Amatic SC/Josefin Sans Regular',
    'Lobster/Cabin',
    'Rancho/Gudea'
  ];

  $scope.fontsObj = [
    ['Playfair Display', 'Open Sans'],
    ['Oswald','Lato'],
    ['Abril Fatface', 'Lato'],
    ['Francois One', 'Didact Gothic'],
    ['Bitter', 'Raleway'],
    ['Montserrat Bold', 'Montserrat Regular'],
    ['Josefin Sans Bold', 'Josefin Sans Regular'],
    ['Amaranth', 'Titillium Web'],
    ['Oswald', 'Droid Serif'],
    ['Raleway', 'Merriweather'],
    [ 'Montserrat', 'Hind' ],
    [ 'Dosis', 'Open Sans' ],
    [ 'Raleway', 'Cabin' ],
    [ 'PT Sans', 'Didact Gothic' ],
    [ 'Oxygen', 'Source Sans Pro' ],
    [ 'Josefin Sans Bold', 'Open Sans' ],
    [ 'Josefin Sans Regular', 'Playfair Display' ],
    [ 'Open Sans', 'Lora' ],
    [ 'Cardo', 'Josefin Sans Regular' ],
    [ 'Amatic SC', 'Josefin Sans Regular' ],
    [ 'Lobster', 'Cabin' ],
    [ 'Rancho', 'Gudea' ]
  ];

  $scope.themeFontDefaults = {
    4: ['Times New Roman','Raleway'],
    5: ['Cardo', 'Josefin Sans Regular']
  };

  if ($scope.themeFontDefaults[$scope._site.theme]) {
    if (!$scope._site.pageStyles.font || !$scope._site.pageStyles.font.length) {
      $scope._site.pageStyles.font = $scope.themeFontDefaults[$scope._site.theme];
    }
  }

  $scope.changeFont = function(font) {
    $scope._site.pageStyles.font = font.split('/');
    GetSite.set({
      pageStyles: $scope._site.pageStyles
    }).then(function() {
      //$state.go($state.current, $state.params, {reload: true});
    });
  };

  $scope.updateSiteMetadata = function(attribute, value) {
    var data = {};
    data[attribute] = value;
    GetSite.set(data).then(function() {
    });
  };

  $scope.updateSearchSettings = function(attribute, value) {
    var data = {};
    data[attribute] = value;
    GetSite.set(data).then(function() {
      swal('Success!', 'Settings saved', 'success');
    }).catch(function(err) {
      swal('Oops!', 'There was an error saving the settings, please try again: ' + JSON.stringify(err.data), 'error');
    });
  };

  $scope.templates = {
    // home: [
    //   {name: 'Full Background', img: '/public/performance/pages/home/1.jpg', id: 1},
    //   {name: 'Carousel', img: '/public/performance/pages/home/2.jpg', id: 2}
    // ],
    about: [
      {name: 'Portrait 1', img: '/public/performance/pages/about/1.jpg', id: 1},
      {name: 'Portrait 2', img: '/public/performance/pages/about/2.jpg', id: 2},
      {name: 'Collage', img: '/public/performance/pages/about/3.jpg', id: 3},
      {name: 'Landscape 1', img: '/public/performance/pages/about/4.jpg', id: 4}
    ],
    contact: [
      {name: 'Portrait 1', img: '/public/performance/pages/contact/1.jpg', id: 1},
      {name: 'Portrait 2', img: '/public/performance/pages/contact/3.jpg', id: 3},
      {name: 'Landscape 1', img: '/public/performance/pages/contact/4.jpg', id: 4},
      {name: 'No Picture', img: '/public/performance/pages/contact/2.jpg', id: 2}
    ],
    news: [
      {name: 'Portrait', img: '/public/performance/pages/news/2.jpg', id: 2},
      {name: 'Landscape', img: '/public/performance/pages/news/1.jpg', id: 1}
    ],
    gallery: [
      {name: 'Album 1', img: '/public/performance/pages/gallery/1.jpg', id: 1},
      {name: 'Album 2', img: '/public/performance/pages/gallery/2.jpg', id: 2},
      {name: 'Carousel', img: '/public/performance/pages/gallery/3.jpg', id: 3},
      {name: 'Expanding', img: '/public/performance/pages/gallery/4.jpg', id: 4}
    ],
    resume: [
      {name: 'Portrait 1', img: '/public/performance/pages/resume/1.jpg', id: 1},
      {name: 'Portrait 2', img: '/public/performance/pages/resume/2.jpg', id: 2},
      {name: 'Resume Builder', img: '/public/performance/pages/resume/3.jpg', id: 3},
      {name: 'Resume Only', img: '/public/performance/pages/resume/5.jpg', id: 5}
    ],
    media: [
      {name: 'Media 1', img: '/public/performance/pages/media/1.jpg', id: 1},
      {name: 'Media 2', img: '/public/performance/pages/media/2.jpg', id: 2}
    ],
    custom: [
      {name: 'Custom Page 1', img: '/public/performance/pages/custom/1.jpg', id: 1},
      {name: 'Custom Page 2', img: '/public/performance/pages/custom/2.jpg', id: 2},
      {name: 'Custom Page 3', img: '/public/performance/pages/custom/3.jpg', id: 3},
      {name: 'Custom Page 4', img: '/public/performance/pages/custom/4.jpg', id: 4},
      {name: 'Custom Page 5', img: '/public/performance/pages/custom/5.jpg', id: 5},
      {name: 'Custom Page 6', img: '/public/performance/pages/custom/6.jpg', id: 6},
    ]
  };

  for (var tmpl in theme.templates) {
    $scope.templates[tmpl] = theme.templates[tmpl];
  }





  $scope.currState = $state;
	$scope.$watch('currState.current.name', function(newValue, oldValue) {
		$scope.stateName = newValue;
	});

  $scope.changeTheme = function(theme) {
    $scope._site.theme = theme.id;
    $http.post(setThemeUrl, {
      theme: theme.id
    }).then(function() {
      cssInjector.removeAll();
      cssPath = '/public/performance/themes/' + $scope._site.theme + '/theme.css';
      cssInjector.add(cssPath);
      $scope.$broadcast('app-start-loading');
      $window.location.reload();
    }).catch(function(err) {
      $scope.$broadcast('app-finish-loading');
      swal('Error!', 'Error saving the template: ' + err, 'error');
    });
  };

  $scope.changePageLayout = function(idx) {
    console.log('idx: ', idx);
    // if ($scope.stateName === 'customPage') {
    //   var title = $state.params.title;
    //   var tag = $state.params.tag;

    //   var component = $scope._site.components[tag][0];
    //   var newComponent = {
    //     site: component.site,
    //     component_specific_data: {
    //       layout: idx,
    //       position: component.component_specific_data.position
    //     },
    //     tag: tag,
    //   };

    //   $http.post('/api/components/update', newComponent).then(function(res) {
    //     console.log('res: ', res);
    //     $scope.$broadcast('app-start-loading');
    //     location.reload();
    //   }).catch(function(err) {
    //     $scope.$broadcast('app-finish-loading');
    //     swal('Error!', 'Error saving the template: ' + err.data.message, 'error');
    //   });
    // } else {
      var layout_category = $rootScope.currentPage.layout_category;
      var oldLayoutIndex = $rootScope.currentPage.layout;
      //$scope._site.layouts[layout_category] = idx;
      $http.put('/api/savepage/' + $rootScope.currentPage._id, {
				layout: idx
			}).then(function() {
        var oldcss = '/public/performance/pages/' + layout_category + '/' + oldLayoutIndex + '.css';
        var newcss = '/public/performance/pages/' + layout_category + '/' + idx + '.css';
        console.log('removing', oldcss);
        console.log('adding', newcss);
        cssInjector.remove(oldcss);
        cssInjector.add(newcss);
        $rootScope.currentPage.layout = idx;
        $state.reload();
      }).catch(function(err) {
        $scope.$broadcast('app-finish-loading');
        swal('Error!', 'Error saving the template: ' + err, 'error');
      });
    //}
  };

  $rootScope.preview = function() {
    $window.location.href = '/preview';
  };


  $scope.updateLayout = function(layoutId) {
    var interiorPage = $state.current.name;
    var opts = {};
    var oldId = $scope._site.layouts[interiorPage];
    $scope.$broadcast('app-start-loading');
    opts.layouts = $scope._site.layouts;
    $http.post(saveUrl, opts).then(function() {
      $scope.$broadcast('app-finish-loading');
      $scope._site.layouts[interiorPage] = parseInt(layoutId, 10);
      cssInjector.remove('/public/performance/pages/' + interiorPage + '/' + oldId + '.css');
      cssInjector.add('/public/performance/pages/' + interiorPage + '/' + layoutId + '.css');
      $state.reload();
    }, function(err) {
      $scope.$broadcast('app-finish-loading');
      swal('Error!', 'Changes not saved.', 'error');
    });
  };


  $scope.getOffset = function(ctxt) {
    if (ctxt === 'tip1') {
      $('.tour-tip').css('margin-left', $('body').width() / 2.25);
    }

    return null;
  };

  $scope.removeOffset = function() {
    $('.tour-tip').css('margin-left', 0);

    return null;
  };

  $scope.deleteItem = function() {
    var key = this.$editable.attrs.editableText || this.$editable.attrs.editableTextarea;
    var item = key.split('.').slice(0, -1).reduce(function(obj, i) {
      return obj[i];
    }, $scope);
    if (item) {
      item[key.split('.').slice(-1)] = '';
    } else {
      this.$data = '';
    }
  };

  $scope.toggleVisible = function(navItem) {
    navItem.hidden = !navItem.hidden;
    $http.post('/api/components/update-page', {_id: navItem._id, hidden: navItem.hidden}).then(function(response) {
      console.log('response', response);
    }).catch(function(err) {
      console.error(err);
      swal('Oops!', 'There was an error updating the visibility: ' + JSON.stringify(err), 'error');
    });

    //$scope.$broadcast('app-start-loading');
    //$window.location.reload();
  };


  $scope.chooseTheme = function(themeObj) {
    var themeId = themeObj._id;

    if ($scope._site && $scope._site.theme != themeId) {
      var originalThemeId = $scope._site.theme;
      $scope._site.theme = themeId;

      var copyExistingLayouts = angular.copy($scope._site.layouts);
      copyExistingLayouts.home = 1; //set home to default

      var updates = {
        theme: themeId,
        layouts: copyExistingLayouts
      };



      for (var layout in updates.layouts) {
        if (themeObj.forceLayout && themeObj.forceLayout[layout]) {
          updates.layouts[layout] = themeObj.forceLayout[layout];
        }
      }

      // Get current site styles
      if ($rootScope._site.colorPalette && $scope.palettes[$scope._site.theme][themeId]) {
        updates.pageStyles = $scope.palettes[$rootScope._site.colorPalette];

        $scope.updateWithQuery(updates);
      } else {
        $http.get('/public/performance/themes/' + originalThemeId + '/pagestyle.json').then(function(originalStylesRes) {
          var originalStyles = originalStylesRes.data;

          // compare site styles to default. if changed, replace in default styles of new theme
          var changedKeys = [];
          for (var i in $scope._site.pageStyles) {
            var pageStyle = $scope._site.pageStyles[i];
            if (pageStyle) {
              if (pageStyle !== originalStyles[i]) {
                changedKeys.push(i);
              }
            }
          }

          // get default styles for new theme
          $http.get('/public/performance/themes/' + themeId + '/pagestyle.json').then(function(styles) {
            var newStyles = styles.data;
            // create new style object with user changed values replacing the default style of same key.
            changedKeys.forEach(function(i) {
              newStyles[i] = $scope._site.pageStyles[i];
            });

            updates.pageStyles = newStyles;
            $scope.updateWithQuery(updates);
          });
        });
      }
    } else {
      swal('This is already your theme', '', 'error');
    }
  };


  var $$scope = $scope;


  $scope.weBuildItForyouPrompt = function() {
    var dialog = ngDialog.open({
      template: '/public/performance/partials/webuilditforyouprompt.html',
      appendClassName: 'newPageModal',
      controller: function($scope, $state) {
      }
    });

    dialog.closePromise.then(function (shouldWeBuildIt) {

      window.localStorage.setItem('builditforyoupopup', true);

      if (shouldWeBuildIt.value === true) {
        $$scope.weBuildItForyou();
      }
    });

  };

  if (window.localStorage && window.localStorage.getItem('builditforyoupopup') !== 'true') {
    setTimeout(function() {
      //$scope.weBuildItForyouPrompt(); removing
    }, 1000);
  }

  $scope.weBuildItForyou = function() {
    var dialog = ngDialog.open({
      template: '/public/performance/partials/webuilditforyou.html',
      appendClassName: 'newPageModalForm',
      controller: function($scope, $state, Upload) {
        $scope.buildItForYou = {
          urls: {},
          filenames: []
        };

        $scope.submit = function() {
          var body = {
            name: $scope.buildItForYou.name,
            email: $scope.buildItForYou.email,
            urls: $scope.buildItForYou.urls,
            comments: $scope.buildItForYou.comments
          };

          $http.post('/api/builditforyou', body).then(function(res) {
            ngDialog.closeAll();
            swal({
              title: 'Success!',
              text: 'We\'re looking forward to creating your site! You\'ll receive an email within 1 busy day about a time to schedule your consultation.  If you have any questions feel free to email ' + window.HELP_EMAIL + '!',
              type: 'success'
            });
          }).catch(function(err) {
            swal('Oops!', 'We had an issue using your information. Please contact us!', 'error');
          });
        };

        $scope.uploadFiles = function(files, type) {
          $scope.fileUploadError = null;

          var validFiles = files.filter(function (f) { return f.size <= 10000000; });
          if (validFiles.length !== files.length) $scope.fileUploadError = 'One or more files uploaded are too large!';
          console.log('FILE LENGTHS ', validFiles.length);

          if (!validFiles.length) {
            $scope.buildItForYou.photo = [];
            return;
          }

          validFiles.forEach(function(file) {
            var fileInfo = {
              name: file.name,
              percentage: 0
            };

            $scope.buildItForYou.filenames.push(fileInfo);
            Upload.upload({
              url: window.CLOUDINARY_UPLOAD_API_URL,
              data: {
                folder: window.location.host,
                upload_preset: 'lthduepg',
                file: file
              }
            }).then(function(resp) {
              console.log('Success ' + resp.config.data.file.name + 'uploaded');
              $scope.buildItForYou.urls[type] = resp.data.secure_url;
              fileInfo.percentage = 100;
            }, function(resp) {
              console.log('resp err', resp);
              console.log('Error status: ' + resp.status);
            }, function(evt) {
              var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
              fileInfo.percentage = progressPercentage;
            });
          });
        };


      }
    });
  };

  $scope.createCustomPageModal = function() {
    var dialog = ngDialog.open({
      template: '/public/performance/themes/partials/newPageModal.html',
      appendClassName: 'newPageModal',
      resolve: {
        customPages: function() {
          return $scope.customPages;
        }
      },
      controller: function($scope, $state, customPages) {
        $scope.pageNameLimit = 14;
        $scope.createNewPage = function() {
          if ($scope.newPageName.length > 0 && $scope.newPageName.length <= $scope.pageNameLimit) {
            var newPage = {
              nav_title: $scope.newPageName,
              layout: 1,
              layout_category: 'custom'
            };

            $http.post('/api/components/update-page', newPage).then(function(res) {
              location.reload();
            }).catch(function(err) {
              console.error(err);
            });
          } else {
            swal('Oops!', 'Page name must not be blank and must be longer than 8 characters', 'error');
          }
        };
      }
    });
  };

  $scope.removePage = function(navItem) {
    swal({
      title: 'Are You Sure?',
      text: 'Your page, <strong>' + navItem.nav_title + '</strong>, will be removed along with all of its pictures/text',
      html: true,
      type: 'warning',
      showCancelButton: true,
      closeOnConfirm: false
    }, function(isConfirm) {
      if (isConfirm) {
        $http.post('/api/components/remove-page', {_id: navItem._id}).then(function(res) {
          console.log('res: ', res);
          swal({
            title: 'Success!',
            text: 'Your page, ' + navItem.nav_title + ', was deleted!',
            type: 'success'
          }, function() {
            location.reload();
          });
        }).catch(function(err) {
          console.error(err);
          swal('Error', 'There was an error deleting your page: ' + err, 'error');
        });
      }
    });
  };

  $scope.updateWithQuery = function(q) {
    $http.post(saveUrl, q).then(function() {
      swal({
        title: 'Success!',
        text: 'Theme Updated',
        type: 'success'
      }, function() {
        location.reload();
      });

    }, function(err) {
      console.error(err);
      swal('Error!', 'Theme could not be saved.', 'error');
    });
  };

  // $scope.editMenu = null;
  $scope.colorPickerOptions = {
    swatchOnly: false
  };

  $scope.chooseEditMenu = function(menu) {
    console.log('menu', menu);
    $scope.editMenu = menu.replace(/\s/g, '_');
    console.log('menu: ', $scope.editMenu);
    var qs = {};
    qs[menu] = true;
    $state.go($state.current, qs);
  };

  $scope.editMenuGoBack = function() {
    $scope.editMenu = null;
    $state.go($state.current, {
      pages: null,
      theme: null,
      layout: null,
      colors: null,
      fonts: null,
      showCustomColors: null
    });
  };

  $scope.showCustomColors = $scope.locationQuery.colors && $scope.locationQuery.showCustomColors ? true : false;

  $scope.showHideCustomizeColors = function() {
    $scope.showCustomColors = !$scope.showCustomColors;
    var q = {
      colors: true
    };
    if ($scope.showCustomColors) q.showCustomColors = true;


    $timeout(function() {
      var d = $('#editPanel');
      d.scrollTop(d.prop("scrollHeight"));
    }, 100);
    // $state.go($state.current, q);
  };

  $scope.showEditMenu = false;

  $scope.toggleShowEditMenu = function() {
    $scope.showEditMenu = !$scope.showEditMenu;
  };

  $scope.editPage = function(itemId, newPageName) {
    console.log('item: ', itemId, newPageName);
    $http.post('/api/components/update-page', {_id: itemId, nav_title: newPageName}).then(function(response) {
      console.log('response', response);
      location.reload();
    }).catch(function(err) {
      console.error(err);
      swal('Oops!', 'There was an error updating the page name: ' + JSON.stringify(err), 'error');
    });

  };

  $(window).keydown(function(e) {
    if ((e.ctrlKey || e.metaKey) && e.which === 13) {
      $scope.save();
    }
  });
}]).filter('convertKeyToTitle', function() {
  return function(val) {
    var result = val.replace('_', ' ');
    var optionalVals = ['height', 'weight'];
    if (val === 'union') result += ' e.g. AEA/SAG-AFTRA, SAG Eligible, E.M.C.';
    if (val === 'range') result = 'vocal ' + result;
    if (optionalVals.indexOf(val) > -1) result += ' (optional)';
    return result;
  };
}).filter('telephone', function() {
  return function(input) {
    if (input && input.length) {
      var arr = input.split('');
      arr.splice(0, 0, '(');
      arr.splice(4, 0, ')');
      arr.splice(5, 0, ' ');
      arr.splice(9, 0, '-');
      return arr.join('');
    } else {
      return input;
    }
  };
});


function capitalize(string) {
  var output = string.split(' ');
  output.forEach(function(i, idx) {
    output[idx] = (i.charAt(0).toUpperCase() + i.substring(1));
  });

  return output.join(' ');
}
