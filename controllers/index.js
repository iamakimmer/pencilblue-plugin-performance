/*
Copyright (C) 2014  PencilBlue, LLC

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
* Index page of the Jennifer theme
*/

//dependencies
var _                 = require('lodash');
var async             = require('async');
var mongoose          = require("mongoose");
const request         = require('request');
const soundcloudApi   = 'http://soundcloud.com/oembed';
const vimeoApi        = 'https://vimeo.com/api/v2/video/';
var SiteData          = require('../mongoose/site_data').SiteData,
  Image               = require('../mongoose/image').Image,
  Component           = require('../mongoose/component').Component,
  Theme               = require('../mongoose/theme');

var SiteDataLive      = require('../mongoose/site_data').SiteDataLive,
  ImageLive           = require('../mongoose/image').ImageLive,
	ComponentLive       = require('../mongoose/component').ComponentLive,
	SiteCache       		= require('../mongoose/site_cache').SiteCache,
	Page 								= require('../mongoose/page').Page,
	PageLive 						= require('../mongoose/page').PageLive;

module.exports = function IndexModule(pb) {

  function Index() {}

  Index.prototype.init = function(props, cb) {
    var self = this;
    pb.BaseController.prototype.init.call(self, props, function() {
      var queryService = new pb.SiteQueryService({ site: self.site });
      self.siteQueryService = queryService;
      cb();
    });
  };


	var util = pb.util;
	var redisClient = pb.redisClient;
  // var TopMenu = pb.TopMenuService;
  var Comments = pb.CommentService;
  var ArticleService = pb.ArticleService;

  //inheritance
  util.inherits(Index, pb.BaseController);

  Index.prototype.onArticle = function(cb) {
    var self = this;
    var dao = new pb.DAO();

    try {
      pb.DAO.getObjectId(this.pathVars.customUrl);
      dao.loadById(self.pathVars.customUrl, 'article', function(err, article) {
        if (util.isError(err) || article == null) {
          throw (err);
        }

        self.article = article._id.toString();
        self.renderPage(cb);
      });
    } catch (e) {
      dao.loadByValues({ url: self.pathVars.customUrl }, 'article', function(err, article) {
        if (util.isError(err) || article == null) {
          self.reqHandler.serve404();
          return;
        }

        self.article = article._id.toString();
        self.renderPage(cb);
      });
      return;
    }
  };

  Index.prototype.onPage = function(cb) {
    var self = this;
    var dao = new pb.DAO();

    try {
      pb.DAO.getObjectId(this.pathVars.customUrl);
      dao.loadById(self.pathVars.customUrl, 'page', function(err, page) {
        if (util.isError(err) || page == null) {
          throw (err);
        }

        self.page = page._id.toString();
        self.renderPage(cb);
      });
    } catch (e) {
      dao = new pb.DAO();
      dao.loadByValues({ url: self.pathVars.customUrl }, 'page', function(err, page) {
        if (util.isError(err) || page == null) {
          self.reqHandler.serve404();
          return;
        }

        self.page = page._id.toString();
        self.renderPage(cb);
      });
      return;
    }
  };

  Index.prototype.onSection = function(cb) {
    var self = this;
    var dao = new pb.DAO();

    try {
      pb.DAO.getObjectId(this.pathVars.customUrl);
      dao.loadById(self.pathVars.customUrl, 'section', function(err, section) {
        if (util.isError(err) || section == null) {
          throw (err);
        }

        self.section = section._id.toString();
        self.renderPage(cb);
      });
    } catch (e) {
      dao.loadByValues({ url: self.pathVars.customUrl }, 'section', function(err, section) {
        if (util.isError(err) || section == null) {
          self.reqHandler.serve404();
          return;
        }

        self.section = section._id.toString();
        self.renderPage(cb);
      });
      return;
    }
  };

  Index.prototype.onTopic = function(cb) {
    var self = this;
    var dao = new pb.DAO();

    try {
      pb.DAO.getObjectId(this.pathVars.customUrl);
      dao.loadById(self.pathVars.customUrl, 'topic', function(err, topic) {
        if (util.isError(err) || topic == null) {
          throw (err);
        }

        self.topic = topic._id.toString();
        self.renderPage(cb);
      });
    } catch (e) {
      dao.loadByValues({ name: self.pathVars.customUrl }, 'topic', function(err, topic) {
        if (util.isError(err) || topic == null) {
          cb({ error: err });
        }

        self.topic = topic._id.toString();
        self.renderPage(cb);
      });
      return;
    }
  };

  Index.prototype.renderPage = function(cb) {
    var self = this;
    var user = self.session.authentication.user;
    var isPreview = self.req.url == '/preview';
    var isAuthenticated = self.session.authentication.admin_level === pb.SecurityService.ACCESS_ADMINISTRATOR || self.session.authentication.admin_level === pb.SecurityService.ACCESS_EDITOR;
    var mode = isPreview ? 'edit':'live';


		redisClient.get((self.req.headers.chost || self.req.headers.host) + self.req.url, function(err, data) {

			self.getSite(mode , function(err, siteData) {        
				if (err) return cb({error: err});
				if (self.shouldRenderSite(siteData.site, user)) {
					//determine and execute the proper call
					var page = self.page || null;

					var pluginService = new pb.PluginService({ site: self.site });

					pluginService.getSettings('performance', function(err, pluginSettings) {
						if (util.isError(err)) {
							self.reqHandler.serveError(err);
						}
						var pluginSettingsToObj = {};

						for (var i = 0; i < pluginSettings.length; i++) {
							self.ts.registerLocal('performance_plugin_' + pluginSettings[i].name, pluginSettings[i].value || '');
							pluginSettingsToObj[pluginSettings[i].name] = pluginSettings[i].value;
						}

            if (siteData.site.favicon) {
              self.ts.registerLocal('site_icon', siteData.site.favicon);
            }

            if (self.siteObj.custom_favicon) {
              self.ts.registerLocal('site_icon', self.siteObj.custom_favicon);
            }

            const currentHost = self.req.headers.chost || self.req.headers.host;
            const currentUrl = self.req.url;
            
						self.ts.registerLocal('current_host', currentHost);
            self.ts.registerLocal('current_url', currentUrl);
            
            let ogUrl = `https://${currentHost}${currentUrl}`;

            if (self.siteObj.custom_domain) {
              ogUrl = `https://www.${self.siteObj.custom_domain}${currentUrl}`;
            }
            self.ts.registerLocal('og_url', ogUrl);

						self.ts.registerLocal('page_name', function(flag, cb) {
							var content = data && data.content.length > 0 ? data.content[0] : null;
							self.getContentSpecificPageName(content, cb);
						});

						self.ts.registerLocal('htmlmode', isPreview ? false : true);

						self.ts.registerLocal('base_href', '/');
						self.ts.registerLocal('help_email', pb.config.help_email);
            self.ts.registerLocal('site_uid', self.site);
            self.ts.registerLocal('page_title', siteData.site.search_title || siteData.site.title);

            self.ts.registerLocal('google_site_verification', self.siteObj.google_site_verification);

            if (siteData.site.description) {
              self.ts.registerLocal('performance_plugin_description', siteData.site.description);
            }

            self.ts.registerLocal('no_index_name', process.env.NODE_ENV === 'production' ? '' : 'robots');
            self.ts.registerLocal('no_index_content', process.env.NODE_ENV === 'production' ? '' : 'noindex');
						self.ts.registerLocal('angular', function(flag, cb) {
              self.ts.registerLocal('preview', isPreview ? true : false);

							var objects = {
								siteData: siteData.site,
								siteImages: siteData.images,
								components: siteData.components,
								pages: siteData.pages,
								isPage: page !== null,
								isPreview: isPreview,
								isAuthenticated: isAuthenticated,
								mode: mode
							};
							var angularData = pb.ClientJs.getAngularController(objects, ['ngSanitize', 'ui.router', 'angular.css.injector', 'ngDialog', 'ui.bootstrap', 'ngFileUpload', 'ngAnimate', 'ngFitText']);
							cb(null, angularData);
						});

						self.ts.load('index', function(err, result) {
							if (util.isError(err)) {
								throw err;
							}
							cb({ content: result });

							if (!isPreview) {
								var siteId = self.siteObj.uid;
								let key = self.req.headers.chost || self.req.headers.host + self.req.url;
								SiteCache.update({_id: siteId}, { $addToSet: { keys: key}}, {upsert: true}, function(err, cache) {
								});
								redisClient.set(key, result);
							}

						});
					});
				} else {
					self.renderInactivePage(cb);
				}
			});
		});



  };

  Index.prototype.renderInactivePage = function(cb) {
    this.ts.registerLocal('context', 'unpublished');
    this.ts.registerLocal('green-room', `${pb.config.wordpressSite}/member-login`);
    this.ts.load('inactive-site', function(err, result) {
      if (util.isError(err)) {
        throw err;
      }
      cb({content: result});
    });
  };

  Index.prototype.shouldRenderSite = function(siteData, user) {
    var self = this;

    if (siteData) {

      if (siteData.trial || siteData.unpublished) {

        if (self.session.authentication.user) {
          return true;
        } else {
          return false;
        }
      } else {
        return true;
      }
    } else {
      return false;
    }

  };

  Index.prototype.renderEditPage = function(cb) {
    var self = this;
    var user = self.session.authentication.user;
    var page = self.page || null;


		self.ts.registerLocal('meta_lang', pb.config.localization.defaultLocale);
		self.ts.registerLocal('current_host', self.req.headers.chost || self.req.headers.host);
		self.ts.registerLocal('current_url', self.req.url);

		self.ts.registerLocal('logged_in_username', self.session.authentication.user.username);
		self.ts.registerLocal('logged_in_email',self.session.authentication.user.email);

		self.ts.registerLocal('page_name', function(flag, cb) {
			var content = data.content.length > 0 ? data.content[0] : null;
			self.getContentSpecificPageName(content, cb);
		});
		self.ts.registerLocal('siteUser', user ? user.site === self.siteObj.uid || user.site === 'global' : false);
    self.ts.registerLocal('help_email', pb.config.help_email);
    self.ts.registerLocal('wwwSite', pb.config.wwwSite);
    self.ts.registerLocal('wordpressSite', pb.config.wordpressSite);
		self.ts.registerLocal('site_designer_help_email', pb.config.site_designer_help_email);
		self.ts.registerLocal('CLOUDINARY_UPLOAD_API_URL', pb.config.cloudinary.CLOUDINARY_UPLOAD_API_URL);



		// check for siteDataLive/siteDataLive.unpublished
		var siteId = self.siteObj.uid;
		SiteDataLive.findById(siteId).select('unpublished').exec(function(err, site) {
			if (err) return cb({error: err, code: 500});

			var isUnpublished = false;

			if (!site) {
				isUnpublished = true;
			} else if (site.unpublished) {
				isUnpublished = site.unpublished;
      }

      self.ts.registerLocal('no_index_name', process.env.NODE_ENV === 'production' ? '' : 'robots');
      self.ts.registerLocal('no_index_content', process.env.NODE_ENV === 'production' ? '' : 'noindex');

			self.ts.registerLocal('angular', function(flag, cb) {
				self.getSite('edit', function(err, siteData) {
          console.log('edit siteData', siteData);
					Theme.find({}, function(err, themes) {
						var sortedThemes = themes.sort(function(a, b) {
							return a.sort - b.sort;
						});
						var objects = {
							siteData: siteData.site,
							siteImages: siteData.images,
							components: siteData.components,
							pages: siteData.pages,
							isPage: page !== null,
							themes: sortedThemes,
							activeTheme: _.find(_.sortBy(themes, [(t) => { return t.sort; }]), { '_id': siteData.site.theme }),
							isUnpublished: isUnpublished,
              helpText:  pb.config.helpText,
              wwwSite: pb.config.wwwSite
						};
						var angularData = pb.ClientJs.getAngularController(objects, ['ngSanitize', 'ui.router', 'xeditable', 'ui.bootstrap', 'ngDialog', 'colorpicker.module','angular.css.injector', 'textAngular', 'ngFileUpload', 'ngAnimate', 'ui.sortable', 'ngFitText']);
						cb(null, angularData);
					});
				});
			});



			self.ts.load('index_edit', function(err, result) {
				if (util.isError(err)) {
					throw err;
				}

				cb({ content: result });
			});
		});

  };

  Index.prototype.getSite = function(mode, cb) {
    var siteId = this.siteObj.uid;

    if (mode == 'live') {
      SiteDataLive.findById(siteId).exec(function(err, site) {
        if (err) return cb(err);
        ImageLive.find({ site: siteId, active: true }, function(err, images) {
          ComponentLive.find({ site: siteId, active: {$ne: false} }).populate('images').exec(function(err, components) {
						PageLive.find({site: siteId}).sort({nav_order: 1}).exec(function(err, pages) {
							cb(err, { site: site, images: images, components: components, pages: pages });
						});
          });
        });
      });
    } else {
      SiteData.findById(siteId).exec(function(err, site) {
        if (err) return cb(err);
        Image.find({ site: siteId, active: true }, function(err, images) {
          Component.find({ site: siteId, active: {$ne: false} }).populate('images').exec(function(err, components) {
						Page.find({site: siteId, $or: [{deleted_at: null}, {deleted_at: {$exists: false}}]}).sort({nav_order: 1}).exec(function(err, pages) {
							cb(err, { site: site, images: images, components: components, pages: pages });
						});
          });
        });
      });

    }
  };


  Index.prototype.gatherData = function(cb) {
    var self = this;
    var tasks = {
      //navigation
      nav: function(callback) {
        self.getNavigation(function(themeSettings, navigation, accountButtons) {
          callback(null, { themeSettings: themeSettings, navigation: navigation, accountButtons: accountButtons });
        });
      },

      //articles, pages, etc.
      content: function(callback) {
        self.loadContent(callback);
      },

      topics: function(callback) {
        var dao = new pb.DAO();
        dao.q('topic', callback);
      }
    };
    async.parallel(tasks, cb);
  };

  Index.prototype.loadContent = function(articleCallback) {
    var section = this.section || null;
    var topic = this.topic || null;
    var article = this.article || null;
    var page = this.page || null;

    var service = new ArticleService();
    if (this.req.pencilblue_preview) {
      if (this.req.pencilblue_preview == page || article) {
        if (page) {
          service.setContentType('page');
        }
        var where = pb.DAO.getIDWhere(page || article);
        where.draft = { $exists: true };
        where.publish_date = { $exists: true };
        service.find(where, articleCallback);
      } else {
        service.find({}, articleCallback);
      }
    } else if (section) {
      service.findBySection(section, articleCallback);
    } else if (topic) {
      service.findByTopic(topic, articleCallback);
    } else if (article) {
      service.findById(article, articleCallback);
    } else if (page) {
      service.setContentType('page');
      service.findById(page, articleCallback);
    } else {
      service.find({}, articleCallback);
    }
  };

  Index.prototype.getNavigation = function(cb) {
    var options = {
      currUrl: this.req.url,
      site: this.site,
      session: this.session,
      ls: this.ls,
      activeTheme: this.activeTheme
    };

    var menuService = new pb.TopMenuService();
    menuService.getNavItems(options, function(err, navItems) {
      if (util.isError(err)) {
        pb.log.error('Index: %s', err.stack);
      }
      cb(navItems.themeSettings, navItems.navigation, navItems.accountButtons);
    });
  };

  Index.prototype.renderContent = function(content, contentSettings, themeSettings, index, cb) {
    var self = this;

    var isPage = content.object_type === 'page';
    var showByLine = contentSettings.display_bylines && !isPage;
    var showTimestamp = contentSettings.display_timestamp && !isPage;
    var ats = new pb.TemplateService(this.ls);
    var contentUrlPrefix = isPage ? '/page/' : '/article/';
    self.ts.reprocess = false;
    ats.registerLocal('article_permalink', pb.UrlService.urlJoin(pb.config.siteRoot, contentUrlPrefix, content.url));
    ats.registerLocal('article_headline', new pb.TemplateValue('<a href="' + pb.UrlService.urlJoin(contentUrlPrefix, content.url) + '">' + content.headline + '</a>', false));
    ats.registerLocal('article_headline_nolink', content.headline);
    ats.registerLocal('article_subheading', content.subheading ? content.subheading : '');
    ats.registerLocal('article_subheading_display', content.subheading ? '' : 'display:none;');
    ats.registerLocal('article_id', content._id.toString());
    ats.registerLocal('article_index', index);
    ats.registerLocal('article_timestamp', showTimestamp && content.timestamp ? content.timestamp : '');
    ats.registerLocal('article_timestamp_display', showTimestamp ? '' : 'display:none;');
    ats.registerLocal('article_layout', new pb.TemplateValue(content.layout, false));
    ats.registerLocal('article_url', content.url);
    ats.registerLocal('display_byline', showByLine ? '' : 'display:none;');
    ats.registerLocal('author_photo', content.author_photo ? content.author_photo : '');
    ats.registerLocal('author_photo_display', content.author_photo ? '' : 'display:none;');
    ats.registerLocal('author_name', content.author_name ? content.author_name : '');
    ats.registerLocal('author_position', content.author_position ? content.author_position : '');
    ats.registerLocal('media_body_style', content.media_body_style ? content.media_body_style : '');
    ats.registerLocal('comments', function(flag, cb) {
      if (isPage || !contentSettings.allow_comments) {
        cb(null, '');
        return;
      }

      self.renderComments(content, ats, function(err, comments) {
        cb(err, new pb.TemplateValue(comments, false));
      });
    });
    ats.load('elements/article', cb);
  };

  Index.prototype.renderComments = function(content, ts, cb) {
    var self = this;
    var commentingUser = null;
    if (pb.security.isAuthenticated(this.session)) {
      commentingUser = Comments.getCommentingUser(this.session.authentication.user);
    }

    ts.registerLocal('user_photo', function(flag, cb) {
      if (commentingUser) {
        cb(null, commentingUser.photo ? commentingUser.photo : '');
      } else {
        cb(null, '');
      }
    });
    ts.registerLocal('user_position', function(flag, cb) {
      if (commentingUser && util.isArray(commentingUser.position) && commentingUser.position.length > 0) {
        cb(null, ', ' + commentingUser.position);
      } else {
        cb(null, '');
      }
    });
    ts.registerLocal('user_name', commentingUser ? commentingUser.name : '');
    ts.registerLocal('display_submit', commentingUser ? 'block' : 'none');
    ts.registerLocal('display_login', commentingUser ? 'none' : 'block');
    ts.registerLocal('comments_length', util.isArray(content.comments) ? content.comments.length : 0);
    ts.registerLocal('individual_comments', function(flag, cb) {
      if (!util.isArray(content.comments) || content.comments.length == 0) {
        cb(null, '');
        return;
      }

      var tasks = util.getTasks(content.comments, function(comments, i) {
        return function(callback) {
          self.renderComment(comments[i], callback);
        };
      });
      async.parallel(tasks, function(err, results) {
        cb(err, new pb.TemplateValue(results.join(''), false));
      });
    });
    ts.load('elements/comments', cb);
  };

  Index.prototype.getHeroImage = function(individualItem, content, cb) {
    var defaultHero = '/public/jennifer/img/hero.jpg';

    if (!content) {
      cb(null, defaultHero);
      return;
    }

    var dao = new pb.DAO();

    var mediaId = '';
    if (content.article_media) {
      mediaId = content.article_media[0];
    } else {
      mediaId = content.page_media[0];
    }

    if (typeof mediaId === 'undefined') {
      cb(null, defaultHero);
      return;
    }

    dao.loadById(mediaId, 'media', function(err, media) {
      if (util.isError(err)) {
        cb(err, media);
        return;
      }

      cb(null, media.location);
    });
  };

  Index.prototype.getContentSpecificPageName = function(content, cb) {
    if (!content) {
      cb(null, pb.config.siteName);
      return;
    }

    if (this.req.pencilblue_article || this.req.pencilblue_page) {
      cb(null, content.headline + ' | ' + pb.config.siteName);
    } else if (this.req.pencilblue_section || this.req.pencilblue_topic) {

      var objType = this.req.pencilblue_section ? 'section' : 'topic';
      var dao = new pb.DAO();
      dao.loadById(this.req.pencilblue_section, objType, function(err, obj) {
        if (util.isError(err) || obj === null) {
          cb(null, pb.config.siteName);
          return;
        }

        cb(null, obj.name + ' | ' + pb.config.siteName);
      });
    } else {
      cb(null, pb.config.siteName);
    }
  };

  Index.prototype.editSite = function() {
    var self = this;
    if (!pb.security.isAuthorized(self.session, { admin_level: 2 })) {
      return self.redirect('/user/login');
      // return cb({
      //     code: 400,
      //     content: pb.BaseController.apiResponse(pb.BaseController.API_ERROR, self.ls.g('generic.INSUFFICIENT_CREDENTIALS'))
      // });
    }
  };

  Index.prototype.getSoundCloudiFrame = function(cb) {
    const self = this;

    this.getJSONPostParams(function(error, post) {
      if (error) return cb({error, code: 500});

      request.post(soundcloudApi, {
        form: {
          format: 'json',
          url: post.url
        }
      }, function(error, repsonse, body) {
        if (error) return cb({error, code: 500});

        return cb({content: JSON.parse(body)});
      });
    });
  };

  Index.prototype.getVimeoThumbnail = function(cb) {
    const self = this;

    this.getJSONPostParams((error, post) => {
      if (error) return cb({error, code: 500});

      const vimeoUrl = `${vimeoApi}${post.videoId}.json`;
      console.log(vimeoUrl);

      request.get(vimeoUrl, function(error, response, body) {
        if (error) return cb({error, code: 500});

        if (response.statusCode >= 200 && response.statusCode < 300) {
          body = JSON.parse(body);
          return cb({content: body});
        } else if (response.statusCode === 404) {
          return cb({error: new Error('Video Not Found. Is your video public?'), code: 404});
        } else {
          return cb({error: new Error('Could not access video. Please check URL'), code: 401});
        }
      });
    });
  };

  Index.prototype.reloadSiteData = function(cb) {
    this.getSite('edit', (error, site) => {
      if (error) return cb({error, code: 500});

      return cb({content: site});
    });
  };

  /**
  * Provides the routes that are to be handled by an instance of this prototype.
  * The route provides a definition of path, permissions, authentication, and
  * expected content type.
  * Method is optional
  * Path is required
  * Permissions are optional
  * Access levels are optional
  * Content type is optional
  *
  * @param cb A callback of the form: cb(error, array of objects)
  */
  Index.getRoutes = function(cb) {
    var routes = [{
      method: 'get',
      path: '/',
      auth_required: false,
      handler: 'renderPage',
      content_type: 'text/html'
		},{
      method: 'get',
      path: '/index',
      auth_required: false,
      handler: 'renderPage',
      content_type: 'text/html'
		},{
      method: 'get',
      path: '/about',
      auth_required: false,
      handler: 'renderPage',
      content_type: 'text/html'
    },{
      method: 'get',
      path: '/resume',
      auth_required: false,
      handler: 'renderPage',
      content_type: 'text/html'
    },{
      method: 'get',
      path: '/news',
      auth_required: false,
      handler: 'renderPage',
      content_type: 'text/html'
    },{
      method: 'get',
      path: '/gallery',
      auth_required: false,
      handler: 'renderPage',
      content_type: 'text/html'
    },{
      method: 'get',
      path: '/media',
      auth_required: false,
      handler: 'renderPage',
      content_type: 'text/html'
    },{
      method: 'get',
      path: '/contact',
      auth_required: false,
      handler: 'renderPage',
      content_type: 'text/html'
    },
		{
      method: 'get',
      path: '/preview',
      auth_required: true,
      inactive_site_access: true,
      handler: 'renderPage',
      access_level: pb.SecurityService.ACCESS_EDITOR,
      content_type: 'text/html'
    }, {
      method: 'get',
      path: '/edit',
      auth_required: true,
      inactive_site_access: true,
      handler: 'renderEditPage',
      access_level: pb.SecurityService.ACCESS_EDITOR,
      content_type: 'text/html'
    }, {
      method: 'get',
      path: '/article/:customUrl',
      auth_required: false,
      handler: 'onArticle',
      content_type: 'text/html'
    }, {
      method: 'get',
      path: '/section/:customUrl',
      auth_required: false,
      handler: 'onSection',
      content_type: 'text/html'
    }, {
      method: 'get',
      path: '/topic/:customUrl',
      auth_required: false,
      handler: 'onTopic',
      content_type: 'text/html'
    }, {
      method: 'post',
      path: '/soundcloud',
      auth_required: false,
      handler: 'getSoundCloudiFrame',
      content_type: 'application/json'
    }, {
      method: 'post',
      path: '/vimeoThumbnail',
      auth_required: false,
      handler: 'getVimeoThumbnail',
      content_type: 'application/json'
    }, {
      method: 'get',
      path: '/reloadSiteData',
      auth_required: true,
      access_level: pb.SecurityService.ACCESS_ADMINISTRATOR,
      handler: 'reloadSiteData',
      content_type: 'text/html'
    },
    {
      method: 'get',
      path: '/:customPage',
      auth_required: false,
      handler: 'renderPage',
      content_type: 'text/html'
    }];
    cb(null, routes);
  };

  //exports
  return Index;
};
