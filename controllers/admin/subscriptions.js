module.exports = function AdminSubscriptionsControllerModule(pb) {
  var util = pb.util;
  var async = require('async');
  var _ = require('lodash');
  var request = require('request');
  var awsApiKey = pb.config.awsApiKey;

  function AdminSubscriptionsController() {}

  util.inherits(AdminSubscriptionsController, pb.BaseAdminController);

  var SUB_NAV_KEY = 'signup';


  //render the howto page
  AdminSubscriptionsController.prototype.howto = function(cb) {

    var self = this;

    var angularObjects = pb.ClientJs.getAngularObjects({
      navigation: pb.AdminNavigation.get(self.session, [], self.ls, self.site),
      pills: self.getAdminPills(SUB_NAV_KEY, self.ls, null)
    }, []);

    self.ts.registerLocal('angular_objects', new pb.TemplateValue(angularObjects, false));
    self.ts.load('/admin/howto', function(err, result) {
      cb({ content: result });
    });
  };




  AdminSubscriptionsController.prototype.validateCustomDomain = function(cb) {
    this.getJSONPostParams(function(err, post) {
      request.post({
        url: 'https://lqiq3tpkbh.execute-api.us-east-1.amazonaws.com/prod/check-domain',
        headers: {
          'Content-Type': 'application/json',
          'x-api-key': awsApiKey
        },
        body: JSON.stringify(post)
      }, function(err, response) {
        if (err) return cb({ error: err });

        if (response.statusCode >= 200 && response.statusCode < 300) {
          return cb({ content: response.body });
        } else {
          return cb({
            code: response.statusCode,
            content: pb.BaseController.apiResponse(pb.BaseController.API_ERROR, 'Error checking domain name')
          });
        }
      });
    });
  };



  AdminSubscriptionsController.getRoutes = function(cb) {
    var routes = [{
      path: '/admin/howto',
      method: 'get',
      auth_required: true,
      handler: 'howto',
      access_level: pb.SecurityService.ACCESS_EDITOR,
      content_type: 'text/html'
    }, {
      path: '/admin/validateCustomDomain',
      method: 'post',
      handler: 'validateCustomDomain',
      auth_required: false,
      access_level: pb.SecurityService.ACCESS_EDITOR,
      content_type: 'text/html'
    }];
    cb(null, routes);
  };

  return AdminSubscriptionsController;
};
