//the modal directive to upload and image and crop
//the main navigation
angular.module('pencilblueApp')
  .directive('goto', ['$window', 'ComponentFactory', 'SiteData', '$uibModal', function($window, ComponentFactory, SiteData, $uibModal) {
    return {
      scope: {
        ngModel: '=',
        tag: '@'
      },
      restrict: 'A',
      replace: true,
      link: function(scope, elem, attrs) {
        var siteData = SiteData.getSiteData();        
        if (scope.ngModel && scope.ngModel.text) {
          elem.text(scope.ngModel.text);
        }        
        if (siteData.editing) {
          elem.on('click', function(e) {
            var modalInstance = $uibModal.open({
              animation: false,
              backdrop: 'static',
              ariaLabelledBy: 'modal-title',
              ariaDescribedBy: 'modal-body',
              templateUrl: '/public/performance/partials/goto.html',
              controllerAs: '$ctrl',
              controller: function($uibModalInstance, text, url) {
                var $ctrl = this;
                $ctrl.text = text;
                $ctrl.url = url;
                $ctrl.siteData = siteData;
                $ctrl.ok = function() {                
                  ComponentFactory.update({
                    text: $ctrl.text,
                    url: $ctrl.url,
                    tag: scope.tag
                  }).then(function(data) {
                    $uibModalInstance.close(data.data);                  
                  }).catch(function(err) {
                    if (err.data && err.data.message) {
                      alert(err.data.message);
                    } else {
                      alert('System Error');
                    }
                  });                
                };

                $ctrl.cancel = function() {
                  $uibModalInstance.dismiss('cancel');
                };
              },
              resolve: {
                text: function() {
                  return scope.ngModel ? scope.ngModel.text : '';
                },
                url: function() {
                  return scope.ngModel ? scope.ngModel.url : '';
                }                
              }
            });

            modalInstance.result.then(function(component) {
              scope.ngModel = component;
              if (component.text) {
                elem.text(component.text);
              }
              if (component.url) {
                //elem.text(component.text);
              }                
            }, function() {
              console.log('Modal dismissed at: ' + new Date());
            });

         
          });          
        } else {
          if (!scope.ngModel) {
            elem.remove();
          } else {
            if (scope.ngModel.url) {
              elem.attr('href', scope.ngModel.url);
            }            
          }
        }

      }
    };
  }]);
