'use strict';

pencilblueApp.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', 'cssInjectorProvider', '$httpProvider', '$provide', function($stateProvider, $urlRouterProvider, $locationProvider, cssInjectorProvider, $httpProvider, $provide) {

  $provide.decorator('$exceptionHandler', ['$delegate', function($delegate) {
    return function(exception, cause) {
      Raygun.send(exception, {
        cause: cause
      });
      $delegate(exception, cause);
    };
  }]);

  // $httpProvider.interceptors.push(['$q', function($q) {
  //   return {
  //     'requestError': function(rejection) {
  //       Raygun.send(new Error('Failed $http request'), rejection);
  //       return $q.reject(rejection);
  //     },
  //     'responseError': function(rejection) {
  //       Raygun.send(new Error('Failed $http response'), rejection);
  //       return $q.reject(rejection);
  //     }
  //   };
  // }]);


  //cssInjectorProvider.setSinglePageMode(true);

  $urlRouterProvider.otherwise('/');

  var editPanelQuery = {
    pages: null,
    theme: null,
    layout: null,
    colors: null,
    fonts: null
  };

  var params = '?pages&theme&layout&colors&fonts&showCustomColors';

/*
  var newsState = {
    name: 'news',
    url: '/news' + params,
    component: 'page',
    params: editPanelQuery,
    reloadOnSearch: false
  };

  var mediaState = {
    name: 'media',
    url: '/media' + params,
    component: 'media',
    params: editPanelQuery,
    reloadOnSearch: false
  };

  var resumeState = {
    name: 'resume',
    url: '/resume',
    component: 'page',
    params: editPanelQuery,
    reloadOnSearch: true
  };

  var contactState = {
    name: 'contact',
    url: '/contact' + params,
    component: 'page',
    params: editPanelQuery,
    reloadOnSearch: false
  };

  var aboutState = {
    name: 'about',
    url: '/about' + params,
    component: 'page',
    params: editPanelQuery,
  };

  var galleryState = {
    name: 'gallery',
    url: '/gallery' + params + '&section',
    component: 'gallery',
    params: editPanelQuery,
    resolve: {
      gallery: function(GetGallery) {
        return GetGallery.get();
      }
    },
    reloadOnSearch: false
  };
  */

  var pageState = {
    name: 'page',
    url: '/:slug',
    component: 'page',
    resolve: {
      pagedata: function($transition$, SiteData, $http) {
        
        var slug = $transition$.params().slug;
        var url = '/api/components/page/' + slug;
        if (!SiteData.getSiteData().editing && window.isLive) {
          url += '/live'
        }

        return $http.get(url).then(function(resp) {
          var data = resp.data;
          return {
            page: data.page,
            images: data.images,
            components: data.components
          };
        });
      },
      editing: function(SiteData) {
        return SiteData.getSiteData().editing;
      }
    },
    reloadOnSearch: false
  };

  var homeState = {
    name: 'home',
    url: '/' + params + '&modal',
    component: 'home',
    params: editPanelQuery,
    reloadOnSearch: false
  };


  var indexState = {
    name: 'index',
    url: '/index',
    component: 'home',
    params: editPanelQuery,
    reloadOnSearch: false
  };

  $stateProvider.state(homeState);
  $stateProvider.state(pageState);
  $stateProvider.state(indexState);

}]).run(['$rootScope', '$state', '$stateParams', '$transitions', function($rootScope, $state, $stateParams, $transitions) {
  $rootScope.$state = $state;
  $rootScope.$stateParams = $stateParams;

  $transitions.onSuccess({}, function(transition) {
    try {
      if (window.analytics) {
        var slug = transition.params().slug;      
        window.analytics.sendPageView(slug);  
      }
    } catch (e) {
      console.error(e);
    }    
  });
}]);


pencilblueApp.controller('SiteController', ['$scope', '$rootScope', '$http', '$location', '$state', '$interval', '$sce', 'cssInjector', 'SiteData', 'colorPalettes', 'NavStyling',  function($scope, $rootScope, $http, $location, $state, $interval, $sce, cssInjector, SiteData, colorPalettes, NavStyling) {
  cssInjector.removeAll();
  
  $scope.sitecontroller = true;
  $scope.state = $state;
  $scope.colorPalettes = colorPalettes;

  $scope.stateName = '';
	$scope.$watch('state.current.name', function(newVal) {
		if (newVal.length) $scope.stateName = newVal;
	});

  $scope.editing = false;
  $rootScope._site = $scope.siteData;
  $rootScope._site.components = _.groupBy($scope.components, 'tag');
  $scope._site.images = _.groupBy($scope.siteImages, 'tag');
  var cssPath;

  $scope._site.theme = $scope._site.theme || 4;
  cssPath = '/public/performance/themes/' + $scope._site.theme + '/theme.css';
  cssInjector.add(cssPath);

  $scope.setSiteStyle = function() {
    
    var length = $scope.pages.length;
    NavStyling.setNavStyle(length);
    $scope.navStyle = NavStyling.getNavStyle();
  };
  

  SiteData.setSiteData({
    images: _.groupBy($scope.siteImages, 'tag'),
    components: _.groupBy($scope.components, 'tag'),
    editing: false,
    pages: $scope.pages,
    _site: $scope._site
  });

  
  $scope.pages.forEach(function(page) {
  
    var css = '/public/performance/pages/' + page.layout_category + '/' + page.layout + '.css';
  
    cssInjector.add(css);
  });


  $scope.publishSite = function() {
    swal({
      title: 'Are You Sure?',
      text: 'Are you sure you want to publish your changes?',
      type: 'warning',
      closeOnCofirm: true,
      showCancelButton: true,
      closeOnCancel: true
    }, function(isConfirm) {
      if (isConfirm) {
        $http.post('/api/publish/site').then(function(response) {
          if (response.data.message === 'ok') {
            location.reload();
          }
        }).catch(function(err) {
          console.error(err);
        });
      }
    });
  };

  $scope.getTrialHoursRemaining = function() {
    var date = new Date();
    var expireDate = new Date($rootScope._site.expiration_date);
    var html;
    if (expireDate < date) {
      html = '<span>Your site is currently expired. Please signup for a subscription.</span>';
    } else {
      html = '<span>Your site is currently in trial mode. Your trial will expire on ' + moment($rootScope._site.expiration_date).format('dddd, MMMM Do YYYY h:mm a') + '.</span>';
    }

    $scope.trialText = $sce.trustAsHtml(html);
  };
}]);

pencilblueApp.filter('externalurl', function() {
  return function(input) {
    var cleanURL = input.replace(/http(\:|s\:)\/\//, '');
    return 'http://' + cleanURL;
  };
});

pencilblueApp.filter('pagestyle', function() {
  var pageStyles = {
    background_color: 'Background',
    header_background_color: 'Header Background',
    color: 'Text',
    nav_text_color: 'Navbar Text',
    nav_active_color: 'Navbar Active Page Text',
    navbar_background: 'Navbar Background',
    link_color: 'Link',
    link_hover_color: 'Link Hover',
    button_text_color: 'Button Text',
    button_bg_color: 'Button Background',
    social_background_color: 'Social Icon Background',
    social_font_color: 'Social Icon Text',
    title_color: 'Header Name'
  };

  return function(input) {

    if (pageStyles[input]) {
      return pageStyles[input];
    }
    if (input === 'color') input = 'Font ' + input;
    if (input === 'color_footer') {
      input = input.replace('color_', '');
      input += ' Color';
    }
    var cleanURL = input.replace(/_|-/gi, ' ');
    return cleanURL;
  };
});

pencilblueApp.filter('externalurl', function() {
  return function(input) {
    var cleanURL = input.replace(/http(\:|s\:)\/\//, '');
    return 'http://' + cleanURL;
  };
});

pencilblueApp.filter('telephone', function() {
  return function(input) {
    if (input && input.length) {
      var arr = input.split('');
      arr.splice(0, 0, '(');
      arr.splice(4, 0, ')');
      arr.splice(5, 0, ' ');
      arr.splice(9, 0, '-');
      return arr.join('');
    } else {
      return input;
    }
  };
});

'use strict';

pencilblueApp
  .constant('videoRegexes', {
    iframe: /(youtube|youtu\.be|vimeo)/i,
    notIframe: /(wistia|[a-z0-9]{5,15})/i,
    soundcloud: /soundcloud\.com/i
  })
  .constant('placeholdertext', {
    role1: 'Click to Edit Role 1',
    role2: 'Click to Edit Role 2',
    role1img: 'Upload a photo or headshot that best represents Role 1 (i.e. Actor, Singer, Choreographer).',
    role2img: 'Upload a photo or headshot that best represents Role 2 (i.e. Actor, Singer, Choreographer).',
    subheadAboutText: 'About',
    upcoming1: 'Click to edit Project 1 description',
    upcoming2: 'Click to edit Project 2 description',
    upcoming3: 'Click to edit Project 3 description',
    aboutme: 'Click to enter a brief bio about your multiple disciplines. ',
    projectdescription: {
      header: 'Edit Project Description',
      placeholder: 'Enter a brief description for your recent or upcoming project. Include your role, dates, and location. Boxes left blank in this section will not appear on your website.'
    },
    role1htmleditor: {
      header: 'Edit Role 1',
      description: 'Enter the title of your first discipline (i.e. Actor, Singer, Choreographer) in the text box, below. This title will appear in your navigation bar, as the title of the corresponding Role 1 page of your website, and above the corresponding image on your homepage when a user hovers over the photo.',
      placeholder: 'Example: Actor, Singer, Choreographer'
    },
    role2htmleditor: {
      header: 'Edit Role 2',
      description: 'Enter the title of your second discipline (i.e. Actor, Singer, Choreographer) in the text box, below. This title will appear in your navigation bar, as the title of the corresponding Role 2 page of your website, and above the corresponding image on your homepage when a user hovers over the photo.',
      placeholder: 'Example: Actor, Singer, Choreographer'
    },
    social: 'Click the blue button below to add your social media accounts.',
    classic: 'Want to use a portrait-oriented headshot for your homepage? The Classic template is designed specifically for landscape-oriented headshots, so we recommend choosing a different template.',
    spotlight: 'The homepage of the Spotlight template works best with a landscape-oriented headshot that has a visible background at the top. The navigation bar should not cover your face, so close-up images may not work with this template.',
    abouteditor: {
      header: 'Edit About',
      description: '',
      placeholder: 'This is where you’ll include your bio, which should provide a narrative snapshot of who you are as an artist. <br/><br/> Some actors go traditional, presenting their professional background more or less they way it would appear in a playbill.  Others choose to get a bit more personal and expand upon the usual credits to include information about other skills, interests, or projects related to their work as an actor. <br/><br/>  Either approach is fine, as long as your bio is well written and relevant. Always write in the third person (she/he/they pronouns), use good grammar, and remember to proofread! '
    },
    resumeHeader: 'Click to Upload Photo or Résumé in .jpg, .png, or .pdf format',
    resumeInstruction: 'Your résumé should be clear, legible, and well organized. Present your experience professionally and honestly, and be sure to update whenever you have a new credit to add or to emphasize the experience that best aligns with the roles you’re going out for at any given time.  <br/><br/> Don’t have a résumé? Don’t worry! Our Résumé Builder page layout will guide you through the process of creating one. <br/><br/>  <strong>For safety’s sake, never include your physical address on your résumé.</strong>',
    resumeInstruction2: 'To include a downloadable résumé that visitors can keep or print, click the link below.',
    newseditor: {
      header: 'Edit News',
      placeholder: 'Create and update your news items here. Feature current or upcoming projects, including casting announcements and show dates. Be sure to include venue names and locations, as well as links to relevant production or ticketing websites, news articles, and reviews. (Links can be created in the text editor using the chain link icon.'
    },
    newsImageInstruction: 'Click to upload portrait-oriented photo, graphic, or press clip in .jpg, .png, or .pdf format.',
    newsImageLandscapeInstruction: 'Click to upload landscape-oriented photo, graphic, or press clip in .jpg, .png, or .pdf format.',
    galleryInstructions: 'Click below to upload headshots, production stills, modeling photos, and any images that represent you as a performer. Choose from thumbnail galleries, image carousels, and expanding photo albums in the Page Layouts feature. Group your photos into albums, by projects, headshot session, or modeling campaigns.',
    mediaInstructions: 'Click below to upload audio and video files to your Media page. Group your media clips into albums by project type (TV appearances, commercials, theatre) or, for projects with multiple clips, create albums for each project. You can upload files from YouTube, Vimeo, Wistia, or Soundcloud.',
    contacteditor: {
      description: 'Click to upload portrait-oriented photo in .jpg, .png, or .pdf format.',
      placeholder: 'Use this optional text box to greet your visitors personally, or to include your contact information. <br/><br/>If you don’t want to share your personal information, the optional contact form allows site visitors to send a message directly to your linked email address. <br/><br/>Or, remove this text box or the contact form altogether! To do this, use the page layout tab in the left-hand sidebar area.',
      header: 'Edit Contact'
    },
    landscape: 'This page layout is best for landscape-oriented photos.',
    portrait: 'This page layout is best for portrait-oriented photos.'
  });

angular.module('pencilblueApp').component('gallery', {
  bindings: {
    gallery: '<'
  },
  templateUrl: function($stateParams, $state, SiteData) {
    var siteData = SiteData.getSiteData();
    var themeId = siteData._site.theme;
    var pageName = $state.current.name;
    console.log('/public/performance/pages/' + pageName + '/' + (siteData._site.layouts[pageName] || 1) + '.html');
    return '/public/performance/pages/' + pageName + '/' + (siteData._site.layouts[pageName] || 1) + '.html';
  },  controller: function(GetGallery, ngDialog, SiteData) {
    var self = this;

    var siteData = SiteData.getSiteData();

    this._site = siteData._site;
    this.images = siteData.images;
    this.components = siteData.components;
    this.editing = siteData.editing;


    this.gallery = this.gallery.data;

    this.carouselIdx = Array(this.gallery.length).fill(0);

    this.slide = function(idx, dir, sectionIdx) {
      console.log('slide dir', dir);
      if (idx) {
        if (idx > -1) {
          this.carouselIdx[sectionIdx] = idx;
        } else {
          this.carouselIdx[sectionIdx]++;
        }
      } else if (!idx && dir) {
        if (sectionIdx) {
          this.carouselIdx[sectionIdx] = dir === 'next' ? this.carouselIdx[sectionIdx] + 1 : this.carouselIdx[sectionIdx] - 1;
          $('#carousel-element-' + sectionIdx).carousel(dir);
        } else {
          $('#carousel-element').carousel(dir);
        }
      }
    };

    
  }
});

angular.module('pencilblueApp').component('page', {
  bindings: {
    pagedata: '<', 
    editing: '<'
  },  
  templateUrl: function($state, SiteData, $rootScope) {    
		var page = _.find(SiteData.getSiteData().pages, {slug: $state.params.slug});
		if (page) {		
      
      $rootScope.currentPage = page;			
      var url = '/public/performance/pages/' + page.layout_category + '/' + page.layout + '.html';
      
			return url;
		} else {
			$rootScope.currentPage = null;
		}
  },
  controller: function($scope, $http, $state, $stateParams, $sce, placeholdertext) {
    var self = this;
    this.placeholdertext = placeholdertext;
    this.controllerName = 'Page Component';
    
    this.page = this.pagedata.page;
    this.images = this.pagedata.images;
    this.components = this.pagedata.components;

    this.pageId = this.page._id;    
    this.stateParams = $stateParams;          
    this.info = {success: null, error: null};
    this.alertClass = null;
    this.showAlert = false;

    var pageName = $state.current.name;
    
    this.trustText = function(text) {
      return $sce.trustAsHtml(text);
    };
    
  }
});

angular.module('pencilblueApp').component('home', {
  templateUrl: function ($stateParams, $state, $location, SiteData) {
    var siteData = SiteData.getSiteData();
    var themeId = siteData._site.theme;
		var pageName = $state.current.name;
		if (pageName == 'index') { //needed for prerender
			pageName = 'home';
		}
		
    var templatePath = '/public/performance/themes/' + siteData._site.theme + '/home' + (siteData._site.layouts[pageName]) + '.html';
    return templatePath;
  },
  controller: function($sce, SiteData, placeholdertext) {
    var siteData = SiteData.getSiteData();
    this._site = siteData._site;
    this.images = siteData.images;
    this.placeholdertext = placeholdertext;    
    this.components = siteData.components;
    this.editing = siteData.editing;
    this.trustText = function(text) {
      return $sce.trustAsHtml(text);
    };
  }
});

angular.module('pencilblueApp').component('media', {
  templateUrl: function($stateParams, $state, SiteData) {
    var siteData = SiteData.getSiteData();
    var themeId = siteData._site.theme;
    var pageName = $state.current.name;
    console.log('siteData._site.layouts[pageName]', siteData._site.layouts[pageName]);
    if (pageName) {
      return '/public/performance/pages/' + pageName + '/' + (siteData._site.layouts[pageName] || 1) + '.html';
    } else {
      console.error('WARNING', pageName, 'not defined.');
    }

  },
  controller: function(ngDialog, $sce, $state, $http, SiteData, $scope, videoRegexes) {



    var $ctrl = this;
    $ctrl.controllerName = 'Media Component';

    var siteData = SiteData.getSiteData();
    $ctrl.layout = siteData._site.layouts[$state.current.name];

    $ctrl._site = siteData._site;
    $ctrl.editing = siteData.editing;
    console.log('IN HERE IN HERE IN HEREIN HERE IN HERE IN HEREIN HERE IN HERE IN HEREIN HERE IN HERE IN HEREIN HERE IN HERE IN HERE');
    $ctrl.images = siteData.images;
    $ctrl.components = siteData.components;




  }
});

angular.module('pencilblueApp').component('customPage', {
  templateUrl: function(SiteData, $state) {
    var siteData = SiteData.getSiteData();
    var title = $state.params.title;
    var tag = $state.params.tag;

    console.log('siteData.components[tag]', siteData.components[tag]);
    if (!siteData.components[tag]) return $state.go('home', {}, {reload: true});

    var pageLayout = siteData.components[tag][0].component_specific_data.layout;
    console.log('pageLayout', pageLayout);
    if (pageLayout > -1) {
      return '/public/performance/pages/custom/' + (pageLayout + 1) + '.html';
    } else {
      $state.go('home', {}, {reload: true});
    }
  },
  controller: function($scope, $rootScope, $state, SiteData, $sce) {

    $scope.editing = $rootScope.editing;

    $scope.title = $state.params.title;
    var tag = $state.params.tag;
    $scope.siteData = SiteData.getSiteData();
    $scope.components = $scope.siteData.components;
    $scope.componentData = $scope.siteData.components[tag][0];

    $scope.images = {};
    $scope.texts = {};

    //todo this is super inefficient, it's gonan go through gallery and all images, and all components
    console.log('IMAGES? ', $scope.siteData.images);
    for (var image in $scope.siteData.images) {
      var imageData = $scope.siteData.images[image];
      imageData.forEach(function(p) {
        if (p && p.page === $scope.componentData._id) {
          if (!$scope.images[image]) $scope.images[image] = [];
          $scope.images[image][0] = p;
        }
      });
    }

    for (var text in $scope.components) {
      var component = $scope.components[text];
      component.forEach(function(c) {
        if (c && c.page === $scope.componentData._id) {
          if (!$scope.texts[text]) $scope.texts[text] = [];
          $scope.texts[text][0] = c;
        }
      });
    }

    $scope.$on('text-updated', function() {
      location.reload();
    });

    $scope.$on('image-updated', function() {
      location.reload();
    });

    $scope.trustText = function(text) {
      return $sce.trustAsHtml(text);
    };
  }
});

  //the main navigation
  angular.module('pencilblueApp')
  .directive('mainnav', function() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: function(elem, attr) {
        return attr.override || '/public/performance/partials/mainnav.html';
      },
      controller: function($scope, $rootScope) {
        $scope.editing = $rootScope.editing;
      }
    };
  });


angular.module('pencilblueApp').component('galleryblock', {
  bindings: {
    pageId: '=',
    editing: '=',
    nocrop: '='
  },
  templateUrl: ['$element', '$attrs', function($element, $attrs) {
    if (!$attrs.partial) {
      console.error('partial url required.');
      return false;
    }
    var url = '/public/performance/pages/gallery/' + $attrs.partial;
    return url;
  }],
  controllerAs: '$ctrl',
  controller: function($http, $sce, GetGallery, ngDialog) {
    var self = this;
    console.log('self.nocrop', self.nocrop);
    GetGallery.get({
      pageId: self.pageId
    }).then(function(resp) {
      
      self.gallery = resp.data;
      self.carouselIdx = Array(self.gallery.length).fill(0);
    });
    
    
    

    this.slide = function(idx, dir, sectionIdx) {
      
      if (idx) {
        if (idx > -1) {
          this.carouselIdx[sectionIdx] = idx;
        } else {
          this.carouselIdx[sectionIdx]++;
        }
      } else if (!idx && dir) {
        if (sectionIdx) {
          this.carouselIdx[sectionIdx] = dir === 'next' ? this.carouselIdx[sectionIdx] + 1 : this.carouselIdx[sectionIdx] - 1;
          $('#carousel-element-' + sectionIdx).carousel(dir);
        } else {
          $('#carousel-element').carousel(dir);
        }
      }
    };

    this.openPicModal = function(section, imgIndex) {
      var copiedSection = angular.copy(section);

      ngDialog.open({
        template: '/public/performance/themes/partials/gallery-modal.html',
        appendClassName: 'gallery-modal',
        resolve: {
          currSection: function() {
            if (imgIndex) {
              var imagesCut = copiedSection.images.splice(0,imgIndex);
              copiedSection.images = copiedSection.images.concat(imagesCut);
            }
            return copiedSection;
          },
          slide: function() {
            return self.slide;
          }
        },
        controller: ['$scope', 'currSection', 'slide', function(scope, currSection, slide) {
          scope.section = currSection;

          scope.slideKey = function($event, idx, dir) {
            var keypress = $event.which;

            if (keypress == 39) {
              slide(idx, 'next');
            } else if (keypress == 37) {
              slide(idx, 'prev');
            }

          };
          scope.slide = function(idx, dir) {
            slide(idx, dir);
          };
        }]
      });
    };    
   
  }
});


angular.module('pencilblueApp').component('mediablock', {
  bindings: {
    pageId: '=',
    editing: '='
  },
  templateUrl: ['$element', '$attrs', function($element, $attrs) {
    if (!$attrs.partial) {
      console.error('partial url required.');
      return false;
    }
    var url = '/public/performance/pages/media/' + $attrs.partial;
    return url;
  }],
  controllerAs: '$ctrl',
  controller: function($http, $sce, GetMedia, ngDialog, videoRegexes) {
    var self = this;
    GetMedia.get({
      pageId: self.pageId
    }).then(function(resp) {
      self.media = resp.data;
    });

    this.regexes = videoRegexes;

		if (! document.getElementById('wistiaScript')) {

			var wistiaTag = document.createElement("script");
			wistiaTag.type = "text/javascript";
			wistiaTag.async;
			wistiaTag.id = "wistiaScript";
			wistiaTag.keepScript = "true";
			wistiaTag.src = "//fast.wistia.net/assets/external/E-v1.js";
			$("head").append(wistiaTag);
		}

		if (! document.getElementById('scScript')) {

			var soundcloudTag = document.createElement("script");
			soundcloudTag.type = "text/javascript";
			soundcloudTag.async;
			soundcloudTag.id = "scScript";
			soundcloudTag.keepScript = "true";
			soundcloudTag.src = "//connect.soundcloud.com/sdk/sdk-3.1.2.js";
			$("head").append(soundcloudTag);
		}


    
    this.testUrl = function(url) {
      if (self.regexes.iframe.test(url)) {
        return 'iframe';
      } else if (self.regexes.soundcloud.test(url)) {
        return 'soundcloud';
      } else {
        return 'javascript';
      }
    };

    this.trustAsUrl = function(url) {
      return $sce.trustAsResourceUrl(url);
    };    
    

  }
});


angular.module('pencilblueApp').component('resumeblock', {
  bindings: {
    pageId: '@',
    editing: '='
  },
  templateUrl: '/public/performance/partials/resumeblock.html',
  controllerAs: '$ctrl',
  controller: function($http, $sce, GetResumeBuilder, ngDialog) {
    var self = this;
    GetResumeBuilder.get({
      pageId: self.pageId
    }).then(function(resp) {

      self.resume = resp.data;
    });    
    
  }
});

angular.module('pencilblueApp').component('social', {
  templateUrl: function() {
    return '/public/performance/partials/social.html';
  },
  bindings: {
    social: '=',
    _site: '=site',
    editing: '=',
    showHandle: '@'
  },
  controller: function(ngDialog, $filter) {

    var scope = this;
    
    
    this.available_social = [{
      name: 'facebook',
      icon: 'facebook',
      url: 'https://www.facebook.com/'
    },{
      name: 'twitter',
      icon: 'twitter',
      url: 'https://www.twitter.com/'
    },{
      name: 'instagram',
      icon: 'instagram',
      url: 'https://www.instagram.com/'
    },{
      name: 'youtube',
      icon: 'youtube',
      url: 'https://www.youtube.com/',
      title: 'Full Youtube url'
    },{
      name: 'email',
      icon: 'envelope-o',
      url: 'mailto:'
    },{
      name: 'imdb',
      icon: 'imdb',
      url: 'http://www.imdb.com/'
    }, {
      name: "backstage",
      icon: 'backstage',
      url: 'https://www.backstage.com/u/'
    }, {
      name: 'actors_access',
      icon: 'actors_access',
      url: 'http://resumes.actorsaccess.com/'
    }];


    this.edit = function() {
      ngDialog.open({
        template: '/public/performance/partials/social-edit-modal.html',
        controllerAs: '$ctrl',
        appendClassName: 'social-edit-modal',
        resolve: {
          social: function() {
            return scope.social;
          },
          available_social: function() {
            return scope.available_social;
          }
        },
        controller: ['social', 'available_social', 'GetSite', function(social, available_social, GetSite) {
          var self = this;
          this.social = social;
          console.log('social', social);
          this.available_social = available_social;
          this.saveSocial = function() {            
            GetSite.set({
              social: self.social
            }).then(function(updatedPage) {
              console.log('upadted');
              console.log('self.social', self.social);
              console.log('social', social);
              ngDialog.close();
            }, function(err) {
              console.error(err);
              ngDialog.close();
            });                        
          };
          this.handleChanged = function(val) {
            console.log('handle changed', self.social[val].handle);
            var clean = self.social[val].handle.replace(/(http(s|)\:\/\/(www\.|)(youtube\.com|youtu\.be)(\/|))/gi, '');
            var noSpaces = clean.replace(/\s| /g, '');
            self.social[val].handle = noSpaces;
          };
        }]
      });
    };
  }
}).filter('socialFormat', function() {
  return function(input) {
    if (input) {
      var mainSplit = input.split('_');
      if (mainSplit.length > 1) {
        mainSplit.forEach(function(i) {
          var split = i.split('');
          var firstLet = split[0];
          i = firstLet.toUpperCase() + split.slice(1);
        });
        return mainSplit.join(' ');
      } else {
        return input;
      }
    }

    return input;
  };
});

//the modal directive to upload and image and crop
//the main navigation
angular.module('pencilblueApp')
.directive('linky', ['$window', 'SiteData', function($window, SiteData) {
	return {
		scope: {
			ngModel: '=',
			tag: '@',
			type: '@',
			accept: '@',
			text: '@',
			editText: '@'
		},
		restrict: 'E',
		replace: true,
		template: function(tElem, tAttrs) {
			return '<' + tAttrs.type + '></' + tAttrs.type + '>';
		},
		link: function(scope, elem, attrs) {
			var siteData = SiteData.getSiteData();
			elem.addClass(scope.type);

			if (attrs.text) {
				elem.text(attrs.text);
			}

			if (!scope.ngModel) {
				elem.remove();
			} else if (scope.ngModel.text) {
				elem.text(scope.ngModel.text);
			}

			elem.on('click', function(e) {
				if (scope.ngModel) {
					$window.open(scope.ngModel.url);
					if (scope.ngModel.text) {
						elem.text(scope.ngModel.text);
					}
				}
			});
		}
	};
}]);

angular.module('pencilblueApp').component('carousel', {
  bindings: {
    ngModel: '=',
    idealSize: '<'
  },
  template: '<div class="carousel">' +
              '<button style="margin-top: 10px;" class="btn btn-primary sitewide" ng-show="$carousel.editing" ng-click="$carousel.editImages()"><i class="fa fa-plus"></i> Add/Edit Images</button>' +
              '<div id="main-carousel" class="slick-carousel" ng-style="$carousel.loadedStyle">' +
                '<div class="slick-img-wrap" ng-repeat="img in $carousel.ngModel.images track by $index">' +
									'<a ng-href="{{img.href || \'#\'}}" target="{{img.href_target}}"><img ng-src="{{img.url}}" alt="" /></a>' +
                '</div>' +
              '</div>' +
            '</div>',
  controllerAs: '$carousel',
  controller: function($scope, $location, $state, $stateParams, SiteData, Upload, $uibModal, $timeout) {
    var self = this;
    self.loadedStyle = {'opacity': 0};
    var openModal = $stateParams.modal === 'true';
    console.log('ngModel: ', self.ngModel);

    var siteData = SiteData.getSiteData();
    self.editing = siteData.editing;

    if (!self.ngModel) {
      self.ngModel = {images: []};
    }

    // if (!self.ngModel || (self.ngModel && (!self.ngModel.images || self.ngModel.images.length < 1))) {
    //   if (self.editing) {
    //     //var image = {url: '/public/performance/img/placeholders/' + (self.idealSize || '200x200') + '.png'};
    //     //self.ngModel.images = [image, image, image];
    //     //self.ngModel.placeholders = true;
    //   }
    // }

    imagesLoaded($('.slick-carousel img'), function() {
      self.loadedStyle = {'opacity': 1};
      $scope.$apply();
      objectFitImages();
    });

    self.editImages = function(hasQueryStringInURL) {
      $state.go($state.current, {modal: true}, {reload: false});
      var modalInstance = $uibModal.open({
        animation: false,
        size: 'lg',
        windowClass: 'home-page-image-edit',
        templateUrl: '/public/performance/partials/carouselUpload.html',
        resolve: {
          ngModel: function() {
            return self.ngModel;
          },
          thumbnailmode: function() {
            return false;
          },
          idealSize: function() {
            return '1200x700';
          }
        },
        controllerAs: 'carouselUploader',
        controller: function($http, ngModel, thumbnailmode, idealSize) {
					var carouselUploader = this;

					carouselUploader.thumbnailmode = thumbnailmode;
					carouselUploader.idealSize = idealSize;

          carouselUploader.ngModel = ngModel;
          carouselUploader.context = 'gallery';

          var firstSort = true;

          var uploadImage = '/api/image/upload';

          carouselUploader.sortableOptions = {
            tolerance: 'pointer',
            handle: '.drag-tab',
            helper: 'clone',
            stop: function() {
              $http.post('/api/carousel/update', carouselUploader.ngModel).then(function(res) {
                carouselUploader.ngModel = res.data;
                $state.go($state.current, {modal: false}, {reload: true});
              });
            }
          };

          carouselUploader.upload = function(files) {
            console.log('carousel files: ', files);
            carouselUploader.fileUploadError = null;

            var validFiles = files.filter(function (f) { return f.size <= 10000000; });
            if (validFiles.length !== files.length) carouselUploader.fileUploadError = 'One or more files uploaded are too large!';

            if (!validFiles.length) return;

            var fileCount = validFiles.length;

            var fileCompleted = 0;
            carouselUploader.updatePercentage();
            validFiles.forEach(function(i) {
              var pendingImage = {};
              pendingImage.file = i;
              pendingImage.name = i.name;
							console.log('siteData', siteData);

							var uploadData = {
                  file: i,
                  tag: 'homeCarouselImage',
									height: 700,
									width: 1200,
									crop: 'pad'
							};
							if (siteData._site.pageStyles && siteData._site.pageStyles.backgroundColor) {
								uploadData.background = siteData._site.pageStyles.backgroundColor;
							}

              Upload.upload({
                url: uploadImage,
                data: uploadData
              }).then(function(img) {
                fileCompleted++;

                if (!carouselUploader.ngModel.images.length || carouselUploader.ngModel.placeholders) carouselUploader.ngModel.images = [];
                carouselUploader.ngModel.images.push(img.data);

								if (fileCompleted === fileCount) {
									$http.post('/api/carousel/update', carouselUploader.ngModel).then(function(res) {
											carouselUploader.ngModel = res.data;
									}).catch(function(err) {
										console.error(err);
									});
								}
              }, function(resp) {
                console.log('resp err', resp);
                console.log('Error status: ' + resp.status);
              }, function(evt) {
                console.log('resp progressPercentage', evt);
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                pendingImage.progressPercentage = progressPercentage;
                // carouselUploader.progressPercentage = progressPercentage;
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
              });
            });
          };

          carouselUploader.deleteImage = function(index) {
            swal({
              title: 'Are you sure?',
              text: 'Delete this image?',
              closeOnConfirm: false,
              showCancelButton: true,
              type: 'warning'
            }, function(confirmed) {
              if (confirmed) {
								carouselUploader.ngModel.images.splice(index, 1);

                $http.post('/api/carousel/update', carouselUploader.ngModel).then(function(res) {
                  carouselUploader.ngModel = res.data;
                  swal('Success', '', 'success');
                });
              }
            });
          };

          carouselUploader.percentageCount = 0;
          carouselUploader.progressPercentage = 0;
          carouselUploader.updatePercentage = function() {
            console.log('progress: ', carouselUploader.percentageCount, carouselUploader.progressPercentage);
            if (carouselUploader.percentageCount <= 5 && carouselUploader.progressPercentage < 100) {
              carouselUploader.percentageCount++;
              carouselUploader.progressPercentage += 25;
              $timeout(function() {
                carouselUploader.updatePercentage();
              }, 200);
            } else {
              carouselUploader.progressPercentage = 100;
              $timeout(function() {
                carouselUploader.progressPercentage = 0;
              }, 3000);
            }
          };
        }
			});

			modalInstance.result.then(function(component) {
				location.replace("/edit");
			}, function() {
				console.log('Modal dismissed at: ' + new Date());
			});
    };

    if (openModal && self.editing) {
      self.editImages(true);
    }
  }
});

pencilblueApp.directive('galleryWithoutSection', [function () {
  return {
    restrict: 'A',
    scope: {
      gallery: '='
    },
    link: function(scope, element, attrs) {
      console.log('scope', scope);
      if (scope.gallery) {
        console.log('in gallery');
        var caption = angular.element('<div id="caption">&nbsp;</div>');

        var ul = angular.element('<ul id="gallery-list"/>');
        scope.gallery.forEach(function(section) {
          if (section.images.length) {
            section.images.forEach(function(image) {
              var caption = image.credit ? '<p style="text-align: center;padding: 1em 0;">Photo By: ' + image.credit + '</p>' : '&nbsp;';
              var title = image.title ? '<h3 style="text-align: center;">' + image.title + '</h3>' : '';
              ul.append(
                '<li data-credit="' + (image.credit || '') + '"' + ' data-thumb="' + image.thumbnail + '" class="carousel-item">' +
                      '<img src="' + image.url + '" alt="' + image.title + '" />' +
                       title +
                       caption +
                '</li>'
                );
            });
          }
        });
        element.append(ul);
        //element.append(caption);

        var updateCaption = function(li) {
          var title = li.getAttribute('data-title') || '';
          var credit = li.getAttribute('data-credit') || '';
          if (credit) {
            caption.html('Photo by ' + credit);
          } else {
            caption.html('&nbsp;');
          }

        };

        $(document).ready(function() {
            $('#gallery-list').lightSlider({
              gallery: true,
              item: 1,
              keyPress: true,
              // slideMargin: 20,
              thumbMargin: 10,
              loop: true,
              slideMargin: 0,
              thumbItem: 7,
              onSliderLoad: function(el) {
                var li = el.children()[0];
                //updateCaption(li);
              },
              onAfterSlide: function(el, index) {
                var li = el.children()[index];
                //updateCaption(li);
              }
            });
        });
      } else {
        console.log('NOT IN GALLERY');
      }
    }
  };
}]);

pencilblueApp.directive('dynamicStyles', function() {
  return {
    scope: {
      pageStyles: '='
    },
    templateUrl: '/public/performance/themes/dynamic-styles.css'
  };
});

pencilblueApp.factory('GetSite', ['$http', function($http) {
  var saveUrl = '/api/savesite';
  return {
    get: function() {
      return $http.get('/api/site');
    },
    set: function(data) {
      return $http.post(saveUrl, data);
    }
  };
}]).factory('GetGallery', ['$http', function($http) {
  var galleryUrl = '/api/gallery';
  return {
    get: function(data) {
      
      return $http.get(galleryUrl, {
        params : data
      });
    },
    set: function(data) {
      return $http.post(galleryUrl, data);
    }
  };
}]).factory('GetResumeBuilder', ['$http', function($http) {
  var resumeUrl = '/api/resume-builder';
  return {
    get: function(data) {
      
      return $http.get(resumeUrl, {
        params : data
      });
    },
    set: function(data) {
      return $http.post(resumeUrl, data);
    }
  };
}]).factory('GetMedia', ['$http', function($http) {
  var mediaUrl = '/api/media';
  return {
    get: function(data) {
      
      return $http.get(mediaUrl, {
        params : data
      });
    },
    set: function(data) {
      return $http.post(mediaUrl, data);
    }
  };
}]).factory('ImgFactory', ['$http', function($http) {
  return {
    get: function() {
      return $http.get('/api/image');
    },
    update: function(img) {
      return $http.post('/api/image/update', img);
    },
    delete: function(img) {
      return $http.post('/api/image/delete', img);
    }
  };
}]).factory('ComponentFactory', ['$http', function($http) {
  return {
    get: function() {
      return $http.get('/api/components');
    },
    update: function(component) {
      return $http.post('/api/components/update', component);
    },
    updatePage: function(component) {
      return $http.post('/api/components/update-page', component);
    }
  };
}]).factory('SiteData', function() {
  var siteData = {};
  return {
    setSiteData: function(data) {
      siteData = data;
    },
    updateSiteData: function(key, value) {
      siteData[key] = value;
    },
    getSiteData: function() {
      return siteData;
    }
  };
}).factory('Component', ['$http', function($http) {
  return {
    query: function() {
      return $http.get('/api/components');
    }
  };
}]);

pencilblueApp.factory('NavStyling', function() {
    var navStyle = {};

    return {
        setNavStyle: function(count) {
            navStyle['font-size'] = 'calc(10vw / ' + count + ')';
            navStyle.width = 'calc(100% / ' + count + ')';
        },
        getNavStyle: function() {
            return navStyle;
        }
    }
});
//the modal directive to upload and image and crop
//the main navigation
angular.module('pencilblueApp')
  .directive('imgcropModal', ['$rootScope', '$uibModal', 'SiteData', 'ImgFactory', function($rootScope, $uibModal, SiteData, ImgFactory) {
    return {
      scope: {
        ngModel: '=',
        mobileImage: '=',
        outputwidth: '@',
        thumbnailmode: '@',
        idealSize:'@',
        mobileIdealSize: '@',
        tag: '@',
        pageId: '@',
        instruction: '@',
        instructionHeader: '@',
        ignoremessage: '=',
        mobileUpload: '='
      },
      restrict: 'A',
      replace: true,
      link: function(scope, elem, attrs) {
        if (!scope.pageId) {
          console.warn('no page id specified.');
        }
        var siteData = SiteData.getSiteData();

        var uploadMessage = '<div class="upload-message"><div class="message" style="padding: 1em;text-align: center;color:white;">' + (scope.instructionHeader || 'Click to Upload Image')+ '</div></div>';
        var u = angular.element(uploadMessage);

        if (siteData.editing && !scope.ignoremessage) {
          elem.wrap('<div class="imgcropwrap" style="position:relative;height: 100%;width: 100%;"></div>');
          //u.css('display', 'none');
          u.on('click', function() {
            openImageCropModal();
          });
          elem.after(u);
        }



        if (!scope.ngModel || (scope.ngModel && !scope.ngModel.url)) {
          if (siteData.editing) {
            var url = '/public/performance/img/placeholders/' + (scope.idealSize || '200x200') + '.png';
            if (elem[0].tagName === 'IMG') {
              attrs.$set('src', url);
              //u.css('display', 'flex');
              //var uploadIcon = angular.element('<div style="position:absolute;top:20px;left:20px;color:white;"><i class="fa fa-upload fa-2x"></i></div>');
              //uploadIcon.on('click', openImageCropModal);
              //elem.after(uploadIcon);
            } else if (!elem[0].hasAttribute('noshowbg')) {
              console.log('in imgcropmodal');
              elem.css({
                backgroundImage: 'url(/public/performance/img/placeholders/' + (scope.idealSize || '200x200') + '.png)'
              });
              elem.addClass('coverbackground');
            }
          } else {
            elem.remove();
            return;
          }
        } else if ((scope.ngModel && scope.ngModel.url) || (scope.mobileImage && scope.mobileImage.url)) {
          if (elem[0].tagName === 'IMG') {
            attrs.$set('src', scope.ngModel.url);
          } else if (!elem[0].hasAttribute('noshowbg')) { //don't do it for the crop button
            updateBackgroundImage(scope.ngModel, scope.mobileImage);
            // elem.css({
            //     backgroundImage: 'url(' + scope.ngModel.url + ')'
            // });
            elem.addClass('coverbackground');
          }
        }
        if (scope.ngModel && scope.ngModel.title) {
          if (elem[0].tagName === 'IMG') {
            attrs.$set('alt', scope.ngModel.title);
          } else {
            attrs.$set('title', scope.ngModel.title);
          }
        }

				function nonEditOpenImage() {
          if (scope.ngModel.href) {
            location.assign(scope.ngModel.href);
            return;
          }
          var $uibModalInstance = $uibModal.open({
            animation: true,
            size: 'lg',
            template: '<div class="imgcropmodal-modal" style="position:absolute;"><i class="fa fa-times pull-right" ng-click="$ctrl.cancel()"></i></div><img src="' + scope.ngModel.url + '" />',
            windowTopClass: 'fullscreen-lb',
            controllerAs: '$ctrl',
            controller: function() {
              this.cancel = function() {
                $uibModalInstance.dismiss('cancel');
              };
            }
          });
        }

        function openImageCropModal() {

          var modalInstance = $uibModal.open({
            animation: false,
            size: 'md',
            ariaLabelledBy: 'modal-title',
						ariaDescribedBy: 'modal-body',
						windowClass: 'imgCropModal',
            templateUrl: '/public/performance/partials/imgcrop-modal.html',
            controllerAs: '$ctrl',

            controller: function($uibModalInstance, ngModel, viewport, mobileViewport, $scope, outputwidth, thumbnailmode, tag, pageId, idealSize, title, instruction, instructionHeader) {
              var $ctrl = this;
							$ctrl.ngModel = ngModel;
              $ctrl.viewport = viewport;
              $ctrl.mobileViewport = mobileViewport;
              $ctrl.outputwidth = outputwidth;
              $ctrl.thumbnailmode = thumbnailmode;
              $ctrl.tag = tag;
              $ctrl.title = title;
              $ctrl.idealSize = idealSize;
              $ctrl.instruction = instruction;
              $ctrl.instructionHeader = instructionHeader;
              $ctrl.pageId = pageId;
              $ctrl.mobileUpload = scope.mobileUpload;
              $ctrl.mobileImage = scope.mobileImage;
              $ctrl.mobileIdealSize = scope.mobileIdealSize;


              $ctrl.updateLink = function(model) {

								if ($ctrl.ngModel.href && !$ctrl.ngModel.href.startsWith('http')) {
									$ctrl.ngModel.href = 'http://' + $ctrl.ngModel.href;
								}
								ImgFactory.update(model).then(function(resp) {
									$uibModalInstance.dismiss('cancel');
								});
              };

              $ctrl.done = function(img) {
                console.log('INFO: ', scope.mobileUpload, $ctrl.mobileUpload);
                $uibModalInstance.close({img: img, isMobile: scope.mobileUpload});
              };

              $ctrl.cancel = function() {

                $uibModalInstance.dismiss('cancel');
              };
            },
            resolve: {
              idealSize: function() {
                return scope.idealSize;
              },
              tag :function() {
                return scope.tag;
              },
              pageId: function() {
                return scope.pageId;
              },
              thumbnailmode: function() {

                return scope.thumbnailmode;
              },
              ngModel: function() {

                return scope.ngModel;
              },
              title: function() {
                return scope.title;
              },
              instruction: function() {

                return scope.instruction;
              },
              instructionHeader: function() {

                return scope.instructionHeader;
              },
              viewport: calculateViewport(scope.thumbnailmode, scope.idealSize),
              mobileViewport: calculateViewport(scope.thumbnailmode, scope.mobileIdealSize),
              outputwidth: function() {
                var tempWidth = scope.outputwidth ? parseInt(scope.outputwidth, 10) : 600;
                var width = tempWidth || 600;
                if (scope.idealSize) {
                  var idealSizeArr = scope.idealSize.split('x');
                  if (idealSizeArr[0] > width) {
                    width = idealSizeArr[0];
                  }
                }
                return width;
              },
              library: function() {
                return scope.library;
              }
            }
          });

          modalInstance.result.then(function(data) {
            var img = data.img;
            var isMobile = data.isMobile;
            console.log('MORE INFO: ', data);
            if (img && img.img) {
              //u.css('display', 'none');
              if (isMobile) {
                scope.mobileImage = img.img;
              } else {
                scope.ngModel = img.img;
              }
							if (elem.attr('background-image') == 'true') {
								// elem.css({
								// 		backgroundImage: 'url(' + scope.ngModel.url + ')'
                // });
                updateBackgroundImage(scope.ngModel, scope.mobileImage);
							} else {
								attrs.$set('src', img.img.url);
								attrs.$set('alt', img.img.title);
								siteData.images[img.img.tag] = [img.img];
							}
            } else {
              //u.css('display', 'flex');

              attrs.$set('src', '/public/performance/img/placeholders/' + (scope.idealSize || '200x200') + '.png');
              scope.ngModel = null;
              if (siteData.images[scope.tag]) {
                siteData.images[scope.tag] = [];
              }
            }
          }, function() {

          });

        }

        if (!siteData.editing) {
          if (elem.parent()[0].tagName == 'A' || elem[0].hasAttribute('hideshadowbox')) {
            return;
          }
          elem.on('click', nonEditOpenImage);
          return;
        }
        elem.addClass('imgcrop');
        elem.on('click', function() {
          openImageCropModal();
        });


      }
    };
  }]);

  function updateBackgroundImage(desktop, mobile) {
    var splashEl = '#hero-image';
    angular.element('#full-page').before('<style>'+
                        splashEl + '{'+
                          'background-image: url(' + desktop.url + ');'+
                        '}'+
                        '@media (max-width: 767px) {'+
                          splashEl + '{'+
                            'background-image: url(' + (mobile ? mobile.url : desktop.url) + ')'+
                          '}'+
                        '}'+
                      '</style>');
  }

  function calculateViewport(thumbnailmode, idealSize) {
    if (thumbnailmode) {
      return {
        w: 200,
        h: 200
      };
    }

    var w,h;


    if (idealSize) {
      var idealSizeArr = idealSize.split('x');
      var idealSizeObj = {
        w: parseInt(idealSizeArr[0], 10),
        h: parseInt(idealSizeArr[1], 10)
      };

      if (idealSizeObj.w >= idealSizeObj.h) {
        w = 300;
        h = w * idealSizeObj.h / idealSizeObj.w;
      } else {
        h = 300;
        w = h * idealSizeObj.w / idealSizeObj.h;
      }

      return {
        w: w,
        h: h
      };
    } else {
      console.warn('WARNING NO IDEAL SIZE INDICATED, PLEASE FIX');
      return {
        w: 300,
        h: 300
      };
    }
  }

pencilblueApp.directive('showHide', [function () {
  return {
    restrict: 'A',
    scope: {
      ngModel: '=',
      multiple: '=',
      identifier: '='
    },
    link: function(scope, element) {
      console.log(scope.ngModel);
      if (scope.ngModel) {
        if (scope.multiple) {
          var hide = 0;
          if (Array.isArray(scope.ngModel)) {
            scope.ngModel.forEach(function(i) {
              if (scope.identifier) {
                if (!i[scope.identifier]) hide++;
              } else {
                if (!i) hide++;
              }
            });
          } else {
            for (var i in scope.ngModel) {
              if (!scope.ngModel[i].length) hide++;
            }
          }

          if (hide === scope.ngModel.length || hide === Object.keys(scope.ngModel).length) element.remove();
        } else {
          if (!scope.ngModel || scope.ngModel === 'undefined') {
            element.remove();
          }
        }
      } else {
        element.remove();
      }
    }
  };
}]);

pencilblueApp.directive('navItem', function($rootScope, $state, $http) {
  return {
    restrict: 'A',
    scope: {
      item: '=',
      link: '@',
      page: '=',
      editPage: '&',
      removePage: '&',
    },
    template:
              '<i ng-show="!link" class="nav-handle fa fa-bars"></i>' +
              '<a ui-sref-active="active" ui-sref="page({ slug: item.slug })" ng-show="link">{{item.nav_title}}</a>' +
              '<span ng-hide="link" style="{{item.role === \'home\' ? \'padding-left: 30px;\' : \'\'}}">{{item.nav_title}}</span>' +
              '<i style="margin-left: 10px;" class="fa fa-pencil" ng-click="editPageName1(item, $index)" title="Edit Page Name" ng-show="!link"></i>',
    link: function(scope, element, attrs) {      
      scope.editPageName1 = function(item) {
        swal({
          title: 'Change Page Name',
          type: 'input',
          inputPlaceholder: item.nav_title,
          showCancelButton: true
        }, function(newPageName) {
          if (newPageName) {
            scope.editPage()(scope.item._id, newPageName);
          }
        });
      };

      scope.removePage1 = function(item) {
        scope.removePage()(item);
      };
    }
  };
});

function capitalize(string) {
  var output = string.split(' ');
  output.forEach(function(i, idx) {
    output[idx] = (i.charAt(0).toUpperCase() + i.substring(1));
  });

  return output.join(' ');
}

pencilblueApp.directive('footertext', ['GetSite', '$location', '$transitions', '$state', function(GetSite, $location, $transitions, $state) {
  return {
    restrict: 'E',
    scope: {
      _site: '=site',
      editing: '='
    },
    template: '<div class="footer-text-wrap" ng-if="show()" ng-cloak>'+
                '<p html-editable global="true" ng-model="_site.components.footerHtml[0]" tag="footerHtml" empty="Footer" ng-bind-html="_site.components.footerHtml[0].html || (editing ? placeholder : null)"></p>'+
                '<div id="footer-text" class="form-group" ng-if="editing && _site.theme !== 7" ng-cloak>'+
                  '<input type="checkbox" ng-model="_site.footerTextHomeOnly" ng-change="toggleFooterText()" />'+
                  '<span class="sitewide">&nbsp;<small>Show footer text only in home page</small></span>'+
                '</div>'+
              '</div>',
    link: function($scope) {
      $scope.onStateChange = function() {
        //var navOrder = $scope._site.navOrder.map(function(i) { return i.slug ? '/page/' + i.slug : '/' + i.title; }).filter(function(i) { return i !== 'home'; });
        
        $scope.show = function() {
          //todo never do stuff like this, unmaintainable          
          // if ($scope._site.theme === 7 || $scope.editing) {
          //   return true;
          // } else {
          //   return navOrder.indexOf($location.path()) > -1 ? !$scope._site.footerTextHomeOnly : true;
          // }
          return $scope._site.footerTextHomeOnly;
        };
      };

      $scope.onStateChange();
      $transitions.onSuccess( { to: '*', from: '*' }, $scope.onStateChange);

      
      
      $scope.placeholder = 'You can put anything you want in this space here -- a quote, a mantra, nothing, anything!<br><br>Click the blue button below to add your social media accounts and email!';
      $scope.toggleFooterText = function() {              
        GetSite.set({
          footerTextHomeOnly: $scope._site.footerTextHomeOnly
        }).then(function() {
          //$state.go($state.current, $state.params, {reload: true});
        });        
        
      };
    }
  };
}]);

//the modal directive to upload and image and crop
//the main navigation
angular.module('pencilblueApp')
  .directive('goto', ['$window', 'ComponentFactory', 'SiteData', '$uibModal', function($window, ComponentFactory, SiteData, $uibModal) {
    return {
      scope: {
        ngModel: '=',
        tag: '@'
      },
      restrict: 'A',
      replace: true,
      link: function(scope, elem, attrs) {
        var siteData = SiteData.getSiteData();        
        if (scope.ngModel && scope.ngModel.text) {
          elem.text(scope.ngModel.text);
        }        
        if (siteData.editing) {
          elem.on('click', function(e) {
            var modalInstance = $uibModal.open({
              animation: false,
              backdrop: 'static',
              ariaLabelledBy: 'modal-title',
              ariaDescribedBy: 'modal-body',
              templateUrl: '/public/performance/partials/goto.html',
              controllerAs: '$ctrl',
              controller: function($uibModalInstance, text, url) {
                var $ctrl = this;
                $ctrl.text = text;
                $ctrl.url = url;
                $ctrl.siteData = siteData;
                $ctrl.ok = function() {                
                  ComponentFactory.update({
                    text: $ctrl.text,
                    url: $ctrl.url,
                    tag: scope.tag
                  }).then(function(data) {
                    $uibModalInstance.close(data.data);                  
                  }).catch(function(err) {
                    if (err.data && err.data.message) {
                      alert(err.data.message);
                    } else {
                      alert('System Error');
                    }
                  });                
                };

                $ctrl.cancel = function() {
                  $uibModalInstance.dismiss('cancel');
                };
              },
              resolve: {
                text: function() {
                  return scope.ngModel ? scope.ngModel.text : '';
                },
                url: function() {
                  return scope.ngModel ? scope.ngModel.url : '';
                }                
              }
            });

            modalInstance.result.then(function(component) {
              scope.ngModel = component;
              if (component.text) {
                elem.text(component.text);
              }
              if (component.url) {
                //elem.text(component.text);
              }                
            }, function() {
              console.log('Modal dismissed at: ' + new Date());
            });

         
          });          
        } else {
          if (!scope.ngModel) {
            elem.remove();
          } else {
            if (scope.ngModel.url) {
              elem.attr('href', scope.ngModel.url);
            }            
          }
        }

      }
    };
  }]);

angular.module('pencilblueApp').component('videoElement', {
  bindings: {
    videoType: '<',
    video: '<'
  },
  templateUrl: '/public/performance/partials/video.html',
  controllerAs: '$video',
  controller: function($sce, $http) {
    var self = this;
    var youtubeThumbUrl = 'https://img.youtube.com/vi/{{videoId}}/hqdefault.jpg';
    var vimeoThumbUrl = 'https://developer.vimeo.com/api/v2/video/{{videoId}}.json';

    self.idRegs = {
      'youtube': /(embed\/)([a-zA-Z0-9_-]+)/i,
      'vimeo': /vimeo\.com\/video\/([a-zA-Z0-9_-]+)/i
    };

    if (self.video.url.match(self.idRegs.youtube)) {
      self.thumbnailUrl = youtubeThumbUrl.replace('{{videoId}}', self.video.url.match(self.idRegs.youtube)[2]);
    } else if (self.video.url.match(self.idRegs.vimeo)) {
      $http.post('/vimeoThumbnail', {videoId: self.video.url.match(self.idRegs.vimeo)[1]}).then(function(response) {
        self.thumbnailUrl = response.data[0].thumbnail_large;
      }).catch(function(err) {
        console.error(err);
      });
      // self.thumbnailUrl = youtubeThumbUrl.replace('{{videoId}}', self.video.url.match(self.idRegs.youtube)[2]);
    } else {
      getIncludeUrl.call(self);
    }

    this.getIncludeUrl = getIncludeUrl.bind(self);

    function getIncludeUrl() {
      console.log('videoType', self.videoType);
      if (self.videoType === 'iframe') {
        console.log('urlurlurl', self.video.url);
        self.videoFile = $sce.trustAsResourceUrl(self.video.url + '?autoplay=true');
        self.includeUrl = '/public/performance/partials/video-iframe.html';
      } else if (this.videoType === 'javascript') {
        self.includeUrl = '/public/performance/partials/video-js.html';
      }
    }
  }
});

angular.module('pencilblueApp').component('soundcloud', {
  bindings: {
    url: '<',
    title: '<'
  },
  template: '<div class="soundcloud"><div ng-bind-html="$sc.iframe"></div><h3>{{$sc.title}}</h3></div>',
  controllerAs: '$sc',
  controller: function($http, $sce) {
    var self = this;
    var soundcloudApi = 'http://soundcloud.com/oembed';

    $http.post('/soundcloud', {url: self.url}).then(function(response) {
      self.iframe = $sce.trustAsHtml(response.data.html);
    }).catch(function(err) {
      console.error(err);
    });
  }
});

pencilblueApp.component('expandingGallery', {
  bindings: {
    gallery: '=',
    nocrop: '='
  },
  template: '<div id="expanding-gallery">'+
                
								'<h1 class="text-center gallery-title">{{$exp.detailSection.name || "GALLERY"}}</h1>' +
								'<h2 ng-show="$exp.detailSection.subtitle" class="gallery-subtitle text-center">{{$exp.detailSection.subtitle || ""}}</h2>' +
                '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>'+
                '<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.css"/>'+
                '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick-theme.min.css" />'+
                '<div class="gallery-overview" ng-if="!$exp.detailSection">'+
                  '<ul>'+
										'<li ng-repeat="section in $exp.gallery" class="gallery-section" ng-click="$exp.expandSection(section)">'+
                      '<div class="main-img-holder">' +
												'<img ng-src="{{$exp.nocrop ? section.images[0].url : (section.images[0].url | expGalleryImage)}}" alt="" />'+
											'</div>' +
                      '<div class="details">'+
                        '<div class="section-name">{{section.name}}</div>'+
                        '<div class="section-subtitle">{{section.subtitle}}</div>'+
                        '<div class="section-view">view photos&nbsp;<i class="fa fa-caret-right"></i></div>'+
                      '</div>'+
                    '</li>'+
                  '</ul>'+
                '</div>'+
                '<div class="section-detail" ng-if="$exp.detailSection">'+
                  '<p class="text-center" ng-click="$exp.goBack()"><i class="fa fa-chevron-left"></i>&nbsp;Return to Gallery</p>'+
                  '<ul>'+
                    '<li ng-repeat="image in $exp.detailSection.images track by image._id" ng-click="$exp.openLightBox($index)">'+
                      '<div class="flex-column-start">'+
                        '<img ng-src="{{image.thumbnail}}" alt="" />'+
                        '<p ng-if="image.title" ng-cloak>{{image.title}}</p>'+
                        '<p ng-if="image.credit" ng-cloak>{{image.credit}}</p>'+
                      '</div>'+
                    '</li>'+
                  '</ul>'+
                '</div>'+
                '<div class="overlay" ng-if="$exp.lightBoxImages" ng-click="$exp.dismiss()">'+
                  '<div class="fa fa-close" ng-click="$exp.dismiss()"></div>'+
                  '<div class="wrapper" ng-click="$event.stopPropagation()">'+
                    '<ul id="light-box-images">'+
                      '<li ng-repeat="image in $exp.lightBoxImages" class="light-box-image" ng-cloak>'+
                        '<img ng-src="{{image.url}}" alt="" />'+
                      '</li>'+
                    '</ul>'+
                  '</div>'+
                  '<script>'+
                    'if ($().slick) {'+
                      'imagesLoaded($("#light-box-images"), function() {'+
                        '$("#light-box-images").slick({'+
                            'infinite: true,'+
                            'accessibility: true'+
                        '});'+
                      '});'+
                    '}'+
                  '</script>'+
                '</div>'+
              '</div>',
  controllerAs: '$exp',
  controller: function($location) {
    var self = this;
    console.log('self expandingGallery', self);

    self.expandSection = function(section) {
        self.detailSection = section;
        $location.search({section: section._id});
    };

    if ($location.search().section) {
      var section = _.find(self.gallery, function(g) { return g._id === $location.search().section; });
      if (section) self.expandSection(section);
    }

    self.goBack = function() {
      self.detailSection = null;
      $location.search({section: null});
    };

    self.openLightBox = function(index) {
      self.lightBoxImages = self.detailSection.images.slice(index).concat(self.detailSection.images.slice(0,index));
    };

    self.dismiss = function() {
      self.lightBoxImages = null;
    };
  }
}).filter('expGalleryImage', function() {
  return function(input, uppercase) {
    if (!input || !input.length) return input;
    var split = input.split(/\/(?!\/)/);
    var sizing = 'c_crop,g_faces,w_650,h_390';
    var idx = split.indexOf('upload');

    if (idx > -1) {
      split.splice(idx + 1, 0, sizing);
      console.log(split.join('/'));
      return split.join('/');
    } else {
      return input;
    }
  };
});

/**
 * Color Palette Constants
 * @type {Object} themeId: {
 *  @type {Object} paletteId: {
 *    @type {String} selector: 'style'
 *  }
 * }
 */

pencilblueApp.constant('colorPalettes', {
  // Theme 1
  1: {
    1: {
      background_color: '#181818',
      header_background_color: '#000',
      color: '#fff',
      nav_text_color: '#fff',
      nav_active_color: '#545454',
      navbar_background: 'rgba(0,0,0,0)',
      link_color: '#C8C8CF',
      link_hover_color: '#C2C2E5',
      button_text_color: '#000',
      button_bg_color: '#fff',
      social_background_color: '#181818',
      social_font_color: '#fff',
      title_color: '#fff'
    },
    2: {
      background_color: '#f5f5f5',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#a5a5a5',
      link_color: '#a5a5a5',
      header_background_color: '#fff',
      nav_text_color: '#333',
      nav_active_color: '#e0dfdf',
      navbar_background: 'rgba(0,0,0,0)',
      social_background_color: '#333',
      social_font_color: '#fff',
      title_color: '#333',
      link_hover_color: '#5b5b99'
    },
    3: {
      background_color: '#f4f4f4',
      color: '#333',
      color_footer: '#f4f4f4',
      button_text_color: '#fff',
      button_bg_color: '#c0b283',
      link_color: '#c0b283',
      header_background_color: '#373737',
      nav_text_color: '#c0b283',
      nav_active_color: '#222',
      navbar_background: 'rgba(0,0,0,0)',
      social_background_color: '#dcd0d0',
      social_font_color: '#373737',
      title_color: '#c0b283',
      link_hover_color: '#a18834'
    },
    4: {
      background_color: '#0b3c5d',
      color: '#ebebeb',
      color_footer: '#ebebeb',
      button_text_color: '#fff',
      button_bg_color: '#d9b310',
      link_color: '#328cc1',
      header_background_color: '#1d2731',
      nav_text_color: '#ebebeb',
      nav_active_color: '#d9b310',
      navbar_background: 'rgba(0,0,0,0)',
      social_background_color: '#328cc1',
      social_font_color: '#fff',
      title_color: '#fff',
      link_hover_color: '#d9b310'
    },
    5: {
      background_color: '#912141',
      color: '#f5f5f5',
      color_footer: '#f5f5f5',
      button_text_color: '#f5f5f5',
      button_bg_color: '#699aa9',
      link_color: '#94c7d7',
      header_background_color: '#7d1935',
      nav_text_color: '#f5f5f5',
      nav_active_color: '#699aa9',
      navbar_background: 'rgba(0,0,0,0)',
      social_background_color: '#699aa9',
      social_font_color: '#f5f5f5',
      title_color: '#f5f5f5',
      link_hover_color: '#f5f5f5'
    },
    6: {
      background_color: '#dbdbdb',
      color: '#333',
      color_footer: '#fff',
      button_text_color: '#fff',
      button_bg_color: '#4e4c4e',
      link_color: '#4e4c4e',
      header_background_color: '#595959',
      nav_text_color: '#fff',
      nav_active_color: '#4e4c4e',
      navbar_background: 'rgba(0,0,0,0)',
      social_background_color: '#fff',
      social_font_color: '#4e4c4e',
      title_color: '#fff',
      link_hover_color: '#000'
    },
    7: {
      background_color: '#fafafa',
      color: '#0f1626',
      color_footer: '#fff',
      button_text_color: '#fff',
      button_bg_color: '#ff533d',
      link_color: '#ff533d',
      header_background_color: '#0f1626',
      nav_text_color: '#fff',
      nav_active_color: '#ff533d',
      navbar_background: 'rgba(0,0,0,0)',
      social_background_color: '#ff533d',
      social_font_color: '#fff',
      title_color: '#fff',
      link_hover_color: '#d13420'
    },
    8: {
      background_color: '#cae4db',
      color: '#00303f',
      color_footer: '#fff',
      button_text_color: '#fff',
      button_bg_color: '#dcae1d',
      link_color: '#908768',
      header_background_color: '#7a9a96',
      nav_text_color: '#fff',
      nav_active_color: '#00303f',
      navbar_background: 'rgba(0,0,0,0)',
      social_background_color: '#00303f',
      social_font_color: '#fff',
      title_color: '#fff',
      link_hover_color: '#dcae1d'
    }
  },
  // End theme 1

  // Theme 2
  2: {
    1: {
      background_color: '#181818',
      color: '#fff',
      color_footer: '#fff',
      button_text_color: '#000',
      button_bg_color: '#fff',
      link_color: '#c8c8cf',
      header_background_color: '#181818',
      nav_text_color: '#fff',
      nav_active_color: '#545454',
      navbar_background: '#000',
      social_background_color: '#000',
      social_font_color: '#fff',
      title_color: '#fff',
      link_hover_color: '#C2C2E5'
    },
    2: {
      background_color: '#fff',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#a0a0a8',
      link_color: '#a0a0a8',
      header_background_color: '#fff',
      nav_text_color: '#333',
      nav_active_color: '#e0dfdf',
      navbar_background: '#f0f0f0',
      social_background_color: '#333',
      social_font_color: '#fff',
      title_color: '#333',
      link_hover_color: '#5b5b99'
    },
    3: {
      background_color: '#f4f4f4',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#c0b283',
      link_color: '#c0b283',
      header_background_color: '#f4f4f4',
      nav_text_color: '#f4f4f4',
      nav_active_color: '#222',
      navbar_background: '#373737',
      social_background_color: '#dcd0d0',
      social_font_color: '#373737',
      title_color: '#c0b283',
      link_hover_color: '#a18834'
    },
    4: {
      background_color: '#0b3c5d',
      color: '#ebebeb',
      color_footer: '#ebebeb',
      button_text_color: '#fff',
      button_bg_color: '#d9b310',
      link_color: '#328cc1',
      header_background_color: '#0b3c5d',
      nav_text_color: '#fff',
      nav_active_color: '#1d2731',
      navbar_background: '#1d2731',
      social_background_color: '#328cc1',
      social_font_color: '#fff',
      title_color: '#fff',
      link_hover_color: '#d9b310'
    },
    5: {
      background_color: '#7d1935',
      color: '#f5f5f5',
      color_footer: '#f5f5f5',
      button_text_color: '#f5f5f5',
      button_bg_color: '#699aa9',
      link_color: '#d7cec7',
      header_background_color: '#7d1935',
      nav_text_color: '#565656',
      nav_active_color: '#d7cec7',
      navbar_background: '#f5f5f5',
      social_background_color: '#699aa9',
      social_font_color: '#f5f5f5',
      title_color: '#f5f5f5',
      link_hover_color: '#f5f5f5'
    },
    6: {
      background_color: '#dbdbdb',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#4e4c4e',
      link_color: '#4e4c4e',
      header_background_color: '#dbdbdb',
      nav_text_color: '#4e4c4e',
      nav_active_color: '#fff',
      navbar_background: '#fff',
      social_background_color: '#4e4c4e',
      social_font_color: '#fff',
      title_color: '#4e4c4e',
      link_hover_color: '#000'
    },
    7: {
      background_color: '#fafafa',
      color: '#0f1626',
      color_footer: '#0f1626',
      button_text_color: '#fff',
      button_bg_color: '#ff533d',
      link_color: '#ff533d',
      header_background_color: '#fafafa',
      nav_text_color: '#fff',
      nav_active_color: '#ff533d',
      navbar_background: '#0f1626',
      social_background_color: '#ff533d',
      social_font_color: '#fff',
      title_color: '#0f1626',
      link_hover_color: '#d13420'
    },
    8: {
      background_color: '#cae4db',
      color: '#00303f',
      color_footer: '#00303f',
      button_text_color: '#fff',
      button_bg_color: '#dcae1d',
      link_color: '#908768',
      header_background_color: '#cae4db',
      nav_text_color: '#fff',
      nav_active_color: '#00303f',
      navbar_background: '#7a9a96',
      social_background_color: '#00303f',
      social_font_color: '#fff',
      title_color: '#00303f',
      link_hover_color: '#dcae1d'
    }
  },
  // End theme 2

  // Theme 3
  3: {
    1: {
      background_color: '#181818',
      color: '#fff',
      color_footer: '#f4f4f4',
      button_text_color: '#000',
      button_bg_color: '#000',
      link_color: '#c8c8cf',
      header_background_color: '#000',
      nav_text_color: '#fff',
      nav_active_color: '#545454',
      navbar_background: '#262525',
      social_background_color: '#545454',
      social_font_color: '#fff',
      title_color: '#fff',
      link_hover_color: '#C2C2E5'
    },
    2: {
      background_color: '#fff',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#a0a0a8',
      link_color: '#a0a0a8',
      header_background_color: '#fff',
      nav_text_color: '#333',
      nav_active_color: '#e0dfdf',
      navbar_background: '#f0f0f0',
      social_background_color: '#333',
      social_font_color: '#fff',
      title_color: '#333',
      link_hover_color: '#5b5b99'
    },
    3: {
      background_color: '#f4f4f4',
      color: '#333',
      color_footer: '#fff',
      button_text_color: '#fff',
      button_bg_color: '#c0b283',
      link_color: '#c0b283',
      header_background_color: '#373737',
      nav_text_color: '#373737',
      nav_active_color: '#c0b283',
      navbar_background: '#fff',
      social_background_color: '#dcd0c0',
      social_font_color: '#373737',
      title_color: '#fff',
      link_hover_color: '#b39d52'
    },
    4: {
      background_color: '#0b3c5d',
      color: '#ebebeb',
      color_footer: '#ebebeb',
      button_text_color: '#ebebeb',
      button_bg_color: '#d9b310',
      link_color: '#328cc1',
      header_background_color: '#2c72a1',
      nav_text_color: '#fff',
      nav_active_color: '#d9b310',
      navbar_background: '#1d2731',
      social_background_color: '#328cc1',
      social_font_color: '#fff',
      title_color: '#fff',
      link_hover_color: '#d9b310'
    },
    5: {
      background_color: '#7d1935',
      color: '#f3f3ee',
      color_footer: '#2e1b13',
      button_text_color: '#f3f3ee',
      button_bg_color: '#699aa9',
      link_color: '#94c7d7',
      header_background_color: '#f5f3ee',
      nav_text_color: '#f3f3ee',
      nav_active_color: '#699aa9',
      navbar_background: '#2e1b13',
      social_background_color: '#699aa9',
      social_font_color: '#2e1b13',
      title_color: '#3b251c',
      link_hover_color: '#f5f3ee'
    },
    6: {
      background_color: '#dbdbdb',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#4e4c4e',
      link_color: '#4e4c4e',
      header_background_color: '#dbdbdb',
      nav_text_color: '#4e4c4e',
      nav_active_color: '#fff',
      navbar_background: '#dbdbdb',
      social_background_color: '#4e4c4e',
      social_font_color: '#fff',
      title_color: '#000',
      link_hover_color: '#000'
    },
    7: {
      background_color: '#fafafa',
      color: '#0f1626',
      color_footer: '#0f1626',
      button_text_color: '#fff',
      button_bg_color: '#ff533d',
      link_color: '#ff533d',
      header_background_color: '#fafafa',
      nav_text_color: '#fff',
      nav_active_color: '#ff533d',
      navbar_background: '#0f1626',
      social_background_color: '#ff533d',
      social_font_color: '#fff',
      title_color: '#0f1626',
      link_hover_color: '#d13420'
    },
    8: {
      background_color: '#cae4db',
      color: '#00303f',
      color_footer: '#00303f',
      button_text_color: '#fff',
      button_bg_color: '#dcae1d',
      link_color: '#908768',
      header_background_color: '#cae4db',
      nav_text_color: '#fff',
      nav_active_color: '#00303f',
      navbar_background: '#7a9a96',
      social_background_color: '#00303f',
      social_font_color: '#fff',
      title_color: '#00303f',
      link_hover_color: '#dcae1d'
    }
  },
  // End theme 3

  // Theme 4
  4: {
    1: {
      background_color: '#000',
      color: '#fff',
      color_footer: '#fff',
      button_text_color: '#000',
      button_bg_color: '#fff',
      link_color: '#c8c8cf',
      header_background_color: '#000',
      nav_text_color: '#e6e6e6',
      nav_active_color: '#262525',
      navbar_background: '#000',
      social_background_color: '#fff',
      social_font_color: '#000',
      title_color: '#fff',
      link_hover_color: '#C2C2E5'
    },
    2: {
      background_color: '#fff',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#888',
      link_color: '#5b5b99',
      header_background_color: '#fff',
      nav_text_color: '#888',
      nav_active_color: '#e0dfdf',
      navbar_background: '#fff',
      social_background_color: '#333',
      social_font_color: '#fff',
      title_color: '#000',
      link_hover_color: '#a0a0a8'
    },
    3: {
      background_color: '#f4f4f4',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#c0b283',
      link_color: '#908768',
      header_background_color: '#f4f4f4',
      nav_text_color: '#fff',
      nav_active_color: '#373737',
      navbar_background: '#c0b283',
      social_background_color: '#dcd0d0',
      social_font_color: '#373737',
      title_color: '#373737',
      link_hover_color: '#c0b283'
    },
    4: {
      background_color: '#0b3c5d',
      color: '#ebebeb',
      color_footer: '#ebebeb',
      button_text_color: '#ebebeb',
      button_bg_color: '#d9b310',
      link_color: '#328cc1',
      header_background_color: '#0b3c5d',
      nav_text_color: '#fff',
      nav_active_color: '#d9b310',
      navbar_background: '#1d2731',
      social_background_color: '#328cc1',
      social_font_color: '#fff',
      title_color: '#fff',
      link_hover_color: '#d9b310'
    },
    5: {
      background_color: '#7d1935',
      color: '#f5f5f5',
      color_footer: '#f5f5f5',
      button_text_color: '#f5f5f5',
      button_bg_color: '#699aa9',
      link_color: '#94c7d7',
      header_background_color: '#7d1935',
      nav_text_color: '#f5f5f5',
      nav_active_color: '#9a3f58',
      navbar_background: '#7d1935',
      social_background_color: '#699aa9',
      social_font_color: '#2e1b13',
      title_color: '#f5f5f5',
      link_hover_color: '#f5f5f5'
    },
    6: {
      background_color: '#dbdbdb',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#4e4c4e',
      link_color: '#4e4c4e',
      header_background_color: '#dbdbdb',
      nav_text_color: '#4e4c4e',
      nav_active_color: '#fff',
      navbar_background: '#fff',
      social_background_color: '#4e4c4e',
      social_font_color: '#fff',
      title_color: '#000',
      link_hover_color: '#000'
    },
    7: {
      background_color: '#fafafa',
      color: '#0f1626',
      color_footer: '#0f1626',
      button_text_color: '#fff',
      button_bg_color: '#ff533d',
      link_color: '#ff533d',
      header_background_color: '#fafafa',
      nav_text_color: '#fff',
      nav_active_color: '#ff533d',
      navbar_background: '#0f1626',
      social_background_color: '#ff533d',
      social_font_color: '#fff',
      title_color: '#0f1626',
      link_hover_color: '#d13420'
    },
    8: {
      background_color: '#cae4db',
      color: '#00303f',
      color_footer: '#00303f',
      button_text_color: '#fff',
      button_bg_color: '#dcae1d',
      link_color: '#908768',
      header_background_color: '#cae4db',
      nav_text_color: '#fff',
      nav_active_color: '#00303f',
      navbar_background: '#7a9a96',
      social_background_color: '#00303f',
      social_font_color: '#fff',
      title_color: '#00303f',
      link_hover_color: '#dcae1d'
    }
  },
  // End theme 4

  // Theme 5
  5: {
    1: {
      background_color: '#181818',
      color: '#fff',
      color_footer: '#fff',
      button_text_color: '#000',
      button_bg_color: '#fff',
      link_color: '#c8c8cf',
      header_background_color: '#000',
      nav_text_color: '#fff',
      nav_active_color: '#545454',
      navbar_background: '#181818',
      social_background_color: '#181818',
      social_font_color: '#fff',
      title_color: '#fff',
      link_hover_color: '#C2C2E5'
    },
    2: {
      background_color: '#fff',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#a0a0a8',
      link_color: '#a0a0a8',
      header_background_color: '#fff',
      nav_text_color: '#333',
      nav_active_color: '#e0dfdf',
      navbar_background: '#f0f0f0',
      social_background_color: '#333',
      social_font_color: '#fff',
      title_color: '#333',
      link_hover_color: '#5b5b99'
    },
    3: {
      background_color: '#f4f4f4',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#c0b283',
      link_color: '#c0b283',
      header_background_color: '#f4f4f4',
      nav_text_color: '#f4f4f4',
      nav_active_color: '#c0b283',
      navbar_background: '#373737',
      social_background_color: '#dcd0d0',
      social_font_color: '#373737',
      title_color: '#333',
      link_hover_color: '#a18834'
    },
    4: {
      background_color: '#0b3c5d',
      color: '#ebebeb',
      color_footer: '#ebebeb',
      button_text_color: '#fff',
      button_bg_color: '#d9b310',
      link_color: '#328cc1',
      header_background_color: '#0b3c5d',
      nav_text_color: '#fff',
      nav_active_color: '#d9b310',
      navbar_background: '#1d2731',
      social_background_color: '#328cc1',
      social_font_color: '#fff',
      title_color: '#fff',
      link_hover_color: '#d9b310'
    },
    5: {
      background_color: '#7d1935',
      color: '#f5f5f5',
      color_footer: '#f5f5f5',
      button_text_color: '#2e1b13',
      button_bg_color: '#699aa9',
      link_color: '#94c7d7',
      header_background_color: '#7d1935',
      nav_text_color: '#f5f5f5',
      nav_active_color: '#2e1b13',
      navbar_background: '#9a3f58',
      social_background_color: '#699aa9',
      social_font_color: '#2e1b13',
      title_color: '#f5f5f5',
      link_hover_color: '#f5f5f5'
    },
    6: {
      background_color: '#dbdbdb',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#4e4c4e',
      link_color: '#4e4c4e',
      header_background_color: '#dbdbdb',
      nav_text_color: '#595959',
      nav_active_color: '#fff',
      navbar_background: '#dbdbdb',
      social_background_color: '#4e4c4e',
      social_font_color: '#fff',
      title_color: '#595959',
      link_hover_color: '#000'
    },
    7: {
      background_color: '#fafafa',
      color: '#0f1626',
      color_footer: '#0f1626',
      button_text_color: '#fff',
      button_bg_color: '#ff533d',
      link_color: '#ff533d',
      header_background_color: '#fafafa',
      nav_text_color: '#fff',
      nav_active_color: '#ff533d',
      navbar_background: '#0f1626',
      social_background_color: '#ff533d',
      social_font_color: '#fff',
      title_color: '#0f1626',
      link_hover_color: '#d13420'
    },
    8: {
      background_color: '#cae4db',
      color: '#00303f',
      color_footer: '#00303f',
      button_text_color: '#fff',
      button_bg_color: '#dcae1d',
      link_color: '#908768',
      header_background_color: '#cae4db',
      nav_text_color: '#fff',
      nav_active_color: '#00303f',
      navbar_background: '#7a9a96',
      social_background_color: '#00303f',
      social_font_color: '#fff',
      title_color: '#00303f',
      link_hover_color: '#dcae1d'
    }
  },
  // end theme 5

  // Theme 6
  6: {
    1: {
      background_color: '#181818',
      color: '#fff',
      color_footer: '#fff',
      button_text_color: '#000',
      button_bg_color: '#fff',
      link_color: '#c8c8cf',
      header_background_color: '#181818',
      nav_text_color: '#fff',
      nav_active_color: '#545454',
      navbar_background: '#000',
      social_background_color: '#000',
      social_font_color: '#fff',
      title_color: '#fff',
      link_hover_color: '#C2C2E5',
      about_background: '#ccc'
    },
    2: {
      background_color: '#fff',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#a0a0a8',
      link_color: '#a0a0a8',
      header_background_color: '#fff',
      nav_text_color: '#333',
      nav_active_color: '#e0dfdf',
      navbar_background: '#f0f0f0',
      social_background_color: '#333',
      social_font_color: '#fff',
      title_color: '#333',
      link_hover_color: '#5b5b99',
      about_background: '#ccc'
    },
    3: {
      background_color: '#f4f4f4',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#c0b283',
      link_color: '#c0b283',
      header_background_color: '#f4f4f4',
      nav_text_color: '#f4f4f4',
      nav_active_color: '#c0b283',
      navbar_background: '#373737',
      social_background_color: '#373737',
      social_font_color: '#c0b283',
      title_color: '#222',
      link_hover_color: '#a18834',
      about_background: '#ccc'
    },
    4: {
      background_color: '#0b3c5d',
      color: '#ebebeb',
      color_footer: '#ebebeb',
      button_text_color: '#fff',
      button_bg_color: '#d9b310',
      link_color: '#328cc1',
      header_background_color: '#0b3c5d',
      nav_text_color: '#fff',
      nav_active_color: '#d9b310',
      navbar_background: '#1d2731',
      social_background_color: '#328cc1',
      social_font_color: '#fff',
      title_color: '#fff',
      link_hover_color: '#d9b310',
      about_background: '#ccc'
    },
    5: {
      background_color: '#7d1935',
      color: '#f5f5f5',
      color_footer: '#f5f5f5',
      button_text_color: '#f5f5f5',
      button_bg_color: '#699aa9',
      link_color: '#d7cec7',
      header_background_color: '#7d1935',
      nav_text_color: '#f5f5f5',
      nav_active_color: '#699aa9',
      navbar_background: '#2e1b13',
      social_background_color: '#699aa9',
      social_font_color: '#f5f5f5',
      title_color: '#f5f5f5',
      link_hover_color: '#f5f5f5',
      about_background: '#ccc'
    },
    6: {
      background_color: '#dbdbdb',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#4e4c4e',
      link_color: '#4e4c4e',
      header_background_color: '#dbdbdb',
      nav_text_color: '#fff',
      nav_active_color: '#4e4c4e',
      navbar_background: '#595959',
      social_background_color: '#4e4c4e',
      social_font_color: '#fff',
      title_color: '#4e4c4e',
      link_hover_color: '#000',
      about_background: '#ccc'
    },
    7: {
      background_color: '#fafafa',
      color: '#0f1626',
      color_footer: '#0f1626',
      button_text_color: '#fff',
      button_bg_color: '#ff533d',
      link_color: '#ff533d',
      header_background_color: '#fafafa',
      nav_text_color: '#fff',
      nav_active_color: '#ff533d',
      navbar_background: '#0f1626',
      social_background_color: '#ff533d',
      social_font_color: '#fff',
      title_color: '#0f1626',
      link_hover_color: '#d13420',
      about_background: '#ccc'
    },
    8: {
      background_color: '#cae4db',
      color: '#00303f',
      color_footer: '#00303f',
      button_text_color: '#fff',
      button_bg_color: '#dcae1d',
      link_color: '#908768',
      header_background_color: '#cae4db',
      nav_text_color: '#fff',
      nav_active_color: '#00303f',
      navbar_background: '#7a9a96',
      social_background_color: '#00303f',
      social_font_color: '#fff',
      title_color: '#00303f',
      link_hover_color: '#dcae1d',
      about_background: '#ccc'
    }
  },
  // theme 7
  7: {
    1: {
      background_color: '#181818',
      header_background_color: '#000',
      color: '#fff',
      nav_text_color: '#fff',
      nav_active_color: '#545454',
      navbar_background: 'rgba(0,0,0,0)',
      link_color: '#C8C8CF',
      link_hover_color: '#C2C2E5',
      button_text_color: '#000',
      button_bg_color: '#fff',
      social_background_color: '#181818',
      social_font_color: '#fff',
      title_color: '#fff'
    },
    2: {
      background_color: '#f5f5f5',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#a5a5a5',
      link_color: '#a5a5a5',
      header_background_color: '#fff',
      nav_text_color: '#333',
      nav_active_color: '#e0dfdf',
      navbar_background: 'rgba(0,0,0,0)',
      social_background_color: '#333',
      social_font_color: '#fff',
      title_color: '#333',
      link_hover_color: '#5b5b99'
    },
    3: {
      background_color: '#f4f4f4',
      color: '#333',
      color_footer: '#f4f4f4',
      button_text_color: '#fff',
      button_bg_color: '#c0b283',
      link_color: '#c0b283',
      header_background_color: '#373737',
      nav_text_color: '#c0b283',
      nav_active_color: '#222',
      navbar_background: 'rgba(0,0,0,0)',
      social_background_color: '#dcd0d0',
      social_font_color: '#373737',
      title_color: '#373737',
      link_hover_color: '#a18834'
    },
    4: {
      background_color: '#0b3c5d',
      color: '#ebebeb',
      color_footer: '#ebebeb',
      button_text_color: '#fff',
      button_bg_color: '#d9b310',
      link_color: '#328cc1',
      header_background_color: '#1d2731',
      nav_text_color: '#ebebeb',
      nav_active_color: '#d9b310',
      navbar_background: 'rgba(0,0,0,0)',
      social_background_color: '#328cc1',
      social_font_color: '#fff',
      title_color: '#fff',
      link_hover_color: '#d9b310'
    },
    5: {
      background_color: '#912141',
      color: '#f5f5f5',
      color_footer: '#f5f5f5',
      button_text_color: '#f5f5f5',
      button_bg_color: '#699aa9',
      link_color: '#94c7d7',
      header_background_color: '#7d1935',
      nav_text_color: '#f5f5f5',
      nav_active_color: '#699aa9',
      navbar_background: 'rgba(0,0,0,0)',
      social_background_color: '#699aa9',
      social_font_color: '#f5f5f5',
      title_color: '#f5f5f5',
      link_hover_color: '#f5f5f5'
    },
    6: {
      background_color: '#dbdbdb',
      color: '#333',
      color_footer: '#fff',
      button_text_color: '#fff',
      button_bg_color: '#4e4c4e',
      link_color: '#4e4c4e',
      header_background_color: '#595959',
      nav_text_color: '#333',
      nav_active_color: '#4e4c4e',
      navbar_background: 'rgba(0,0,0,0)',
      social_background_color: '#fff',
      social_font_color: '#4e4c4e',
      title_color: '#333',
      link_hover_color: '#000'
    },
    7: {
      background_color: '#fafafa',
      color: '#0f1626',
      color_footer: '#fff',
      button_text_color: '#fff',
      button_bg_color: '#ff533d',
      link_color: '#ff533d',
      header_background_color: '#0f1626',
      nav_text_color: '#000',
      nav_active_color: '#ff533d',
      navbar_background: 'rgba(0,0,0,0)',
      social_background_color: '#ff533d',
      social_font_color: '#fff',
      title_color: '#000',
      link_hover_color: '#d13420'
    },
    8: {
      background_color: '#cae4db',
      color: '#00303f',
      color_footer: '#fff',
      button_text_color: '#fff',
      button_bg_color: '#dcae1d',
      link_color: '#908768',
      header_background_color: '#7a9a96',
      nav_text_color: '#7a9a96',
      nav_active_color: '#00303f',
      navbar_background: 'rgba(0,0,0,0)',
      social_background_color: '#00303f',
      social_font_color: '#fff',
      title_color: '#00303f',
      link_hover_color: '#dcae1d'
    }
  },
  // theme 8
  8: {
    1: {
      background_color: '#181818',
      header_background_color: '#181818',
      color: '#fff',
      nav_text_color: '#fff',
      nav_active_color: '#545454',
      navbar_background: '#000',
      link_color: '#C8C8CF',
      link_hover_color: '#C2C2E5',
      button_text_color: '#000',
      button_bg_color: '#fff',
      social_background_color: '#000',
      social_font_color: '#fff',
      title_color: '#fff'
    },
    2: {
      background_color: '#f5f5f5',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#a5a5a5',
      link_color: '#a5a5a5',
      header_background_color: '#f5f5f5',
      nav_text_color: '#333',
      nav_active_color: '#e0dfdf',
      navbar_background: '#fff',
      social_background_color: '#333',
      social_font_color: '#fff',
      title_color: '#222',
      link_hover_color: '#5b5b99'
    },
    3: {
      background_color: '#f4f4f4',
      color: '#333',
      color_footer: '#373737',
      button_text_color: '#fff',
      button_bg_color: '#c0b283',
      link_color: '#c0b283',
      header_background_color: '#f4f4f4',
      nav_text_color: '#c0b283',
      nav_active_color: '#222',
      navbar_background: '#373737',
      social_background_color: '#dcd0d0',
      social_font_color: '#373737',
      title_color: '#222',
      link_hover_color: '#a18834'
    },
    4: {
      background_color: '#0b3c5d',
      color: '#ebebeb',
      color_footer: '#ebebeb',
      button_text_color: '#fff',
      button_bg_color: '#d9b310',
      link_color: '#328cc1',
      header_background_color: '#0b3c5d',
      nav_text_color: '#ebebeb',
      nav_active_color: '#d9b310',
      navbar_background: '#1d2731',
      social_background_color: '#328cc1',
      social_font_color: '#fff',
      title_color: '#fff',
      link_hover_color: '#d9b310'
    },
    5: {
      background_color: '#912141',
      color: '#f5f5f5',
      color_footer: '#f5f5f5',
      button_text_color: '#f5f5f5',
      button_bg_color: '#699aa9',
      link_color: '#94c7d7',
      header_background_color: '#912141',
      nav_text_color: '#f5f5f5',
      nav_active_color: '#699aa9',
      navbar_background: '#7d1935',
      social_background_color: '#699aa9',
      social_font_color: '#f5f5f5',
      title_color: '#f5f5f5',
      link_hover_color: '#f5f5f5'
    },
    6: {
      background_color: '#dbdbdb',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#4e4c4e',
      link_color: '#4e4c4e',
      header_background_color: '#dbdbdb',
      nav_text_color: '#fff',
      nav_active_color: '#4e4c4e',
      navbar_background: '#595959',
      social_background_color: '#fff',
      social_font_color: '#4e4c4e',
      title_color: '#333',
      link_hover_color: '#000'
    },
    7: {
      background_color: '#fafafa',
      color: '#0f1626',
      color_footer: '#0f1626',
      button_text_color: '#fff',
      button_bg_color: '#ff533d',
      link_color: '#ff533d',
      header_background_color: '#fafafa',
      nav_text_color: '#fff',
      nav_active_color: '#ff533d',
      navbar_background: '#0f1626',
      social_background_color: '#ff533d',
      social_font_color: '#fff',
      title_color: '#0f1626',
      link_hover_color: '#d13420'
    },
    8: {
      background_color: '#cae4db',
      color: '#00303f',
      color_footer: '#00303f',
      button_text_color: '#fff',
      button_bg_color: '#dcae1d',
      link_color: '#908768',
      header_background_color: '#cae4db',
      nav_text_color: '#fff',
      nav_active_color: '#00303f',
      navbar_background: '#7a9a96',
      social_background_color: '#00303f',
      social_font_color: '#fff',
      title_color: '#00303f',
      link_hover_color: '#dcae1d'
    }
  },

});

pencilblueApp.directive('gRecaptcha', function() {
  return {
    restrict: 'E',
    template: '<div id="grecaptcha-element"></div>',
    controllerAs: '$gRecap',
    scope: {
      callback: '&'
    },
    link: function(scope, el) {
      var elem = el.find('#contact-submit-btn');
      console.log('the elem: ', elem);
      var self = this;
      var id;

      self.checkTheCaptcha = function(token) {
        console.log('checkTheCaptcha: ', token);
        scope.callback({token: token});
      };

      var isCaptchaReady = function() {
        if (typeof grecaptcha !== 'object') {
          console.log('grecaptcha no good try again');
          return setTimeout(isCaptchaReady, 300);
        }

        if (!id) {
          id = grecaptcha.render('grecaptcha-element', {
            sitekey: '6LcGUTEUAAAAAD3r9K0NARXgGHQ46qOmU-1R0Voz',
            callback: self.checkTheCaptcha,
            size: 'normal'
          });
        }
      };

      isCaptchaReady();
    }
  };
});

angular.module('pencilblueApp').component('contactForm', {
  templateUrl: function(SiteData, $state) {
    return '/public/performance/themes/partials/contact-form.html';
  },
  controllerAs: '$ctrl',
  controller: function($http) {

    var self = this;
    self.info = {};
    self.registerToken = function(token) {
      console.log('registering token', token);
      self.token = token;
    }

    self.submitForm = function() {
      console.log('SUBMIT FORM');
      $http.post('/api/contact', {
        name: self.formData.name,
        message: self.formData.message,
        fromEmail: self.formData.fromEmail,
        gRecaptchaResponse: self.token
      }).then(function(res) {
        if (res.status >= 200 && res.status < 300) {
          if (self.info.error) self.info.error = null;
          self.alertClass = 'alert-success';
          self.info.success = 'Thanks! Your email was sent and I will get back to you as soon as possible!'; 
          self.showAlert = true;
          console.log('SHOULD MAKE VALID AGAIN');
 
          self.formData = {};
          self.contactForm.$setPristine();
          self.contactForm.$setUntouched();
        } else {
          var error = JSON.parse(res.data.body);
          console.error(error);
          var text = 'It seems there was a problem sending an email: <pre>STATUS CODE: ' + res.data.statusCode + '\n' + error.errors[0].message + '</pre> You may try to contact me at: <a href="mailto:' + self.page.social.email.handle + '">' + self.page.social.email.handle + '</a>, or you can contact <a href="mailto:' + window.HELP_EMAIL + '">' + window.HELP_EMAIL + '</a> and let them know!';
          if (self.info.success) self.info.success = null;
          self.alertClass = 'alert-danger';
          self.info.error = text;
          self.showAlert = true;
        }
      }).catch(function(err) {
        console.error(err);
        if (self.info.success) self.info.success = null;
        self.info.error = err.data;
        self.showAlert = true;
      });      
    }
  }
});

//the main navigation
angular.module('pencilblueApp')
  .directive('htmlEditable', ['$rootScope', '$uibModal', 'ComponentFactory', 'SiteData', '$state', '$sce', function($rootScope, $uibModal, ComponentFactory, SiteData, $state, $sce) {
    return {
      require: 'ngModel',
      scope: {
        ngModel: '=',
        tag: '@',
        ctype: '@',
        pageId: '@',
        global: '@', //if doesnt belong to a particular page id, look footer elemnts
        metadata: '=',
        placeholder: '=',
        // If notify, send slack message on change
        notify: '=',
      },
      restrict: 'A',
      template: '<div ng-bind-html="trustedHtml"></div>',
      replace: true,
      link: function(scope, elem, attrs, ctrl) {
        scope.trustedHtml = '';
        var ngModel = scope.ngModel;
        if (ngModel) scope.trustedHtml = $sce.trustAsHtml(ngModel.html);
        console.log('$rootScope.placeholdertext', $rootScope.placeholdertext);
        if (!scope.tag) {
          console.error('WARNING: no tag specified on tag: ' + scope.tag + '. Please include unique identifier  if this text is on particular page.');
          return;
        }
        if (!scope.pageId && !scope.global) {
          console.error('WARNING: no page id specified on tag ' + scope.tag + '. Please include unique identifier if this text is on particular page.');
        }
        if (!$rootScope.editing) {
          return;
        } else if (scope.placeholder && (!ngModel || !ngModel.html || !ngModel.html.length)) {
          scope.trustedHtml = $sce.trustAsHtml(scope.placeholder);
        }
        elem.addClass('html-editable');
        elem.on('click', function() {
          var modalInstance = $uibModal.open({
            animation: false,
            backdrop: 'static',
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: '/public/performance/partials/html-editable.html',
            controllerAs: '$ctrl',
            controller: function($uibModalInstance, component_type, html, text, metadata) {
              var $ctrl = this;
              $ctrl.html = html;
              $ctrl.component_type = component_type;
              $ctrl.text = text;
              $ctrl.metadata = metadata || {
                header: 'Edit',
                description: '',
                placeholder: ''
              };
              console.log('$ctrl.metadata', $ctrl.metadata);
              var updateOpts = {
                tag: scope.tag,
                component_type: $ctrl.component_type,
                pageId: scope.pageId
              };


              $ctrl.saveCustomText = function() {
                console.log('save custom text: ', scope.notify);
                if ($ctrl.component_type == 'text') {
                  updateOpts.text = $ctrl.text;
                } else if ($ctrl.component_type == 'html') {
                  updateOpts.html = $ctrl.html;
                }

                if (scope.notify) updateOpts.notify = scope.notify;

                if (scope.pageId) updateOpts.page = scope.pageId;

                ComponentFactory.update(updateOpts).then(function(data) {
                  console.log(updateOpts);
                  scope.trustedHtml = $sce.trustAsHtml(data.data[updateOpts.component_type]);
                  $rootScope.$broadcast('text-updated');
                  $uibModalInstance.close(data.data);
                }).catch(function(err) {
                  if (err.data && err.data.message) {
                    alert(err.data.message);
                  } else {
                    alert('System Error');
                  }
                });
              };

              $ctrl.cancel = function() {
                $uibModalInstance.dismiss('cancel');
              };
            },
            resolve: {
              component_type: function() {
                return scope.ctype == 'text' ? 'text': 'html';
              },
              html: function() {
                return angular.copy(scope.ngModel ? scope.ngModel.html:'');
              },
              text: function() {
                return angular.copy(scope.ngModel ? scope.ngModel.text:'');
              },
              metadata: function() {
                console.log('metadata', scope.metadata);
                return scope.metadata;
              }
            }
          });

          modalInstance.result.then(function(component) {
            scope.ngModel = component;
          }, function() {
            console.log('Modal dismissed at: ' + new Date());
          });


        });
      }
    };
  }]);

//# sourceMappingURL=sourcemaps/js/index_non_edit.js.map
