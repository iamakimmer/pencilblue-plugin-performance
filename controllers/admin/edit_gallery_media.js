
var fs         = require('fs');
var async      = require('async');

var SiteData = require('../../mongoose/site_data').SiteData,
  Image = require('../../mongoose/image').Image;


module.exports = function(pb) {
  var util = pb.util;
  function EditGalleryMedia(){}



  util.inherits(EditGalleryMedia, pb.BaseAdminController);

  var SUB_NAV_KEY = 'edit_gallery';



  EditGalleryMedia.prototype.getGallery = function(cb) {
    var self = this;
    this.getJSONPostParams(function(err, post) {
      var siteId = self.siteObj.uid;
      SiteData.findById(siteId).populate('gallery.images').exec(function(err, siteData) {
        if (err) {
          console.error(err);
          return cb(err);
        }
        if (!siteData) {
          return cb(new Error('No Site Found.'));
        }
        cb({ content: siteData.gallery });
      });
    });
  };


  EditGalleryMedia.prototype.getMedia = function(cb) {
    var self = this;
    this.getJSONPostParams(function(err, post) {
      var siteId = self.siteObj.uid;
      SiteData.findById(siteId).exec(function(err, siteData) {
        if (err) {
          console.error(err);
          return cb(err);
        }
        if (!siteData) {
          return cb(new Error('No Site Found.'));
        }
        
        cb({ content: siteData.media });
      });
    });
  };

  EditGalleryMedia.prototype.getResume = function(cb) {
    var self = this;
    this.getJSONPostParams(function(err, post) {
      var siteId = self.siteObj.uid;
      SiteData.findById(siteId).exec(function(err, siteData) {
        if (err) {
          console.error(err);
          return cb(err);
        }
        if (!siteData) {
          return cb(new Error('No Site Found.'));
        }
        cb({ content: siteData.resume });
      });
    });
  };


  EditGalleryMedia.prototype.addGallerySection = function(cb) {
    var self = this;

    self.getJSONPostParams((error, post) => {
      if (error) {
        console.error(error);
        return cb({error});
      }

      var siteId = self.siteObj.uid;
      let data = {};
      data.gallery = { $each: [post.gallery], $position: 0 };

      SiteData.findOneAndUpdate({_id: siteId}, {$push: data}, (error, siteData) => {
        if (error) {
          console.error(error);
          return cb({error});
        }

        return cb({content: {message: 'ok'}});
      });
    });
  };

  EditGalleryMedia.prototype.addMediaSection = function(cb) {
    var self = this;

    self.getJSONPostParams((error, post) => {
      if (error) {
        console.error(error);
        return cb({error});
      }

      var siteId = self.siteObj.uid;
      let data = {};
      data.media = { $each: [post.media], $position: 0 };
      
      SiteData.findOneAndUpdate({_id: siteId}, {$push: data}, (error, siteData) => {
        if (error) {
          console.error(error);
          return cb({error});
        }

        return cb({content: {message: 'ok'}});
      });
    });
  };


  EditGalleryMedia.prototype.updateGallery = function(cb) {
    var self = this;
    this.getJSONPostParams(function(err, post) {
      var siteId = self.siteObj.uid;


      SiteData.findOneAndUpdate({_id: siteId}, { gallery: post.gallery } , {new: true}, (err, siteData) => {
        if (err) return cb({error: err, code: 500});

        return cb({content: siteData});
      });
    });
  };

  EditGalleryMedia.prototype.updateMedia = function(cb) {
    var self = this;
    this.getJSONPostParams(function(err, post) {
      var siteId = self.siteObj.uid;
      

      SiteData.findOneAndUpdate({_id: siteId}, { media: post.media } , {new: true}, (err, siteData) => {
        if (err) return cb({error: err, code: 500});

        return cb({content: siteData});
      });
    });
  };  

  EditGalleryMedia.prototype.updateResume = function(cb) {
    var self = this;
    this.getJSONPostParams(function(err, post) {
      var siteId = self.siteObj.uid;
      

      SiteData.findOneAndUpdate({_id: siteId}, { resume: post.resume } , {new: true}, (err, siteData) => {
        if (err) return cb({error: err, code: 500});

        return cb({content: siteData.resume});
      });
    });
  };    

  EditGalleryMedia.getRoutes = function(cb) {
    var routes = [
      // {
      //   method: 'get',
      //   path: '/admin/gallery',
      //   auth_required: true,
      //   access_level: pb.SecurityService.ACCESS_EDITOR,
      //   content_type: 'text/html'
      // },
      {
        method: 'get',
        path: '/api/gallery',
        handler: 'getGallery',
        content_type: 'application/json'
      }, {
        method: 'post',
        path: '/api/gallery/update',
        handler: 'updateGallery',
        auth_required: true,
        access_level: pb.SecurityService.ACCESS_EDITOR,
        content_type: 'application/json'
      }, {
        method: 'post',
        path: '/api/gallery/addGallerySection',
        handler: 'addGallerySection',
        auth_required: true,
        access_level: pb.SecurityService.ACCESS_EDITOR,
        content_type: 'application/json'
      },{
        method: 'get',
        path: '/api/media',
        handler: 'getMedia',
        content_type: 'application/json'
      }, {
        method: 'post',
        path: '/api/media/update',
        handler: 'updateMedia',
        auth_required: true,
        access_level: pb.SecurityService.ACCESS_EDITOR,
        content_type: 'application/json'
      }, {
        method: 'post',
        path: '/api/media/addGallerySection',
        handler: 'addMediaSection',
        auth_required: true,
        access_level: pb.SecurityService.ACCESS_EDITOR,
        content_type: 'application/json'
      },{
        method: 'get',
        path: '/api/resume-builder',
        handler: 'getResume',
        content_type: 'application/json'
      }, {
        method: 'post',
        path: '/api/resume-builder/update',
        handler: 'updateResume',
        auth_required: true,
        access_level: pb.SecurityService.ACCESS_EDITOR,
        content_type: 'application/json'
      }];
    cb(null, routes);
  };

  return EditGalleryMedia;
};
