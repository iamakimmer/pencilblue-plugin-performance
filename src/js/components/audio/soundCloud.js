angular.module('pencilblueApp').component('soundcloud', {
  bindings: {
    url: '<',
    title: '<'
  },
  template: '<div class="soundcloud"><div ng-bind-html="$sc.iframe"></div><h3>{{$sc.title}}</h3></div>',
  controllerAs: '$sc',
  controller: function($http, $sce) {
    var self = this;
    var soundcloudApi = 'http://soundcloud.com/oembed';

    $http.post('/soundcloud', {url: self.url}).then(function(response) {
      self.iframe = $sce.trustAsHtml(response.data.html);
    }).catch(function(err) {
      console.error(err);
    });
  }
});
