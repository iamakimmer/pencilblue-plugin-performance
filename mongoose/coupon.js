var mongoose = require('mongoose');

var couponSchema = mongoose.Schema({
  _id: String,
  trial_days: Number,
  description: String,
	short_description: String,
	infusion_tags: []
});

var Coupon = mongoose.model('Coupon', couponSchema);
module.exports = Coupon;
