//the main navigation
angular.module('pencilblueApp')
  .directive('htmlEditable', ['$rootScope', '$uibModal', 'ComponentFactory', 'SiteData', '$state', '$sce', function($rootScope, $uibModal, ComponentFactory, SiteData, $state, $sce) {
    return {
      require: 'ngModel',
      scope: {
        ngModel: '=',
        tag: '@',
        ctype: '@',
        pageId: '@',
        global: '@', //if doesnt belong to a particular page id, look footer elemnts
        metadata: '=',
        placeholder: '=',
        // If notify, send slack message on change
        notify: '=',
      },
      restrict: 'A',
      template: '<div ng-bind-html="trustedHtml"></div>',
      replace: true,
      link: function(scope, elem, attrs, ctrl) {
        scope.trustedHtml = '';
        var ngModel = scope.ngModel;
        if (ngModel) scope.trustedHtml = $sce.trustAsHtml(ngModel.html);
        console.log('$rootScope.placeholdertext', $rootScope.placeholdertext);
        if (!scope.tag) {
          console.error('WARNING: no tag specified on tag: ' + scope.tag + '. Please include unique identifier  if this text is on particular page.');
          return;
        }
        if (!scope.pageId && !scope.global) {
          console.error('WARNING: no page id specified on tag ' + scope.tag + '. Please include unique identifier if this text is on particular page.');
        }
        if (!$rootScope.editing) {
          return;
        } else if (scope.placeholder && (!ngModel || !ngModel.html || !ngModel.html.length)) {
          scope.trustedHtml = $sce.trustAsHtml(scope.placeholder);
        }
        elem.addClass('html-editable');
        elem.on('click', function() {
          var modalInstance = $uibModal.open({
            animation: false,
            backdrop: 'static',
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: '/public/performance/partials/html-editable.html',
            controllerAs: '$ctrl',
            controller: function($uibModalInstance, component_type, html, text, metadata) {
              var $ctrl = this;
              $ctrl.html = html;
              $ctrl.component_type = component_type;
              $ctrl.text = text;
              $ctrl.metadata = metadata || {
                header: 'Edit',
                description: '',
                placeholder: ''
              };
              console.log('$ctrl.metadata', $ctrl.metadata);
              var updateOpts = {
                tag: scope.tag,
                component_type: $ctrl.component_type,
                pageId: scope.pageId
              };


              $ctrl.saveCustomText = function() {
                console.log('save custom text: ', scope.notify);
                if ($ctrl.component_type == 'text') {
                  updateOpts.text = $ctrl.text;
                } else if ($ctrl.component_type == 'html') {
                  updateOpts.html = $ctrl.html;
                }

                if (scope.notify) updateOpts.notify = scope.notify;

                if (scope.pageId) updateOpts.page = scope.pageId;

                ComponentFactory.update(updateOpts).then(function(data) {
                  console.log(updateOpts);
                  scope.trustedHtml = $sce.trustAsHtml(data.data[updateOpts.component_type]);
                  $rootScope.$broadcast('text-updated');
                  $uibModalInstance.close(data.data);
                }).catch(function(err) {
                  if (err.data && err.data.message) {
                    alert(err.data.message);
                  } else {
                    alert('System Error');
                  }
                });
              };

              $ctrl.cancel = function() {
                $uibModalInstance.dismiss('cancel');
              };
            },
            resolve: {
              component_type: function() {
                return scope.ctype == 'text' ? 'text': 'html';
              },
              html: function() {
                return angular.copy(scope.ngModel ? scope.ngModel.html:'');
              },
              text: function() {
                return angular.copy(scope.ngModel ? scope.ngModel.text:'');
              },
              metadata: function() {
                console.log('metadata', scope.metadata);
                return scope.metadata;
              }
            }
          });

          modalInstance.result.then(function(component) {
            scope.ngModel = component;
          }, function() {
            console.log('Modal dismissed at: ' + new Date());
          });


        });
      }
    };
  }]);
