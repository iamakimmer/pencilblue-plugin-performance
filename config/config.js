module.exports = {
    sendgrid: {
        api_key: process.env.SENDGRID_APIKEY,
        email: "noreply@actorindex.com"
    }
};