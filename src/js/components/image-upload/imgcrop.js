//the modal directive to upload and image and crop
//the main navigation
angular.module('pencilblueApp')
.directive('croppie', ['$rootScope', 'Upload', 'ImgFactory', function($rootScope, Upload, ImgFactory) {
  return {
    //require: ['ngModel'],
    scope: {
      ngModel: '=',
      viewport: '=',
      thumbnailmode: '@',
      title: '@',
      outputwidth: '@',
      done: '&',
      tag: '@',
      pageId: '@'
    },
    restrict: 'E',
    templateUrl: '/public/performance/partials/imgcrop.html',
    link: function(scope, elem) {

      if (!scope.pageId) {
        console.warn('No Page Id Specified on ImageCrop' + scope.tag);
      }
			// defaults
      if (scope.viewport == undefined) {
        console.warn('WARNING: MUST SET W or H for proper cropping.');
        scope.viewport = { w: null, h: null };
      }
      scope.boundry = { w: 300, h: 300 };

      scope.viewport.w = (scope.viewport.w != undefined) ? scope.viewport.w : 300;
      scope.viewport.h = (scope.viewport.h != undefined) ? scope.viewport.h : 300;
      // viewport cannot be larger than the boundaries
      if (scope.viewport.w > scope.boundry.w) { scope.viewport.w = scope.boundry.w; }
      if (scope.viewport.h > scope.boundry.h) { scope.viewport.h = scope.boundry.h; }

      console.log('scope.viewport', scope.viewport.w);
      console.log('scope.viewport', scope.viewport.h);
      // define options
      var options = {
        enableOrientation: true,
        enableExif: true,
        viewport: {
          width: scope.viewport.w,
          height: scope.viewport.h,
          type: 'square'
        },
        boundary: {
          width: scope.viewport.w,
          height: scope.viewport.h
        },
        showZoom: true,
        mouseWheelZoom: true
      };

      var croppie, bindOpts = {};

      if (scope.ngModel) {

        croppie = new Croppie(elem[0], options);

        bindOpts.url = scope.ngModel.original.url;
        if (scope.ngModel.points) {
          bindOpts.points = scope.ngModel.points.split(',');
        }

        croppie.bind(bindOpts).then(function(res) {
          croppie.setZoom(scope.ngModel.zoom || 0);
        });
      }


      scope.$on('$destroy', function() {
        if (croppie && croppie.elements && croppie.elements.boundary) {

          croppie.destroy();
          $rootScope.$emit('imageUpdated');
        }
      });

      scope.delete = function() {
        if (scope.ngModel._id){
          ImgFactory.delete(scope.ngModel).then(function(data) {
            scope.done(null);
          });
        } else {
          scope.done(null);
        }
      };

      scope.update = function() {
        if (croppie) {
          var zoomDimensions = croppie.get();
          scope.ngModel.points = zoomDimensions.points.join(',');
          scope.ngModel.zoom = zoomDimensions.zoom;

          croppie.result({
            type: 'canvas'
          }).then(function(img) {
            if (scope.thumbnailmode) {
              scope.createThumbnail(dataURItoBlob(img));
            } else {
              scope.cropExistingImage(dataURItoBlob(img));
            }
          });
        } else {
          alert('Please upload an image.');
        }
      };


      function dataURItoBlob(dataURI) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        var byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0)
          byteString = atob(dataURI.split(',')[1]);
        else
        byteString = unescape(dataURI.split(',')[1]);

        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to a typed array
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
          ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ia], {type:mimeString});
      }


      scope.uploadFile = function(file, errorFile) {
        if (!file) return;
        console.log('Croppie file size: ', file);
        scope.fileUploadError = null;
        if (errorFile) {
          switch (errorFile.$error) {
            case 'maxSize':
            scope.fileUploadError = 'File is too large!';
            break;
            default:
            scope.fileUploadError = 'There was an error uploading the file. Please try uploading again.';
            break;
          }
          return;
        }
        if (!file) return;
        console.log('Croppie file size: ', file.size);
        if (file.size > 10000000) {
          scope.fileUploadError = 'File size is too large';
          return;
        }
        scope.errorFile = errorFile;

        if (errorFile) {
          return false;
        }

        if (croppie && croppie.elements && croppie.elements.boundary) {
          croppie.destroy();
        }

        if (!file) {
          return false;
        }

        Upload.upload({
          url: "/api/image/upload",
          data: {
            file: file,
            tag: scope.tag,
            caption: scope.caption,
            page: scope.pageId
          }
        }).then(function(resp) {
          console.log('DONE', resp);
          delete scope.progressPercentage;
          scope.ngModel = resp.data;
          croppie = new Croppie(elem[0], options);
          croppie.bind({url: scope.ngModel.url}).then(function() {
            croppie.setZoom(0);
          });
        }, function(resp) {
          //todo error handling on pending image
          console.log('resp err', resp);
          console.log('Error status: ' + resp.status);
          alert(resp.data ? resp.data.message : 'Error Uploading File');
          delete scope.progressPercentage;
        }, function(evt) {
          //todo loading info
          console.log('resp progressPercentage', evt);
          scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          console.log('progress: ' + scope.progressPercentage + '% ' + evt.config.data.file.name);
        });



        // console.log('in uploadFile', file);
        // var reader = new FileReader();
        // reader.onload = function (e) {
        //   croppie.bind({url: e.target.result}).then(function() {
        //     console.log('bind compelte');
        //   });
        // };
        // reader.readAsDataURL(file);
      };

      scope.cropExistingImage = function(file) {
        if (!file) {
          return false;
        }
        Upload.upload({
          url: window.CLOUDINARY_UPLOAD_API_URL,
          data: {
            folder: window.location.host,
            upload_preset: scope.outputwidth ? 'hcfwk7ej' : 'zymnlox3',
            file: file,
            title: scope.ngModel.title
            //transformation: 'c_limit,w_' + (scope.outputwidth || 600) not allowed in unsignd uploads
          }
        }).then(function(resp) {
          console.log('DONE', resp, scope.ngModel);
          scope.ngModel.url = resp.data.secure_url;
          delete scope.progressPercentage;
          if (!scope.ngModel.page) scope.ngModel.page = null;
          ImgFactory.update(scope.ngModel).then(function(resp) {
            //$rootScope.$broadcast('image-updated');
            scope.done({img: resp.data});
          });
        }, function(resp) {
          //todo error handling on pending image
          console.log('resp err', resp);
          console.log('Error status: ' + resp.status);
          delete scope.progressPercentage;
        }, function(evt) {
          //todo loading info
          console.log('resp progressPercentage', evt);
          scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          console.log('progress: ' + scope.progressPercentage + '% ' + evt.config.data.file.name);
        });

      };


      scope.createThumbnail = function(file) {
        if (!file) {
          return false;
        }
        Upload.upload({
          url: window.CLOUDINARY_UPLOAD_API_URL,
          data: {
            folder: window.location.host,
            upload_preset: 'zymnlox3',
            file: file
          }
        }).then(function(resp) {
          console.log('DONE', resp);
          scope.ngModel.thumbnail = resp.data.secure_url;
          scope.ngModel.thumbnail_id = resp.data.public_id;
          ImgFactory.update(scope.ngModel).then(function(data) {
            //$rootScope.$broadcast('image-updated');
            scope.done({img: data.data});
          });
        }, function(resp) {
          //todo error handling on pending image
          console.log('resp err', resp);
          console.log('Error status: ' + resp.status);
        }, function(evt) {
          //todo loading info
          console.log('resp progressPercentage', evt);
          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        });

      };



    }
  };
}]);
