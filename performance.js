var mongoose = require('mongoose');
var SiteData = require('./mongoose/site_data').SiteData;
var defaultPageStyles = require("./public/themes/1/pagestyle.json");
module.exports = function PerformanceModule(pb) {
  /**
  * PerformancePlugin - A performance for exemplifying what the main module file should
  * look like.
  *
  * @author Brian Hyder <brian@pencilblue.org>
  * @copyright 2015 PencilBlue, LLC
  */
  function Performance(){}
  var self = this;

  Performance.onInstall = function(cb) {
    cb(null, true);
  };

  Performance.onUninstall = function (cb) {
    cb(null, true);
  };

  Performance.onStartupWithContext = function (context, cb) {
    // var site = pb.SiteService.getCurrentSite(context.site);
    // //pb.AdminNavigation.removeFromSite('content', site);

    // if (site == 'global') {
    //   return cb(null, true);
    // }

    // pb.AdminNavigation.addToSite({
    //   id: 'templates',
    //   title: 'Themes',
    //   icon: 'image',
    //   'href': '/admin',
    //   access: pb.SecurityService.ACCESS_USER
    // }, site);


    // // Add a new top level node
    // pb.AdminNavigation.addToSite({
    //   id: 'edit',
    //   title: 'Edit Website',
    //   icon: 'cogs',
    //   href: '/edit',
    //   access: pb.SecurityService.ACCESS_USER
    // }, site);

    // pb.AdminNavigation.addToSite({
    //   id: 'social-settings',
    //   title: 'Social Settings',
    //   icon: 'list-alt',
    //   'href': '/admin/social',
    //   access: pb.SecurityService.ACCESS_USER
    // }, site);

    // pb.AdminNavigation.addToSite({
    //   id: 'signup',
    //   title: 'Billing',
    //   icon: 'credit-card',
    //   'href': '/admin/billing',
    //   access: pb.SecurityService.ACCESS_USER
    // }, site);

    // SiteData.findById(site, function(err, siteData) {
    //   if (siteData) {
    //     cb(err, true);
    //   } else {
    //     console.log('CREATING NEW SITE DATA!');
    //     var newSiteData = new SiteData({
    //       _id: site
    //     });
    //     newSiteData.pageStyles = defaultPageStyles;
    //     newSiteData.save(function(err) {
    //       cb(err, true);
    //     });
    //   }
		// });
		cb(null, true);

  };

  Performance.onShutdown = function(cb) {
    cb(null, true);
  };

  //exports
  return Performance;
};
