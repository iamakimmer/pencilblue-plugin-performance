'use strict';

var mongoose = require('mongoose');

var schema = {
  site: String, //website
  metadata: {},
  url: String,
  component_type: {
    type: String,
    enum: ['link', 'html', 'text', 'carousel']
  },
  component_specific_data: {},
  text: String,
  html: String,
  tag: {
    type: String,
    trim: true
  },
	page: {
		type: mongoose.Schema.Types.ObjectId //you cant ref Page explicitly here because you have Page and PageLive			
	},
  title: String,
  active: {
    type: Boolean,
    default: true
  },
  images: [{
		type: String,
		ref: 'Image',
		_id: false
	}],
	deleted_at: {
		type: Date
	},
	created_at: {
		type: Date,
		default: Date.now()
	}
};

var componentSchema = mongoose.Schema(schema, {
  timestamps: true,
  collection: 'components'
});

var componentSchemaLive = mongoose.Schema(schema, {
  timestamps: true,
  collection: 'components_live'
});

var Component = mongoose.model('Component', componentSchema);
var ComponentLive = mongoose.model('ComponentLive', componentSchemaLive);

module.exports = {
  Component: Component, //edit mode component
  ComponentLive: ComponentLive //live mode component
};
