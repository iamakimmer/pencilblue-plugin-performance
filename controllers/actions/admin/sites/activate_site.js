/*
Copyright (C) 2015  PencilBlue, LLC

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

module.exports = function(pb) {

  //pb dependencies
  var util = pb.util;


  function ActivateSite(){}
  util.inherits(ActivateSite, pb.BaseController);

  ActivateSite.prototype.render = function(cb)
  {
    console.log('activating site');
    var vars = this.pathVars;

    var message = this.hasRequiredParams(vars, ['id']);
    if(message) {
      return cb({
        code: 400,
        content: pb.BaseController.apiResponse(pb.BaseController.API_FAILURE, message)
      });
    }

    var siteService = new pb.SiteService();
    var jobId = siteService.activateSite(vars.id);
    var content = pb.BaseController.apiResponse(pb.BaseController.API_SUCCESS, '', jobId);

		pb.dbm.getDb((err, db) => {
			if (err) {
				return cb(err);
			}
			var col = db.collection('site');
			col.update({uid: vars.id}, {$set: {isPublished: true}});
			cb({content: content});
		});  
  };

  ActivateSite.getRoutes = function(cb) {
    var routes = [{
      path: '/actions/admin/sites/activate/:id',
      method: 'post',
      access_level: pb.SecurityService.ACCESS_WRITER,
      auth_required: true,
      inactive_site_access: true
    }];

    cb(null, routes);
  };

  //exports
  return ActivateSite;
};
