/**
 * Controller to handle to saving/publish edits to site content.
 */
var _            = require('lodash');
var formidable   = require('formidable');
var cloudinary   = require('cloudinary');
const slug       = require('slug');
const mongoose   = require('mongoose');
const request = require('request');

if (process.env.NODE_ENV != 'production') {
  mongoose.set('debug', true);
}

const gRecaptchaURL = 'https://www.google.com/recaptcha/api/siteverify';

cloudinary.config({
  cloud_name: process.env.cloudinary_cloud_name,
  api_key: process.env.cloudinary_api_key,
  api_secret: process.env.cloudinary_api_secret
});

var ObjectID       = require('mongodb').ObjectID;
var fs             = require('fs');
var async          = require('async');
var SiteData       = require('../../mongoose/site_data').SiteData,
  Image            = require('../../mongoose/image').Image,
  Component        = require('../../mongoose/component').Component;

var SiteDataLive   = require('../../mongoose/site_data').SiteDataLive,
  ImageLive        = require('../../mongoose/image').ImageLive,
  CustomBuild      = require('../../mongoose/custombuild').CustomBuild,
	ComponentLive    = require('../../mongoose/component').ComponentLive,

	Page    = require('../../mongoose/page').Page,
	PageLive    = require('../../mongoose/page').PageLive,

  SiteCache = require('../../mongoose/site_cache').SiteCache;


var Slack = require('slack-node');



module.exports = function IndexModule(pb) {

  function SaveSite() {}

	cloudinary.config(pb.config.cloudinary);
	slack = new Slack();
	slack.setWebhook(process.env.Slack_webhookUri);


  var sendGridApiKey = pb.config.SENDGRID_API_KEY;
  var sg = require('sendgrid').SendGrid(sendGridApiKey);
  var redisClient = pb.redisClient;

  //dependencies

  SaveSite.prototype.init = function(props, cb) {
    var self = this;

    pb.BaseController.prototype.init.call(self, props, function() {
      var queryService = new pb.SiteQueryService({ site: self.site });
      self.siteQueryService = queryService;
      cb();
    });
  };


  var util = pb.util;

  //inheritance
  util.inherits(SaveSite, pb.BaseController);

  SaveSite.prototype.getSite = function(cb) {
    var siteId = this.siteObj.uid;
    SiteData.findById(siteId).populate('images').exec(function(err, site) {
      if (err) {
        console.log('err', err);
        return cb(err);
      }
      if (err) {
        cb(err);
      } else {
        cb({ content: site });
      }
    });
  };


  SaveSite.prototype.getMembers = function(cb) {
    var siteService = new pb.SiteService();

    var dao = new pb.DAO();


    siteService.getActiveSites(function(err, data) {
      if (err) {
        return cb(err);
      } else {
        var sm = data.map(function(site) {
          return {
            uid: site.uid,
            hostname: site.hostname
          };
        });

        sm = _.indexBy(sm, 'uid');

        dao.q('plugin_settings', { select: pb.DAO.SELECT_ALL, where: { plugin_uid: 'performance' } }, function(err, data) {
          data = data.map(function(siteInfo) {
            var settings = _.indexBy(siteInfo.settings, 'name');
            var hostname;

            if (sm[siteInfo.site]) {
              hostname = sm[siteInfo.site].hostname;
            }
            return {
              hostname: hostname,
              settings: settings
            };
          });

          var onlyActiveSites = data.filter(function(site) {
            return site.hostname != null;
          });

          cb({
            content: onlyActiveSites
          });
        });
      }
    });
  };


  SaveSite.prototype.getComponents = function(cb) {
    var siteId = this.siteObj.uid;

    Component.find({ site: siteId, active: {$ne: false} }).populate('images').exec(function(err, components) {
      if (err) {
        console.log('err', err);
        return cb(err);
      }

      if (err) {
        cb(err);
      } else {
        cb({ content: components });
      }
    });
  };




  SaveSite.prototype.getPageBySlug = function(cb) {
    var siteId = this.siteObj.uid;
    var self = this;
    var vars = this.pathVars;
    console.log('vars', vars);
    const query = {slug: vars.slug, site: siteId, $or: [{deleted_at: { $exists: false}}, {deleted_at: null}]};
    console.log('gettingpge by slug non-live', query);
    Page.findOne(query).exec(function(err, page) {
      if (!page) {
        return cb(new Error('Non Live Page not found!'));
      } else {
        Image.find({ site: siteId, page: page._id, active: true }, function(err, images) {
          Component.find({ site: siteId, page: page._id, active: {$ne: false} }).populate('images').exec(function(err, components) {
            cb({content: { page: page, images: _.groupBy(images, 'tag'), components: _.groupBy(components, 'tag')}});
          });
        });
      }
    });
  };


  SaveSite.prototype.getPageBySlugLive = function(cb) {
    var siteId = this.siteObj.uid;
    var self = this;
    var vars = this.pathVars;

    const query = {slug: vars.slug, site: siteId, $or: [{deleted_at: { $exists: false}}, {deleted_at: null}]};
    console.log('gettingpge by slug live', query);
    PageLive.findOne(query).exec(function(err, page) {
      if (!page) {
        return cb(new Error('Live Page not found!'));
      } else {
        ImageLive.find({ site: siteId, page: page._id, active: true }, function(err, images) {
          ComponentLive.find({ site: siteId, page: page._id, active: {$ne: false} }).populate('images').exec(function(err, components) {
            cb({content: { page: page, images: _.groupBy(images, 'tag'), components: _.groupBy(components, 'tag')}});
          });
        });
      }
    });
  };


  SaveSite.prototype.updatePage = function(cb) {
    var siteId = this.siteObj.uid;
		var self = this;
    this.getJSONPostParams(function(err, post) {
			console.log('err', err);
			console.log('post', post);
      console.log('post data', post);
			console.log('pathVars', self.pathVars);
      const q = {
				site: siteId,
				_id: new ObjectID(self.pathVars.pageId)
			};
			console.log('q', q);

      Page.findOneAndUpdate(q, {$set: {
        last_modified: new Date(),
				layout: post.layout
			}}, function(err, page) {
        if (err) {
          cb({error: err, code: 500});
        } else {
          console.log('Page Updated');

          cb({ content: {page} });
        }
      });
    });
  };



  SaveSite.prototype.updateOrCreatePage = function(cb) {
    var siteId = this.siteObj.uid;

    this.getJSONPostParams(function(err, post) {
      console.log('post data', post);

      const q = {
        site: siteId,
			};

      let _id;
      let isNew = false;

      if (post._id) {
         _id = new ObjectID(post._id);
      } else {
        console.log('CREATING NEW POST', post);
				_id = new ObjectID();
        post.slug = slug(post.nav_title);
        post.layout = post.layout || 1;
        post.last_modified = new Date();
        isNew = true;
      }
      delete post['_id'];

			q._id = _id;


      Page.findOneAndUpdate(q, {$set: post}, { upsert: true, new: true }, function(err, page) {
        if (err) {
          cb({error: err, code: 500});
        } else {
          console.log('Page Updated');

          cb({ content: {page} });
        }
      });
    });
  };




  SaveSite.prototype.updateComponent = function(cb) {
    var siteId = this.siteObj.uid;
    var self = this;

    this.getJSONPostParams(function(err, post) {
      console.log('post data', post);

      if (!post.tag) {
        console.log('returning cb');
        return cb(new Error('No Tag Provided.'));
			}

      post.title = post.tag;

      const q = {
        site: siteId,
				tag: post.tag
			};

			if (post.page) {
        q.page = post.page;
			}

      Component.findOne(q, 'text html', (err, component) => {
        // Check if any changes were made
        if (component && (
            (post.component_type === 'text' && post.text === component.text) ||
            (post.component_type === 'html' && post.html === component.html)
          )) {
          console.log('Component unchanged');
          return cb({ content: component });
        }
        Component.findOneAndUpdate(q, {$set: post}, { upsert: true, new: true }, function(err, newComponent) {
          if (err) {
            cb({error: err, code: 500});
          } else {
            console.log('THE INFO: ', JSON.stringify(self.session.authentication.user, null, 2));
            // if (post.notify) {
            //   console.log('MORE INFO: ', pb.config.slack_channel_notify);
            //   slack.webhook(
            //     {
            //       text:  `${process.env.NODE_ENV !== 'production' && 'TEST: '}${self.session.authentication.user.email} updated ${q.tag} for site ${self.siteObj.hostname}\n\`\`\`${post[post.component_type]}\`\`\``,
            //       channel: pb.config.slack_channel_notify
            //     }
            //   );
            // }
            cb({ content: newComponent });
          }
        });
      });
    });
  };


  SaveSite.prototype.removePage = function(cb) {
    var self = this;
    var siteId = this.siteObj.uid;

    self.getJSONPostParams((error, post) => {
      if (error) {
        console.log('error: ', error);
        return cb({error, code: 500});
      }
      console.log('removing page', post);
      const q = {
        site: siteId,
        _id: post._id,
        layout_category: 'custom'
      };
      console.log('q', q);

      Page.update(q, {deleted_at: new Date()}, (err, updated) => {
        if (err) {
          console.log('err', err);
          return cb({error: err, code: 500});
        }
        console.log('updated', updated);

        Component.update(q, {$set : {active: false, deleted_at: new Date()}}, function(err, updated) {
          console.log('updated', updated);
          console.log('err', err);
          if (err) return cb({error: err, code: 500});
          return cb({});
        });
      });
    });
  };


  /**
   * Handles the cleanup after the incoming form data has been processed.  It
   * attempts to remove uploaded files or file partials after a failure or
   * completion.
   * @method onDone
   * @param {Error} err
   * @param {Object} content
   * @param {Object} [files={}]
   * @param {Function} cb
   */
  SaveSite.prototype.onDone = function(err, content, files, cb) {
    if (util.isFunction(files)) {
      cb = files;
      files = null;
    }
    if (!util.isObject(files)) {
      files = {};
    }

    //ensure all files are removed
    var self = this;
    var tasks = util.getTasks(Object.keys(files), function(fileFields, i) {
      return function(callback) {
        var fileDescriptor = files[fileFields[i]];

        //ensure file has a path to delete
        if (!fileDescriptor.path) {
          return callback();
        }

        //remove the file
        fs.unlink(fileDescriptor.path, function(err) {
          pb.log.info('Removed temporary file: %s', fileDescriptor.path);
          callback();
        });
      };
    });
    async.parallel(tasks, function(error, results) {

      //weird case where formidable continues to process content even after
      //we cancel the stream with a 413.  This is a check to make sure we
      //don't call back again
      if (self.errored > 1) {
        return;
      }

      //we only care about the passed in error
      if (util.isError(err)) {
        var code = err.message === self.ls.get('FILE_TOO_BIG') ? 413 : 500;
        console.log('err', err);
        return cb({ content: pb.BaseController.apiResponse(pb.BaseController.API_FAILURE, err.message), code: code });
      }
      cb({ content: content });
    });
  };


  SaveSite.prototype.setTheme = function(cb) {
    var self = this;
    this.getJSONPostParams(function(err, post) {
      var siteId = self.siteObj.uid;
      SiteData.findById(siteId, function(err, siteData) {
        if (err) {
          console.error(err);
          return cb(err);
        }
        if (!post) {
          return cb(new Error('No Site Found.'));
        }
        siteData.layouts.home = 1;
        siteData.theme = post.theme;
        siteData.save(function(err) {
          if (err) {
            console.error(err);
            cb(err);
          } else {
            cb({ content: siteData });
          }
        });
      });
    });
  };

  SaveSite.prototype.save = function(cb) {
    var self = this;
    this.getJSONPostParams(function(err, post) {
      var siteId = self.siteObj.uid;
      console.log('SaveSite.prototype.save siteId', siteId);
      SiteData.findById(siteId, function(err, site) {
        if (err) {
          console.error(err);
          return cb(err);
        }
        if (!post) {
          return cb(new Error('No Site Found.'));
        }
				delete post.__v;
				delete post._id;

				site = _.extend(site, post);
				console.log('site before save', site);
        site.save(function(err) {
          if (err) {
            console.error(err);
            cb(err);
          } else {
            cb({ content: site });
          }
        });
      });
    });
  };

  SaveSite.prototype.updateSite = function(cb) {
    var self = this;
    this.getJSONPostParams(function(err, post) {
      var siteId = self.siteObj.uid;
      delete post.__v;
			delete post._id;
      SiteData.findByIdAndUpdate(siteId, post, {new : true}, function(err, update) {
        if (err) {
          return cb(err);
        }
        cb({ content: update });
      });
    });
  };


  SaveSite.prototype.builditforyou = function(cb) {
    var self = this;
    this.getJSONPostParams(function(err, post) {
      console.log('post', post);
      var siteId = self.siteObj.uid;
      var customBuild = new CustomBuild(post);
      customBuild.site = siteId;
      customBuild.save(function(err) {
        if (err) {
          console.error(err);
          cb(err);
        } else {
          if (pb.config.NODE_ENV !== 'development') slack.webhook({text: self.siteObj.hostname + ' Requested Build It For You' + '```' + JSON.stringify(customBuild, null, 4) + '```', channel: pb.config.slack_channel_notify}, function(err) {});

          var emailFields = ['name', 'urls', 'comments'];
          var emailBody = '<div style="font-family: Arial, sans-serif;">' +
          '<h1 style="background-color: #ccc;text-align: center;color: white;padding: 50px 0;text-transform: capitalize">' + self.siteObj.hostname + ' Wants Build It For You, </h1>' +
          emailFields.map((i) => {
            let value = post[i];
            if (typeof value === 'string') {
              return `<p>${i}: ${value}</p>`;
            } else if (typeof value === 'object' && value !== 'undefined') {
              var str = '';
              for (let x in value) {
                str += `<p>${x}: ${value[x]}</p>\n`;
              }

              return str;
            }
          }).join('');
          '</div>';

          var helper = require('sendgrid').mail,
            personalization = new helper.Personalization(),
            from_email = new helper.Email(pb.config.noreplyemail),
            subject = self.siteObj.hostname + ' wants to build it for you service',
            content = new helper.Content('text/html', emailBody);


          if (pb.config.NODE_ENV !== 'development') {
            personalization.addTo(new helper.Email(pb.config.builditforyouemail));
            personalization.addTo(new helper.Email(pb.config.dev_email));
          } else {
            personalization.addTo(new helper.Email(pb.config.dev_email));
          }

          personalization.setSubject(subject);

          var mail = new helper.Mail();
          mail.addPersonalization(personalization);
          mail.setFrom(from_email);
          mail.setSubject(subject);
          mail.addContent(content);

          var requestBody = mail.toJSON(),
            request = sg.emptyRequest();

          request.method = 'POST';
          request.path = '/v3/mail/send';
          request.body = requestBody;

          sg.API(request, function(err, res) {
            console.log('builditforyouerr', err);
            console.log('builditforyoures', res);
          });


          cb({ content: customBuild });
        }
      });
    });
  };


  SaveSite.prototype.uploadFileToS3 = function(cb) {
    console.log('cb', cb);
    var self = this;
    var form = new formidable.IncomingForm();
    var siteId = self.siteObj.uid;


    form.parse(self.req, function(err, fields, files) {
      if (util.isError(err)) {
        return self.onDone(err, null, files, cb);
      }

      //todo
      self.onDone(err, null, files, cb);

    });
    //parse the form out and let us know when its done
  };



  SaveSite.prototype.uploadImage = function(cb) {
    console.log('cb', cb);
    var self = this;
    var form = new formidable.IncomingForm();
    var siteId = self.siteObj.uid;


    form.parse(self.req, function(err, fields, files) {
      if (util.isError(err)) {
        return self.onDone(err, null, files, cb);
      }

      console.log('files.file', files.file);
      console.log('fields', fields);

      var opts = {
        width: 2000,
        crop: "limit",
        format: 'jpg',
        eager: [{
          width: 200,
          height: 200,
          crop: 'thumb'
        }],
        tags: siteId
      };

      // if (!fields.page) {
      //cant do this because of the home pages have no "page id"
      //   return self.onDone(new Error('No Page Specified'), null, files, cb);
      // }

      if (fields.height) {
        console.log("GOT HEIGHT OPTION", fields.height);
        opts.height = fields.height;
      }

      if (fields.width) {
        console.log("GOT width OPTION", fields.width);
        opts.width = fields.width;
      }
      if (fields.crop) {
        opts.crop = fields.crop;
      }
      if (fields.background) {
        opts.background = fields.background;
      }


      if (files.file.type == 'application/pdf') { //lets do the normal width for a pdf
        delete opts.width;
        delete opts.height;
      }

      cloudinary.v2.uploader.upload(files.file.path, opts, function(err, res) {
        console.log('err', err);
        console.log('res', res);
        if (err) {
          console.log('err!', err);
          return self.onDone(err, null, files, cb);
        } else {
          console.log('done!', res);
          var q = {
            tag: fields.tag,
            site: siteId
          };

          if (fields.page) q.page = fields.page;
          console.log('THA Q!!! ', q);

          Image.update(q, {
            $set: {
              tag: null
            }
          }, function(err) {
            var image = new Image({
              _id: res.public_id,
              site: siteId,
              tag: fields.tag,
              page: fields.page ? mongoose.Types.ObjectId(fields.page) : null,
              title: fields.title,
              metadata: res.data,
              url: res.secure_url,
              thumbnail: res.eager[0].secure_url,
              original: {
                public_id: res.public_id,
                url: res.secure_url
              }
            });
            image.save(function(err, img) {
              console.log('image saving err', err);
              self.onDone(err, image, files, cb);
            });
          });
        }
      });

    });
    //parse the form out and let us know when its done
  };

  SaveSite.prototype.deleteImage = function(cb) {
    var self = this;
    this.getJSONPostParams(function(err, post) {
      var siteId = self.siteObj.uid;
      Image.findOneAndUpdate({
        _id: post._id,
        site: siteId
      }, { active: false }, function(err) {
        if (err) {
          // console.log('updating newSite in err', newSite);
          console.error(err);
          cb(err);
        } else {
          console.log('Image Deleted');
          cb({ content: {} });
        }
      });

    });
  };


  SaveSite.prototype.updateImage = function(cb) {
    var self = this;
    this.getJSONPostParams(function(err, post) {
      console.log('Update this: ', post.tag);
      var siteId = self.siteObj.uid;
      console.log('siteId', siteId, post._id);
      Image.findOne({
        _id: post._id,
        site: siteId
      }, function(err, image) {
        if (err) {
          cb(err);
        } else if (!image) {
          return cb(new Error('Image was not found. Please refresh your page and try again.'));
        } else {
          image = _.extend(image, post);
          image.save(function(err) {
            if (err) {
              // console.log('updating newSite in err', newSite);
              console.error(err);
              cb(err);
            } else {
              console.log('Image Updated');
              cb({ content: image });
            }
          });
        }
      });

    });
  };

  SaveSite.prototype.updateCarousel = function(cb) {
    let self = this;
    let siteId = self.siteObj.uid;

    console.log('before get post');
    this.getJSONPostParams((error, post) => {
      console.log('post params: ', post);
      if (error) return cb({error, code: 500});

      if (typeof post._id === 'undefined') {
        post._id = new mongoose.mongo.ObjectID();
        console.log('post id is null: ', post._id);
      }

      const q = {
        _id: post._id
      };

      console.log('the q: ', q);

      const update = {
        _id: post._id,
        component_type: 'carousel',
        tag: 'homePageCarousel',
        site: siteId
      };

      update.images = post.images;
      console.log('update: ', update);
      Component.findOneAndUpdate(q, {$set: update}, {new: true, upsert: true}, (error, component) => {
        if (error) return cb({error, code: 500});
        Component.findOne(q).populate('images').exec(function(err, populatedComponent) {
          cb({content: populatedComponent});
        });
      });
    });
  };

  SaveSite.prototype.contact = function(cb) {
    var self = this;
    pb.dbm.getDb((err, db) => {
      var col = db.collection('user');
      var apiKey = process.env.GRECAPTCHA_API_KEY;

      this.getJSONPostParams(function(err, post) {
        if (err) {
          console.error(err);
          return cb(err);
        }

        if (!post.gRecaptchaResponse) {
          console.error('No recaptcha token');
          return cb(new Error('No recaptcha token provided'));
        }

        if (!apiKey) {
          console.log('NO GRECAPTCHA API KEY');
          return cb(new Error('Invalid recaptcha credentials'));
        }
        console.log('post params !!!!!!! ', post);

        let form = {
          secret: apiKey,
          response: post.gRecaptchaResponse,
          // remoteip: req.ip
        };
        request.post({
          url: 'https://www.google.com/recaptcha/api/siteverify',
          form
        }, (err, response, body) => {
          console.log('the body: ', body);
          if (err) {
            console.error(err);
            return cb({error: err});
          }

          body = JSON.parse(body);
          if (body.success) {
            var emailBody = '<div style="font-family: Arial, sans-serif;">' +
              '<h1 style="background-color: #ccc;text-align: center;color: white;padding: 50px 0;text-transform: capitalize">' + post.name + ' Says, </h1>' +
              '<p>' + post.message + '</p>' +
              '</div>';

            var helper = require('sendgrid').mail,
              personalization = new helper.Personalization(),
              from_email = new helper.Email(post.fromEmail),
              subject = post.name + ' has sent you an email on ActorIndex',
              content = new helper.Content('text/html', emailBody);

            col.find({ site: self.siteObj.uid }).toArray(function(err, users) {
              if (err) return cb({ error: err });

              for (var u in users) {
                personalization.addTo(new helper.Email(users[u].email));
              }

              personalization.setSubject(subject);

              // email.setFrom(post.fromEmail);
              // email.setSubject = post.name + ' has sent you an email on ActorIndex';
              // email.setHtml = emailBody;

              // sg.send(email);

              var mail = new helper.Mail();
              mail.addPersonalization(personalization);
              mail.setFrom(from_email);
              mail.setSubject(subject);
              mail.addContent(content);

              var requestBody = mail.toJSON(),
                request = sg.emptyRequest();

              request.method = 'POST';
              request.path = '/v3/mail/send';
              request.body = requestBody;

              sg.API(request, function(err, res) {
                if (err) return cb({ error: err });

                cb({ content: res, status: 200 });
              });
            });
          } else {
            return cb({ error: JSON.stringify(body['error-codes']) });
          }
        });
      });
    });

  };

  SaveSite.prototype.publishSite = function(cb) {
    console.log('in publish site');
    var self = this;


    async.waterfall([
      function(next) {

        SiteDataLive.findByIdAndRemove(self.siteObj.uid, function(err) {
          if (err) {
            return next(err);
          }
          SiteData.findById(self.siteObj.uid, function(err, siteData) {
            if (err) {
              return next(err);
            }

            SiteDataLive.insertMany([siteData], function(err, data) {
              next(err);
            });
          });
        });
      },
      function(next) {
        ComponentLive.remove({site: self.siteObj.uid}, function(err) {
          if (err) {
            return next(err);
          }
          Component.find({site: self.siteObj.uid, active: { $ne: false}}).populate('images').exec(function(err, components) {
            if (err) {
              return next(err);
            }
            if (components.length === 0) {
              return next(err);
            }
            ComponentLive.insertMany(components, function(err, data) {
              next(err);
            });
          });
        });
			},
      function(next) {
        PageLive.remove({site: self.siteObj.uid}, function(err) {
          if (err) {
            return next(err);
          }
          Page.find({site: self.siteObj.uid, $or: [{deleted_at: null} , {deleted_at: {$exists: false}}], active: { $ne: false}}).exec(function(err, pages) {
            if (err) {
              return next(err);
            }
            if (pages.length === 0) {
              return next(err);
            }
            var lastModifiedDate = new Date();

            pages.forEach(p => {
              p.last_modified = lastModifiedDate;
            });

            PageLive.insertMany(pages, function(err, data) {
              next(err);
            });
          });
        });
      },
      function(next) {
        ImageLive.remove({site: self.siteObj.uid}, function(err) {
          if (err) {
            return next(err);
          }
          Image.find({site: self.siteObj.uid}, function(err, images) {
            if (err) {
              return next(err);
            }
            if (images.length === 0) {
              return next(err);
            }
            ImageLive.insertMany(images, function(err, data) {
              next(err);
            });
          });
        });
      },
      function(next) {
        SiteCache.findById({_id: self.siteObj.uid}, function(err, cache) {
          if (cache && cache.keys) {
            redisClient.del(cache.keys, function(err, response) {
              console.log('err', err);
              console.log(`Deleted ${response} Keys Successfully!`);
              SiteCache.remove({_id: self.siteObj.uid}, function(err) {
                next(null);
              });
            });
          } else {
            next(null);
          }
        });
      }
    ], function(err) {
      console.log('err', err);
      if (err) {
        return cb({error: err});
      }

      if (pb.config.NODE_ENV !== 'development') {
        slack.webhook(
          {
            text:  `${self.session.authentication.user.email} published their ${self.siteObj.hostname}`,
            channel: pb.config.slack_channel_notify
          }, function(err) {
        });
      }

      cb({content: {message: 'ok'}});
    });

  };

  SaveSite.prototype.unpublishSite = function(callback) {
    var self = this;
    console.log(self.siteObj.uid);
    SiteDataLive.update({_id: self.siteObj.uid}, {$set: {unpublished: true}}, function(err) {
      if (err) return callback({error: {message: err}, code: 500});

      return callback({content: {message: 'ok'}, code: 200});
    });
  };

  SaveSite.prototype.updateNavOrder = function(cb) {
    var self = this;
    var _id = self.siteObj.uid;

    self.getJSONPostParams((error, post) => {
      if (error) return cb({error, code: 500});
      async.eachLimit(post.pages, 1, function(page, cb) {
        Page.findByIdAndUpdate(page._id, {nav_order: page.nav_order, last_modified: new Date()}, (error, page) => {
          cb(error);
        });
      }, function(error) {
        if (error) return cb({error, code: 500});
        return cb({content: {}});
      });

    });
  };


  SaveSite.getRoutes = function(cb) {
    var routes = [{
      method: 'post',
      path: '/api/image/update',
      handler: 'updateImage',
      auth_required: true,
      access_level: pb.SecurityService.ACCESS_EDITOR,
      content_type: 'application/json'
    }, {
      method: 'post',
      path: '/api/image/delete',
      handler: 'deleteImage',
      auth_required: true,
      access_level: pb.SecurityService.ACCESS_EDITOR,
      content_type: 'application/json'
    }, {
      method: 'post',
      path: '/api/image/upload',
      handler: 'uploadImage',
      auth_required: true,
      access_level: pb.SecurityService.ACCESS_EDITOR,
      content_type: 'application/json'
    }, {
      method: 'post',
      path: '/api/carousel/update',
      handler: 'updateCarousel',
      auth_required: true,
      access_level: pb.SecurityService.ACCESS_EDITOR,
      content_type: 'application/json'
    }, {
      //this s3 is not impleemtned here
      method: 'post',
      path: '/api/upload',
      handler: 'uploadFileToS3',
      auth_required: true,
      access_level: pb.SecurityService.ACCESS_EDITOR,
      content_type: 'application/json'
    },{
      method: 'post',
      path: '/api/builditforyou',
      handler: 'builditforyou',
      auth_required: true,
      access_level: pb.SecurityService.ACCESS_EDITOR,
      content_type: 'application/json'
    }, {
      method: 'get',
      path: '/api/site',
      handler: 'getSite',
      content_type: 'application/json'
    }, {
      method: 'get',
      path: '/api/components',
      handler: 'getComponents',
      content_type: 'application/json'
    }, {
      method: 'post',
      path: '/api/components/update',
      handler: 'updateComponent',
      auth_required: true,
      access_level: pb.SecurityService.ACCESS_EDITOR,
      content_type: 'application/json'
    },{
      method: 'get',
      path: '/api/components/page/:slug',
      handler: 'getPageBySlug',
      content_type: 'application/json'
    },{
      method: 'get',
      path: '/api/components/page/:slug/live',
      handler: 'getPageBySlugLive',
      content_type: 'application/json'
    },{
      method: 'post',
      path: '/api/components/update-page',
      handler: 'updateOrCreatePage',
      auth_required: true,
      access_level: pb.SecurityService.ACCESS_EDITOR,
      content_type: 'application/json'
    }, {
      method: 'post',
      path: '/api/components/remove-page',
      handler: 'removePage',
      auth_required: true,
      access_level: pb.SecurityService.ACCESS_EDITOR,
      content_type: 'application/json'
    }, {
      method: 'post',
      path: '/api/savesite',
      auth_required: true,
      access_level: pb.SecurityService.ACCESS_EDITOR,
      handler: 'save'
    }, {
      method: 'put',
      path: '/api/savesite',
      auth_required: true,
      access_level: pb.SecurityService.ACCESS_EDITOR,
      handler: 'updateSite'
    }, {
      method: 'put',
      path: '/api/savepage/:pageId',
      auth_required: true,
      access_level: pb.SecurityService.ACCESS_EDITOR,
      handler: 'updatePage'
    }, {
      method: 'post',
      path: '/api/settheme',
      auth_required: true,
      access_level: pb.SecurityService.ACCESS_EDITOR,
      handler: 'setTheme'
    }, {
      method: 'post',
      path: '/api/contact',
      handler: 'contact',
      content_type: 'application/json'
    }, {
      method: 'get',
      path: '/api/members',
      handler: 'getMembers',
      auth_required: true,
      access_level: pb.SecurityService.ACCESS_EDITOR,
      content_type: 'application/json'
    }, {
      method: 'post',
      path: '/api/publish/site',
      handler: 'publishSite',
      auth_required: true,
      access_level: pb.SecurityService.ACCESS_EDITOR,
      content_type: 'application/json'
    }, {
      method: 'post',
      path: '/api/unpublish/site',
      handler: 'unpublishSite',
      auth_required: true,
      access_level: pb.SecurityService.ACCESS_EDITOR,
      content_type: 'application/json'
    }, {
      method: 'post',
      path: '/api/updateNavOrder',
      handler: 'updateNavOrder',
      auth_required: true,
      access_level: pb.SecurityService.ACCESS_EDITOR,
      content_type: 'application/json'
    }];
    cb(null, routes);
  };

  //exports
  return SaveSite;
};
