'use strict';

pencilblueApp.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', 'cssInjectorProvider', '$httpProvider', '$provide', function($stateProvider, $urlRouterProvider, $locationProvider, cssInjectorProvider, $httpProvider, $provide) {

  $provide.decorator('$exceptionHandler', ['$delegate', function($delegate) {
    return function(exception, cause) {
      Raygun.send(exception, {
        cause: cause
      });
      $delegate(exception, cause);
    };
  }]);

  // $httpProvider.interceptors.push(['$q', function($q) {
  //   return {
  //     'requestError': function(rejection) {
  //       Raygun.send(new Error('Failed $http request'), rejection);
  //       return $q.reject(rejection);
  //     },
  //     'responseError': function(rejection) {
  //       Raygun.send(new Error('Failed $http response'), rejection);
  //       return $q.reject(rejection);
  //     }
  //   };
  // }]);


  //cssInjectorProvider.setSinglePageMode(true);

  $urlRouterProvider.otherwise('/');

  var editPanelQuery = {
    pages: null,
    theme: null,
    layout: null,
    colors: null,
    fonts: null
  };

  var params = '?pages&theme&layout&colors&fonts&showCustomColors';

/*
  var newsState = {
    name: 'news',
    url: '/news' + params,
    component: 'page',
    params: editPanelQuery,
    reloadOnSearch: false
  };

  var mediaState = {
    name: 'media',
    url: '/media' + params,
    component: 'media',
    params: editPanelQuery,
    reloadOnSearch: false
  };

  var resumeState = {
    name: 'resume',
    url: '/resume',
    component: 'page',
    params: editPanelQuery,
    reloadOnSearch: true
  };

  var contactState = {
    name: 'contact',
    url: '/contact' + params,
    component: 'page',
    params: editPanelQuery,
    reloadOnSearch: false
  };

  var aboutState = {
    name: 'about',
    url: '/about' + params,
    component: 'page',
    params: editPanelQuery,
  };

  var galleryState = {
    name: 'gallery',
    url: '/gallery' + params + '&section',
    component: 'gallery',
    params: editPanelQuery,
    resolve: {
      gallery: function(GetGallery) {
        return GetGallery.get();
      }
    },
    reloadOnSearch: false
  };
  */

  var pageState = {
    name: 'page',
    url: '/:slug',
    component: 'page',
    resolve: {
      pagedata: function($transition$, SiteData, $http) {
        
        var slug = $transition$.params().slug;
        var url = '/api/components/page/' + slug;
        if (!SiteData.getSiteData().editing && window.isLive) {
          url += '/live'
        }

        return $http.get(url).then(function(resp) {
          var data = resp.data;
          return {
            page: data.page,
            images: data.images,
            components: data.components
          };
        });
      },
      editing: function(SiteData) {
        return SiteData.getSiteData().editing;
      }
    },
    reloadOnSearch: false
  };

  var homeState = {
    name: 'home',
    url: '/' + params + '&modal',
    component: 'home',
    params: editPanelQuery,
    reloadOnSearch: false
  };


  var indexState = {
    name: 'index',
    url: '/index',
    component: 'home',
    params: editPanelQuery,
    reloadOnSearch: false
  };

  $stateProvider.state(homeState);
  $stateProvider.state(pageState);
  $stateProvider.state(indexState);

}]).run(['$rootScope', '$state', '$stateParams', '$transitions', function($rootScope, $state, $stateParams, $transitions) {
  $rootScope.$state = $state;
  $rootScope.$stateParams = $stateParams;

  $transitions.onSuccess({}, function(transition) {
    try {
      if (window.analytics) {
        var slug = transition.params().slug;      
        window.analytics.sendPageView(slug);  
      }
    } catch (e) {
      console.error(e);
    }    
  });
}]);


pencilblueApp.controller('SiteController', ['$scope', '$rootScope', '$http', '$location', '$state', '$interval', '$sce', 'cssInjector', 'SiteData', 'colorPalettes', 'NavStyling',  function($scope, $rootScope, $http, $location, $state, $interval, $sce, cssInjector, SiteData, colorPalettes, NavStyling) {
  cssInjector.removeAll();
  
  $scope.sitecontroller = true;
  $scope.state = $state;
  $scope.colorPalettes = colorPalettes;

  $scope.stateName = '';
	$scope.$watch('state.current.name', function(newVal) {
		if (newVal.length) $scope.stateName = newVal;
	});

  $scope.editing = false;
  $rootScope._site = $scope.siteData;
  $rootScope._site.components = _.groupBy($scope.components, 'tag');
  $scope._site.images = _.groupBy($scope.siteImages, 'tag');
  var cssPath;

  $scope._site.theme = $scope._site.theme || 4;
  cssPath = '/public/performance/themes/' + $scope._site.theme + '/theme.css';
  cssInjector.add(cssPath);

  $scope.setSiteStyle = function() {
    
    var length = $scope.pages.length;
    NavStyling.setNavStyle(length);
    $scope.navStyle = NavStyling.getNavStyle();
  };
  

  SiteData.setSiteData({
    images: _.groupBy($scope.siteImages, 'tag'),
    components: _.groupBy($scope.components, 'tag'),
    editing: false,
    pages: $scope.pages,
    _site: $scope._site
  });

  
  $scope.pages.forEach(function(page) {
  
    var css = '/public/performance/pages/' + page.layout_category + '/' + page.layout + '.css';
  
    cssInjector.add(css);
  });


  $scope.publishSite = function() {
    swal({
      title: 'Are You Sure?',
      text: 'Are you sure you want to publish your changes?',
      type: 'warning',
      closeOnCofirm: true,
      showCancelButton: true,
      closeOnCancel: true
    }, function(isConfirm) {
      if (isConfirm) {
        $http.post('/api/publish/site').then(function(response) {
          if (response.data.message === 'ok') {
            location.reload();
          }
        }).catch(function(err) {
          console.error(err);
        });
      }
    });
  };

  $scope.getTrialHoursRemaining = function() {
    var date = new Date();
    var expireDate = new Date($rootScope._site.expiration_date);
    var html;
    if (expireDate < date) {
      html = '<span>Your site is currently expired. Please signup for a subscription.</span>';
    } else {
      html = '<span>Your site is currently in trial mode. Your trial will expire on ' + moment($rootScope._site.expiration_date).format('dddd, MMMM Do YYYY h:mm a') + '.</span>';
    }

    $scope.trialText = $sce.trustAsHtml(html);
  };
}]);

pencilblueApp.filter('externalurl', function() {
  return function(input) {
    var cleanURL = input.replace(/http(\:|s\:)\/\//, '');
    return 'http://' + cleanURL;
  };
});

pencilblueApp.filter('pagestyle', function() {
  var pageStyles = {
    background_color: 'Background',
    header_background_color: 'Header Background',
    color: 'Text',
    nav_text_color: 'Navbar Text',
    nav_active_color: 'Navbar Active Page Text',
    navbar_background: 'Navbar Background',
    link_color: 'Link',
    link_hover_color: 'Link Hover',
    button_text_color: 'Button Text',
    button_bg_color: 'Button Background',
    social_background_color: 'Social Icon Background',
    social_font_color: 'Social Icon Text',
    title_color: 'Header Name'
  };

  return function(input) {

    if (pageStyles[input]) {
      return pageStyles[input];
    }
    if (input === 'color') input = 'Font ' + input;
    if (input === 'color_footer') {
      input = input.replace('color_', '');
      input += ' Color';
    }
    var cleanURL = input.replace(/_|-/gi, ' ');
    return cleanURL;
  };
});

pencilblueApp.filter('externalurl', function() {
  return function(input) {
    var cleanURL = input.replace(/http(\:|s\:)\/\//, '');
    return 'http://' + cleanURL;
  };
});

pencilblueApp.filter('telephone', function() {
  return function(input) {
    if (input && input.length) {
      var arr = input.split('');
      arr.splice(0, 0, '(');
      arr.splice(4, 0, ')');
      arr.splice(5, 0, ' ');
      arr.splice(9, 0, '-');
      return arr.join('');
    } else {
      return input;
    }
  };
});

'use strict';

pencilblueApp
  .constant('videoRegexes', {
    iframe: /(youtube|youtu\.be|vimeo)/i,
    notIframe: /(wistia|[a-z0-9]{5,15})/i,
    soundcloud: /soundcloud\.com/i
  })
  .constant('placeholdertext', {
    role1: 'Click to Edit Role 1',
    role2: 'Click to Edit Role 2',
    role1img: 'Upload a photo or headshot that best represents Role 1 (i.e. Actor, Singer, Choreographer).',
    role2img: 'Upload a photo or headshot that best represents Role 2 (i.e. Actor, Singer, Choreographer).',
    subheadAboutText: 'About',
    upcoming1: 'Click to edit Project 1 description',
    upcoming2: 'Click to edit Project 2 description',
    upcoming3: 'Click to edit Project 3 description',
    aboutme: 'Click to enter a brief bio about your multiple disciplines. ',
    projectdescription: {
      header: 'Edit Project Description',
      placeholder: 'Enter a brief description for your recent or upcoming project. Include your role, dates, and location. Boxes left blank in this section will not appear on your website.'
    },
    role1htmleditor: {
      header: 'Edit Role 1',
      description: 'Enter the title of your first discipline (i.e. Actor, Singer, Choreographer) in the text box, below. This title will appear in your navigation bar, as the title of the corresponding Role 1 page of your website, and above the corresponding image on your homepage when a user hovers over the photo.',
      placeholder: 'Example: Actor, Singer, Choreographer'
    },
    role2htmleditor: {
      header: 'Edit Role 2',
      description: 'Enter the title of your second discipline (i.e. Actor, Singer, Choreographer) in the text box, below. This title will appear in your navigation bar, as the title of the corresponding Role 2 page of your website, and above the corresponding image on your homepage when a user hovers over the photo.',
      placeholder: 'Example: Actor, Singer, Choreographer'
    },
    social: 'Click the blue button below to add your social media accounts.',
    classic: 'Want to use a portrait-oriented headshot for your homepage? The Classic template is designed specifically for landscape-oriented headshots, so we recommend choosing a different template.',
    spotlight: 'The homepage of the Spotlight template works best with a landscape-oriented headshot that has a visible background at the top. The navigation bar should not cover your face, so close-up images may not work with this template.',
    abouteditor: {
      header: 'Edit About',
      description: '',
      placeholder: 'This is where you’ll include your bio, which should provide a narrative snapshot of who you are as an artist. <br/><br/> Some actors go traditional, presenting their professional background more or less they way it would appear in a playbill.  Others choose to get a bit more personal and expand upon the usual credits to include information about other skills, interests, or projects related to their work as an actor. <br/><br/>  Either approach is fine, as long as your bio is well written and relevant. Always write in the third person (she/he/they pronouns), use good grammar, and remember to proofread! '
    },
    resumeHeader: 'Click to Upload Photo or Résumé in .jpg, .png, or .pdf format',
    resumeInstruction: 'Your résumé should be clear, legible, and well organized. Present your experience professionally and honestly, and be sure to update whenever you have a new credit to add or to emphasize the experience that best aligns with the roles you’re going out for at any given time.  <br/><br/> Don’t have a résumé? Don’t worry! Our Résumé Builder page layout will guide you through the process of creating one. <br/><br/>  <strong>For safety’s sake, never include your physical address on your résumé.</strong>',
    resumeInstruction2: 'To include a downloadable résumé that visitors can keep or print, click the link below.',
    newseditor: {
      header: 'Edit News',
      placeholder: 'Create and update your news items here. Feature current or upcoming projects, including casting announcements and show dates. Be sure to include venue names and locations, as well as links to relevant production or ticketing websites, news articles, and reviews. (Links can be created in the text editor using the chain link icon.'
    },
    newsImageInstruction: 'Click to upload portrait-oriented photo, graphic, or press clip in .jpg, .png, or .pdf format.',
    newsImageLandscapeInstruction: 'Click to upload landscape-oriented photo, graphic, or press clip in .jpg, .png, or .pdf format.',
    galleryInstructions: 'Click below to upload headshots, production stills, modeling photos, and any images that represent you as a performer. Choose from thumbnail galleries, image carousels, and expanding photo albums in the Page Layouts feature. Group your photos into albums, by projects, headshot session, or modeling campaigns.',
    mediaInstructions: 'Click below to upload audio and video files to your Media page. Group your media clips into albums by project type (TV appearances, commercials, theatre) or, for projects with multiple clips, create albums for each project. You can upload files from YouTube, Vimeo, Wistia, or Soundcloud.',
    contacteditor: {
      description: 'Click to upload portrait-oriented photo in .jpg, .png, or .pdf format.',
      placeholder: 'Use this optional text box to greet your visitors personally, or to include your contact information. <br/><br/>If you don’t want to share your personal information, the optional contact form allows site visitors to send a message directly to your linked email address. <br/><br/>Or, remove this text box or the contact form altogether! To do this, use the page layout tab in the left-hand sidebar area.',
      header: 'Edit Contact'
    },
    landscape: 'This page layout is best for landscape-oriented photos.',
    portrait: 'This page layout is best for portrait-oriented photos.'
  });

angular.module('pencilblueApp').component('gallery', {
  bindings: {
    gallery: '<'
  },
  templateUrl: function($stateParams, $state, SiteData) {
    var siteData = SiteData.getSiteData();
    var themeId = siteData._site.theme;
    var pageName = $state.current.name;
    console.log('/public/performance/pages/' + pageName + '/' + (siteData._site.layouts[pageName] || 1) + '.html');
    return '/public/performance/pages/' + pageName + '/' + (siteData._site.layouts[pageName] || 1) + '.html';
  },  controller: function(GetGallery, ngDialog, SiteData) {
    var self = this;

    var siteData = SiteData.getSiteData();

    this._site = siteData._site;
    this.images = siteData.images;
    this.components = siteData.components;
    this.editing = siteData.editing;


    this.gallery = this.gallery.data;

    this.carouselIdx = Array(this.gallery.length).fill(0);

    this.slide = function(idx, dir, sectionIdx) {
      console.log('slide dir', dir);
      if (idx) {
        if (idx > -1) {
          this.carouselIdx[sectionIdx] = idx;
        } else {
          this.carouselIdx[sectionIdx]++;
        }
      } else if (!idx && dir) {
        if (sectionIdx) {
          this.carouselIdx[sectionIdx] = dir === 'next' ? this.carouselIdx[sectionIdx] + 1 : this.carouselIdx[sectionIdx] - 1;
          $('#carousel-element-' + sectionIdx).carousel(dir);
        } else {
          $('#carousel-element').carousel(dir);
        }
      }
    };

    
  }
});

angular.module('pencilblueApp').component('page', {
  bindings: {
    pagedata: '<', 
    editing: '<'
  },  
  templateUrl: function($state, SiteData, $rootScope) {    
		var page = _.find(SiteData.getSiteData().pages, {slug: $state.params.slug});
		if (page) {		
      
      $rootScope.currentPage = page;			
      var url = '/public/performance/pages/' + page.layout_category + '/' + page.layout + '.html';
      
			return url;
		} else {
			$rootScope.currentPage = null;
		}
  },
  controller: function($scope, $http, $state, $stateParams, $sce, placeholdertext) {
    var self = this;
    this.placeholdertext = placeholdertext;
    this.controllerName = 'Page Component';
    
    this.page = this.pagedata.page;
    this.images = this.pagedata.images;
    this.components = this.pagedata.components;

    this.pageId = this.page._id;    
    this.stateParams = $stateParams;          
    this.info = {success: null, error: null};
    this.alertClass = null;
    this.showAlert = false;

    var pageName = $state.current.name;
    
    this.trustText = function(text) {
      return $sce.trustAsHtml(text);
    };
    
  }
});

angular.module('pencilblueApp').component('home', {
  templateUrl: function ($stateParams, $state, $location, SiteData) {
    var siteData = SiteData.getSiteData();
    var themeId = siteData._site.theme;
		var pageName = $state.current.name;
		if (pageName == 'index') { //needed for prerender
			pageName = 'home';
		}
		
    var templatePath = '/public/performance/themes/' + siteData._site.theme + '/home' + (siteData._site.layouts[pageName]) + '.html';
    return templatePath;
  },
  controller: function($sce, SiteData, placeholdertext) {
    var siteData = SiteData.getSiteData();
    this._site = siteData._site;
    this.images = siteData.images;
    this.placeholdertext = placeholdertext;    
    this.components = siteData.components;
    this.editing = siteData.editing;
    this.trustText = function(text) {
      return $sce.trustAsHtml(text);
    };
  }
});

angular.module('pencilblueApp').component('media', {
  templateUrl: function($stateParams, $state, SiteData) {
    var siteData = SiteData.getSiteData();
    var themeId = siteData._site.theme;
    var pageName = $state.current.name;
    console.log('siteData._site.layouts[pageName]', siteData._site.layouts[pageName]);
    if (pageName) {
      return '/public/performance/pages/' + pageName + '/' + (siteData._site.layouts[pageName] || 1) + '.html';
    } else {
      console.error('WARNING', pageName, 'not defined.');
    }

  },
  controller: function(ngDialog, $sce, $state, $http, SiteData, $scope, videoRegexes) {



    var $ctrl = this;
    $ctrl.controllerName = 'Media Component';

    var siteData = SiteData.getSiteData();
    $ctrl.layout = siteData._site.layouts[$state.current.name];

    $ctrl._site = siteData._site;
    $ctrl.editing = siteData.editing;
    console.log('IN HERE IN HERE IN HEREIN HERE IN HERE IN HEREIN HERE IN HERE IN HEREIN HERE IN HERE IN HEREIN HERE IN HERE IN HERE');
    $ctrl.images = siteData.images;
    $ctrl.components = siteData.components;




  }
});

angular.module('pencilblueApp').component('customPage', {
  templateUrl: function(SiteData, $state) {
    var siteData = SiteData.getSiteData();
    var title = $state.params.title;
    var tag = $state.params.tag;

    console.log('siteData.components[tag]', siteData.components[tag]);
    if (!siteData.components[tag]) return $state.go('home', {}, {reload: true});

    var pageLayout = siteData.components[tag][0].component_specific_data.layout;
    console.log('pageLayout', pageLayout);
    if (pageLayout > -1) {
      return '/public/performance/pages/custom/' + (pageLayout + 1) + '.html';
    } else {
      $state.go('home', {}, {reload: true});
    }
  },
  controller: function($scope, $rootScope, $state, SiteData, $sce) {

    $scope.editing = $rootScope.editing;

    $scope.title = $state.params.title;
    var tag = $state.params.tag;
    $scope.siteData = SiteData.getSiteData();
    $scope.components = $scope.siteData.components;
    $scope.componentData = $scope.siteData.components[tag][0];

    $scope.images = {};
    $scope.texts = {};

    //todo this is super inefficient, it's gonan go through gallery and all images, and all components
    console.log('IMAGES? ', $scope.siteData.images);
    for (var image in $scope.siteData.images) {
      var imageData = $scope.siteData.images[image];
      imageData.forEach(function(p) {
        if (p && p.page === $scope.componentData._id) {
          if (!$scope.images[image]) $scope.images[image] = [];
          $scope.images[image][0] = p;
        }
      });
    }

    for (var text in $scope.components) {
      var component = $scope.components[text];
      component.forEach(function(c) {
        if (c && c.page === $scope.componentData._id) {
          if (!$scope.texts[text]) $scope.texts[text] = [];
          $scope.texts[text][0] = c;
        }
      });
    }

    $scope.$on('text-updated', function() {
      location.reload();
    });

    $scope.$on('image-updated', function() {
      location.reload();
    });

    $scope.trustText = function(text) {
      return $sce.trustAsHtml(text);
    };
  }
});


angular.module('pencilblueApp').component('galleryblock', {
  bindings: {
    pageId: '=',
    editing: '=',
    nocrop: '='
  },
  templateUrl: ['$element', '$attrs', function($element, $attrs) {
    if (!$attrs.partial) {
      console.error('partial url required.');
      return false;
    }
    var url = '/public/performance/pages/gallery/' + $attrs.partial;
    return url;
  }],
  controllerAs: '$ctrl',
  controller: function($http, $sce, GetGallery, ngDialog) {
    var self = this;
    console.log('self.nocrop', self.nocrop);
    GetGallery.get({
      pageId: self.pageId
    }).then(function(resp) {
      
      self.gallery = resp.data;
      self.carouselIdx = Array(self.gallery.length).fill(0);
    });
    
    
    

    this.slide = function(idx, dir, sectionIdx) {
      
      if (idx) {
        if (idx > -1) {
          this.carouselIdx[sectionIdx] = idx;
        } else {
          this.carouselIdx[sectionIdx]++;
        }
      } else if (!idx && dir) {
        if (sectionIdx) {
          this.carouselIdx[sectionIdx] = dir === 'next' ? this.carouselIdx[sectionIdx] + 1 : this.carouselIdx[sectionIdx] - 1;
          $('#carousel-element-' + sectionIdx).carousel(dir);
        } else {
          $('#carousel-element').carousel(dir);
        }
      }
    };

    this.openPicModal = function(section, imgIndex) {
      var copiedSection = angular.copy(section);

      ngDialog.open({
        template: '/public/performance/themes/partials/gallery-modal.html',
        appendClassName: 'gallery-modal',
        resolve: {
          currSection: function() {
            if (imgIndex) {
              var imagesCut = copiedSection.images.splice(0,imgIndex);
              copiedSection.images = copiedSection.images.concat(imagesCut);
            }
            return copiedSection;
          },
          slide: function() {
            return self.slide;
          }
        },
        controller: ['$scope', 'currSection', 'slide', function(scope, currSection, slide) {
          scope.section = currSection;

          scope.slideKey = function($event, idx, dir) {
            var keypress = $event.which;

            if (keypress == 39) {
              slide(idx, 'next');
            } else if (keypress == 37) {
              slide(idx, 'prev');
            }

          };
          scope.slide = function(idx, dir) {
            slide(idx, dir);
          };
        }]
      });
    };    
   
  }
});


angular.module('pencilblueApp').component('mediablock', {
  bindings: {
    pageId: '=',
    editing: '='
  },
  templateUrl: ['$element', '$attrs', function($element, $attrs) {
    if (!$attrs.partial) {
      console.error('partial url required.');
      return false;
    }
    var url = '/public/performance/pages/media/' + $attrs.partial;
    return url;
  }],
  controllerAs: '$ctrl',
  controller: function($http, $sce, GetMedia, ngDialog, videoRegexes) {
    var self = this;
    GetMedia.get({
      pageId: self.pageId
    }).then(function(resp) {
      self.media = resp.data;
    });

    this.regexes = videoRegexes;

		if (! document.getElementById('wistiaScript')) {

			var wistiaTag = document.createElement("script");
			wistiaTag.type = "text/javascript";
			wistiaTag.async;
			wistiaTag.id = "wistiaScript";
			wistiaTag.keepScript = "true";
			wistiaTag.src = "//fast.wistia.net/assets/external/E-v1.js";
			$("head").append(wistiaTag);
		}

		if (! document.getElementById('scScript')) {

			var soundcloudTag = document.createElement("script");
			soundcloudTag.type = "text/javascript";
			soundcloudTag.async;
			soundcloudTag.id = "scScript";
			soundcloudTag.keepScript = "true";
			soundcloudTag.src = "//connect.soundcloud.com/sdk/sdk-3.1.2.js";
			$("head").append(soundcloudTag);
		}


    
    this.testUrl = function(url) {
      if (self.regexes.iframe.test(url)) {
        return 'iframe';
      } else if (self.regexes.soundcloud.test(url)) {
        return 'soundcloud';
      } else {
        return 'javascript';
      }
    };

    this.trustAsUrl = function(url) {
      return $sce.trustAsResourceUrl(url);
    };    
    

  }
});


angular.module('pencilblueApp').component('resumeblock', {
  bindings: {
    pageId: '@',
    editing: '='
  },
  templateUrl: '/public/performance/partials/resumeblock.html',
  controllerAs: '$ctrl',
  controller: function($http, $sce, GetResumeBuilder, ngDialog) {
    var self = this;
    GetResumeBuilder.get({
      pageId: self.pageId
    }).then(function(resp) {

      self.resume = resp.data;
    });    
    
  }
});

  //the main navigation
  angular.module('pencilblueApp')
  .directive('mainnav', function() {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: function(elem, attr) {
        return attr.override || '/public/performance/partials/mainnav.html';
      },
      controller: function($scope, $rootScope) {
        $scope.editing = $rootScope.editing;
      }
    };
  });

angular.module('pencilblueApp').component('social', {
  templateUrl: function() {
    return '/public/performance/partials/social.html';
  },
  bindings: {
    social: '=',
    _site: '=site',
    editing: '=',
    showHandle: '@'
  },
  controller: function(ngDialog, $filter) {

    var scope = this;
    
    
    this.available_social = [{
      name: 'facebook',
      icon: 'facebook',
      url: 'https://www.facebook.com/'
    },{
      name: 'twitter',
      icon: 'twitter',
      url: 'https://www.twitter.com/'
    },{
      name: 'instagram',
      icon: 'instagram',
      url: 'https://www.instagram.com/'
    },{
      name: 'youtube',
      icon: 'youtube',
      url: 'https://www.youtube.com/',
      title: 'Full Youtube url'
    },{
      name: 'email',
      icon: 'envelope-o',
      url: 'mailto:'
    },{
      name: 'imdb',
      icon: 'imdb',
      url: 'http://www.imdb.com/'
    }, {
      name: "backstage",
      icon: 'backstage',
      url: 'https://www.backstage.com/u/'
    }, {
      name: 'actors_access',
      icon: 'actors_access',
      url: 'http://resumes.actorsaccess.com/'
    }];


    this.edit = function() {
      ngDialog.open({
        template: '/public/performance/partials/social-edit-modal.html',
        controllerAs: '$ctrl',
        appendClassName: 'social-edit-modal',
        resolve: {
          social: function() {
            return scope.social;
          },
          available_social: function() {
            return scope.available_social;
          }
        },
        controller: ['social', 'available_social', 'GetSite', function(social, available_social, GetSite) {
          var self = this;
          this.social = social;
          console.log('social', social);
          this.available_social = available_social;
          this.saveSocial = function() {            
            GetSite.set({
              social: self.social
            }).then(function(updatedPage) {
              console.log('upadted');
              console.log('self.social', self.social);
              console.log('social', social);
              ngDialog.close();
            }, function(err) {
              console.error(err);
              ngDialog.close();
            });                        
          };
          this.handleChanged = function(val) {
            console.log('handle changed', self.social[val].handle);
            var clean = self.social[val].handle.replace(/(http(s|)\:\/\/(www\.|)(youtube\.com|youtu\.be)(\/|))/gi, '');
            var noSpaces = clean.replace(/\s| /g, '');
            self.social[val].handle = noSpaces;
          };
        }]
      });
    };
  }
}).filter('socialFormat', function() {
  return function(input) {
    if (input) {
      var mainSplit = input.split('_');
      if (mainSplit.length > 1) {
        mainSplit.forEach(function(i) {
          var split = i.split('');
          var firstLet = split[0];
          i = firstLet.toUpperCase() + split.slice(1);
        });
        return mainSplit.join(' ');
      } else {
        return input;
      }
    }

    return input;
  };
});

//the modal directive to upload and image and crop
//the main navigation
angular.module('pencilblueApp')
  .directive('linky', ['$window', 'Upload', 'ComponentFactory', 'SiteData', function($window, Upload, ComponentFactory, SiteData) {
    return {
      scope: {
        ngModel: '=',
        tag: '@',
        type: '@',
        accept: '@',
        text: '@',
        editText: '@',
        pageId: '@'
      },
      restrict: 'E',
      replace: true,
      template: function(tElem, tAttrs) {
        var accept = tAttrs.accept ? ' accept="' + tAttrs.accept + '" ngf-pattern="\'' + tAttrs.accept + '\'"' : '';
        console.log('accept', accept);

        return '<' + tAttrs.type +  (window.editing ? ' ngf-select="uploadFile($files[0])" ' + accept : ' ') + '></' + tAttrs.type + '>';
      },
      link: function(scope, elem, attrs) {
        var siteData = SiteData.getSiteData();
        elem.addClass(scope.type);

        if (!scope.pageId) {
          console.error('No Page Id Specified on ImageCrop' + scope.tag);
        }

        if (attrs.text) {
          elem.text(attrs.text);
        }

        if (siteData.editing) {
          console.log('THE TEXT: ', scope.ngModel);
          if (!scope.ngModel) {
            elem.text(scope.editText || 'Click here to upload résumé in .pdf format.');
          } else if (scope.ngModel.metadata) {
            console.log('ngModel', scope.ngModel);
            elem.text(scope.ngModel.metadata.original_filename + '.' + scope.ngModel.metadata.format);
          }
        } else if (!scope.ngModel) {
          elem.remove();
        } else if (scope.ngModel.text) {
          elem.text(scope.ngModel.text);
        }


        elem.on('click', function(e) {
          console.log('clicked!');
          if (scope.ngModel && !siteData.editing) {
            $window.open(scope.ngModel.url);
            if (scope.ngModel.text) {
              elem.text(scope.ngModel.text);
            }
          }
        });

        if (siteData.editing) {
          scope.uploadFile = function(file) {
            Upload.upload({
              url: window.CLOUDINARY_UPLOAD_API_URL,
              data: {
                folder: window.location.host,
                upload_preset: 'zymnlox3',
                file: file
              }
            }).then(function(resp) {
              console.log('update component');
              ComponentFactory.update({
                url: resp.data.secure_url,
                tag: scope.tag,
                page: scope.pageId,
                metadata: resp.data
              }).then(function(data) {
                scope.ngModel = data.data;
                alert('Uploaded!');
                $window.open(scope.ngModel.url);
              });
            }, function(resp) {
              //todo error handling on pending image
              console.log('resp err', resp);
              console.log('Error status: ' + resp.status);
            }, function(evt) {
              //todo loading info
              console.log('resp progressPercentage', evt);
              var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
              console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
          };
        }
      }
    };
  }]);

angular.module('pencilblueApp').component('carousel', {
  bindings: {
    ngModel: '=',
    idealSize: '<'
  },
  template: '<div class="carousel">' +
              '<button style="margin-top: 10px;" class="btn btn-primary sitewide" ng-show="$carousel.editing" ng-click="$carousel.editImages()"><i class="fa fa-plus"></i> Add/Edit Images</button>' +
              '<div id="main-carousel" class="slick-carousel" ng-style="$carousel.loadedStyle">' +
                '<div class="slick-img-wrap" ng-repeat="img in $carousel.ngModel.images track by $index">' +
									'<a ng-href="{{img.href || \'#\'}}" target="{{img.href_target}}"><img ng-src="{{img.url}}" alt="" /></a>' +
                '</div>' +
              '</div>' +
            '</div>',
  controllerAs: '$carousel',
  controller: function($scope, $location, $state, $stateParams, SiteData, Upload, $uibModal, $timeout) {
    var self = this;
    self.loadedStyle = {'opacity': 0};
    var openModal = $stateParams.modal === 'true';
    console.log('ngModel: ', self.ngModel);

    var siteData = SiteData.getSiteData();
    self.editing = siteData.editing;

    if (!self.ngModel) {
      self.ngModel = {images: []};
    }

    // if (!self.ngModel || (self.ngModel && (!self.ngModel.images || self.ngModel.images.length < 1))) {
    //   if (self.editing) {
    //     //var image = {url: '/public/performance/img/placeholders/' + (self.idealSize || '200x200') + '.png'};
    //     //self.ngModel.images = [image, image, image];
    //     //self.ngModel.placeholders = true;
    //   }
    // }

    imagesLoaded($('.slick-carousel img'), function() {
      self.loadedStyle = {'opacity': 1};
      $scope.$apply();
      objectFitImages();
    });

    self.editImages = function(hasQueryStringInURL) {
      $state.go($state.current, {modal: true}, {reload: false});
      var modalInstance = $uibModal.open({
        animation: false,
        size: 'lg',
        windowClass: 'home-page-image-edit',
        templateUrl: '/public/performance/partials/carouselUpload.html',
        resolve: {
          ngModel: function() {
            return self.ngModel;
          },
          thumbnailmode: function() {
            return false;
          },
          idealSize: function() {
            return '1200x700';
          }
        },
        controllerAs: 'carouselUploader',
        controller: function($http, ngModel, thumbnailmode, idealSize) {
					var carouselUploader = this;

					carouselUploader.thumbnailmode = thumbnailmode;
					carouselUploader.idealSize = idealSize;

          carouselUploader.ngModel = ngModel;
          carouselUploader.context = 'gallery';

          var firstSort = true;

          var uploadImage = '/api/image/upload';

          carouselUploader.sortableOptions = {
            tolerance: 'pointer',
            handle: '.drag-tab',
            helper: 'clone',
            stop: function() {
              $http.post('/api/carousel/update', carouselUploader.ngModel).then(function(res) {
                carouselUploader.ngModel = res.data;
                $state.go($state.current, {modal: false}, {reload: true});
              });
            }
          };

          carouselUploader.upload = function(files) {
            console.log('carousel files: ', files);
            carouselUploader.fileUploadError = null;

            var validFiles = files.filter(function (f) { return f.size <= 10000000; });
            if (validFiles.length !== files.length) carouselUploader.fileUploadError = 'One or more files uploaded are too large!';

            if (!validFiles.length) return;

            var fileCount = validFiles.length;

            var fileCompleted = 0;
            carouselUploader.updatePercentage();
            validFiles.forEach(function(i) {
              var pendingImage = {};
              pendingImage.file = i;
              pendingImage.name = i.name;
							console.log('siteData', siteData);

							var uploadData = {
                  file: i,
                  tag: 'homeCarouselImage',
									height: 700,
									width: 1200,
									crop: 'pad'
							};
							if (siteData._site.pageStyles && siteData._site.pageStyles.backgroundColor) {
								uploadData.background = siteData._site.pageStyles.backgroundColor;
							}

              Upload.upload({
                url: uploadImage,
                data: uploadData
              }).then(function(img) {
                fileCompleted++;

                if (!carouselUploader.ngModel.images.length || carouselUploader.ngModel.placeholders) carouselUploader.ngModel.images = [];
                carouselUploader.ngModel.images.push(img.data);

								if (fileCompleted === fileCount) {
									$http.post('/api/carousel/update', carouselUploader.ngModel).then(function(res) {
											carouselUploader.ngModel = res.data;
									}).catch(function(err) {
										console.error(err);
									});
								}
              }, function(resp) {
                console.log('resp err', resp);
                console.log('Error status: ' + resp.status);
              }, function(evt) {
                console.log('resp progressPercentage', evt);
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                pendingImage.progressPercentage = progressPercentage;
                // carouselUploader.progressPercentage = progressPercentage;
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
              });
            });
          };

          carouselUploader.deleteImage = function(index) {
            swal({
              title: 'Are you sure?',
              text: 'Delete this image?',
              closeOnConfirm: false,
              showCancelButton: true,
              type: 'warning'
            }, function(confirmed) {
              if (confirmed) {
								carouselUploader.ngModel.images.splice(index, 1);

                $http.post('/api/carousel/update', carouselUploader.ngModel).then(function(res) {
                  carouselUploader.ngModel = res.data;
                  swal('Success', '', 'success');
                });
              }
            });
          };

          carouselUploader.percentageCount = 0;
          carouselUploader.progressPercentage = 0;
          carouselUploader.updatePercentage = function() {
            console.log('progress: ', carouselUploader.percentageCount, carouselUploader.progressPercentage);
            if (carouselUploader.percentageCount <= 5 && carouselUploader.progressPercentage < 100) {
              carouselUploader.percentageCount++;
              carouselUploader.progressPercentage += 25;
              $timeout(function() {
                carouselUploader.updatePercentage();
              }, 200);
            } else {
              carouselUploader.progressPercentage = 100;
              $timeout(function() {
                carouselUploader.progressPercentage = 0;
              }, 3000);
            }
          };
        }
			});

			modalInstance.result.then(function(component) {
				location.replace("/edit");
			}, function() {
				console.log('Modal dismissed at: ' + new Date());
			});
    };

    if (openModal && self.editing) {
      self.editImages(true);
    }
  }
});

pencilblueApp.directive('moveToNextInput', function() {
  return {
    restrict: 'A',
    scope: {
      closest: '@'
    },
    link: function($scope, elem) {
      elem.bind('keydown', function(e) {
        var code = e.keyCode || e.which;
        var closest = $scope.closest.replace('class_', '.').replace('id_', '#');

        if (code === 13) {
          e.preventDefault();
          if ($scope.closest === 'input') {
            elem.next('input').focus();
          } else if (elem.parent(closest).next()[0]) {
            var length = elem.parent('.form-group').next().find('input').length;

            if (length > 1) {
              elem.parent(closest || 'div').next().find('input')[0].focus();
            } else {
              elem.parent(closest || 'div').next().find('input').focus();
            }
          } else {
            e.preventDefault();
          }
        }
      });
    }
  };
});

pencilblueApp.directive('noEnter', function() {
  return {
    link: function(scope, elem) {
      elem.bind('keydown', function(e) {
        var code = e.keyCode || e.which;

        if (code === 13) {
          e.preventDefault();
        }
      });
    }
  };
});

pencilblueApp.directive('galleryWithoutSection', [function () {
  return {
    restrict: 'A',
    scope: {
      gallery: '='
    },
    link: function(scope, element, attrs) {
      console.log('scope', scope);
      if (scope.gallery) {
        console.log('in gallery');
        var caption = angular.element('<div id="caption">&nbsp;</div>');

        var ul = angular.element('<ul id="gallery-list"/>');
        scope.gallery.forEach(function(section) {
          if (section.images.length) {
            section.images.forEach(function(image) {
              var caption = image.credit ? '<p style="text-align: center;padding: 1em 0;">Photo By: ' + image.credit + '</p>' : '&nbsp;';
              var title = image.title ? '<h3 style="text-align: center;">' + image.title + '</h3>' : '';
              ul.append(
                '<li data-credit="' + (image.credit || '') + '"' + ' data-thumb="' + image.thumbnail + '" class="carousel-item">' +
                      '<img src="' + image.url + '" alt="' + image.title + '" />' +
                       title +
                       caption +
                '</li>'
                );
            });
          }
        });
        element.append(ul);
        //element.append(caption);

        var updateCaption = function(li) {
          var title = li.getAttribute('data-title') || '';
          var credit = li.getAttribute('data-credit') || '';
          if (credit) {
            caption.html('Photo by ' + credit);
          } else {
            caption.html('&nbsp;');
          }

        };

        $(document).ready(function() {
            $('#gallery-list').lightSlider({
              gallery: true,
              item: 1,
              keyPress: true,
              // slideMargin: 20,
              thumbMargin: 10,
              loop: true,
              slideMargin: 0,
              thumbItem: 7,
              onSliderLoad: function(el) {
                var li = el.children()[0];
                //updateCaption(li);
              },
              onAfterSlide: function(el, index) {
                var li = el.children()[index];
                //updateCaption(li);
              }
            });
        });
      } else {
        console.log('NOT IN GALLERY');
      }
    }
  };
}]);

pencilblueApp.directive('resumeBuilder', ['ngDialog', function(ngDialog) {
  return {
    restrict: 'A',
    scope: {
      ngModel: '=',
      pageId: '@'
    },
    link: function(scope, el) {
      el.bind('click', function() {
        ngDialog.open({
          template: '/public/performance/themes/partials/resumeBuilder.html',
          appendClassName: 'resume-modal',
          resolve: {
            ngModel: function() {
              return scope.ngModel;
            },
            updateTable: function() {
              return scope.updateTable;
            }
          },
          controller: function($scope, ngModel, updateTable, GetSite) {  
            console.log('ngModel', ngModel);          
            $scope.table = ngModel;
            $scope.onlyNumbers = /^\d+$/;

            if (ngModel && ngModel.length) {
              $scope.tableInitialized = true;
            } else {
              $scope.tableInitialized = false;
            }

            // $scope.phoneReg = function(infoKey) {
            //   return /^\d+$/;
            //   if (key === 'phone') {
            //     return /^[0-9]*$/;
            //   } else {
            //     return null;
            //   }
            // };

            $scope.addData = function(row) {
              row.data.push({});
              updateTable($scope.table);
            };

            $scope.addRow = function(key) {
              var newData;
              switch (key) {
              case 'experience':
                newData = {header: '', data: [{title: '', role: '', credit: ''}]};
                break;
              case 'education':
                newData = {school: '', degree: '', data: [{title: '', professors: ''}]};
                break;
              }

              $scope.table[key].push(newData);
              updateTable($scope.table);
            };

            $scope.updateTable = function() {
              updateTable($scope.table);
            };

            $scope.saveTable = function() {
              ngDialog.closeAll();
              
              GetSite.set({
                resume: {
                  table: ngModel
                }
              }, true);
            };

            $scope.removeRow = function(section, rowIdx, idx) {
              $scope.table[section][rowIdx].data.splice(idx, 1);
            };

            $scope.deleteSection = function(rowIdx) {
              $scope.table.experience.splice(rowIdx, 1);
            };
          }
        });
      });

      scope.updateTable = function(table) {
        scope.ngModel = table;
      };
    }
  };
}]);

pencilblueApp.directive('dynamicStyles', function() {
  return {
    scope: {
      pageStyles: '='
    },
    templateUrl: '/public/performance/themes/dynamic-styles.css'
  };
});

pencilblueApp.value('ui.config', {
  sortable: {
    connectWith: '.draggable'
  }
}).directive('editGalleryMedia', function(ngDialog) {
  return {
    restrict: 'E',
    scope: {
      modal: '=',
      media: '=',
      images: '=',
      layout: '<',
      title: '@',
      context: '@',//gallery or media
      pageId: '@'
    },
    templateUrl: '/public/performance/partials/edit-gallery-media.html',
    controller: function($scope, $rootScope, $state, $http, $window, Upload, videoRegexes) {
      $scope.regexes = videoRegexes;
      $scope.contextKeys = {
        gallery: 'images',
        media: 'files'
      };
      if ($scope.images) {
        $scope.files = $scope.images;
      } else if ($scope.media) {
        $scope.files = $scope.media;
        console.log('setting filse to ', $scope.files);
      }

      $scope.setShowModal = function(bool) {
        $scope.saveMedia(false, function() {
          $scope.showModal = bool;
          if (bool == true) {
            angular.element('body').css({
              overflow: 'hidden'
            });
          } else {
            angular.element('body').css({
              overflow: 'auto'
            });
            $state.go($state.current, {}, {reload: true});
          }
        });
      };

      var saveUrl = '/api/' + $scope.context + '/update';
      var updateImage = '/api/image/update';
      var uploadImage = '/api/image/upload';

      $scope.sortableOptions = {
        tolerance: 'pointer',
        connectWith: '.draggable',
        start: function(e, ui) {
          $scope.dragEl = ui.helper;
          $(ui.helper).css('boxShadow', '2px 4px 6px rgba(59,59,59,0.4)');
          $('.draggable').css('background-color', '#ccc');
        },
        stop: function() {
          console.log('stop dragging');
          $($scope.dragEl).css('boxShadow', 'none');
          $('.draggable').css('background-color', 'transparent');
          $scope.dragEl = null;

          var newGallery = angular.copy($rootScope._site[$scope.context]);
          for (var i in $scope.files) {
            newGallery[i] = {};
            var key = $scope.contextKeys[$scope.context];
            newGallery[i][key] = $scope.files[i][key].map(function(obj) {
              if (key === 'images') {
                return obj._id;
              } else {
                return obj;
              }
            });
          }
          console.log('The New Gallery: ', newGallery);
          $rootScope._site[$scope.context] = newGallery;
          $scope.saveMedia(false);
        }
      };

      $http.get('/api/' + $scope.context).then(function(res) {
        $scope.files = res.data;
      }, function(res) {
        console.log('error: ', res);
      });


      $scope.getSectionFiles = function(section) {
        if (!section) return;
        if ($scope.context === 'gallery' && section.images) {
          return {files: section.images};
        } else if (section.files) {
          return {files: section.files};
        }
      };

      $scope.testUrl = function(url) {
        if ($scope.regexes.iframe.test(url)) {
          return 'iframe';
        } else if ($scope.regexes.soundcloud.test(url)) {
          return 'soundcloud';
        } else {
          return 'javascript';
        }
      };

      $scope.addVideoModal = function(sectionIndex) {
        ngDialog.openConfirm({
          className: 'add-video-dialog',
          appendClassName: 'video-modal',
          template: '/public/performance/themes/upload.html',
          scope: $scope,
          controller: ['$scope', '$rootScope', function($scope, $rootScope) {
            $scope.selection = 'youtube';
            $scope.newVideo = {};
            $scope.placeholders = {
              youtube: 'https://youtube.com/watch?v=<your_video_id> or youtu.be/<your_video_id>',
              vimeo: 'Enter the vimeo id',
              wistia: 'Enter the id of your Wistia video here',
              soundcloud: 'https://soundcloud.com/<username>/<track-name>'
            };
            $scope.changeSelection = function(val) {
              $scope.selection = val;
            };


            $scope.regexes = videoRegexes;

            $scope.testUrl = function(url) {
              return $scope.regexes.iframe.test(url);
            };

            var vimeoErrorSwal = swal.bind(swal, {
              title: 'Oops!',
              text: 'You\'ve entered an invalid vimeo id, the Vimeo id must formatted like this: <pre>294208496</pre>',
              html: true,
              type: 'error'
            });

            $scope.addVideo = function(isValid) {
              if (isValid) {
                $scope.newVideo.provider = $scope.selection;

                if ($scope.newVideo.provider === 'youtube' && $scope.newVideo.url.indexOf('embed') === -1) {
                  // youtube
                  var url = $scope.newVideo.url.replace('watch?v=', 'embed/').replace(/&.*$/, '');
                  if (!/(youtu\.be)/gi.test(url)) {
                    $scope.newVideo.url = url;
                  } else {
                    return swal('Oops!', 'We currently do not support shortened youtube links: "https://youtu.be/videoid", please enter the full video link', 'error');
                  }
                } else if ($scope.newVideo.provider === 'vimeo') {
                  // vimeo
                  var id = $scope.newVideo.url;
                  if (id.indexOf('vimeo.com') >= 0) {
                    var vimeoReg = /(http(s|):\/\/|)vimeo\.com(\/\w*)*\/(\d{1,15}$)/i;
                    var vimeoIdMatch = id.match(vimeoReg);
                    console.log('REG MATCH: ', vimeoIdMatch);
                    try {
                      var vimeoId = vimeoIdMatch[vimeoIdMatch.length - 1];
                      console.log('vimeoid: ', vimeoId);
                      $scope.newVideo.url = 'https://player.vimeo.com/video/' + vimeoId;
                    } catch (error) {
                      vimeoErrorSwal();
                      return;
                    }
                  } else if (/^\d{1,}$/.test(id)) {
                    $scope.newVideo.url = 'https://player.vimeo.com/video/' + id;
                  } else {
                    vimeoErrorSwal();
                    return;
                  }

                } else if ($scope.newVideo.provider === 'wistia') {
                  if ($scope.newVideo.url.length > 15) {
                    return swal('Oops!', 'It looks like the wistia video url you entered is invalid, please make sure you enter only the 10-digit video id', 'error');
                  }
                }

                console.log('files', $scope.files);
                console.log('sectionIndex', sectionIndex);
                console.log('$scope.newVideo', $scope.newVideo);
                if (!$scope.files[sectionIndex]) $scope.files[sectionIndex] = { files: [] };
                $scope.files[sectionIndex].files.push($scope.newVideo);

                $scope.saveMedia(false, function() {
                  $scope.newVideo = {};
                  $scope.addVideoForm.$setPristine(true);
                  $scope.addVideoForm.$setUntouched(true);
                  angular.element('.ng-invalid').removeClass('ng-invalid');
                });
              } else {
                return swal('Oops!', 'You must fill out all fields and the url must match the video type', 'error');
              }
            };
          }]
        }).then(function(data) {
          console.log('closed with data', data);

        });
      };
      //
      $scope.addSection = function() {
        if (!$scope.files) $scope.files = [];

        var obj = {
          name: $scope.newSection,
          created_at: new Date()
        };
        console.log('context', $scope.context);

        console.log('NEW SECTION: ', obj);

        obj[$scope.contextKeys[$scope.context]] = [];

        $scope.files.unshift(obj);

        var url = '/api/' + $scope.context + '/addGallerySection';
        console.log('url', url);
        $('#addSection').modal('hide');
        $scope.newSection = '';
        $http.post(url, {gallery: obj}).then(function() {
          // all good
        }).catch(function(err) {
          swal('Oops!', 'There was an error creating the ' + $scope.context + ': ' + JSON.stringify(err.data, null, 2), 'error');
        });
      };

      $scope.saveMedia = function(reload, cb) {
        console.log('saveMedia $scope.files', $scope.files);
        var newData = $scope.files.map(function(section) {
          console.log('in saveMedia', section);
          if (!section) {
            return;
          }
          var obj = {
            name: section.name,
            subtitle: section.subtitle,
            created_at: section.created_at
          };

          if ($scope.context === 'gallery') {
            if (section.images) obj.images = section.images.filter(function (image) { return image !== null; }).map(function(image) { return image._id; });
          } else {
            if (section.files) obj.files = section.files.filter(function (file) { return file !== null; }).map(function(file) { return {
              title: file.title,
              url: file.url,
              provider: file.provider
            }; });
          }

          return obj;
        });

        var body = {};
        body[$scope.context] = newData;

        $http.post(saveUrl, body).then(function(response) {
          if (reload) {
            swal('Success!', 'Changes saved.', 'success');
            angular.element('body').css({
              overflow: 'auto'
            });
            $window.location.reload();
          }

          console.log('back from saving the stuff');
          console.log(response);
          $rootScope._site = response.data;
          if (typeof cb === 'function') cb();
        }, function(err) {
          console.error(err);
          swal('Error!', 'Chages not saved.', 'error');
        });
      };


      $scope.updateImage = function(imageId, field, data) {
        if ($scope.images) {
          var opts = {
            _id: imageId
          };
          opts[field] = data;
          return $http.post(updateImage, opts).then(function() {
            $scope.saveMedia(false);
          });
        }
      };

      $scope.cropImage = function($sectionIdx, $index, img) {
        console.log('sectionIdx', $sectionIdx);
        console.log('index', $index);
        $scope.gallery[$sectionIdx].images.splice($index, 1);
        $scope.saveMedia(false);
      };


      $scope.deleteImage = function($sectionIdx, $index) {
        console.log('sectionIdx', $sectionIdx);
        console.log('index', $index);
        console.log($scope[$scope.context], $scope.context);
        $scope.files[$sectionIdx][$scope.contextKeys[$scope.context]].splice($index, 1);
        $scope.saveMedia(false);
      };

      $scope.deleteSection = function(idx) {
        swal({
          title: 'Are You Sure?',
          text: 'Do you want to delete this album?',
          type: 'warning',
          showCancelButton: true,
          closeOnConfirm: false,
          confirmButtonText: 'Yes, delete it!'
        }, function(isConfirm) {
          if (isConfirm) {
            removeSection(idx, function() {
              swal({
                title: 'Success',
                text: 'Section Deleted!',
                type: 'success',
                timer: 5000
              });
            });
          }
        });
      };

      $scope.moveSection = function(index, direction) {
        var newIndex = index + direction;
        if (newIndex > $scope.files.length - 1) {
          newIndex = $scope.files.length - 1;
        } else if (newIndex < 0) {
          newIndex = 0;
        }

        var item = $scope.files.splice(index, 1);
        if (item) {
          $scope.files.splice(newIndex, 0, item[0]);
          $scope.saveMedia(false);
        }
      };

      function removeSection(idx, callback) {
        $scope.$apply(function() {
          $scope.files.splice(idx, 1);

          $scope.saveMedia(false);
          callback();
        });
      }

      $scope.upload = function(files, section) {
        $scope.fileUploadError = null;

        var validFiles = files.filter(function (f) { return f.size <= 4000000; });
        if (validFiles.length !== files.length) $scope.fileUploadError = 'One or more files uploaded are too large!';

        if (!validFiles.length) return;

        var fileCount = validFiles.length;

        var fileCompleted = 0;
        validFiles.forEach(function(i) {
          var pendingImage = {};
          pendingImage.file = i;
          pendingImage.name = i.name;
          section.images.push(pendingImage);
          Upload.upload({
            url: uploadImage,
            data: {
              file: i,
              tag: 'gallery',
              height: 800
            }
          }).then(function(img) {
            fileCompleted++;
            var imgIndex = _.findIndex(section.images, { name: pendingImage.name});
            section.images[imgIndex] = img.data;
            if (fileCompleted === fileCount) {
              $scope.saveMedia(false);
            }
          }, function(resp) {
            //todo error handling on pending image
            console.log('resp err', resp);
            console.log('Error status: ' + resp.status);
          }, function(evt) {
            //todo loading info
            console.log('resp progressPercentage', evt);
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            pendingImage.progressPercentage = progressPercentage;
            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
          });
        });
      };

      $scope.updateSectionName = function() {
        $scope.saveMedia(false);
      };
    }
  };
});

pencilblueApp.directive('navItem', function($rootScope, $state, $http) {
  return {
    restrict: 'A',
    scope: {
      item: '=',
      link: '@',
      page: '=',
      editPage: '&',
      removePage: '&',
    },
    template:
              '<i ng-show="!link" class="nav-handle fa fa-bars"></i>' +
              '<a ui-sref-active="active" ui-sref="page({ slug: item.slug })" ng-show="link">{{item.nav_title}}</a>' +
              '<span ng-hide="link" style="{{item.role === \'home\' ? \'padding-left: 30px;\' : \'\'}}">{{item.nav_title}}</span>' +
              '<i style="margin-left: 10px;" class="fa fa-pencil" ng-click="editPageName1(item, $index)" title="Edit Page Name" ng-show="!link"></i>',
    link: function(scope, element, attrs) {      
      scope.editPageName1 = function(item) {
        swal({
          title: 'Change Page Name',
          type: 'input',
          inputPlaceholder: item.nav_title,
          showCancelButton: true
        }, function(newPageName) {
          if (newPageName) {
            scope.editPage()(scope.item._id, newPageName);
          }
        });
      };

      scope.removePage1 = function(item) {
        scope.removePage()(item);
      };
    }
  };
});

function capitalize(string) {
  var output = string.split(' ');
  output.forEach(function(i, idx) {
    output[idx] = (i.charAt(0).toUpperCase() + i.substring(1));
  });

  return output.join(' ');
}

pencilblueApp.directive('footertext', ['GetSite', '$location', '$transitions', '$state', function(GetSite, $location, $transitions, $state) {
  return {
    restrict: 'E',
    scope: {
      _site: '=site',
      editing: '='
    },
    template: '<div class="footer-text-wrap" ng-if="show()" ng-cloak>'+
                '<p html-editable global="true" ng-model="_site.components.footerHtml[0]" tag="footerHtml" empty="Footer" ng-bind-html="_site.components.footerHtml[0].html || (editing ? placeholder : null)"></p>'+
                '<div id="footer-text" class="form-group" ng-if="editing && _site.theme !== 7" ng-cloak>'+
                  '<input type="checkbox" ng-model="_site.footerTextHomeOnly" ng-change="toggleFooterText()" />'+
                  '<span class="sitewide">&nbsp;<small>Show footer text only in home page</small></span>'+
                '</div>'+
              '</div>',
    link: function($scope) {
      $scope.onStateChange = function() {
        //var navOrder = $scope._site.navOrder.map(function(i) { return i.slug ? '/page/' + i.slug : '/' + i.title; }).filter(function(i) { return i !== 'home'; });
        
        $scope.show = function() {
          //todo never do stuff like this, unmaintainable          
          // if ($scope._site.theme === 7 || $scope.editing) {
          //   return true;
          // } else {
          //   return navOrder.indexOf($location.path()) > -1 ? !$scope._site.footerTextHomeOnly : true;
          // }
          return $scope._site.footerTextHomeOnly;
        };
      };

      $scope.onStateChange();
      $transitions.onSuccess( { to: '*', from: '*' }, $scope.onStateChange);

      
      
      $scope.placeholder = 'You can put anything you want in this space here -- a quote, a mantra, nothing, anything!<br><br>Click the blue button below to add your social media accounts and email!';
      $scope.toggleFooterText = function() {              
        GetSite.set({
          footerTextHomeOnly: $scope._site.footerTextHomeOnly
        }).then(function() {
          //$state.go($state.current, $state.params, {reload: true});
        });        
        
      };
    }
  };
}]);

pencilblueApp.factory('GetSite', ['$http', function($http) {
  var saveUrl = '/api/savesite';
  return {
    get: function() {
      return $http.get('/api/site');
    },
    set: function(data) {
      return $http.post(saveUrl, data);
    }
  };
}]).factory('GetGallery', ['$http', function($http) {
  var galleryUrl = '/api/gallery';
  return {
    get: function(data) {
      
      return $http.get(galleryUrl, {
        params : data
      });
    },
    set: function(data) {
      return $http.post(galleryUrl, data);
    }
  };
}]).factory('GetResumeBuilder', ['$http', function($http) {
  var resumeUrl = '/api/resume-builder';
  return {
    get: function(data) {
      
      return $http.get(resumeUrl, {
        params : data
      });
    },
    set: function(data) {
      return $http.post(resumeUrl, data);
    }
  };
}]).factory('GetMedia', ['$http', function($http) {
  var mediaUrl = '/api/media';
  return {
    get: function(data) {
      
      return $http.get(mediaUrl, {
        params : data
      });
    },
    set: function(data) {
      return $http.post(mediaUrl, data);
    }
  };
}]).factory('ImgFactory', ['$http', function($http) {
  return {
    get: function() {
      return $http.get('/api/image');
    },
    update: function(img) {
      return $http.post('/api/image/update', img);
    },
    delete: function(img) {
      return $http.post('/api/image/delete', img);
    }
  };
}]).factory('ComponentFactory', ['$http', function($http) {
  return {
    get: function() {
      return $http.get('/api/components');
    },
    update: function(component) {
      return $http.post('/api/components/update', component);
    },
    updatePage: function(component) {
      return $http.post('/api/components/update-page', component);
    }
  };
}]).factory('SiteData', function() {
  var siteData = {};
  return {
    setSiteData: function(data) {
      siteData = data;
    },
    updateSiteData: function(key, value) {
      siteData[key] = value;
    },
    getSiteData: function() {
      return siteData;
    }
  };
}).factory('Component', ['$http', function($http) {
  return {
    query: function() {
      return $http.get('/api/components');
    }
  };
}]);

pencilblueApp.factory('NavStyling', function() {
    var navStyle = {};

    return {
        setNavStyle: function(count) {
            navStyle['font-size'] = 'calc(10vw / ' + count + ')';
            navStyle.width = 'calc(100% / ' + count + ')';
        },
        getNavStyle: function() {
            return navStyle;
        }
    }
});
pencilblueApp.config(['$provide', function($provide) {
  $provide.decorator('taOptions', ['$delegate', 'taRegisterTool', function(taOptions, taRegisterTool) {
    taOptions.toolbar = [
      [/*'h1', 'h2', 'h3', 'h4', 'h5', 'h6',*/ 'p', 'quote'],
      ['bold', 'italics', 'underline', 'strikeThrough', 'ul', 'ol', 'redo', 'undo', 'clear'],
      ['justifyLeft', 'justifyCenter', 'justifyRight'],
      ['html','insertLink', 'wordcount', 'charcount']
    ];
    taRegisterTool('fontSize', {
      display:  "<span class='bar-btn-dropdown dropdown'>" +
                  "<button title='text-size' class='btn btn-blue btn-xs dropdown-toggle' type='button' ng-disabled='showHtml()' style='padding-top: 4px'>"+
                    "<span>Text Size</span><i class='fa fa-caret-down'></i>"+
                  "</button>" +
                  "<ul id='font-size-dropdown' class='dropdown-menu' style='color: white;'>"+
                    "<p style='margin-bottom: 0;padding: 0 5px;color: black;'>Text Size:</p>" +
                    "<li ng-repeat='o in options'>"+
                      "<button class='btn btn-blue checked-dropdown' style='font-size: {{o.css}}; width: 100%' type='button' ng-click='action($event, o.value)'>"+
                        "<i ng-if='o.active' class='fa fa-check'></i> {{o.name}}"+
                        "</button>"+
                      "</li>"+
                    "</ul>" +
                "</span>",
      action: function (event, size) {
        //Ask if event is really an event.
        if (event.stopPropagation) {
          //With this, you stop the event of textAngular.
          event.stopPropagation();
          //Then click in the body to close the dropdown.
          $("body").trigger("click");
        }
        return this.$editor().wrapSelection('fontSize', parseInt(size));
      },
      options: [
        { name: 'xx-small', css: 'xx-small', value: 1 },
        { name: 'x-small', css: 'x-small', value: 2 },
        { name: 'small', css: 'small', value: 3 },
        { name: 'medium', css: 'medium', value: 4 },
        { name: 'large', css: 'large', value: 5 },
        { name: 'x-large', css: 'x-large', value: 6 },
        { name: 'xx-large', css: 'xx-large', value: 7 }

      ]
    });
    taOptions.toolbar[3].push('fontSize');
    return taOptions;
  }]);
}]).run(['editableOptions', function(editableOptions) {
  editableOptions.buttons = 'no';
  editableOptions.blurElem = 'submit';
}]).directive('dropdownToggle', ['$document', '$location', function ($document, $location) {
  var openElement = null,
    closeMenu = angular.noop;
  return {
    restrict: 'CA',
    link: function (scope, element, attrs) {
      scope.$watch('$location.path', function () { closeMenu(); });
      element.parent().bind('click', function () { closeMenu(); });
      element.bind('click', function (event) {
        var elementWasOpen = (element === openElement);

        event.preventDefault();
        event.stopPropagation();

        if (openElement) {
          closeMenu();
        }

        if (!elementWasOpen && !element.hasClass('disabled') && !element.prop('disabled')) {
          element.parent().addClass('open');
          openElement = element;
          closeMenu = function (event) {
            if (event) {
              event.preventDefault();
              event.stopPropagation();
            }
            $document.unbind('click', closeMenu);
            element.parent().removeClass('open');
            closeMenu = angular.noop;
            openElement = null;
          };
          $document.bind('click', closeMenu);
        }
      });
    }
  };
}]);

pencilblueApp.controller('EditController', ['$scope', '$rootScope', '$http', '$location', '$window', '$state', '$stateParams', '$interval', '$sce', 'cssInjector', '$transitions', 'SiteData', 'colorPalettes', 'NavStyling', 'ngDialog', '$timeout', 'placeholdertext', 'GetSite', function($scope, $rootScope, $http, $location, $window, $state, $stateParams, $interval, $sce, cssInjector, $transitions, SiteData, colorPalettes, NavStyling, ngDialog, $timeout, placeholdertext, GetSite) {
  $scope.locationQuery = $location.search();
  $scope.editMenu = null;
  $scope.state = $state;
  $scope.stateName = '';

	$scope.$watch('state.current.name', function(newVal) {
		if (newVal.length) $scope.stateName = newVal;
	});
  $scope.stateParams = $stateParams;

  $scope.editPanelNavItems = [
    'theme',
    'pages',
    'page layout',
    'colors',
    'fonts'
  ];


  for (var i in $scope.locationQuery) {
    if ($scope.editPanelNavItems.indexOf(i) > -1) {
      $scope.editMenu = i;
      break;
    }
  }

  $scope.$state = $state;
  $scope.navSortableOptions = {
    tolerance: 'pointer',
    handle: '.nav-handle',
    update: function(e, ui) {
      if (ui.item.sortable.model.role === 'home') {
        ui.item.sortable.cancel();
      }
    },
    stop: function(e, ui) {
      $scope.pages.forEach(function(page, index) {
        page.nav_order = index + 1;
      });
      $http.post('/api/updateNavOrder', {pages: $scope.pages}).then(function(response) {

      }).catch(function(err) {
        console.error(err);
        swal({
          title: 'Error',
          text: 'There was an error saving the nav order: ' + JSON.stringify(err.message, null, 2),
          type: 'error'
        });
      });
    }
  };

  $scope.shouldShowNavItem = function(item) {
    // todo why hardcode this never do this
    return (($scope._site.theme === 6 && item.role === 'resume') || ($scope._site.theme !== 6 && (item.role === 'role-1' || item.role === 'role-2')) || item.role === 'home');
  };

  $scope.setSiteStyle = function() {
    var length = $scope.pages.length;
    NavStyling.setNavStyle(length);
    $scope.navStyle = NavStyling.getNavStyle();
  };

  $scope.colorType = 'palette';

  // $scope.$on('colorpicker-selected', function(event, colorObject) {
  //   console.log('colorpicker event', event);
  //   console.log('colorpicker colorObject', colorObject);
  //   GetSite.set({
  //     pageStyles: $rootScope._site.pageStyles
  //   }).then(function() {
  //   });
  // });

  $scope.$on('colorpicker-closed', function(event, colorObject) {
    GetSite.set({
      pageStyles: $rootScope._site.pageStyles
    }).then(function() {
    });
  });

  $scope.isHomeState = function() {
    return $state.current.name === 'home';
  };

  $scope.orderPageStyles = function() {
    var pageStyles = $rootScope._site.pageStyles;
    $scope._site.pageStyles = {
      background_color: pageStyles.background_color,
      header_background_color: pageStyles.header_background_color,
      navbar_background: pageStyles.navbar_background,
      nav_text_color: pageStyles.nav_text_color,
      nav_active_color: pageStyles.nav_active_color,
      title_color: pageStyles.title_color,
      color: pageStyles.color,
      link_color: pageStyles.link_color,
      link_hover_color: pageStyles.link_hover_color,
      button_bg_color: pageStyles.button_bg_color,
      button_text_color: pageStyles.button_text_color,
      social_background_color: pageStyles.social_background_color,
      social_font_color: pageStyles.social_font_color,
      font: pageStyles.font,
      color_footer: pageStyles.color_footer
    };
  };

  $scope.getTrialHoursRemaining = function() {
    var date = new Date();
    var expireDate = new Date($rootScope._site.expiration_date);
    var html;
    if (expireDate < date) {
      html = '<span>Your site is currently expired. Please signup for a subscription.</span>';
    } else {
      html = '<span>Your site is currently in trial mode. Your trial will expire on ' + moment($rootScope._site.expiration_date).format('dddd, MMMM Do YYYY h:mm a') + '.</span>';
    }

    $scope.trialText = $sce.trustAsHtml(html);
  };

  $(document).ready(function() {
    var $myGroup = $('#bottom-menu');
    $myGroup.on('show.bs.collapse', '.collapse', function() {
      $myGroup.find('.collapse.in').collapse('hide');
    });
  });


  $scope.state = $state;

  cssInjector.removeAll();
  var saveUrl = '/api/savesite';
  var setThemeUrl = '/api/settheme';


  $rootScope.editing = window.editing = true;

  $rootScope._site = $scope.siteData;
  $scope.palettes = colorPalettes;
  //todo what is this?

  $scope.choosePalette = function(idx) {
    var pageStyles = $scope.palettes[$rootScope._site.theme][idx];
    pageStyles.font = $rootScope._site.pageStyles.font || [];
    console.log('choose palette');
    GetSite.set({
      pageStyles: pageStyles,
      colorPalette: idx
    }).then(function() {
      $rootScope._site.pageStyles = pageStyles;
    });
  };

   //BAN BAN BAN THIS FUNCTION IT"S CRAZY TO SAVE ENTIRE OBJECT.
  $rootScope.save = function(showAlert, cb) {
    alert('Could not save. Please contact support');
    // console.log('savesite');
    // if (typeof showAlert === 'undefined') showAlert = true;
    // $scope.$broadcast('app-start-loading');
    // $http.post(saveUrl, $scope._site).then(function(updatedPage) {
    //   $scope._site.__v = updatedPage.data.__v;
    //   $scope.$broadcast('app-finish-loading');
    //   if (showAlert) {
    //     swal('Success!', 'Changes saved.', 'success');
    //   }
    //   if (cb) {
    //     if (typeof cb === 'function') cb(null);
    //   } else {
    //     // $state.reload();
    //     location.reload();
    //   }

    // }, function(err) {
    //   $scope.$broadcast('app-finish-loading');
    //   if (showAlert) {
    //     swal('Error!', 'Changes not saved. Error: ' + err.message, 'error');
    //   }
    //   if (cb && typeof cb === 'function') {
    //     cb('Changes not saved');
    //   }
    // });
  };

  $scope.greenRoom = function() {
    location.href = window.GREEN_ROOM;
  }
  $scope.publishSite = function() {
    swal({
      title: 'Are You Sure?',
      text: 'Are you sure you want to publish your changes?',
      type: 'warning',
      closeOnCofirm: true,
      showCancelButton: true,
      closeOnCancel: true
    }, function(isConfirm) {
      if (isConfirm) {
        $http.post('/api/publish/site').then(function(response) {
          if (response.data.message === 'ok') {
            location.reload();
          }
        }).catch(function(err) {
          console.error(err);
        });
      }
    });
  };

  $scope.unpublishSite = function() {
    swal({
      title: 'Are You Sure?',
      text: 'Are you sure you want to unpublish your site? It won\'t be accessible to anyone but you.',
      type: 'warning',
      showCancelButton: true,
      closeOnCancel: true,
      closeOnConfirm: false
    }, function(isConfirm) {
      if (isConfirm) {
        $http.post('/api/unpublish/site').then(function(response) {
          if (response.data.message === 'ok') {
            console.log('good response');
            swal({
              title: 'Success',
              text: 'Your site is now only visible to you. If you want to make it live again, click "Publish"',
              type: 'success'
            }, function() {
              location.reload();
            });
          }
        }).catch(function(err) {
          console.error(err);
        });
      }
    });
  };

  var cssPath;

  $scope._site.theme = $scope._site.theme || 1;
  $scope._site.components = _.groupBy($scope.components, 'tag');
  console.log('components', $scope._site.components);
  $scope._site.images = _.groupBy($scope.siteImages, 'tag');
  cssPath = '/public/performance/themes/' + $scope._site.theme + '/theme.css';
  cssInjector.add(cssPath);
  //TODO This needs to be dynamic
  var theme = _.find($scope.themes, {_id: $scope._site.theme});

  SiteData.setSiteData({
    images: $scope._site.images,
    components: $scope._site.components,
		editing: true,
		pages: $scope.pages,
    _site: $scope.siteData
  });

  $scope.pages.forEach(function(page) {
    var css =       '/public/performance/pages/' + page.layout_category + '/' + page.layout + '.css';
    cssInjector.add(css);
  });


  $scope.currentStep = localStorage.getItem('editTourComplete') ? -1 : 0;

  $scope.endTour = function() {
    localStorage.setItem('editTourComplete', true);
  };

  $scope.pageStyleUrl = '/public/performance/themes/' + $rootScope._site.theme + '/pagestyle.json';
  console.log('pageStyleUrl: ', $scope.pageStyleUrl);
  $http.get($scope.pageStyleUrl).then(function(response) {
    console.log('pageStyle: ', response.data);
    $scope.defaultStyles = response.data;
  }).catch(function(err) {
    console.error(err);
  });

  $scope.defaultColor = function(key) {
    $scope.userColors = $scope._site.pageStyles;
    console.log('pageStyle: ', $scope.defaultStyles);
    $scope._site.pageStyles = $scope.defaultStyles;

    $scope.revertColorsPreview = true;
  };

  $scope.revertColors = function() {
    $scope.revertColorsPreview = false;
  };

  $scope.keepColors = function() {
    $scope._site.pageStyles = $scope.userColors;
    $scope.revertColorsPreview = false;
  };


  $scope.fonts = [
    'Playfair Display/Open Sans',
    'Oswald/Lato',
    'Abril Fatface/Lato',
    'Fjalla One/Didact Gothic',
    'Bitter/Raleway',
    'Montserrat Bold/Montserrat Regular',
    'Josefin Sans Bold/Josefin Sans Regular',
    'Alegreya/Open Sans',
    'Oswald/Droid Serif',
    'Raleway/Libre Baskerville',
    'Montserrat/Hind',
    'Dosis/Open Sans',
    'Raleway/Cabin',
    'PT Sans/Didact Gothic',
    'Oxygen/Source Sans Pro',
    'Josefin Sans Bold/Open Sans',
    'Josefin Sans Regular/Playfair Display',
    'Open Sans/Lora',
    'Cardo/Josefin Sans Regular',
    'Amatic SC/Josefin Sans Regular',
    'Lobster/Cabin',
    'Rancho/Gudea'
  ];

  $scope.fontsObj = [
    ['Playfair Display', 'Open Sans'],
    ['Oswald','Lato'],
    ['Abril Fatface', 'Lato'],
    ['Francois One', 'Didact Gothic'],
    ['Bitter', 'Raleway'],
    ['Montserrat Bold', 'Montserrat Regular'],
    ['Josefin Sans Bold', 'Josefin Sans Regular'],
    ['Amaranth', 'Titillium Web'],
    ['Oswald', 'Droid Serif'],
    ['Raleway', 'Merriweather'],
    [ 'Montserrat', 'Hind' ],
    [ 'Dosis', 'Open Sans' ],
    [ 'Raleway', 'Cabin' ],
    [ 'PT Sans', 'Didact Gothic' ],
    [ 'Oxygen', 'Source Sans Pro' ],
    [ 'Josefin Sans Bold', 'Open Sans' ],
    [ 'Josefin Sans Regular', 'Playfair Display' ],
    [ 'Open Sans', 'Lora' ],
    [ 'Cardo', 'Josefin Sans Regular' ],
    [ 'Amatic SC', 'Josefin Sans Regular' ],
    [ 'Lobster', 'Cabin' ],
    [ 'Rancho', 'Gudea' ]
  ];

  $scope.themeFontDefaults = {
    4: ['Times New Roman','Raleway'],
    5: ['Cardo', 'Josefin Sans Regular']
  };

  if ($scope.themeFontDefaults[$scope._site.theme]) {
    if (!$scope._site.pageStyles.font || !$scope._site.pageStyles.font.length) {
      $scope._site.pageStyles.font = $scope.themeFontDefaults[$scope._site.theme];
    }
  }

  $scope.changeFont = function(font) {
    $scope._site.pageStyles.font = font.split('/');
    GetSite.set({
      pageStyles: $scope._site.pageStyles
    }).then(function() {
      //$state.go($state.current, $state.params, {reload: true});
    });
  };

  $scope.updateSiteMetadata = function(attribute, value) {
    var data = {};
    data[attribute] = value;
    GetSite.set(data).then(function() {
    });
  };

  $scope.updateSearchSettings = function(attribute, value) {
    var data = {};
    data[attribute] = value;
    GetSite.set(data).then(function() {
      swal('Success!', 'Settings saved', 'success');
    }).catch(function(err) {
      swal('Oops!', 'There was an error saving the settings, please try again: ' + JSON.stringify(err.data), 'error');
    });
  };

  $scope.templates = {
    // home: [
    //   {name: 'Full Background', img: '/public/performance/pages/home/1.jpg', id: 1},
    //   {name: 'Carousel', img: '/public/performance/pages/home/2.jpg', id: 2}
    // ],
    about: [
      {name: 'Portrait 1', img: '/public/performance/pages/about/1.jpg', id: 1},
      {name: 'Portrait 2', img: '/public/performance/pages/about/2.jpg', id: 2},
      {name: 'Collage', img: '/public/performance/pages/about/3.jpg', id: 3},
      {name: 'Landscape 1', img: '/public/performance/pages/about/4.jpg', id: 4}
    ],
    contact: [
      {name: 'Portrait 1', img: '/public/performance/pages/contact/1.jpg', id: 1},
      {name: 'Portrait 2', img: '/public/performance/pages/contact/3.jpg', id: 3},
      {name: 'Landscape 1', img: '/public/performance/pages/contact/4.jpg', id: 4},
      {name: 'No Picture', img: '/public/performance/pages/contact/2.jpg', id: 2}
    ],
    news: [
      {name: 'Portrait', img: '/public/performance/pages/news/2.jpg', id: 2},
      {name: 'Landscape', img: '/public/performance/pages/news/1.jpg', id: 1}
    ],
    gallery: [
      {name: 'Album 1', img: '/public/performance/pages/gallery/1.jpg', id: 1},
      {name: 'Album 2', img: '/public/performance/pages/gallery/2.jpg', id: 2},
      {name: 'Carousel', img: '/public/performance/pages/gallery/3.jpg', id: 3},
      {name: 'Expanding', img: '/public/performance/pages/gallery/4.jpg', id: 4}
    ],
    resume: [
      {name: 'Portrait 1', img: '/public/performance/pages/resume/1.jpg', id: 1},
      {name: 'Portrait 2', img: '/public/performance/pages/resume/2.jpg', id: 2},
      {name: 'Resume Builder', img: '/public/performance/pages/resume/3.jpg', id: 3},
      {name: 'Resume Only', img: '/public/performance/pages/resume/5.jpg', id: 5}
    ],
    media: [
      {name: 'Media 1', img: '/public/performance/pages/media/1.jpg', id: 1},
      {name: 'Media 2', img: '/public/performance/pages/media/2.jpg', id: 2}
    ],
    custom: [
      {name: 'Custom Page 1', img: '/public/performance/pages/custom/1.jpg', id: 1},
      {name: 'Custom Page 2', img: '/public/performance/pages/custom/2.jpg', id: 2},
      {name: 'Custom Page 3', img: '/public/performance/pages/custom/3.jpg', id: 3},
      {name: 'Custom Page 4', img: '/public/performance/pages/custom/4.jpg', id: 4},
      {name: 'Custom Page 5', img: '/public/performance/pages/custom/5.jpg', id: 5},
      {name: 'Custom Page 6', img: '/public/performance/pages/custom/6.jpg', id: 6},
    ]
  };

  for (var tmpl in theme.templates) {
    $scope.templates[tmpl] = theme.templates[tmpl];
  }





  $scope.currState = $state;
	$scope.$watch('currState.current.name', function(newValue, oldValue) {
		$scope.stateName = newValue;
	});

  $scope.changeTheme = function(theme) {
    $scope._site.theme = theme.id;
    $http.post(setThemeUrl, {
      theme: theme.id
    }).then(function() {
      cssInjector.removeAll();
      cssPath = '/public/performance/themes/' + $scope._site.theme + '/theme.css';
      cssInjector.add(cssPath);
      $scope.$broadcast('app-start-loading');
      $window.location.reload();
    }).catch(function(err) {
      $scope.$broadcast('app-finish-loading');
      swal('Error!', 'Error saving the template: ' + err, 'error');
    });
  };

  $scope.changePageLayout = function(idx) {
    console.log('idx: ', idx);
    // if ($scope.stateName === 'customPage') {
    //   var title = $state.params.title;
    //   var tag = $state.params.tag;

    //   var component = $scope._site.components[tag][0];
    //   var newComponent = {
    //     site: component.site,
    //     component_specific_data: {
    //       layout: idx,
    //       position: component.component_specific_data.position
    //     },
    //     tag: tag,
    //   };

    //   $http.post('/api/components/update', newComponent).then(function(res) {
    //     console.log('res: ', res);
    //     $scope.$broadcast('app-start-loading');
    //     location.reload();
    //   }).catch(function(err) {
    //     $scope.$broadcast('app-finish-loading');
    //     swal('Error!', 'Error saving the template: ' + err.data.message, 'error');
    //   });
    // } else {
      var layout_category = $rootScope.currentPage.layout_category;
      var oldLayoutIndex = $rootScope.currentPage.layout;
      //$scope._site.layouts[layout_category] = idx;
      $http.put('/api/savepage/' + $rootScope.currentPage._id, {
				layout: idx
			}).then(function() {
        var oldcss = '/public/performance/pages/' + layout_category + '/' + oldLayoutIndex + '.css';
        var newcss = '/public/performance/pages/' + layout_category + '/' + idx + '.css';
        console.log('removing', oldcss);
        console.log('adding', newcss);
        cssInjector.remove(oldcss);
        cssInjector.add(newcss);
        $rootScope.currentPage.layout = idx;
        $state.reload();
      }).catch(function(err) {
        $scope.$broadcast('app-finish-loading');
        swal('Error!', 'Error saving the template: ' + err, 'error');
      });
    //}
  };

  $rootScope.preview = function() {
    $window.location.href = '/preview';
  };


  $scope.updateLayout = function(layoutId) {
    var interiorPage = $state.current.name;
    var opts = {};
    var oldId = $scope._site.layouts[interiorPage];
    $scope.$broadcast('app-start-loading');
    opts.layouts = $scope._site.layouts;
    $http.post(saveUrl, opts).then(function() {
      $scope.$broadcast('app-finish-loading');
      $scope._site.layouts[interiorPage] = parseInt(layoutId, 10);
      cssInjector.remove('/public/performance/pages/' + interiorPage + '/' + oldId + '.css');
      cssInjector.add('/public/performance/pages/' + interiorPage + '/' + layoutId + '.css');
      $state.reload();
    }, function(err) {
      $scope.$broadcast('app-finish-loading');
      swal('Error!', 'Changes not saved.', 'error');
    });
  };


  $scope.getOffset = function(ctxt) {
    if (ctxt === 'tip1') {
      $('.tour-tip').css('margin-left', $('body').width() / 2.25);
    }

    return null;
  };

  $scope.removeOffset = function() {
    $('.tour-tip').css('margin-left', 0);

    return null;
  };

  $scope.deleteItem = function() {
    var key = this.$editable.attrs.editableText || this.$editable.attrs.editableTextarea;
    var item = key.split('.').slice(0, -1).reduce(function(obj, i) {
      return obj[i];
    }, $scope);
    if (item) {
      item[key.split('.').slice(-1)] = '';
    } else {
      this.$data = '';
    }
  };

  $scope.toggleVisible = function(navItem) {
    navItem.hidden = !navItem.hidden;
    $http.post('/api/components/update-page', {_id: navItem._id, hidden: navItem.hidden}).then(function(response) {
      console.log('response', response);
    }).catch(function(err) {
      console.error(err);
      swal('Oops!', 'There was an error updating the visibility: ' + JSON.stringify(err), 'error');
    });

    //$scope.$broadcast('app-start-loading');
    //$window.location.reload();
  };


  $scope.chooseTheme = function(themeObj) {
    var themeId = themeObj._id;

    if ($scope._site && $scope._site.theme != themeId) {
      var originalThemeId = $scope._site.theme;
      $scope._site.theme = themeId;

      var copyExistingLayouts = angular.copy($scope._site.layouts);
      copyExistingLayouts.home = 1; //set home to default

      var updates = {
        theme: themeId,
        layouts: copyExistingLayouts
      };



      for (var layout in updates.layouts) {
        if (themeObj.forceLayout && themeObj.forceLayout[layout]) {
          updates.layouts[layout] = themeObj.forceLayout[layout];
        }
      }

      // Get current site styles
      if ($rootScope._site.colorPalette && $scope.palettes[$scope._site.theme][themeId]) {
        updates.pageStyles = $scope.palettes[$rootScope._site.colorPalette];

        $scope.updateWithQuery(updates);
      } else {
        $http.get('/public/performance/themes/' + originalThemeId + '/pagestyle.json').then(function(originalStylesRes) {
          var originalStyles = originalStylesRes.data;

          // compare site styles to default. if changed, replace in default styles of new theme
          var changedKeys = [];
          for (var i in $scope._site.pageStyles) {
            var pageStyle = $scope._site.pageStyles[i];
            if (pageStyle) {
              if (pageStyle !== originalStyles[i]) {
                changedKeys.push(i);
              }
            }
          }

          // get default styles for new theme
          $http.get('/public/performance/themes/' + themeId + '/pagestyle.json').then(function(styles) {
            var newStyles = styles.data;
            // create new style object with user changed values replacing the default style of same key.
            changedKeys.forEach(function(i) {
              newStyles[i] = $scope._site.pageStyles[i];
            });

            updates.pageStyles = newStyles;
            $scope.updateWithQuery(updates);
          });
        });
      }
    } else {
      swal('This is already your theme', '', 'error');
    }
  };


  var $$scope = $scope;


  $scope.weBuildItForyouPrompt = function() {
    var dialog = ngDialog.open({
      template: '/public/performance/partials/webuilditforyouprompt.html',
      appendClassName: 'newPageModal',
      controller: function($scope, $state) {
      }
    });

    dialog.closePromise.then(function (shouldWeBuildIt) {

      window.localStorage.setItem('builditforyoupopup', true);

      if (shouldWeBuildIt.value === true) {
        $$scope.weBuildItForyou();
      }
    });

  };

  if (window.localStorage && window.localStorage.getItem('builditforyoupopup') !== 'true') {
    setTimeout(function() {
      //$scope.weBuildItForyouPrompt(); removing
    }, 1000);
  }

  $scope.weBuildItForyou = function() {
    var dialog = ngDialog.open({
      template: '/public/performance/partials/webuilditforyou.html',
      appendClassName: 'newPageModalForm',
      controller: function($scope, $state, Upload) {
        $scope.buildItForYou = {
          urls: {},
          filenames: []
        };

        $scope.submit = function() {
          var body = {
            name: $scope.buildItForYou.name,
            email: $scope.buildItForYou.email,
            urls: $scope.buildItForYou.urls,
            comments: $scope.buildItForYou.comments
          };

          $http.post('/api/builditforyou', body).then(function(res) {
            ngDialog.closeAll();
            swal({
              title: 'Success!',
              text: 'We\'re looking forward to creating your site! You\'ll receive an email within 1 busy day about a time to schedule your consultation.  If you have any questions feel free to email ' + window.HELP_EMAIL + '!',
              type: 'success'
            });
          }).catch(function(err) {
            swal('Oops!', 'We had an issue using your information. Please contact us!', 'error');
          });
        };

        $scope.uploadFiles = function(files, type) {
          $scope.fileUploadError = null;

          var validFiles = files.filter(function (f) { return f.size <= 10000000; });
          if (validFiles.length !== files.length) $scope.fileUploadError = 'One or more files uploaded are too large!';
          console.log('FILE LENGTHS ', validFiles.length);

          if (!validFiles.length) {
            $scope.buildItForYou.photo = [];
            return;
          }

          validFiles.forEach(function(file) {
            var fileInfo = {
              name: file.name,
              percentage: 0
            };

            $scope.buildItForYou.filenames.push(fileInfo);
            Upload.upload({
              url: window.CLOUDINARY_UPLOAD_API_URL,
              data: {
                folder: window.location.host,
                upload_preset: 'lthduepg',
                file: file
              }
            }).then(function(resp) {
              console.log('Success ' + resp.config.data.file.name + 'uploaded');
              $scope.buildItForYou.urls[type] = resp.data.secure_url;
              fileInfo.percentage = 100;
            }, function(resp) {
              console.log('resp err', resp);
              console.log('Error status: ' + resp.status);
            }, function(evt) {
              var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
              fileInfo.percentage = progressPercentage;
            });
          });
        };


      }
    });
  };

  $scope.createCustomPageModal = function() {
    var dialog = ngDialog.open({
      template: '/public/performance/themes/partials/newPageModal.html',
      appendClassName: 'newPageModal',
      resolve: {
        customPages: function() {
          return $scope.customPages;
        }
      },
      controller: function($scope, $state, customPages) {
        $scope.pageNameLimit = 14;
        $scope.createNewPage = function() {
          if ($scope.newPageName.length > 0 && $scope.newPageName.length <= $scope.pageNameLimit) {
            var newPage = {
              nav_title: $scope.newPageName,
              layout: 1,
              layout_category: 'custom'
            };

            $http.post('/api/components/update-page', newPage).then(function(res) {
              location.reload();
            }).catch(function(err) {
              console.error(err);
            });
          } else {
            swal('Oops!', 'Page name must not be blank and must be longer than 8 characters', 'error');
          }
        };
      }
    });
  };

  $scope.removePage = function(navItem) {
    swal({
      title: 'Are You Sure?',
      text: 'Your page, <strong>' + navItem.nav_title + '</strong>, will be removed along with all of its pictures/text',
      html: true,
      type: 'warning',
      showCancelButton: true,
      closeOnConfirm: false
    }, function(isConfirm) {
      if (isConfirm) {
        $http.post('/api/components/remove-page', {_id: navItem._id}).then(function(res) {
          console.log('res: ', res);
          swal({
            title: 'Success!',
            text: 'Your page, ' + navItem.nav_title + ', was deleted!',
            type: 'success'
          }, function() {
            location.reload();
          });
        }).catch(function(err) {
          console.error(err);
          swal('Error', 'There was an error deleting your page: ' + err, 'error');
        });
      }
    });
  };

  $scope.updateWithQuery = function(q) {
    $http.post(saveUrl, q).then(function() {
      swal({
        title: 'Success!',
        text: 'Theme Updated',
        type: 'success'
      }, function() {
        location.reload();
      });

    }, function(err) {
      console.error(err);
      swal('Error!', 'Theme could not be saved.', 'error');
    });
  };

  // $scope.editMenu = null;
  $scope.colorPickerOptions = {
    swatchOnly: false
  };

  $scope.chooseEditMenu = function(menu) {
    console.log('menu', menu);
    $scope.editMenu = menu.replace(/\s/g, '_');
    console.log('menu: ', $scope.editMenu);
    var qs = {};
    qs[menu] = true;
    $state.go($state.current, qs);
  };

  $scope.editMenuGoBack = function() {
    $scope.editMenu = null;
    $state.go($state.current, {
      pages: null,
      theme: null,
      layout: null,
      colors: null,
      fonts: null,
      showCustomColors: null
    });
  };

  $scope.showCustomColors = $scope.locationQuery.colors && $scope.locationQuery.showCustomColors ? true : false;

  $scope.showHideCustomizeColors = function() {
    $scope.showCustomColors = !$scope.showCustomColors;
    var q = {
      colors: true
    };
    if ($scope.showCustomColors) q.showCustomColors = true;


    $timeout(function() {
      var d = $('#editPanel');
      d.scrollTop(d.prop("scrollHeight"));
    }, 100);
    // $state.go($state.current, q);
  };

  $scope.showEditMenu = false;

  $scope.toggleShowEditMenu = function() {
    $scope.showEditMenu = !$scope.showEditMenu;
  };

  $scope.editPage = function(itemId, newPageName) {
    console.log('item: ', itemId, newPageName);
    $http.post('/api/components/update-page', {_id: itemId, nav_title: newPageName}).then(function(response) {
      console.log('response', response);
      location.reload();
    }).catch(function(err) {
      console.error(err);
      swal('Oops!', 'There was an error updating the page name: ' + JSON.stringify(err), 'error');
    });

  };

  $(window).keydown(function(e) {
    if ((e.ctrlKey || e.metaKey) && e.which === 13) {
      $scope.save();
    }
  });
}]).filter('convertKeyToTitle', function() {
  return function(val) {
    var result = val.replace('_', ' ');
    var optionalVals = ['height', 'weight'];
    if (val === 'union') result += ' e.g. AEA/SAG-AFTRA, SAG Eligible, E.M.C.';
    if (val === 'range') result = 'vocal ' + result;
    if (optionalVals.indexOf(val) > -1) result += ' (optional)';
    return result;
  };
}).filter('telephone', function() {
  return function(input) {
    if (input && input.length) {
      var arr = input.split('');
      arr.splice(0, 0, '(');
      arr.splice(4, 0, ')');
      arr.splice(5, 0, ' ');
      arr.splice(9, 0, '-');
      return arr.join('');
    } else {
      return input;
    }
  };
});


function capitalize(string) {
  var output = string.split(' ');
  output.forEach(function(i, idx) {
    output[idx] = (i.charAt(0).toUpperCase() + i.substring(1));
  });

  return output.join(' ');
}

//the modal directive to upload and image and crop
//the main navigation
angular.module('pencilblueApp')
.directive('croppie', ['$rootScope', 'Upload', 'ImgFactory', function($rootScope, Upload, ImgFactory) {
  return {
    //require: ['ngModel'],
    scope: {
      ngModel: '=',
      viewport: '=',
      thumbnailmode: '@',
      title: '@',
      outputwidth: '@',
      done: '&',
      tag: '@',
      pageId: '@'
    },
    restrict: 'E',
    templateUrl: '/public/performance/partials/imgcrop.html',
    link: function(scope, elem) {

      if (!scope.pageId) {
        console.warn('No Page Id Specified on ImageCrop' + scope.tag);
      }
			// defaults
      if (scope.viewport == undefined) {
        console.warn('WARNING: MUST SET W or H for proper cropping.');
        scope.viewport = { w: null, h: null };
      }
      scope.boundry = { w: 300, h: 300 };

      scope.viewport.w = (scope.viewport.w != undefined) ? scope.viewport.w : 300;
      scope.viewport.h = (scope.viewport.h != undefined) ? scope.viewport.h : 300;
      // viewport cannot be larger than the boundaries
      if (scope.viewport.w > scope.boundry.w) { scope.viewport.w = scope.boundry.w; }
      if (scope.viewport.h > scope.boundry.h) { scope.viewport.h = scope.boundry.h; }

      console.log('scope.viewport', scope.viewport.w);
      console.log('scope.viewport', scope.viewport.h);
      // define options
      var options = {
        enableOrientation: true,
        enableExif: true,
        viewport: {
          width: scope.viewport.w,
          height: scope.viewport.h,
          type: 'square'
        },
        boundary: {
          width: scope.viewport.w,
          height: scope.viewport.h
        },
        showZoom: true,
        mouseWheelZoom: true
      };

      var croppie, bindOpts = {};

      if (scope.ngModel) {

        croppie = new Croppie(elem[0], options);

        bindOpts.url = scope.ngModel.original.url;
        if (scope.ngModel.points) {
          bindOpts.points = scope.ngModel.points.split(',');
        }

        croppie.bind(bindOpts).then(function(res) {
          croppie.setZoom(scope.ngModel.zoom || 0);
        });
      }


      scope.$on('$destroy', function() {
        if (croppie && croppie.elements && croppie.elements.boundary) {

          croppie.destroy();
          $rootScope.$emit('imageUpdated');
        }
      });

      scope.delete = function() {
        if (scope.ngModel._id){
          ImgFactory.delete(scope.ngModel).then(function(data) {
            scope.done(null);
          });
        } else {
          scope.done(null);
        }
      };

      scope.update = function() {
        if (croppie) {
          var zoomDimensions = croppie.get();
          scope.ngModel.points = zoomDimensions.points.join(',');
          scope.ngModel.zoom = zoomDimensions.zoom;

          croppie.result({
            type: 'canvas'
          }).then(function(img) {
            if (scope.thumbnailmode) {
              scope.createThumbnail(dataURItoBlob(img));
            } else {
              scope.cropExistingImage(dataURItoBlob(img));
            }
          });
        } else {
          alert('Please upload an image.');
        }
      };


      function dataURItoBlob(dataURI) {
        // convert base64/URLEncoded data component to raw binary data held in a string
        var byteString;
        if (dataURI.split(',')[0].indexOf('base64') >= 0)
          byteString = atob(dataURI.split(',')[1]);
        else
        byteString = unescape(dataURI.split(',')[1]);

        // separate out the mime component
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

        // write the bytes of the string to a typed array
        var ia = new Uint8Array(byteString.length);
        for (var i = 0; i < byteString.length; i++) {
          ia[i] = byteString.charCodeAt(i);
        }

        return new Blob([ia], {type:mimeString});
      }


      scope.uploadFile = function(file, errorFile) {
        if (!file) return;
        console.log('Croppie file size: ', file);
        scope.fileUploadError = null;
        if (errorFile) {
          switch (errorFile.$error) {
            case 'maxSize':
            scope.fileUploadError = 'File is too large!';
            break;
            default:
            scope.fileUploadError = 'There was an error uploading the file. Please try uploading again.';
            break;
          }
          return;
        }
        if (!file) return;
        console.log('Croppie file size: ', file.size);
        if (file.size > 10000000) {
          scope.fileUploadError = 'File size is too large';
          return;
        }
        scope.errorFile = errorFile;

        if (errorFile) {
          return false;
        }

        if (croppie && croppie.elements && croppie.elements.boundary) {
          croppie.destroy();
        }

        if (!file) {
          return false;
        }

        Upload.upload({
          url: "/api/image/upload",
          data: {
            file: file,
            tag: scope.tag,
            caption: scope.caption,
            page: scope.pageId
          }
        }).then(function(resp) {
          console.log('DONE', resp);
          delete scope.progressPercentage;
          scope.ngModel = resp.data;
          croppie = new Croppie(elem[0], options);
          croppie.bind({url: scope.ngModel.url}).then(function() {
            croppie.setZoom(0);
          });
        }, function(resp) {
          //todo error handling on pending image
          console.log('resp err', resp);
          console.log('Error status: ' + resp.status);
          alert(resp.data ? resp.data.message : 'Error Uploading File');
          delete scope.progressPercentage;
        }, function(evt) {
          //todo loading info
          console.log('resp progressPercentage', evt);
          scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          console.log('progress: ' + scope.progressPercentage + '% ' + evt.config.data.file.name);
        });



        // console.log('in uploadFile', file);
        // var reader = new FileReader();
        // reader.onload = function (e) {
        //   croppie.bind({url: e.target.result}).then(function() {
        //     console.log('bind compelte');
        //   });
        // };
        // reader.readAsDataURL(file);
      };

      scope.cropExistingImage = function(file) {
        if (!file) {
          return false;
        }
        Upload.upload({
          url: window.CLOUDINARY_UPLOAD_API_URL,
          data: {
            folder: window.location.host,
            upload_preset: scope.outputwidth ? 'hcfwk7ej' : 'zymnlox3',
            file: file,
            title: scope.ngModel.title
            //transformation: 'c_limit,w_' + (scope.outputwidth || 600) not allowed in unsignd uploads
          }
        }).then(function(resp) {
          console.log('DONE', resp, scope.ngModel);
          scope.ngModel.url = resp.data.secure_url;
          delete scope.progressPercentage;
          if (!scope.ngModel.page) scope.ngModel.page = null;
          ImgFactory.update(scope.ngModel).then(function(resp) {
            //$rootScope.$broadcast('image-updated');
            scope.done({img: resp.data});
          });
        }, function(resp) {
          //todo error handling on pending image
          console.log('resp err', resp);
          console.log('Error status: ' + resp.status);
          delete scope.progressPercentage;
        }, function(evt) {
          //todo loading info
          console.log('resp progressPercentage', evt);
          scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          console.log('progress: ' + scope.progressPercentage + '% ' + evt.config.data.file.name);
        });

      };


      scope.createThumbnail = function(file) {
        if (!file) {
          return false;
        }
        Upload.upload({
          url: window.CLOUDINARY_UPLOAD_API_URL,
          data: {
            folder: window.location.host,
            upload_preset: 'zymnlox3',
            file: file
          }
        }).then(function(resp) {
          console.log('DONE', resp);
          scope.ngModel.thumbnail = resp.data.secure_url;
          scope.ngModel.thumbnail_id = resp.data.public_id;
          ImgFactory.update(scope.ngModel).then(function(data) {
            //$rootScope.$broadcast('image-updated');
            scope.done({img: data.data});
          });
        }, function(resp) {
          //todo error handling on pending image
          console.log('resp err', resp);
          console.log('Error status: ' + resp.status);
        }, function(evt) {
          //todo loading info
          console.log('resp progressPercentage', evt);
          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        });

      };



    }
  };
}]);

//the modal directive to upload and image and crop
//the main navigation
angular.module('pencilblueApp')
  .directive('imgcropModal', ['$rootScope', '$uibModal', 'SiteData', 'ImgFactory', function($rootScope, $uibModal, SiteData, ImgFactory) {
    return {
      scope: {
        ngModel: '=',
        mobileImage: '=',
        outputwidth: '@',
        thumbnailmode: '@',
        idealSize:'@',
        mobileIdealSize: '@',
        tag: '@',
        pageId: '@',
        instruction: '@',
        instructionHeader: '@',
        ignoremessage: '=',
        mobileUpload: '='
      },
      restrict: 'A',
      replace: true,
      link: function(scope, elem, attrs) {
        if (!scope.pageId) {
          console.warn('no page id specified.');
        }
        var siteData = SiteData.getSiteData();

        var uploadMessage = '<div class="upload-message"><div class="message" style="padding: 1em;text-align: center;color:white;">' + (scope.instructionHeader || 'Click to Upload Image')+ '</div></div>';
        var u = angular.element(uploadMessage);

        if (siteData.editing && !scope.ignoremessage) {
          elem.wrap('<div class="imgcropwrap" style="position:relative;height: 100%;width: 100%;"></div>');
          //u.css('display', 'none');
          u.on('click', function() {
            openImageCropModal();
          });
          elem.after(u);
        }



        if (!scope.ngModel || (scope.ngModel && !scope.ngModel.url)) {
          if (siteData.editing) {
            var url = '/public/performance/img/placeholders/' + (scope.idealSize || '200x200') + '.png';
            if (elem[0].tagName === 'IMG') {
              attrs.$set('src', url);
              //u.css('display', 'flex');
              //var uploadIcon = angular.element('<div style="position:absolute;top:20px;left:20px;color:white;"><i class="fa fa-upload fa-2x"></i></div>');
              //uploadIcon.on('click', openImageCropModal);
              //elem.after(uploadIcon);
            } else if (!elem[0].hasAttribute('noshowbg')) {
              console.log('in imgcropmodal');
              elem.css({
                backgroundImage: 'url(/public/performance/img/placeholders/' + (scope.idealSize || '200x200') + '.png)'
              });
              elem.addClass('coverbackground');
            }
          } else {
            elem.remove();
            return;
          }
        } else if ((scope.ngModel && scope.ngModel.url) || (scope.mobileImage && scope.mobileImage.url)) {
          if (elem[0].tagName === 'IMG') {
            attrs.$set('src', scope.ngModel.url);
          } else if (!elem[0].hasAttribute('noshowbg')) { //don't do it for the crop button
            updateBackgroundImage(scope.ngModel, scope.mobileImage);
            // elem.css({
            //     backgroundImage: 'url(' + scope.ngModel.url + ')'
            // });
            elem.addClass('coverbackground');
          }
        }
        if (scope.ngModel && scope.ngModel.title) {
          if (elem[0].tagName === 'IMG') {
            attrs.$set('alt', scope.ngModel.title);
          } else {
            attrs.$set('title', scope.ngModel.title);
          }
        }

				function nonEditOpenImage() {
          if (scope.ngModel.href) {
            location.assign(scope.ngModel.href);
            return;
          }
          var $uibModalInstance = $uibModal.open({
            animation: true,
            size: 'lg',
            template: '<div class="imgcropmodal-modal" style="position:absolute;"><i class="fa fa-times pull-right" ng-click="$ctrl.cancel()"></i></div><img src="' + scope.ngModel.url + '" />',
            windowTopClass: 'fullscreen-lb',
            controllerAs: '$ctrl',
            controller: function() {
              this.cancel = function() {
                $uibModalInstance.dismiss('cancel');
              };
            }
          });
        }

        function openImageCropModal() {

          var modalInstance = $uibModal.open({
            animation: false,
            size: 'md',
            ariaLabelledBy: 'modal-title',
						ariaDescribedBy: 'modal-body',
						windowClass: 'imgCropModal',
            templateUrl: '/public/performance/partials/imgcrop-modal.html',
            controllerAs: '$ctrl',

            controller: function($uibModalInstance, ngModel, viewport, mobileViewport, $scope, outputwidth, thumbnailmode, tag, pageId, idealSize, title, instruction, instructionHeader) {
              var $ctrl = this;
							$ctrl.ngModel = ngModel;
              $ctrl.viewport = viewport;
              $ctrl.mobileViewport = mobileViewport;
              $ctrl.outputwidth = outputwidth;
              $ctrl.thumbnailmode = thumbnailmode;
              $ctrl.tag = tag;
              $ctrl.title = title;
              $ctrl.idealSize = idealSize;
              $ctrl.instruction = instruction;
              $ctrl.instructionHeader = instructionHeader;
              $ctrl.pageId = pageId;
              $ctrl.mobileUpload = scope.mobileUpload;
              $ctrl.mobileImage = scope.mobileImage;
              $ctrl.mobileIdealSize = scope.mobileIdealSize;


              $ctrl.updateLink = function(model) {

								if ($ctrl.ngModel.href && !$ctrl.ngModel.href.startsWith('http')) {
									$ctrl.ngModel.href = 'http://' + $ctrl.ngModel.href;
								}
								ImgFactory.update(model).then(function(resp) {
									$uibModalInstance.dismiss('cancel');
								});
              };

              $ctrl.done = function(img) {
                console.log('INFO: ', scope.mobileUpload, $ctrl.mobileUpload);
                $uibModalInstance.close({img: img, isMobile: scope.mobileUpload});
              };

              $ctrl.cancel = function() {

                $uibModalInstance.dismiss('cancel');
              };
            },
            resolve: {
              idealSize: function() {
                return scope.idealSize;
              },
              tag :function() {
                return scope.tag;
              },
              pageId: function() {
                return scope.pageId;
              },
              thumbnailmode: function() {

                return scope.thumbnailmode;
              },
              ngModel: function() {

                return scope.ngModel;
              },
              title: function() {
                return scope.title;
              },
              instruction: function() {

                return scope.instruction;
              },
              instructionHeader: function() {

                return scope.instructionHeader;
              },
              viewport: calculateViewport(scope.thumbnailmode, scope.idealSize),
              mobileViewport: calculateViewport(scope.thumbnailmode, scope.mobileIdealSize),
              outputwidth: function() {
                var tempWidth = scope.outputwidth ? parseInt(scope.outputwidth, 10) : 600;
                var width = tempWidth || 600;
                if (scope.idealSize) {
                  var idealSizeArr = scope.idealSize.split('x');
                  if (idealSizeArr[0] > width) {
                    width = idealSizeArr[0];
                  }
                }
                return width;
              },
              library: function() {
                return scope.library;
              }
            }
          });

          modalInstance.result.then(function(data) {
            var img = data.img;
            var isMobile = data.isMobile;
            console.log('MORE INFO: ', data);
            if (img && img.img) {
              //u.css('display', 'none');
              if (isMobile) {
                scope.mobileImage = img.img;
              } else {
                scope.ngModel = img.img;
              }
							if (elem.attr('background-image') == 'true') {
								// elem.css({
								// 		backgroundImage: 'url(' + scope.ngModel.url + ')'
                // });
                updateBackgroundImage(scope.ngModel, scope.mobileImage);
							} else {
								attrs.$set('src', img.img.url);
								attrs.$set('alt', img.img.title);
								siteData.images[img.img.tag] = [img.img];
							}
            } else {
              //u.css('display', 'flex');

              attrs.$set('src', '/public/performance/img/placeholders/' + (scope.idealSize || '200x200') + '.png');
              scope.ngModel = null;
              if (siteData.images[scope.tag]) {
                siteData.images[scope.tag] = [];
              }
            }
          }, function() {

          });

        }

        if (!siteData.editing) {
          if (elem.parent()[0].tagName == 'A' || elem[0].hasAttribute('hideshadowbox')) {
            return;
          }
          elem.on('click', nonEditOpenImage);
          return;
        }
        elem.addClass('imgcrop');
        elem.on('click', function() {
          openImageCropModal();
        });


      }
    };
  }]);

  function updateBackgroundImage(desktop, mobile) {
    var splashEl = '#hero-image';
    angular.element('#full-page').before('<style>'+
                        splashEl + '{'+
                          'background-image: url(' + desktop.url + ');'+
                        '}'+
                        '@media (max-width: 767px) {'+
                          splashEl + '{'+
                            'background-image: url(' + (mobile ? mobile.url : desktop.url) + ')'+
                          '}'+
                        '}'+
                      '</style>');
  }

  function calculateViewport(thumbnailmode, idealSize) {
    if (thumbnailmode) {
      return {
        w: 200,
        h: 200
      };
    }

    var w,h;


    if (idealSize) {
      var idealSizeArr = idealSize.split('x');
      var idealSizeObj = {
        w: parseInt(idealSizeArr[0], 10),
        h: parseInt(idealSizeArr[1], 10)
      };

      if (idealSizeObj.w >= idealSizeObj.h) {
        w = 300;
        h = w * idealSizeObj.h / idealSizeObj.w;
      } else {
        h = 300;
        w = h * idealSizeObj.w / idealSizeObj.h;
      }

      return {
        w: w,
        h: h
      };
    } else {
      console.warn('WARNING NO IDEAL SIZE INDICATED, PLEASE FIX');
      return {
        w: 300,
        h: 300
      };
    }
  }

angular.module('pencilblueApp')
  .directive('contenteditable', [function() {
  return {
    restrict: "A",
    require: "ngModel",
    link: function(scope, element, attrs, ngModel) {

      function read() {
        ngModel.$setViewValue(element.html());
      }

      // element.bind("keydown keypress", function (event) {
      //     if(event.which === 13) {
      //       console.log('enter key');
      //       event.preventDefault();
      //       scope.$apply(read);
      //     }
      // });

      ngModel.$render = function() {
        element.html(ngModel.$viewValue);
      };

      element.bind("keyup change", function() {
        scope.$apply(read);
        console.log('onblue keyup change');
      });
      element.bind("blur", function() {
        scope.$apply(read);
        console.log('onblur');
      });

    }
  };
}]);

//the main navigation
angular.module('pencilblueApp')
  .directive('htmlEditable', ['$rootScope', '$uibModal', 'ComponentFactory', 'SiteData', '$state', '$sce', function($rootScope, $uibModal, ComponentFactory, SiteData, $state, $sce) {
    return {
      require: 'ngModel',
      scope: {
        ngModel: '=',
        tag: '@',
        ctype: '@',
        pageId: '@',
        global: '@', //if doesnt belong to a particular page id, look footer elemnts
        metadata: '=',
        placeholder: '=',
        // If notify, send slack message on change
        notify: '=',
      },
      restrict: 'A',
      template: '<div ng-bind-html="trustedHtml"></div>',
      replace: true,
      link: function(scope, elem, attrs, ctrl) {
        scope.trustedHtml = '';
        var ngModel = scope.ngModel;
        if (ngModel) scope.trustedHtml = $sce.trustAsHtml(ngModel.html);
        console.log('$rootScope.placeholdertext', $rootScope.placeholdertext);
        if (!scope.tag) {
          console.error('WARNING: no tag specified on tag: ' + scope.tag + '. Please include unique identifier  if this text is on particular page.');
          return;
        }
        if (!scope.pageId && !scope.global) {
          console.error('WARNING: no page id specified on tag ' + scope.tag + '. Please include unique identifier if this text is on particular page.');
        }
        if (!$rootScope.editing) {
          return;
        } else if (scope.placeholder && (!ngModel || !ngModel.html || !ngModel.html.length)) {
          scope.trustedHtml = $sce.trustAsHtml(scope.placeholder);
        }
        elem.addClass('html-editable');
        elem.on('click', function() {
          var modalInstance = $uibModal.open({
            animation: false,
            backdrop: 'static',
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: '/public/performance/partials/html-editable.html',
            controllerAs: '$ctrl',
            controller: function($uibModalInstance, component_type, html, text, metadata) {
              var $ctrl = this;
              $ctrl.html = html;
              $ctrl.component_type = component_type;
              $ctrl.text = text;
              $ctrl.metadata = metadata || {
                header: 'Edit',
                description: '',
                placeholder: ''
              };
              console.log('$ctrl.metadata', $ctrl.metadata);
              var updateOpts = {
                tag: scope.tag,
                component_type: $ctrl.component_type,
                pageId: scope.pageId
              };


              $ctrl.saveCustomText = function() {
                console.log('save custom text: ', scope.notify);
                if ($ctrl.component_type == 'text') {
                  updateOpts.text = $ctrl.text;
                } else if ($ctrl.component_type == 'html') {
                  updateOpts.html = $ctrl.html;
                }

                if (scope.notify) updateOpts.notify = scope.notify;

                if (scope.pageId) updateOpts.page = scope.pageId;

                ComponentFactory.update(updateOpts).then(function(data) {
                  console.log(updateOpts);
                  scope.trustedHtml = $sce.trustAsHtml(data.data[updateOpts.component_type]);
                  $rootScope.$broadcast('text-updated');
                  $uibModalInstance.close(data.data);
                }).catch(function(err) {
                  if (err.data && err.data.message) {
                    alert(err.data.message);
                  } else {
                    alert('System Error');
                  }
                });
              };

              $ctrl.cancel = function() {
                $uibModalInstance.dismiss('cancel');
              };
            },
            resolve: {
              component_type: function() {
                return scope.ctype == 'text' ? 'text': 'html';
              },
              html: function() {
                return angular.copy(scope.ngModel ? scope.ngModel.html:'');
              },
              text: function() {
                return angular.copy(scope.ngModel ? scope.ngModel.text:'');
              },
              metadata: function() {
                console.log('metadata', scope.metadata);
                return scope.metadata;
              }
            }
          });

          modalInstance.result.then(function(component) {
            scope.ngModel = component;
          }, function() {
            console.log('Modal dismissed at: ' + new Date());
          });


        });
      }
    };
  }]);

angular.module('pencilblueApp')
  .directive('appLoading', function() {
    return {
      restrict: 'E',
      templateUrl: '/public/performance/partials/app-loading.html', // or template: 'template html code inline' Display none to the code is important so is not visible if youre not caling the methods
      replace: true,
      link: function(scope, elem) {
        scope.$on('app-start-loading', function() {
          elem.addClass('active');
        });
        scope.$on('app-finish-loading', function() {
          elem.removeClass('active');
        });
      }
    }
  })

//the modal directive to upload and image and crop
//the main navigation
angular.module('pencilblueApp')
  .directive('goto', ['$window', 'ComponentFactory', 'SiteData', '$uibModal', function($window, ComponentFactory, SiteData, $uibModal) {
    return {
      scope: {
        ngModel: '=',
        tag: '@'
      },
      restrict: 'A',
      replace: true,
      link: function(scope, elem, attrs) {
        var siteData = SiteData.getSiteData();        
        if (scope.ngModel && scope.ngModel.text) {
          elem.text(scope.ngModel.text);
        }        
        if (siteData.editing) {
          elem.on('click', function(e) {
            var modalInstance = $uibModal.open({
              animation: false,
              backdrop: 'static',
              ariaLabelledBy: 'modal-title',
              ariaDescribedBy: 'modal-body',
              templateUrl: '/public/performance/partials/goto.html',
              controllerAs: '$ctrl',
              controller: function($uibModalInstance, text, url) {
                var $ctrl = this;
                $ctrl.text = text;
                $ctrl.url = url;
                $ctrl.siteData = siteData;
                $ctrl.ok = function() {                
                  ComponentFactory.update({
                    text: $ctrl.text,
                    url: $ctrl.url,
                    tag: scope.tag
                  }).then(function(data) {
                    $uibModalInstance.close(data.data);                  
                  }).catch(function(err) {
                    if (err.data && err.data.message) {
                      alert(err.data.message);
                    } else {
                      alert('System Error');
                    }
                  });                
                };

                $ctrl.cancel = function() {
                  $uibModalInstance.dismiss('cancel');
                };
              },
              resolve: {
                text: function() {
                  return scope.ngModel ? scope.ngModel.text : '';
                },
                url: function() {
                  return scope.ngModel ? scope.ngModel.url : '';
                }                
              }
            });

            modalInstance.result.then(function(component) {
              scope.ngModel = component;
              if (component.text) {
                elem.text(component.text);
              }
              if (component.url) {
                //elem.text(component.text);
              }                
            }, function() {
              console.log('Modal dismissed at: ' + new Date());
            });

         
          });          
        } else {
          if (!scope.ngModel) {
            elem.remove();
          } else {
            if (scope.ngModel.url) {
              elem.attr('href', scope.ngModel.url);
            }            
          }
        }

      }
    };
  }]);

angular.module('pencilblueApp').component('videoElement', {
  bindings: {
    videoType: '<',
    video: '<'
  },
  templateUrl: '/public/performance/partials/video.html',
  controllerAs: '$video',
  controller: function($sce, $http) {
    var self = this;
    var youtubeThumbUrl = 'https://img.youtube.com/vi/{{videoId}}/hqdefault.jpg';
    var vimeoThumbUrl = 'https://developer.vimeo.com/api/v2/video/{{videoId}}.json';

    self.idRegs = {
      'youtube': /(embed\/)([a-zA-Z0-9_-]+)/i,
      'vimeo': /vimeo\.com\/video\/([a-zA-Z0-9_-]+)/i
    };

    if (self.video.url.match(self.idRegs.youtube)) {
      self.thumbnailUrl = youtubeThumbUrl.replace('{{videoId}}', self.video.url.match(self.idRegs.youtube)[2]);
    } else if (self.video.url.match(self.idRegs.vimeo)) {
      $http.post('/vimeoThumbnail', {videoId: self.video.url.match(self.idRegs.vimeo)[1]}).then(function(response) {
        self.thumbnailUrl = response.data[0].thumbnail_large;
      }).catch(function(err) {
        console.error(err);
      });
      // self.thumbnailUrl = youtubeThumbUrl.replace('{{videoId}}', self.video.url.match(self.idRegs.youtube)[2]);
    } else {
      getIncludeUrl.call(self);
    }

    this.getIncludeUrl = getIncludeUrl.bind(self);

    function getIncludeUrl() {
      console.log('videoType', self.videoType);
      if (self.videoType === 'iframe') {
        console.log('urlurlurl', self.video.url);
        self.videoFile = $sce.trustAsResourceUrl(self.video.url + '?autoplay=true');
        self.includeUrl = '/public/performance/partials/video-iframe.html';
      } else if (this.videoType === 'javascript') {
        self.includeUrl = '/public/performance/partials/video-js.html';
      }
    }
  }
});

pencilblueApp.component('expandingGallery', {
  bindings: {
    gallery: '=',
    nocrop: '='
  },
  template: '<div id="expanding-gallery">'+
                
								'<h1 class="text-center gallery-title">{{$exp.detailSection.name || "GALLERY"}}</h1>' +
								'<h2 ng-show="$exp.detailSection.subtitle" class="gallery-subtitle text-center">{{$exp.detailSection.subtitle || ""}}</h2>' +
                '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.js"></script>'+
                '<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick.min.css"/>'+
                '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.7.1/slick-theme.min.css" />'+
                '<div class="gallery-overview" ng-if="!$exp.detailSection">'+
                  '<ul>'+
										'<li ng-repeat="section in $exp.gallery" class="gallery-section" ng-click="$exp.expandSection(section)">'+
                      '<div class="main-img-holder">' +
												'<img ng-src="{{$exp.nocrop ? section.images[0].url : (section.images[0].url | expGalleryImage)}}" alt="" />'+
											'</div>' +
                      '<div class="details">'+
                        '<div class="section-name">{{section.name}}</div>'+
                        '<div class="section-subtitle">{{section.subtitle}}</div>'+
                        '<div class="section-view">view photos&nbsp;<i class="fa fa-caret-right"></i></div>'+
                      '</div>'+
                    '</li>'+
                  '</ul>'+
                '</div>'+
                '<div class="section-detail" ng-if="$exp.detailSection">'+
                  '<p class="text-center" ng-click="$exp.goBack()"><i class="fa fa-chevron-left"></i>&nbsp;Return to Gallery</p>'+
                  '<ul>'+
                    '<li ng-repeat="image in $exp.detailSection.images track by image._id" ng-click="$exp.openLightBox($index)">'+
                      '<div class="flex-column-start">'+
                        '<img ng-src="{{image.thumbnail}}" alt="" />'+
                        '<p ng-if="image.title" ng-cloak>{{image.title}}</p>'+
                        '<p ng-if="image.credit" ng-cloak>{{image.credit}}</p>'+
                      '</div>'+
                    '</li>'+
                  '</ul>'+
                '</div>'+
                '<div class="overlay" ng-if="$exp.lightBoxImages" ng-click="$exp.dismiss()">'+
                  '<div class="fa fa-close" ng-click="$exp.dismiss()"></div>'+
                  '<div class="wrapper" ng-click="$event.stopPropagation()">'+
                    '<ul id="light-box-images">'+
                      '<li ng-repeat="image in $exp.lightBoxImages" class="light-box-image" ng-cloak>'+
                        '<img ng-src="{{image.url}}" alt="" />'+
                      '</li>'+
                    '</ul>'+
                  '</div>'+
                  '<script>'+
                    'if ($().slick) {'+
                      'imagesLoaded($("#light-box-images"), function() {'+
                        '$("#light-box-images").slick({'+
                            'infinite: true,'+
                            'accessibility: true'+
                        '});'+
                      '});'+
                    '}'+
                  '</script>'+
                '</div>'+
              '</div>',
  controllerAs: '$exp',
  controller: function($location) {
    var self = this;
    console.log('self expandingGallery', self);

    self.expandSection = function(section) {
        self.detailSection = section;
        $location.search({section: section._id});
    };

    if ($location.search().section) {
      var section = _.find(self.gallery, function(g) { return g._id === $location.search().section; });
      if (section) self.expandSection(section);
    }

    self.goBack = function() {
      self.detailSection = null;
      $location.search({section: null});
    };

    self.openLightBox = function(index) {
      self.lightBoxImages = self.detailSection.images.slice(index).concat(self.detailSection.images.slice(0,index));
    };

    self.dismiss = function() {
      self.lightBoxImages = null;
    };
  }
}).filter('expGalleryImage', function() {
  return function(input, uppercase) {
    if (!input || !input.length) return input;
    var split = input.split(/\/(?!\/)/);
    var sizing = 'c_crop,g_faces,w_650,h_390';
    var idx = split.indexOf('upload');

    if (idx > -1) {
      split.splice(idx + 1, 0, sizing);
      console.log(split.join('/'));
      return split.join('/');
    } else {
      return input;
    }
  };
});

/**
 * Color Palette Constants
 * @type {Object} themeId: {
 *  @type {Object} paletteId: {
 *    @type {String} selector: 'style'
 *  }
 * }
 */

pencilblueApp.constant('colorPalettes', {
  // Theme 1
  1: {
    1: {
      background_color: '#181818',
      header_background_color: '#000',
      color: '#fff',
      nav_text_color: '#fff',
      nav_active_color: '#545454',
      navbar_background: 'rgba(0,0,0,0)',
      link_color: '#C8C8CF',
      link_hover_color: '#C2C2E5',
      button_text_color: '#000',
      button_bg_color: '#fff',
      social_background_color: '#181818',
      social_font_color: '#fff',
      title_color: '#fff'
    },
    2: {
      background_color: '#f5f5f5',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#a5a5a5',
      link_color: '#a5a5a5',
      header_background_color: '#fff',
      nav_text_color: '#333',
      nav_active_color: '#e0dfdf',
      navbar_background: 'rgba(0,0,0,0)',
      social_background_color: '#333',
      social_font_color: '#fff',
      title_color: '#333',
      link_hover_color: '#5b5b99'
    },
    3: {
      background_color: '#f4f4f4',
      color: '#333',
      color_footer: '#f4f4f4',
      button_text_color: '#fff',
      button_bg_color: '#c0b283',
      link_color: '#c0b283',
      header_background_color: '#373737',
      nav_text_color: '#c0b283',
      nav_active_color: '#222',
      navbar_background: 'rgba(0,0,0,0)',
      social_background_color: '#dcd0d0',
      social_font_color: '#373737',
      title_color: '#c0b283',
      link_hover_color: '#a18834'
    },
    4: {
      background_color: '#0b3c5d',
      color: '#ebebeb',
      color_footer: '#ebebeb',
      button_text_color: '#fff',
      button_bg_color: '#d9b310',
      link_color: '#328cc1',
      header_background_color: '#1d2731',
      nav_text_color: '#ebebeb',
      nav_active_color: '#d9b310',
      navbar_background: 'rgba(0,0,0,0)',
      social_background_color: '#328cc1',
      social_font_color: '#fff',
      title_color: '#fff',
      link_hover_color: '#d9b310'
    },
    5: {
      background_color: '#912141',
      color: '#f5f5f5',
      color_footer: '#f5f5f5',
      button_text_color: '#f5f5f5',
      button_bg_color: '#699aa9',
      link_color: '#94c7d7',
      header_background_color: '#7d1935',
      nav_text_color: '#f5f5f5',
      nav_active_color: '#699aa9',
      navbar_background: 'rgba(0,0,0,0)',
      social_background_color: '#699aa9',
      social_font_color: '#f5f5f5',
      title_color: '#f5f5f5',
      link_hover_color: '#f5f5f5'
    },
    6: {
      background_color: '#dbdbdb',
      color: '#333',
      color_footer: '#fff',
      button_text_color: '#fff',
      button_bg_color: '#4e4c4e',
      link_color: '#4e4c4e',
      header_background_color: '#595959',
      nav_text_color: '#fff',
      nav_active_color: '#4e4c4e',
      navbar_background: 'rgba(0,0,0,0)',
      social_background_color: '#fff',
      social_font_color: '#4e4c4e',
      title_color: '#fff',
      link_hover_color: '#000'
    },
    7: {
      background_color: '#fafafa',
      color: '#0f1626',
      color_footer: '#fff',
      button_text_color: '#fff',
      button_bg_color: '#ff533d',
      link_color: '#ff533d',
      header_background_color: '#0f1626',
      nav_text_color: '#fff',
      nav_active_color: '#ff533d',
      navbar_background: 'rgba(0,0,0,0)',
      social_background_color: '#ff533d',
      social_font_color: '#fff',
      title_color: '#fff',
      link_hover_color: '#d13420'
    },
    8: {
      background_color: '#cae4db',
      color: '#00303f',
      color_footer: '#fff',
      button_text_color: '#fff',
      button_bg_color: '#dcae1d',
      link_color: '#908768',
      header_background_color: '#7a9a96',
      nav_text_color: '#fff',
      nav_active_color: '#00303f',
      navbar_background: 'rgba(0,0,0,0)',
      social_background_color: '#00303f',
      social_font_color: '#fff',
      title_color: '#fff',
      link_hover_color: '#dcae1d'
    }
  },
  // End theme 1

  // Theme 2
  2: {
    1: {
      background_color: '#181818',
      color: '#fff',
      color_footer: '#fff',
      button_text_color: '#000',
      button_bg_color: '#fff',
      link_color: '#c8c8cf',
      header_background_color: '#181818',
      nav_text_color: '#fff',
      nav_active_color: '#545454',
      navbar_background: '#000',
      social_background_color: '#000',
      social_font_color: '#fff',
      title_color: '#fff',
      link_hover_color: '#C2C2E5'
    },
    2: {
      background_color: '#fff',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#a0a0a8',
      link_color: '#a0a0a8',
      header_background_color: '#fff',
      nav_text_color: '#333',
      nav_active_color: '#e0dfdf',
      navbar_background: '#f0f0f0',
      social_background_color: '#333',
      social_font_color: '#fff',
      title_color: '#333',
      link_hover_color: '#5b5b99'
    },
    3: {
      background_color: '#f4f4f4',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#c0b283',
      link_color: '#c0b283',
      header_background_color: '#f4f4f4',
      nav_text_color: '#f4f4f4',
      nav_active_color: '#222',
      navbar_background: '#373737',
      social_background_color: '#dcd0d0',
      social_font_color: '#373737',
      title_color: '#c0b283',
      link_hover_color: '#a18834'
    },
    4: {
      background_color: '#0b3c5d',
      color: '#ebebeb',
      color_footer: '#ebebeb',
      button_text_color: '#fff',
      button_bg_color: '#d9b310',
      link_color: '#328cc1',
      header_background_color: '#0b3c5d',
      nav_text_color: '#fff',
      nav_active_color: '#1d2731',
      navbar_background: '#1d2731',
      social_background_color: '#328cc1',
      social_font_color: '#fff',
      title_color: '#fff',
      link_hover_color: '#d9b310'
    },
    5: {
      background_color: '#7d1935',
      color: '#f5f5f5',
      color_footer: '#f5f5f5',
      button_text_color: '#f5f5f5',
      button_bg_color: '#699aa9',
      link_color: '#d7cec7',
      header_background_color: '#7d1935',
      nav_text_color: '#565656',
      nav_active_color: '#d7cec7',
      navbar_background: '#f5f5f5',
      social_background_color: '#699aa9',
      social_font_color: '#f5f5f5',
      title_color: '#f5f5f5',
      link_hover_color: '#f5f5f5'
    },
    6: {
      background_color: '#dbdbdb',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#4e4c4e',
      link_color: '#4e4c4e',
      header_background_color: '#dbdbdb',
      nav_text_color: '#4e4c4e',
      nav_active_color: '#fff',
      navbar_background: '#fff',
      social_background_color: '#4e4c4e',
      social_font_color: '#fff',
      title_color: '#4e4c4e',
      link_hover_color: '#000'
    },
    7: {
      background_color: '#fafafa',
      color: '#0f1626',
      color_footer: '#0f1626',
      button_text_color: '#fff',
      button_bg_color: '#ff533d',
      link_color: '#ff533d',
      header_background_color: '#fafafa',
      nav_text_color: '#fff',
      nav_active_color: '#ff533d',
      navbar_background: '#0f1626',
      social_background_color: '#ff533d',
      social_font_color: '#fff',
      title_color: '#0f1626',
      link_hover_color: '#d13420'
    },
    8: {
      background_color: '#cae4db',
      color: '#00303f',
      color_footer: '#00303f',
      button_text_color: '#fff',
      button_bg_color: '#dcae1d',
      link_color: '#908768',
      header_background_color: '#cae4db',
      nav_text_color: '#fff',
      nav_active_color: '#00303f',
      navbar_background: '#7a9a96',
      social_background_color: '#00303f',
      social_font_color: '#fff',
      title_color: '#00303f',
      link_hover_color: '#dcae1d'
    }
  },
  // End theme 2

  // Theme 3
  3: {
    1: {
      background_color: '#181818',
      color: '#fff',
      color_footer: '#f4f4f4',
      button_text_color: '#000',
      button_bg_color: '#000',
      link_color: '#c8c8cf',
      header_background_color: '#000',
      nav_text_color: '#fff',
      nav_active_color: '#545454',
      navbar_background: '#262525',
      social_background_color: '#545454',
      social_font_color: '#fff',
      title_color: '#fff',
      link_hover_color: '#C2C2E5'
    },
    2: {
      background_color: '#fff',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#a0a0a8',
      link_color: '#a0a0a8',
      header_background_color: '#fff',
      nav_text_color: '#333',
      nav_active_color: '#e0dfdf',
      navbar_background: '#f0f0f0',
      social_background_color: '#333',
      social_font_color: '#fff',
      title_color: '#333',
      link_hover_color: '#5b5b99'
    },
    3: {
      background_color: '#f4f4f4',
      color: '#333',
      color_footer: '#fff',
      button_text_color: '#fff',
      button_bg_color: '#c0b283',
      link_color: '#c0b283',
      header_background_color: '#373737',
      nav_text_color: '#373737',
      nav_active_color: '#c0b283',
      navbar_background: '#fff',
      social_background_color: '#dcd0c0',
      social_font_color: '#373737',
      title_color: '#fff',
      link_hover_color: '#b39d52'
    },
    4: {
      background_color: '#0b3c5d',
      color: '#ebebeb',
      color_footer: '#ebebeb',
      button_text_color: '#ebebeb',
      button_bg_color: '#d9b310',
      link_color: '#328cc1',
      header_background_color: '#2c72a1',
      nav_text_color: '#fff',
      nav_active_color: '#d9b310',
      navbar_background: '#1d2731',
      social_background_color: '#328cc1',
      social_font_color: '#fff',
      title_color: '#fff',
      link_hover_color: '#d9b310'
    },
    5: {
      background_color: '#7d1935',
      color: '#f3f3ee',
      color_footer: '#2e1b13',
      button_text_color: '#f3f3ee',
      button_bg_color: '#699aa9',
      link_color: '#94c7d7',
      header_background_color: '#f5f3ee',
      nav_text_color: '#f3f3ee',
      nav_active_color: '#699aa9',
      navbar_background: '#2e1b13',
      social_background_color: '#699aa9',
      social_font_color: '#2e1b13',
      title_color: '#3b251c',
      link_hover_color: '#f5f3ee'
    },
    6: {
      background_color: '#dbdbdb',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#4e4c4e',
      link_color: '#4e4c4e',
      header_background_color: '#dbdbdb',
      nav_text_color: '#4e4c4e',
      nav_active_color: '#fff',
      navbar_background: '#dbdbdb',
      social_background_color: '#4e4c4e',
      social_font_color: '#fff',
      title_color: '#000',
      link_hover_color: '#000'
    },
    7: {
      background_color: '#fafafa',
      color: '#0f1626',
      color_footer: '#0f1626',
      button_text_color: '#fff',
      button_bg_color: '#ff533d',
      link_color: '#ff533d',
      header_background_color: '#fafafa',
      nav_text_color: '#fff',
      nav_active_color: '#ff533d',
      navbar_background: '#0f1626',
      social_background_color: '#ff533d',
      social_font_color: '#fff',
      title_color: '#0f1626',
      link_hover_color: '#d13420'
    },
    8: {
      background_color: '#cae4db',
      color: '#00303f',
      color_footer: '#00303f',
      button_text_color: '#fff',
      button_bg_color: '#dcae1d',
      link_color: '#908768',
      header_background_color: '#cae4db',
      nav_text_color: '#fff',
      nav_active_color: '#00303f',
      navbar_background: '#7a9a96',
      social_background_color: '#00303f',
      social_font_color: '#fff',
      title_color: '#00303f',
      link_hover_color: '#dcae1d'
    }
  },
  // End theme 3

  // Theme 4
  4: {
    1: {
      background_color: '#000',
      color: '#fff',
      color_footer: '#fff',
      button_text_color: '#000',
      button_bg_color: '#fff',
      link_color: '#c8c8cf',
      header_background_color: '#000',
      nav_text_color: '#e6e6e6',
      nav_active_color: '#262525',
      navbar_background: '#000',
      social_background_color: '#fff',
      social_font_color: '#000',
      title_color: '#fff',
      link_hover_color: '#C2C2E5'
    },
    2: {
      background_color: '#fff',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#888',
      link_color: '#5b5b99',
      header_background_color: '#fff',
      nav_text_color: '#888',
      nav_active_color: '#e0dfdf',
      navbar_background: '#fff',
      social_background_color: '#333',
      social_font_color: '#fff',
      title_color: '#000',
      link_hover_color: '#a0a0a8'
    },
    3: {
      background_color: '#f4f4f4',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#c0b283',
      link_color: '#908768',
      header_background_color: '#f4f4f4',
      nav_text_color: '#fff',
      nav_active_color: '#373737',
      navbar_background: '#c0b283',
      social_background_color: '#dcd0d0',
      social_font_color: '#373737',
      title_color: '#373737',
      link_hover_color: '#c0b283'
    },
    4: {
      background_color: '#0b3c5d',
      color: '#ebebeb',
      color_footer: '#ebebeb',
      button_text_color: '#ebebeb',
      button_bg_color: '#d9b310',
      link_color: '#328cc1',
      header_background_color: '#0b3c5d',
      nav_text_color: '#fff',
      nav_active_color: '#d9b310',
      navbar_background: '#1d2731',
      social_background_color: '#328cc1',
      social_font_color: '#fff',
      title_color: '#fff',
      link_hover_color: '#d9b310'
    },
    5: {
      background_color: '#7d1935',
      color: '#f5f5f5',
      color_footer: '#f5f5f5',
      button_text_color: '#f5f5f5',
      button_bg_color: '#699aa9',
      link_color: '#94c7d7',
      header_background_color: '#7d1935',
      nav_text_color: '#f5f5f5',
      nav_active_color: '#9a3f58',
      navbar_background: '#7d1935',
      social_background_color: '#699aa9',
      social_font_color: '#2e1b13',
      title_color: '#f5f5f5',
      link_hover_color: '#f5f5f5'
    },
    6: {
      background_color: '#dbdbdb',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#4e4c4e',
      link_color: '#4e4c4e',
      header_background_color: '#dbdbdb',
      nav_text_color: '#4e4c4e',
      nav_active_color: '#fff',
      navbar_background: '#fff',
      social_background_color: '#4e4c4e',
      social_font_color: '#fff',
      title_color: '#000',
      link_hover_color: '#000'
    },
    7: {
      background_color: '#fafafa',
      color: '#0f1626',
      color_footer: '#0f1626',
      button_text_color: '#fff',
      button_bg_color: '#ff533d',
      link_color: '#ff533d',
      header_background_color: '#fafafa',
      nav_text_color: '#fff',
      nav_active_color: '#ff533d',
      navbar_background: '#0f1626',
      social_background_color: '#ff533d',
      social_font_color: '#fff',
      title_color: '#0f1626',
      link_hover_color: '#d13420'
    },
    8: {
      background_color: '#cae4db',
      color: '#00303f',
      color_footer: '#00303f',
      button_text_color: '#fff',
      button_bg_color: '#dcae1d',
      link_color: '#908768',
      header_background_color: '#cae4db',
      nav_text_color: '#fff',
      nav_active_color: '#00303f',
      navbar_background: '#7a9a96',
      social_background_color: '#00303f',
      social_font_color: '#fff',
      title_color: '#00303f',
      link_hover_color: '#dcae1d'
    }
  },
  // End theme 4

  // Theme 5
  5: {
    1: {
      background_color: '#181818',
      color: '#fff',
      color_footer: '#fff',
      button_text_color: '#000',
      button_bg_color: '#fff',
      link_color: '#c8c8cf',
      header_background_color: '#000',
      nav_text_color: '#fff',
      nav_active_color: '#545454',
      navbar_background: '#181818',
      social_background_color: '#181818',
      social_font_color: '#fff',
      title_color: '#fff',
      link_hover_color: '#C2C2E5'
    },
    2: {
      background_color: '#fff',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#a0a0a8',
      link_color: '#a0a0a8',
      header_background_color: '#fff',
      nav_text_color: '#333',
      nav_active_color: '#e0dfdf',
      navbar_background: '#f0f0f0',
      social_background_color: '#333',
      social_font_color: '#fff',
      title_color: '#333',
      link_hover_color: '#5b5b99'
    },
    3: {
      background_color: '#f4f4f4',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#c0b283',
      link_color: '#c0b283',
      header_background_color: '#f4f4f4',
      nav_text_color: '#f4f4f4',
      nav_active_color: '#c0b283',
      navbar_background: '#373737',
      social_background_color: '#dcd0d0',
      social_font_color: '#373737',
      title_color: '#333',
      link_hover_color: '#a18834'
    },
    4: {
      background_color: '#0b3c5d',
      color: '#ebebeb',
      color_footer: '#ebebeb',
      button_text_color: '#fff',
      button_bg_color: '#d9b310',
      link_color: '#328cc1',
      header_background_color: '#0b3c5d',
      nav_text_color: '#fff',
      nav_active_color: '#d9b310',
      navbar_background: '#1d2731',
      social_background_color: '#328cc1',
      social_font_color: '#fff',
      title_color: '#fff',
      link_hover_color: '#d9b310'
    },
    5: {
      background_color: '#7d1935',
      color: '#f5f5f5',
      color_footer: '#f5f5f5',
      button_text_color: '#2e1b13',
      button_bg_color: '#699aa9',
      link_color: '#94c7d7',
      header_background_color: '#7d1935',
      nav_text_color: '#f5f5f5',
      nav_active_color: '#2e1b13',
      navbar_background: '#9a3f58',
      social_background_color: '#699aa9',
      social_font_color: '#2e1b13',
      title_color: '#f5f5f5',
      link_hover_color: '#f5f5f5'
    },
    6: {
      background_color: '#dbdbdb',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#4e4c4e',
      link_color: '#4e4c4e',
      header_background_color: '#dbdbdb',
      nav_text_color: '#595959',
      nav_active_color: '#fff',
      navbar_background: '#dbdbdb',
      social_background_color: '#4e4c4e',
      social_font_color: '#fff',
      title_color: '#595959',
      link_hover_color: '#000'
    },
    7: {
      background_color: '#fafafa',
      color: '#0f1626',
      color_footer: '#0f1626',
      button_text_color: '#fff',
      button_bg_color: '#ff533d',
      link_color: '#ff533d',
      header_background_color: '#fafafa',
      nav_text_color: '#fff',
      nav_active_color: '#ff533d',
      navbar_background: '#0f1626',
      social_background_color: '#ff533d',
      social_font_color: '#fff',
      title_color: '#0f1626',
      link_hover_color: '#d13420'
    },
    8: {
      background_color: '#cae4db',
      color: '#00303f',
      color_footer: '#00303f',
      button_text_color: '#fff',
      button_bg_color: '#dcae1d',
      link_color: '#908768',
      header_background_color: '#cae4db',
      nav_text_color: '#fff',
      nav_active_color: '#00303f',
      navbar_background: '#7a9a96',
      social_background_color: '#00303f',
      social_font_color: '#fff',
      title_color: '#00303f',
      link_hover_color: '#dcae1d'
    }
  },
  // end theme 5

  // Theme 6
  6: {
    1: {
      background_color: '#181818',
      color: '#fff',
      color_footer: '#fff',
      button_text_color: '#000',
      button_bg_color: '#fff',
      link_color: '#c8c8cf',
      header_background_color: '#181818',
      nav_text_color: '#fff',
      nav_active_color: '#545454',
      navbar_background: '#000',
      social_background_color: '#000',
      social_font_color: '#fff',
      title_color: '#fff',
      link_hover_color: '#C2C2E5',
      about_background: '#ccc'
    },
    2: {
      background_color: '#fff',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#a0a0a8',
      link_color: '#a0a0a8',
      header_background_color: '#fff',
      nav_text_color: '#333',
      nav_active_color: '#e0dfdf',
      navbar_background: '#f0f0f0',
      social_background_color: '#333',
      social_font_color: '#fff',
      title_color: '#333',
      link_hover_color: '#5b5b99',
      about_background: '#ccc'
    },
    3: {
      background_color: '#f4f4f4',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#c0b283',
      link_color: '#c0b283',
      header_background_color: '#f4f4f4',
      nav_text_color: '#f4f4f4',
      nav_active_color: '#c0b283',
      navbar_background: '#373737',
      social_background_color: '#373737',
      social_font_color: '#c0b283',
      title_color: '#222',
      link_hover_color: '#a18834',
      about_background: '#ccc'
    },
    4: {
      background_color: '#0b3c5d',
      color: '#ebebeb',
      color_footer: '#ebebeb',
      button_text_color: '#fff',
      button_bg_color: '#d9b310',
      link_color: '#328cc1',
      header_background_color: '#0b3c5d',
      nav_text_color: '#fff',
      nav_active_color: '#d9b310',
      navbar_background: '#1d2731',
      social_background_color: '#328cc1',
      social_font_color: '#fff',
      title_color: '#fff',
      link_hover_color: '#d9b310',
      about_background: '#ccc'
    },
    5: {
      background_color: '#7d1935',
      color: '#f5f5f5',
      color_footer: '#f5f5f5',
      button_text_color: '#f5f5f5',
      button_bg_color: '#699aa9',
      link_color: '#d7cec7',
      header_background_color: '#7d1935',
      nav_text_color: '#f5f5f5',
      nav_active_color: '#699aa9',
      navbar_background: '#2e1b13',
      social_background_color: '#699aa9',
      social_font_color: '#f5f5f5',
      title_color: '#f5f5f5',
      link_hover_color: '#f5f5f5',
      about_background: '#ccc'
    },
    6: {
      background_color: '#dbdbdb',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#4e4c4e',
      link_color: '#4e4c4e',
      header_background_color: '#dbdbdb',
      nav_text_color: '#fff',
      nav_active_color: '#4e4c4e',
      navbar_background: '#595959',
      social_background_color: '#4e4c4e',
      social_font_color: '#fff',
      title_color: '#4e4c4e',
      link_hover_color: '#000',
      about_background: '#ccc'
    },
    7: {
      background_color: '#fafafa',
      color: '#0f1626',
      color_footer: '#0f1626',
      button_text_color: '#fff',
      button_bg_color: '#ff533d',
      link_color: '#ff533d',
      header_background_color: '#fafafa',
      nav_text_color: '#fff',
      nav_active_color: '#ff533d',
      navbar_background: '#0f1626',
      social_background_color: '#ff533d',
      social_font_color: '#fff',
      title_color: '#0f1626',
      link_hover_color: '#d13420',
      about_background: '#ccc'
    },
    8: {
      background_color: '#cae4db',
      color: '#00303f',
      color_footer: '#00303f',
      button_text_color: '#fff',
      button_bg_color: '#dcae1d',
      link_color: '#908768',
      header_background_color: '#cae4db',
      nav_text_color: '#fff',
      nav_active_color: '#00303f',
      navbar_background: '#7a9a96',
      social_background_color: '#00303f',
      social_font_color: '#fff',
      title_color: '#00303f',
      link_hover_color: '#dcae1d',
      about_background: '#ccc'
    }
  },
  // theme 7
  7: {
    1: {
      background_color: '#181818',
      header_background_color: '#000',
      color: '#fff',
      nav_text_color: '#fff',
      nav_active_color: '#545454',
      navbar_background: 'rgba(0,0,0,0)',
      link_color: '#C8C8CF',
      link_hover_color: '#C2C2E5',
      button_text_color: '#000',
      button_bg_color: '#fff',
      social_background_color: '#181818',
      social_font_color: '#fff',
      title_color: '#fff'
    },
    2: {
      background_color: '#f5f5f5',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#a5a5a5',
      link_color: '#a5a5a5',
      header_background_color: '#fff',
      nav_text_color: '#333',
      nav_active_color: '#e0dfdf',
      navbar_background: 'rgba(0,0,0,0)',
      social_background_color: '#333',
      social_font_color: '#fff',
      title_color: '#333',
      link_hover_color: '#5b5b99'
    },
    3: {
      background_color: '#f4f4f4',
      color: '#333',
      color_footer: '#f4f4f4',
      button_text_color: '#fff',
      button_bg_color: '#c0b283',
      link_color: '#c0b283',
      header_background_color: '#373737',
      nav_text_color: '#c0b283',
      nav_active_color: '#222',
      navbar_background: 'rgba(0,0,0,0)',
      social_background_color: '#dcd0d0',
      social_font_color: '#373737',
      title_color: '#373737',
      link_hover_color: '#a18834'
    },
    4: {
      background_color: '#0b3c5d',
      color: '#ebebeb',
      color_footer: '#ebebeb',
      button_text_color: '#fff',
      button_bg_color: '#d9b310',
      link_color: '#328cc1',
      header_background_color: '#1d2731',
      nav_text_color: '#ebebeb',
      nav_active_color: '#d9b310',
      navbar_background: 'rgba(0,0,0,0)',
      social_background_color: '#328cc1',
      social_font_color: '#fff',
      title_color: '#fff',
      link_hover_color: '#d9b310'
    },
    5: {
      background_color: '#912141',
      color: '#f5f5f5',
      color_footer: '#f5f5f5',
      button_text_color: '#f5f5f5',
      button_bg_color: '#699aa9',
      link_color: '#94c7d7',
      header_background_color: '#7d1935',
      nav_text_color: '#f5f5f5',
      nav_active_color: '#699aa9',
      navbar_background: 'rgba(0,0,0,0)',
      social_background_color: '#699aa9',
      social_font_color: '#f5f5f5',
      title_color: '#f5f5f5',
      link_hover_color: '#f5f5f5'
    },
    6: {
      background_color: '#dbdbdb',
      color: '#333',
      color_footer: '#fff',
      button_text_color: '#fff',
      button_bg_color: '#4e4c4e',
      link_color: '#4e4c4e',
      header_background_color: '#595959',
      nav_text_color: '#333',
      nav_active_color: '#4e4c4e',
      navbar_background: 'rgba(0,0,0,0)',
      social_background_color: '#fff',
      social_font_color: '#4e4c4e',
      title_color: '#333',
      link_hover_color: '#000'
    },
    7: {
      background_color: '#fafafa',
      color: '#0f1626',
      color_footer: '#fff',
      button_text_color: '#fff',
      button_bg_color: '#ff533d',
      link_color: '#ff533d',
      header_background_color: '#0f1626',
      nav_text_color: '#000',
      nav_active_color: '#ff533d',
      navbar_background: 'rgba(0,0,0,0)',
      social_background_color: '#ff533d',
      social_font_color: '#fff',
      title_color: '#000',
      link_hover_color: '#d13420'
    },
    8: {
      background_color: '#cae4db',
      color: '#00303f',
      color_footer: '#fff',
      button_text_color: '#fff',
      button_bg_color: '#dcae1d',
      link_color: '#908768',
      header_background_color: '#7a9a96',
      nav_text_color: '#7a9a96',
      nav_active_color: '#00303f',
      navbar_background: 'rgba(0,0,0,0)',
      social_background_color: '#00303f',
      social_font_color: '#fff',
      title_color: '#00303f',
      link_hover_color: '#dcae1d'
    }
  },
  // theme 8
  8: {
    1: {
      background_color: '#181818',
      header_background_color: '#181818',
      color: '#fff',
      nav_text_color: '#fff',
      nav_active_color: '#545454',
      navbar_background: '#000',
      link_color: '#C8C8CF',
      link_hover_color: '#C2C2E5',
      button_text_color: '#000',
      button_bg_color: '#fff',
      social_background_color: '#000',
      social_font_color: '#fff',
      title_color: '#fff'
    },
    2: {
      background_color: '#f5f5f5',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#a5a5a5',
      link_color: '#a5a5a5',
      header_background_color: '#f5f5f5',
      nav_text_color: '#333',
      nav_active_color: '#e0dfdf',
      navbar_background: '#fff',
      social_background_color: '#333',
      social_font_color: '#fff',
      title_color: '#222',
      link_hover_color: '#5b5b99'
    },
    3: {
      background_color: '#f4f4f4',
      color: '#333',
      color_footer: '#373737',
      button_text_color: '#fff',
      button_bg_color: '#c0b283',
      link_color: '#c0b283',
      header_background_color: '#f4f4f4',
      nav_text_color: '#c0b283',
      nav_active_color: '#222',
      navbar_background: '#373737',
      social_background_color: '#dcd0d0',
      social_font_color: '#373737',
      title_color: '#222',
      link_hover_color: '#a18834'
    },
    4: {
      background_color: '#0b3c5d',
      color: '#ebebeb',
      color_footer: '#ebebeb',
      button_text_color: '#fff',
      button_bg_color: '#d9b310',
      link_color: '#328cc1',
      header_background_color: '#0b3c5d',
      nav_text_color: '#ebebeb',
      nav_active_color: '#d9b310',
      navbar_background: '#1d2731',
      social_background_color: '#328cc1',
      social_font_color: '#fff',
      title_color: '#fff',
      link_hover_color: '#d9b310'
    },
    5: {
      background_color: '#912141',
      color: '#f5f5f5',
      color_footer: '#f5f5f5',
      button_text_color: '#f5f5f5',
      button_bg_color: '#699aa9',
      link_color: '#94c7d7',
      header_background_color: '#912141',
      nav_text_color: '#f5f5f5',
      nav_active_color: '#699aa9',
      navbar_background: '#7d1935',
      social_background_color: '#699aa9',
      social_font_color: '#f5f5f5',
      title_color: '#f5f5f5',
      link_hover_color: '#f5f5f5'
    },
    6: {
      background_color: '#dbdbdb',
      color: '#333',
      color_footer: '#333',
      button_text_color: '#fff',
      button_bg_color: '#4e4c4e',
      link_color: '#4e4c4e',
      header_background_color: '#dbdbdb',
      nav_text_color: '#fff',
      nav_active_color: '#4e4c4e',
      navbar_background: '#595959',
      social_background_color: '#fff',
      social_font_color: '#4e4c4e',
      title_color: '#333',
      link_hover_color: '#000'
    },
    7: {
      background_color: '#fafafa',
      color: '#0f1626',
      color_footer: '#0f1626',
      button_text_color: '#fff',
      button_bg_color: '#ff533d',
      link_color: '#ff533d',
      header_background_color: '#fafafa',
      nav_text_color: '#fff',
      nav_active_color: '#ff533d',
      navbar_background: '#0f1626',
      social_background_color: '#ff533d',
      social_font_color: '#fff',
      title_color: '#0f1626',
      link_hover_color: '#d13420'
    },
    8: {
      background_color: '#cae4db',
      color: '#00303f',
      color_footer: '#00303f',
      button_text_color: '#fff',
      button_bg_color: '#dcae1d',
      link_color: '#908768',
      header_background_color: '#cae4db',
      nav_text_color: '#fff',
      nav_active_color: '#00303f',
      navbar_background: '#7a9a96',
      social_background_color: '#00303f',
      social_font_color: '#fff',
      title_color: '#00303f',
      link_hover_color: '#dcae1d'
    }
  },

});

angular.module('pencilblueApp').component('soundcloud', {
  bindings: {
    url: '<',
    title: '<'
  },
  template: '<div class="soundcloud"><div ng-bind-html="$sc.iframe"></div><h3>{{$sc.title}}</h3></div>',
  controllerAs: '$sc',
  controller: function($http, $sce) {
    var self = this;
    var soundcloudApi = 'http://soundcloud.com/oembed';

    $http.post('/soundcloud', {url: self.url}).then(function(response) {
      self.iframe = $sce.trustAsHtml(response.data.html);
    }).catch(function(err) {
      console.error(err);
    });
  }
});

angular.module('pencilblueApp').component('contactForm', {
  templateUrl: function(SiteData, $state) {
    return '/public/performance/themes/partials/contact-form.html';
  },
  controllerAs: '$ctrl',
  controller: function($http) {

    var self = this;
    self.info = {};
    self.registerToken = function(token) {
      console.log('registering token', token);
      self.token = token;
    }

    self.submitForm = function() {
      console.log('SUBMIT FORM');
      $http.post('/api/contact', {
        name: self.formData.name,
        message: self.formData.message,
        fromEmail: self.formData.fromEmail,
        gRecaptchaResponse: self.token
      }).then(function(res) {
        if (res.status >= 200 && res.status < 300) {
          if (self.info.error) self.info.error = null;
          self.alertClass = 'alert-success';
          self.info.success = 'Thanks! Your email was sent and I will get back to you as soon as possible!'; 
          self.showAlert = true;
          console.log('SHOULD MAKE VALID AGAIN');
 
          self.formData = {};
          self.contactForm.$setPristine();
          self.contactForm.$setUntouched();
        } else {
          var error = JSON.parse(res.data.body);
          console.error(error);
          var text = 'It seems there was a problem sending an email: <pre>STATUS CODE: ' + res.data.statusCode + '\n' + error.errors[0].message + '</pre> You may try to contact me at: <a href="mailto:' + self.page.social.email.handle + '">' + self.page.social.email.handle + '</a>, or you can contact <a href="mailto:' + window.HELP_EMAIL + '">' + window.HELP_EMAIL + '</a> and let them know!';
          if (self.info.success) self.info.success = null;
          self.alertClass = 'alert-danger';
          self.info.error = text;
          self.showAlert = true;
        }
      }).catch(function(err) {
        console.error(err);
        if (self.info.success) self.info.success = null;
        self.info.error = err.data;
        self.showAlert = true;
      });      
    }
  }
});

//# sourceMappingURL=sourcemaps/js/index_edit.js.map
