pencilblueApp.directive('dynamicStyles', function() {
  return {
    scope: {
      pageStyles: '='
    },
    templateUrl: '/public/performance/themes/dynamic-styles.css'
  };
});
