
angular.module('pencilblueApp').component('galleryblock', {
  bindings: {
    pageId: '=',
    editing: '=',
    nocrop: '='
  },
  templateUrl: ['$element', '$attrs', function($element, $attrs) {
    if (!$attrs.partial) {
      console.error('partial url required.');
      return false;
    }
    var url = '/public/performance/pages/gallery/' + $attrs.partial;
    return url;
  }],
  controllerAs: '$ctrl',
  controller: function($http, $sce, GetGallery, ngDialog) {
    var self = this;
    console.log('self.nocrop', self.nocrop);
    GetGallery.get({
      pageId: self.pageId
    }).then(function(resp) {
      
      self.gallery = resp.data;
      self.carouselIdx = Array(self.gallery.length).fill(0);
    });
    
    
    

    this.slide = function(idx, dir, sectionIdx) {
      
      if (idx) {
        if (idx > -1) {
          this.carouselIdx[sectionIdx] = idx;
        } else {
          this.carouselIdx[sectionIdx]++;
        }
      } else if (!idx && dir) {
        if (sectionIdx) {
          this.carouselIdx[sectionIdx] = dir === 'next' ? this.carouselIdx[sectionIdx] + 1 : this.carouselIdx[sectionIdx] - 1;
          $('#carousel-element-' + sectionIdx).carousel(dir);
        } else {
          $('#carousel-element').carousel(dir);
        }
      }
    };

    this.openPicModal = function(section, imgIndex) {
      var copiedSection = angular.copy(section);

      ngDialog.open({
        template: '/public/performance/themes/partials/gallery-modal.html',
        appendClassName: 'gallery-modal',
        resolve: {
          currSection: function() {
            if (imgIndex) {
              var imagesCut = copiedSection.images.splice(0,imgIndex);
              copiedSection.images = copiedSection.images.concat(imagesCut);
            }
            return copiedSection;
          },
          slide: function() {
            return self.slide;
          }
        },
        controller: ['$scope', 'currSection', 'slide', function(scope, currSection, slide) {
          scope.section = currSection;

          scope.slideKey = function($event, idx, dir) {
            var keypress = $event.which;

            if (keypress == 39) {
              slide(idx, 'next');
            } else if (keypress == 37) {
              slide(idx, 'prev');
            }

          };
          scope.slide = function(idx, dir) {
            slide(idx, dir);
          };
        }]
      });
    };    
   
  }
});
